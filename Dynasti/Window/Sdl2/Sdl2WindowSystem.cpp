#include "glad/glad.h"
#include "Dynasti/Core/Debug.h"
#include "Dynasti/Core/FrameTimer.h"
#include "Dynasti/Event/EventListener.h"
#include "Dynasti/Event/EventManager.h"
#include "Dynasti/Input/InputManager.h"
#include "Dynasti/Window/Sdl2/Sdl2WindowSystem.h"


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{
    
    void Sdl2WindowSystem::Setup()
    {
        SetupSdl();
        SetupSdlOpenGlDisplay();
        sdl2Window_ = std::make_shared<Sdl2Window>("Main Window", 960, 640, 1, glm::vec3(0.0f, 0.0f, 0.0f));
    
        // Initialise Glad
        if (!gladLoadGLLoader((GLADloadproc) SDL_GL_GetProcAddress))
        {
            DYNASTI_FATAL("Error initialising GLAD");
        }
        
        quitAppEventId = eventManager_->RegisterEventName("QuitApp");
    }
    
    
    void Sdl2WindowSystem::Shutdown()
    {
    }
    
    
    void Sdl2WindowSystem::Update()
    {
        DYNASTI_TIMER(WINDOW_SYSTEM);
        SDL_Event sdlEvent;
        
        while (SDL_PollEvent(&sdlEvent) != 0)
        {
            OnSdlEvent(sdlEvent);
        }
        
        sdl2Window_->SwapBuffers();
    }
    
    
    void Sdl2WindowSystem::SetupSdl()
    {
        int sdlStatus;
        std::string sdlError;
    
        // System information
        std::string platformString = SDL_GetPlatform();
        DYNASTI_LOG("Detected platform        : " + platformString);
        DYNASTI_LOG("No. of logical CPU cores : " + std::to_string(SDL_GetCPUCount()));
        DYNASTI_LOG("System RAM (MB)          : " + std::to_string(SDL_GetSystemRAM()));
        DYNASTI_LOG("L1 Cache (B)             : " + std::to_string(SDL_GetCPUCacheLineSize()));
    
        // SDL version information
        SDL_VERSION(&sdlCompiled_);
        SDL_GetVersion(&sdlLinked_);
        DYNASTI_LOG("Compiled with SDL version : " + std::to_string(sdlCompiled_.major) + "." + std::to_string(sdlCompiled_.minor) + "." + std::to_string(sdlCompiled_.patch))
        DYNASTI_LOG("Linked with SDL version   : " + std::to_string(sdlLinked_.major) + "." + std::to_string(sdlLinked_.minor) + "." + std::to_string(sdlLinked_.patch))
    
        // Ensure that code is compiled with at least version 2.0.10
        sdlStatus = SDL_VERSION_ATLEAST(2, 0, 8);
        DYNASTI_ASSERT(sdlStatus, "Compiled SDL version is less than 2.0.8");
    
        // Initialise SDL
        sdlStatus = SDL_Init(SDL_INIT_EVERYTHING);
        if (sdlStatus < 0)
        {
            sdlError = SDL_GetError();
            DYNASTI_FATAL("SDL could not initialise : " + sdlError);
        }
        else
        {
            DYNASTI_LOG("SDL initialised successfully");
        }
    }
    
    
    void Dynasti::Sdl2WindowSystem::SetupSdlOpenGlDisplay()
    {
        int sdlStatus;
        std::string sdlError;
    
        // Set attributes to create OpenGL context via SDL
        SDL_GL_SetAttribute(SDL_GL_CONTEXT_FLAGS, 0);
        SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
        SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 4);
        SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 6);
        DYNASTI_LOG("Setting OpenGL 4.6 attributes");
    
        // Set other attributes
        SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
        SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);
    
        // Video display information
        numDisplays_ = SDL_GetNumVideoDisplays();
        if (numDisplays_ < 1)
        {
            DYNASTI_FATAL("SDL could not detect any video displays");
        }
        else
        {
            DYNASTI_LOG("No. of SDL video displays : " + std::to_string(numDisplays_));
        }
    
        // Available display modes
        sdlStatus = SDL_GetCurrentDisplayMode(0, &sdlDisplayMode_);
        if (sdlStatus != 0)
        {
            sdlError = SDL_GetError();
            DYNASTI_FATAL("SDL could not read current display mode : " + sdlError);
        }
        else
        {
            displayWidth_  = sdlDisplayMode_.w;
            displayHeight_ = sdlDisplayMode_.h;
            displayWidth_  = 640;
            displayHeight_ = 480;
            DYNASTI_LOG("Window width : " + std::to_string(displayWidth_) + "   height : " + std::to_string(displayHeight_));
        }
        
    }
    
    
    /*void SdlWindowSystem::SetupSdlInput()
    {
        return;
    }*/
    
    
    bool Sdl2WindowSystem::OnSdlEvent(const SDL_Event &sdlEvent)
    {
        switch (sdlEvent.type)
        {
            case SDL_AUDIODEVICEADDED:
            {
                DYNASTI_LOG_VERBOSE("Audio device added");
                return true;
            }
            case SDL_AUDIODEVICEREMOVED:
                DYNASTI_LOG_VERBOSE("Audio device removed");
                break;
        
            case SDL_CONTROLLERAXISMOTION:
                DYNASTI_LOG_VERBOSE("Controller axis motion");
                break;
        
            case SDL_CONTROLLERBUTTONDOWN:
                DYNASTI_LOG_VERBOSE("Controller button down!");
                break;
        
            case SDL_CONTROLLERBUTTONUP:
                DYNASTI_LOG_VERBOSE("Controller button up");
                break;
        
            case SDL_CONTROLLERDEVICEADDED:
                DYNASTI_LOG_VERBOSE("Controller device added : " + std::to_string(sdlEvent.cdevice.which));
                break;
        
            case SDL_CONTROLLERDEVICEREMAPPED:
                DYNASTI_LOG_VERBOSE("Controller device remapped");
                break;
        
            case SDL_CONTROLLERDEVICEREMOVED:
                DYNASTI_LOG_VERBOSE("Controller device removed : " + std::to_string(sdlEvent.cdevice.which));
                break;
        
            case SDL_JOYAXISMOTION:
                DYNASTI_LOG_VERBOSE("Joystick axis motion : " + std::to_string(sdlEvent.jaxis.which) + "  " + std::to_string(sdlEvent.jaxis.axis) + "  " + std::to_string(sdlEvent.jaxis.value));
                break;
        
            case SDL_JOYBALLMOTION:
                DYNASTI_LOG_VERBOSE("Joystick ball motion detected");
                break;
        
            case SDL_JOYBUTTONDOWN:
                DYNASTI_LOG_VERBOSE("Joystick button down : " + std::to_string(sdlEvent.jbutton.which) + "  " + std::to_string(sdlEvent.jbutton.button));
                break;
        
            case SDL_JOYBUTTONUP:
                DYNASTI_LOG_VERBOSE("Joystick button up : " + std::to_string(sdlEvent.jbutton.which) + "  " + std::to_string(sdlEvent.jbutton.button));
                break;
        
            case SDL_JOYDEVICEADDED:
                DYNASTI_LOG_VERBOSE("Joystick device added event : " + std::to_string(sdlEvent.jdevice.which));
                break;
        
            case SDL_JOYDEVICEREMOVED:
                DYNASTI_LOG_VERBOSE("Joystick removed : " + std::to_string(sdlEvent.jdevice.which));
                break;
        
            case SDL_JOYHATMOTION:
                DYNASTI_LOG_VERBOSE("Joystick hat motion detected");
                break;
        
            case SDL_QUIT:
            {
                DYNASTI_LOG_VERBOSE("Quit event");
                Event quitAppEvent(quitAppEventId);
                eventManager_->BroadcastEvent(quitAppEvent);
                return true;
            }
            case SDL_SYSWMEVENT:
                DYNASTI_LOG_VERBOSE("Sys wm event??");
                break;
        
            case SDL_TEXTEDITING:
                DYNASTI_LOG_VERBOSE("Text editing event");
                break;
        
            case SDL_TEXTINPUT:
                DYNASTI_LOG_VERBOSE("Text input event");
                break;
        
            case SDL_WINDOWEVENT:
            {
                std::shared_ptr<Sdl2Window> primaryWindow = GetPrimarySdl2Window();
                return primaryWindow->OnSdlWindowEvent(sdlEvent);
            }
            case SDL_FIRSTEVENT:
            case SDL_LASTEVENT:
                DYNASTI_LOG_VERBOSE("First or last event");
                break;
        
            case SDL_CLIPBOARDUPDATE:
                DYNASTI_LOG_VERBOSE("Clipboard update");
                break;
        
            case SDL_RENDER_DEVICE_RESET:
            case SDL_RENDER_TARGETS_RESET:
                DYNASTI_LOG_VERBOSE("Render event");
                break;
        
            case SDL_USEREVENT:
                DYNASTI_LOG_VERBOSE("User event detected : " + std::to_string(sdlEvent.type));
                break;

#ifdef DYNASTI_DESKTOP
            case SDL_KEYDOWN:
                inputManager_->OnKeyDown(static_cast<KeyCode>(sdlEvent.key.keysym.scancode));
                DYNASTI_LOG_VERBOSE("Keydown");
                break;
    
            case SDL_KEYMAPCHANGED:
                DYNASTI_LOG_VERBOSE("Keymap changed");
                break;
    
            case SDL_KEYUP:
                inputManager_->OnKeyUp(static_cast<KeyCode>(sdlEvent.key.keysym.scancode));
                DYNASTI_LOG_VERBOSE("Keyup");
                break;
    
            case SDL_MOUSEBUTTONDOWN:
                //inputManager->OnSdlMouseButtonDown(sdlEvent);
                DYNASTI_LOG_VERBOSE("Mouse button down");
                break;
    
            case SDL_MOUSEBUTTONUP:
                //inputManager->OnSdlMouseButtonUp(sdlEvent);
                DYNASTI_LOG_VERBOSE("Mouse button up");
                break;
    
            case SDL_MOUSEMOTION:
                //inputManager->OnSdlMouseMotion(sdlEvent);
                DYNASTI_LOG_VERBOSE("Mouse motion");
                break;
    
            case SDL_MOUSEWHEEL:
                //inputManager->OnSdlMouseWheel(sdlEvent);
                DYNASTI_LOG_VERBOSE("Mouse wheel");
                break;
    
            case SDL_DROPFILE:
            case SDL_DROPBEGIN:
            case SDL_DROPCOMPLETE:
            case SDL_DROPTEXT:
                DYNASTI_LOG_VERBOSE("Drop event detected");
                break;
#endif

#ifdef DYNASTI_MOBILE
            case SDL_APP_LOWMEMORY:
            case SDL_APP_TERMINATING:
            case SDL_APP_DIDENTERFOREGROUND:
            case SDL_APP_DIDENTERBACKGROUND:
            case SDL_APP_WILLENTERFOREGROUND:
            case SDL_APP_WILLENTERBACKGROUND:
                DYNASTI_LOG_VERBOSE("Unused App event");
                break;
    
            case SDL_DOLLARGESTURE:
            case SDL_DOLLARRECORD:
                DYNASTI_LOG_VERBOSE("Dollar gesture event?")
                break;
    
            case SDL_FINGERDOWN:
                //touchScreen.OnSdlFingerDown(sdlEvent);
                break;
    
            case SDL_FINGERMOTION:
                //touchScreen.OnSdlFingerMotion(sdlEvent);
                break;
    
            case SDL_FINGERUP:
                //touchScreen.OnSdlFingerUp(sdlEvent);
                break;
    
            case SDL_MULTIGESTURE:
                DYNASTI_LOG_VERBOSE("Multi-gesture event");
                break;
#endif
        
            default:
            {
                DYNASTI_WARNING("Unrecognised SDL event : " + std::to_string(sdlEvent.type));
                return false;
            }
        }
        return false;
    }
    
}
//---------------------------------------------------------------------------------------------------------------------