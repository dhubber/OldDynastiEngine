#include "Dynasti/Core/Constants.h"
#include "Dynasti/Core/Debug.h"
#include "Dynasti/Core/IncludeOpenGl.h"
#include "Dynasti/Window/Sdl2/Sdl2Window.h"


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{

    Sdl2Window::Sdl2Window(const std::string title, const int width, const int height, const int vSync,
        const glm::vec3 bgColor) : Window(title)
    {
        DYNASTI_ASSERT(width > 0, "Invalid window width : " + std::to_string(width));
        DYNASTI_ASSERT(height > 0, "Invalid window height : " + std::to_string(height));
        DYNASTI_ASSERT((vSync == 0 || vSync == 1), "Invalid value for vSync : " + std::to_string(vSync));
        std::string sdlError;
    
        sdlWindowInternal_ = SDL_CreateWindow(title_.c_str(), SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
                                              width, height, SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE);
        
        if (sdlWindowInternal_)
        {
            width_         = width;
            height_        = height;
            keyboardFocus_ = true;
            mouseFocus_    = true;
            sdlWindowId_   = SDL_GetWindowID(sdlWindowInternal_);
            DYNASTI_LOG("SDL window id : " + std::to_string(sdlWindowId_));
        }
        else
        {
            sdlError = SDL_GetError();
            DYNASTI_FATAL("SDL could not create a window : " + sdlError);
        }
    
        // Create OpenGL context
        glContext_ = SDL_GL_CreateContext(sdlWindowInternal_);
        if (glContext_ == nullptr)
        {
            sdlError = SDL_GetError();
            SDL_DestroyWindow(sdlWindowInternal_);
            sdlWindowInternal_ = nullptr;
            DYNASTI_FATAL("SDL could not create a GL context : " + sdlError);
        }
        
        SDL_GL_MakeCurrent(sdlWindowInternal_, glContext_);
        SDL_ShowWindow(sdlWindowInternal_);
        SDL_GL_SetSwapInterval(vSync);
    }
    
    
    Sdl2Window::~Sdl2Window()
    {
        if (sdlWindowInternal_)
        {
            SDL_DestroyWindow(sdlWindowInternal_);
        }
    }
    
    
    void Sdl2Window::SwapBuffers()
    {
        SDL_GL_SwapWindow(sdlWindowInternal_);
    }
    
    
    bool Sdl2Window::OnSdlWindowEvent(const SDL_Event &e)
    {
        DYNASTI_ASSERT(e.type == SDL_WINDOWEVENT, "OnSdlWindowEvent function received incompatible SDL event");
    
        // If an SDL event was not detected for this window, then ignore
        if (e.window.windowID == (unsigned int) sdlWindowId_)
        {
            switch(e.window.type)
            {
                case SDL_WINDOWEVENT_CLOSE:
                {
                    SDL_HideWindow(sdlWindowInternal_);
                    DYNASTI_LOG("Window closed;  id : " + std::to_string(sdlWindowId_));
                    return true;
                }
                case SDL_WINDOWEVENT_ENTER:
                {
                    mouseFocus_ = true;
                    DYNASTI_LOG("Mouse entered window;  id : " + std::to_string(sdlWindowId_));
                    return true;
                }
                case SDL_WINDOWEVENT_EXPOSED:
                {
                    //SDL_RenderPresent(sdlRenderer);
                    DYNASTI_LOG("Window exposed;  id : " + std::to_string(sdlWindowId_));
                    return true;
                }
                case SDL_WINDOWEVENT_FOCUS_GAINED:
                {
                    keyboardFocus_ = true;
                    DYNASTI_LOG("Window gained keyboard focus;  id : " + std::to_string(sdlWindowId_));
                    return true;
                }
                case SDL_WINDOWEVENT_FOCUS_LOST:
                {
                    DYNASTI_LOG("Window lost keyboard focus;  id : " + std::to_string(sdlWindowId_));
                    keyboardFocus_ = false;
                    return true;
                }
                case SDL_WINDOWEVENT_HIDDEN:
                {
                    shown_ = false;
                    DYNASTI_LOG("Window hidden;  id : " + std::to_string(sdlWindowId_));
                    return true;
                }
                case SDL_WINDOWEVENT_LEAVE:
                {
                    DYNASTI_LOG("Mouse left;  id : " + std::to_string(sdlWindowId_));
                    mouseFocus_ = false;
                    return true;
                }
                case SDL_WINDOWEVENT_MAXIMIZED:
                {
                    minimized_ = false;
                    DYNASTI_LOG("Window maximized;  id : " + std::to_string(sdlWindowId_));
                    return true;
                }
                case SDL_WINDOWEVENT_MINIMIZED:
                {
                    minimized_ = true;
                    DYNASTI_LOG("Window minimized;  id : " + std::to_string(sdlWindowId_));
                    return true;
                }
                case SDL_WINDOWEVENT_MOVED:
                {
                    DYNASTI_LOG("Window moved; id : " + std::to_string(sdlWindowId_));
                    return true;
                }
                case SDL_WINDOWEVENT_RESIZED:
                {
                    width_ = e.window.data1;
                    height_ = e.window.data2;
                    DYNASTI_LOG("Window resized;  id : " + std::to_string(sdlWindowId_) + "  w : " +
                        std::to_string(width_) + "  h : " + std::to_string(height_));
                    return true;
                }
                case SDL_WINDOWEVENT_RESTORED:
                {
                    minimized_ = false;
                    DYNASTI_LOG("Window restored;  id : " + std::to_string(sdlWindowId_));
                    return true;
                }
                case SDL_WINDOWEVENT_SHOWN:
                {
                    shown_ = true;
                    DYNASTI_LOG("Window shown;  id : " + std::to_string(sdlWindowId_));
                    return true;
                }
                case SDL_WINDOWEVENT_SIZE_CHANGED:
                {
                    width_ = e.window.data1;
                    height_ = e.window.data2;
                    DYNASTI_LOG("Window size changed;  id : " + std::to_string(sdlWindowId_) + "  w : " +
                                std::to_string(width_) + "  h : " + std::to_string(height_));
                    return true;
                }
                default:
                {
                    DYNASTI_LOG("Window received unknown event;  id : " + std::to_string(sdlWindowId_) + "  event : " +
                        std::to_string(e.window.event));
                    return false;
                }
    
            }
        }
        
        return false;
    }
    
}
//---------------------------------------------------------------------------------------------------------------------