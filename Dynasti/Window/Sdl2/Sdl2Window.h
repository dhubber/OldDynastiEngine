#ifndef DYNASTI_SDL2_WINDOW_H
#define DYNASTI_SDL2_WINDOW_H


#include <memory>
#include <SDL.h>
#include "Dynasti/Window/Window.h"


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{
    
    //=================================================================================================================
    /// \brief   Class representing a SDL2 window
    /// \author  D. A. Hubber
    /// \date    21/06/2020
    //=================================================================================================================
    class Sdl2Window : public Window
    {
    public:
        
        Sdl2Window(const std::string title, const int width, const int height, const int vSync, const glm::vec3 bgColor);
        
        virtual ~Sdl2Window();
        
        virtual void SwapBuffers() override;
        
        virtual bool OnSdlWindowEvent(const SDL_Event &e);

        SDL_Window* GetSdlInternalWindow() {return sdlWindowInternal_;}
        SDL_GLContext GetSdlGlContext() {return glContext_;}

    protected:
        
        int sdlWindowId_;                                  ///< (Unique) id of Window object
        SDL_GLContext glContext_;                          ///< SDL OpenGL context object for window
        SDL_Window* sdlWindowInternal_;                    ///< Pointer to SDL Window object (internal to SDL API)
    
    };
    
}
//---------------------------------------------------------------------------------------------------------------------
#endif
