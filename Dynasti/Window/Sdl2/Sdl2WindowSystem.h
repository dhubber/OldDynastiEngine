#ifndef DYNASTI_SDL2_WINDOW_SYSTEM_H
#define DYNASTI_SDL2_WINDOW_SYSTEM_H


#include <memory>
#include <SDL.h>
#include "Dynasti/Core/Constants.h"
#include "Dynasti/Window/WindowSystem.h"
#include "Dynasti/Window/Sdl2/Sdl2Window.h"


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{
    
    //class EventManager;
    
    //=================================================================================================================
    /// \brief   System for creating and managing all Windows during the application lifetime.
    /// \author  D. A. Hubber
    /// \date    12/06/2020
    //=================================================================================================================
    class Sdl2WindowSystem : public WindowSystem
    {
    public:
        
        Sdl2WindowSystem(std::shared_ptr<AssetManager> assetManager, std::shared_ptr<EventManager> eventManager,
                         std::shared_ptr<FrameTimer> frameTimer, std::shared_ptr<InputManager> inputManager) :
            WindowSystem("Sdl2WindowSystem", assetManager, eventManager, frameTimer, inputManager), sdl2Window_(nullptr)
        {
        };
    
        virtual void Setup() override;
        virtual void Shutdown() override;
        virtual void Update() override;
        

    protected:
        
        /// ...
        virtual void SetupSdl();
        
        /// Set-up all Graphical and OpenGL functionality via SDL.  Also create the primary window.
        virtual void SetupSdlOpenGlDisplay();
        
        /// Processes a given SDL event popped from the SDL event queue
        virtual bool OnSdlEvent(const SDL_Event &);
        
        virtual std::shared_ptr<Window> GetPrimaryWindow() override {return sdl2Window_;}

        virtual std::shared_ptr<Sdl2Window> GetPrimarySdl2Window() {return sdl2Window_;}
    
        EventType quitAppEventId;                          ///<...
        SDL_DisplayMode sdlDisplayMode_;                   ///< Detected display mode (by SDL)
        SDL_version sdlCompiled_;                          ///< Version of SDL that Dynasti is compiled with
        SDL_version sdlLinked_;                            ///< Version of SDL that Dynasti is linked with
        std::shared_ptr<Sdl2Window> sdl2Window_;           ///< Primary SDL window created during start-up
    
    };
    
}
//---------------------------------------------------------------------------------------------------------------------
#endif
