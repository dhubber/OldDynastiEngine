#include "glad/glad.h"
#include "Dynasti/Core/Constants.h"
#include "Dynasti/Core/Debug.h"
#include "Dynasti/Window/Glfw/GlfwWindow.h"


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{

    GlfwWindow::GlfwWindow(const std::string title, const int width, const int height, const int vSync,
        const glm::vec3 bgColor) : Window(title)
    {
        DYNASTI_ASSERT(width > 0, "Invalid window width : " + std::to_string(width));
        DYNASTI_ASSERT(height > 0, "Invalid window height : " + std::to_string(height));
        DYNASTI_ASSERT((vSync == 0 || vSync == 1), "Invalid value for vSync : " + std::to_string(vSync));
    
        glfwWindowInternal_ = glfwCreateWindow(width, height, title.c_str(), NULL, NULL);
        if (glfwWindowInternal_)
        {
            width_         = width;
            height_        = height;
            keyboardFocus_ = true;
            mouseFocus_    = true;
            DYNASTI_LOG("Successfully created GLFW window");
        }
        else
        {
            DYNASTI_FATAL("GLFW could not create a window");
        }
        
        glfwMakeContextCurrent(glfwWindowInternal_);
        glfwSwapInterval(1);
    }
    
    
    GlfwWindow::~GlfwWindow()
    {
        if (glfwWindowInternal_)
        {
            glfwDestroyWindow(glfwWindowInternal_);
        }
    }
    
    
    void GlfwWindow::SwapBuffers()
    {
        int width, height;
        glfwGetFramebufferSize(glfwWindowInternal_, &width, &height);
        glViewport(0, 0, width, height);
        glfwSwapBuffers(glfwWindowInternal_);
    }
    
    
}
//---------------------------------------------------------------------------------------------------------------------