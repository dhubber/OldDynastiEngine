#define GLFW_INCLUDE_NONE
#include "glad/glad.h"
#include "GLFW/glfw3.h"
#include "Dynasti/Core/Debug.h"
#include "Dynasti/Core/FrameTimer.h"
#include "Dynasti/Event/EventListener.h"
#include "Dynasti/Event/EventManager.h"
#include "Dynasti/Input/InputManager.h"
#include "Dynasti/Window/Glfw/GlfwWindowSystem.h"


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{
    
    std::shared_ptr<InputManager> GlfwWindowSystem::staticInputManagerPtr_{nullptr};
    
    
    void GlfwWindowSystem::Setup()
    {
        if (glfwInit())
        {
            DYNASTI_LOG("GLFW successfully initialised");
        }
        else
        {
            DYNASTI_FATAL("GLFW initialisation failed");
        }
    
        glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
        glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 6);
        glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
        
        glfwWindow_ = std::make_shared<GlfwWindow>("Main Window", 1200, 800, 1, glm::vec3(0.0f, 0.0f, 0.0f));
        glfwSetKeyCallback(glfwWindow_->GetGlfwInternalWindow(), GlfwWindowSystem::OnGlfwKeyInputEvent);
        
        // Initialise Glad
        //if (gladLoadGLLoader((GLADloadproc) glfwGetProcAddress))
        if (gladLoadGL())
        {
            DYNASTI_LOG("GLAD successfully initialised");
        }
        else
        {
            DYNASTI_FATAL("Error initialising GLAD");
        }
        
        quitAppEventId = eventManager_->RegisterEventName("QuitApp");
    }
    
    
    void GlfwWindowSystem::Shutdown()
    {
        glfwTerminate();
    }
    
    
    void GlfwWindowSystem::Update()
    {
        DYNASTI_TIMER(WINDOW_SYSTEM);
        glfwPollEvents();
        glfwWindow_->SwapBuffers();
    }
    
    
    void GlfwWindowSystem::OnGlfwKeyInputEvent(GLFWwindow* window, int key, int scancode, int action, int mods)
    {
        switch(action)
        {
            case GLFW_PRESS:
            {
                DYNASTI_LOG("Keydown : " + std::to_string(key));
                staticInputManagerPtr_->OnKeyDown(static_cast<KeyCode>(key));
                break;
            }
            case GLFW_RELEASE:
            {
                DYNASTI_LOG("Keyup : " + std::to_string(key));
                staticInputManagerPtr_->OnKeyUp(static_cast<KeyCode>(key));
                break;
            }
            case GLFW_REPEAT:
            {
                break;
            }
            default:
            {
                DYNASTI_WARNING("Unrecognised GLFW event : " + std::to_string(action));
                break;
            }
        }
    }
    
}
//---------------------------------------------------------------------------------------------------------------------