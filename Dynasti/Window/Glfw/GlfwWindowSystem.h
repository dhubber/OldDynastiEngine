#ifndef DYNASTI_GLFW_WINDOW_SYSTEM_H
#define DYNASTI_GLFW_WINDOW_SYSTEM_H


#include <memory>
#include "Dynasti/Core/Constants.h"
#include "Dynasti/Window/WindowSystem.h"
#include "Dynasti/Window/Glfw/GlfwWindow.h"


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{
    
    class WindowSystem;

    
    //=================================================================================================================
    /// \brief   GLFW implementation of WindowSystem class.
    /// \author  D. A. Hubber
    /// \date    12/06/2020
    //=================================================================================================================
    class GlfwWindowSystem : public WindowSystem
    {
    public:
        
        GlfwWindowSystem(std::shared_ptr<AssetManager> assetManager, std::shared_ptr<EventManager> eventManager,
                         std::shared_ptr<FrameTimer> frameTimer, std::shared_ptr<InputManager> inputManager) :
            WindowSystem("GlfwWindowSystem", assetManager, eventManager, frameTimer, inputManager), glfwWindow_(nullptr)
        {
            staticInputManagerPtr_ = inputManager;
        };
    
        // Implementations of pure virtual functions
        virtual void Setup() override;
        virtual void Shutdown() override;
        virtual void Update() override;
        virtual std::shared_ptr<Window> GetPrimaryWindow() override { return glfwWindow_; }
        

    protected:
        
        static void OnGlfwKeyInputEvent(GLFWwindow* window, int key, int scancode, int action, int mods);

        virtual std::shared_ptr<GlfwWindow> GetPrimaryGlfwWindow() { return glfwWindow_; }
    
        EventType quitAppEventId;                          ///< Event id broadcast to quit the application
        std::shared_ptr<GlfwWindow> glfwWindow_;           ///< Primary SDL window created during start-up
        static std::shared_ptr<InputManager> staticInputManagerPtr_;  ///< Pointer to input manager
    
    };
    
}
//---------------------------------------------------------------------------------------------------------------------
#endif
