#ifndef DYNASTI_GLFW_WINDOW_H
#define DYNASTI_GLFW_WINDOW_H

//#if defined(_WIN32) || defined(_WIN64)
//#define GLFW_DLL
// #endif
#define GLFW_INCLUDE_NONE
#include <memory>
#include "glad/glad.h"
#include "GLFW/glfw3.h"
#include "Dynasti/Window/Window.h"


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{
    
    //=================================================================================================================
    /// \brief   Class representing a single GLFW3 window.
    /// \author  D. A. Hubber
    /// \date    17/09/2020
    //=================================================================================================================
    class GlfwWindow : public Window
    {
    public:
        
        GlfwWindow(const std::string title, const int width, const int height, const int vSync, const glm::vec3 bgColor);
        
        virtual ~GlfwWindow();
        
        // Implementation of pure virtual function
        virtual void SwapBuffers() override;
        
        // Getter function
        inline GLFWwindow* GetGlfwInternalWindow() { return glfwWindowInternal_; }

        
    protected:
    
        GLFWwindow* glfwWindowInternal_;                   ///< Pointer to GLFW window (internal to GLFW API)
    
    };
    
}
//---------------------------------------------------------------------------------------------------------------------
#endif
