#ifndef DYNASTI_WINDOW_SYSTEM_H
#define DYNASTI_WINDOW_SYSTEM_H


#include <memory>
#include "Dynasti/Core/System.h"
//#include "Dynasti/Window/Window.h"


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{
    
    class EventManager;
    class InputManager;
    class Window;
    
    
    //=================================================================================================================
    /// \brief   System for creating and managing all Windows during the application lifetime.
    /// \author  D. A. Hubber
    /// \date    12/06/2020
    //=================================================================================================================
    class WindowSystem : public System
    {
    public:
        
        WindowSystem(const std::string name, std::shared_ptr<AssetManager> assetManager,
                     std::shared_ptr<EventManager> eventManager, std::shared_ptr<FrameTimer> frameTimer,
                     std::shared_ptr<InputManager> inputManager) :
          System(name, assetManager, eventManager, frameTimer), inputManager_(inputManager) {};
    
        /// Returns a pointer to the primary window
        /// \return - Pointer to the primary window
        virtual std::shared_ptr<Window> GetPrimaryWindow() = 0;
        
        
    protected:
        
        int displayWidth_;                                 ///< Width of display (in pixels)
        int displayHeight_;                                ///< Height of display (in pixels)
        int numDisplays_;                                  ///< No. of detected displays
        std::shared_ptr<InputManager> inputManager_;       ///< Main player input manager object
    
    };
    
}
//---------------------------------------------------------------------------------------------------------------------
#endif
