#ifndef DYNASTI_WINDOW_H
#define DYNASTI_WINDOW_H


#include <glm/glm.hpp>
#include "Dynasti/Core/Debug.h"


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{
    
    //=================================================================================================================
    /// \brief   Virtual base class defining all Window class types.
    /// \author  D. A. Hubber
    /// \date    08/06/2020
    //=================================================================================================================
    class Window
    {
    public:
        
        Window(const std::string title="Window") : title_(title), hidden_(false), keyboardFocus_(false),
          mouseFocus_(false), shown_(false), height_(-1), width_(-1), bgColor_(glm::vec3(0.0f, 0.0f, 0.0f))
        {
            DYNASTI_LOG_VERBOSE("Constructing window object : " + title_);
        };
        
        virtual ~Window() {};
        
        /// Swaps the display buffer for double-buffered rendering
        virtual void SwapBuffers() = 0;
        
        // Getters
        inline int GetHeight() const {return height_;}
        inline int GetWidth() const {return width_;}
        inline const std::string& GetTitle() const {return title_;}
        
        
    protected:
        
        std::string title_;                                ///< Window display title
        bool hidden_;                                      ///< Is Window hidden?
        bool keyboardFocus_;                               ///< Does Window have keyboard focus?
        bool minimized_;                                   ///< Is Window in minimized state?
        bool mouseFocus_;                                  ///< Does Window have mouse focus?
        bool shown_;                                       ///< Is Window currently shown?
        int height_;                                       ///< Height (in pixels) of window
        int width_;                                        ///< Width (in pixels) of window
        glm::vec3 bgColor_;                                ///< Background window color
    
    };
    
}
//---------------------------------------------------------------------------------------------------------------------
#endif
