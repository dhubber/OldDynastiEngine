#include "Dynasti/Core/Constants.h"
#include "Dynasti/Core/Debug.h"
#include "Dynasti/Mesh/Mesh.h"


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{
    
    /*AnnulusMesh2d::AnnulusMesh2d(const float rInner, const float rOuter, const float angle, const int n)
    {
        DYNASTI_LOG_VERBOSE("Constructing annulus mesh; n : " + std::to_string(n));
        DYNASTI_ASSERT(n > 2, "Invalid no. of grid points for generating annulus mesh.");
        float dTheta = angle / static_cast<float>(n);
    
        // Create vertex buffer
        for (int i = 0; i < n; ++i)
        {
            const float theta = dTheta * static_cast<float>(i);
            Vertex2d vertex1(glm::vec2(rInner*cos(theta), rInner*sin(theta)));
            Vertex2d vertex2(glm::vec2(rOuter*cos(theta), rOuter*sin(theta)));
            AddVertices({vertex1, vertex2});
        }
    
        // Create index buffer
        if (angle >= 0.999f*twopi)
        {
            dTheta = twopi / static_cast<float>(n);
            for (int i = 0; i < n; ++i)
            {
                AddIndices({2*(i%n), 2*((i + 1)%n), 2*(i%n) + 1,
                            2*(i%n) + 1, 2*((i + 1)%n), 2*((i + 1)%n) + 1});
            }
        }
        else
        {
            dTheta = angle / static_cast<float>(n);
            for (int i = 0; i < n-1; ++i)
            {
                AddIndices({2*i, 2*(i + 1), 2*i + 1, 2*i + 1, 2*(i + 1), 2*(i + 1)+ 1});
            }
        }
        
    }
    
    
    EllipseMesh2d::EllipseMesh2d(const float a, const float b, const float angle, const int n)
    {
        DYNASTI_LOG_VERBOSE("Generating ellipse mesh; n : " + std::to_string(n));
        DYNASTI_ASSERT(n > 3, "Invalid no. of grid points for generating sheet mesh.");
        float dTheta = angle / static_cast<float>(n);
    
        // Set Vertex2d positions
        AddVertex(glm::vec2(0.0f, 0.0f));
        for (int i = 0; i < n; ++i)
        {
            const float theta = dTheta * static_cast<float>(i);
            AddVertex(glm::vec2(a*cos(theta), b*sin(theta)));
        }

        // Set indices
        if (angle >= 0.999f*twopi)
        {
            dTheta = twopi / static_cast<float>(n);
            for (int i = 0; i < n; ++i)
            {
                AddIndices({0, (i + 1)%n + 1, i%n + 1});
            }
        }
        else
        {
            dTheta = angle / static_cast<float>(n);
            for (int i = 0; i < n-1; ++i)
            {
                AddIndices({0, i + 2, i + 1});
            }
        }
        
        // Quick sanity-checking to ensure everything is consistent
        DYNASTI_ASSERT(NumVertices() == 1 + n, "No. of vertices inconsistent/incorrect : " + std::to_string(NumVertices()));
        DYNASTI_ASSERT(NumIndices() == 3*(n - 1 + (angle >= 0.999f*twopi)), "No, of indices inconsistent/incorrect : " + std::to_string(NumIndices()));
    }*/
    
    
    std::shared_ptr<Mesh> Mesh::SheetFactory(const float xSize, const float ySize, const int nx, const int ny)
    {
        DYNASTI_LOG_VERBOSE("Generating sheet mesh; nx : " + std::to_string(nx) + "   ny : " + std::to_string(ny));
        DYNASTI_ASSERT(nx > 1 && ny > 1, "Invalid no. of grid points for generating sheet mesh.");
        
        VertexFormat format(VertexFormatType::INTERLEAVED, {VertexAttributeType::POSITION2, VertexAttributeType::UV2});
        std::shared_ptr<Mesh> mesh = Mesh::MeshFactory(format, 6*(nx - 1)*(ny - 1), nx*ny);
        
        const float dx = xSize / static_cast<float>(nx - 1);
        const float dy = ySize / static_cast<float>(ny - 1);
    
        // Set vertex positions
        for (int j = 0; j < ny; ++j)
        {
            for (int i = 0; i < nx; ++i)
            {
                Vertex vertex(glm::vec2(dx*static_cast<float>(i) - 0.5f*xSize, dy*static_cast<float>(j) - 0.5f*ySize),
                                glm::vec2(static_cast<float>(i) / static_cast<float>(nx - 1), static_cast<float>(j) / static_cast<float>(ny - 1)));
                mesh->AddVertex(vertex);
            }
        }
        
        // Set indices
        for (int j = 0; j < ny-1; ++j)
        {
            for (int i = 0; i < nx-1; ++i)
            {
                mesh->AddIndices({i + nx*j, i + nx*j + 1, i + nx*(j + 1),
                                 i + nx*(j + 1), i + nx*j + 1, i + nx*(j + 1) + 1});
            }
        }
        
        // Quick sanity-checking to ensure everything is consistent
        DYNASTI_ASSERT(mesh->GetNumVertices() == nx * ny, "No. of vertices inconsistent/incorrect : " + std::to_string(
            mesh->GetNumVertices()));
        DYNASTI_ASSERT(mesh->GetNumIndices() == 6 * (nx - 1) * (ny - 1), "No, of indices inconsistent/incorrect : " + std::to_string(
            mesh->GetNumIndices()));
        return mesh;
    }
    
}
//---------------------------------------------------------------------------------------------------------------------
