#ifndef DYNASTI_MESH_H
#define DYNASTI_MESH_H


#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>
#include <glm/gtx/euler_angles.hpp>
#include <memory>
#include <vector>
#include <unordered_map>
#include "glad/glad.h"
#include "Dynasti/Asset/Asset.h"
#include "Dynasti/Core/Constants.h"
#include "Dynasti/Core/Debug.h"
#include "Dynasti/Mesh/Vertex.h"
#include "Dynasti/Renderer/VertexBuffer.h"

// Forward declaration from tinygltf library
#ifdef DYNASTI_USE_TINYGLTF
namespace tinygltf
{
    class Model;
    struct Mesh;
}
#endif


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{
    class AssetManager;
    //class VertexBuffer;
    
    
    //=================================================================================================================
    /// \brief   Base class for all mesh assets.  Contains functions for index buffers.
    /// \author  D. A. Hubber
    /// \date    04/08/2020
    //=================================================================================================================
    class Mesh : public Asset
    {
    public:
    
        /// General factory function to create mesh objects
        /// \param[in] maxNumVertices - Max. no. of vertices to allocate memory for
        /// \param[in] maxNumIndices - Max. no. of indices to allocate memory for
        /// \param[in] format - Memory format of vertices in mesh
        static std::shared_ptr<Mesh> MeshFactory(const VertexFormat &format, int maxNumIndices, int maxNumVertices);
        
        /// \param[in] xSize - Size of sheet in x-direction
        /// \param[in] ySize - Size of sheet in y-direction
        /// \param[in] nx - No. of vertices in x-direction
        /// \param[in] ny - No. of vertices in y-direction
        static std::shared_ptr<Mesh> SheetFactory(const float xSize, const float ySize, const int nx=2, const int ny=2);
    
        /// \param[in] xSize - x-size of cuboid
        /// \param[in] ySize - y-size of cuboid
        /// \param[in] zSize - z-size of cuboid
        /// \param[in] nx - No. of vertices in x-direction
        /// \param[in] ny - No. of vertices in y-direction
        /// \param[in] nz - No. of vertices in z-direction
        static std::shared_ptr<Mesh> CuboidFactory(const float xSize, const float ySize, const float zSize,
                                                   const int nx=2, const int ny=2, const int nz=2);
    
        /// \param[in] radius - Radius of sphere
        /// \param[in] nTheta - No. of vertices ...
        /// \param[in] nz - No. of vertices ...
        static std::shared_ptr<Mesh> SphereFactory(const float radius, const int nTheta = 16, const int nz = 8);
        
        /// Converts a 2d mesh (i.e. a mesh using 2d-specific vertex data like POSITION2) into a 3d mesh.
        /// Used to create 3d objects from 2d primitives, or to bring 2d objects into a 3d scene.
        /// \param[in] mesh2d - Pointer to the 2d mesh
        /// \param[in] format3d - Vertex format of the final 3d-compatible mesh
        static std::shared_ptr<Mesh> Convert2dTo3d(std::shared_ptr<Mesh> mesh2d, VertexFormat format3d);
        
        /// Mesh constructor
        Mesh(const VertexFormat &format, int maxNumIndices, int maxNumVertices);
        
        /*void operator=(const Mesh &other)
        {
            indices_ = other.indices_;
            vertices_ = other.vertices_;
        }*/
        
        void operator+=(const Mesh &other)
        {
            Append(other);
        }
    
        virtual void Clear()
        {
            indices_.clear();
            vertices_.clear();
        }
        
        /// Adds a single vertex index to the index array.
        /// \param[in] index - Vertex index to be added to indices array
        template <typename T>
        inline void AddIndex(const T index)
        {
            DYNASTI_ASSERT(index >= 0, "Invalid index : " + std::to_string(index));
            indices_.push_back(static_cast<unsigned int>(index));
            ++numIndices_;
        }
        
        /// Adds a list of array indices to the index array
        /// \param[in] newIndices - List of vertex indices to be added to the array
        template <typename T>
        inline void AddIndices(std::initializer_list<T> newIndices)
        {
            for (const int index : newIndices)
            {
                AddIndex(index);
            }
        }
    
        /// Adds a single vertex object to the vertex buffer
        /// \param[in] vertex - Vertex struct containing all data to be added
        void AddVertex(const Vertex &vertex);
    
        /// Adds a list of vertices to the vertex buffer
        //void AddVertices(std::initializer_list<Vertex> newVertices);
        
        /// Sets the data for a single attribute for the given vertex
        void SetAttributeData(const VertexAttributeType attributeType, const int vertexIndex, const Vertex &vertex);
    
        /// Appends all vertex and index data of a second mesh onto this mesh instance
        /// \param[in] other - Second mesh instance that is appended to this instance
        void Append(const Mesh &other);
        
        /// Constructs and returns the vertex data object for the requested vertex
        Vertex GetVertex(const int vertexIndex) const;
    
        /// Copies the vertex and index data to the GPU via the member vertex buffer object
        void CopyDataToGpu()
        {
            vertexBuffer_->CopyDataToGpu(GetFormat(), GetNumVertices(), GetNumIndices(),
                                         VertexArrayPointer(), IndexArrayPointer());
        }
    
        /// Translates/moves the positions of all mesh vertices by the given displacement
        /// \param[in] dr - Vector displacement to move all mesh vertices by
        inline void Translate(const glm::vec3 dr)
        {
            for (int i = 0; i < numVertices_; ++i)
            {
                Vertex vertex = GetVertex(i);
                vertex.pos3 += dr;
                SetAttributeData(VertexAttributeType::POSITION3, i, vertex);
            }
        }
    
        /// Rotates all mesh vertices around the origin by the given Euler angles
        /// \param[in] rot - Euler angles (yaw, pitch, roll) to rotate the vertex positions around the origin
        inline void Rotate(const glm::vec3 rot)
        {
            const glm::mat4 R = glm::yawPitchRoll(rot.x, rot.y, rot.z); //(yaw, pitch, roll);
            for (int i = 0; i < numVertices_; ++i)
            {
                Vertex vertex = GetVertex(i);
                vertex.pos3 = glm::vec3(R * glm::vec4(vertex.pos3, 0.0f));
                const glm::vec3 norm = glm::mat3(glm::transpose(glm::inverse(R))) * vertex.norm;
                vertex.norm = norm;
                SetAttributeData(VertexAttributeType::POSITION3, i, vertex);
            }
        }
        
        /// \param[in] i - Array index inthe index buffer
        /// \return - Vertex array index at the ith position in the index buffer
        inline GLuint GetIndex(const int i) const
        {
            DYNASTI_ASSERT(i < GetNumIndices(), "Invalid index : " + std::to_string(i));
            return indices_[i];
        }
        
        inline unsigned int *IndexArrayPointer() { return indices_.data(); }
        inline float* VertexArrayPointer() { return vertices_.data(); }
        
        // Getters and setters
        inline const VertexFormat& GetFormat() const { return format_; }
        inline int GetMaxNumIndices() const { return maxNumIndices_; }
        inline int GetMaxNumVertices() const {return maxNumVertices_; }
        inline int GetNumIndices() const { return numIndices_; }
        inline int GetNumVertices() const { return numVertices_; }
        inline GlobalId GetMaterialId() const { return materialId_; }
        inline std::shared_ptr<VertexBuffer> GetVertexBuffer() { return vertexBuffer_; }
        void SetMaterialId(const GlobalId materialId) { materialId_ = materialId; }

#ifdef DYNASTI_USE_TINYGLTF
        static std::shared_ptr<Mesh> ImportGltfMesh(const tinygltf::Mesh& gltfMesh, const tinygltf::Model& gltfModel,
                                                    std::shared_ptr<AssetManager> assetManager);
#endif
    
    
    protected:
    
        std::shared_ptr<VertexBuffer> vertexBuffer_;       ///< Pointer to vertex buffer containing mesh vertex data
        VertexFormat format_;                              ///< Format of mesh vertices
        GlobalId materialId_;                              ///< Id of the material used
        int maxNumIndices_;                                ///< Allocated/max no. of indices
        int maxNumVertices_;                               ///< Allocated/max no. of vertices
        int numIndices_;                                   ///< No. of indices in index buffer
        int numVertices_;                                  ///< No. of vertices
        std::vector<unsigned int> indices_;                ///< Mesh triangle indices
        std::vector<float> vertices_;                      ///< Mesh vertices
        
    };
    
}
//---------------------------------------------------------------------------------------------------------------------
#endif
