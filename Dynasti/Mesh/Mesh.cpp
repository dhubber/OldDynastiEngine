#include <iostream>
#include "Dynasti/Asset/AssetManager.h"
#include "Dynasti/Core/Constants.h"
#include "Dynasti/Core/Debug.h"
#include "Dynasti/Mesh/Mesh.h"
#include "Dynasti/Renderer/VertexBuffer.h"
#ifdef DYNASTI_USE_TINYGLTF
#include "Dynasti/Dependencies/tinygltf/tiny_gltf.h"
#endif


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{
    
    std::shared_ptr<Mesh> Mesh::MeshFactory(const VertexFormat &format, int maxNumIndices, int maxNumVertices)
    {
        return std::make_shared<Mesh>(format, maxNumIndices, maxNumVertices);
    }
    
    
    Mesh::Mesh(const VertexFormat &format, int maxNumIndices, int maxNumVertices) :
        format_(format), materialId_(-1), maxNumIndices_(maxNumIndices),
        maxNumVertices_(maxNumVertices), numIndices_(0), numVertices_(0), //indices_(maxNumIndices, 0),
        vertices_(maxNumVertices*format.GetVertexSize(), 0.0f)
    {
        DYNASTI_ASSERT(vertices_.size() >= maxNumVertices*format.GetVertexSize(), "Verices not allocated");
        indices_.reserve(maxNumIndices);
        vertexBuffer_ = VertexBuffer::VertexBufferFactory();
    }
    
    
    void Mesh::AddVertex(const Vertex &vertex)
    {
        DYNASTI_ASSERT(numVertices_ < maxNumVertices_, "Exceeded allocated memory for vertices");
        const VertexFormatType formatType = format_.GetFormatType();
        const int vertexSize = format_.GetVertexSize();
        for (int i = 0; i < format_.NumAttributes(); ++i)
        {
            const VertexAttributeType attributeType = format_.GetAttribute(i);
            SetAttributeData(attributeType, numVertices_, vertex);
        }
        ++numVertices_;
    }
    
    
    /*inline void Mesh::AddVertices(std::initializer_list<Vertex> newVertices)
    {
        for (const Vertex& vertex : newVertices)
        {
            AddVertex(vertex);
        }
    }*/
    
    
    Vertex Mesh::GetVertex(const int vertexIndex) const
    {
        Vertex vertex;
    
        for (int i = 0; i < format_.NumAttributes(); ++i)
        {
            const VertexAttributeType attributeType = format_.GetAttribute(i);
            const int vertexBufferIndex = format_.VertexBufferIndex(attributeType, vertexIndex, maxNumVertices_, format_);
            
            switch (attributeType)
            {
                case VertexAttributeType::POSITION2:
                {
                    vertex.pos2.x = vertices_[vertexBufferIndex];
                    vertex.pos2.y = vertices_[vertexBufferIndex + 1];
                    break;
                }
                case VertexAttributeType::POSITION3:
                {
                    vertex.pos3.x = vertices_[vertexBufferIndex];
                    vertex.pos3.y = vertices_[vertexBufferIndex + 1];
                    vertex.pos3.z = vertices_[vertexBufferIndex + 2];
                    break;
                }
                case VertexAttributeType::NORMAL3:
                {
                    vertex.norm.x = vertices_[vertexBufferIndex];
                    vertex.norm.y = vertices_[vertexBufferIndex + 1];
                    vertex.norm.z = vertices_[vertexBufferIndex + 2];
                    break;
                }
                case VertexAttributeType::UV2:
                {
                    vertex.uv.x = vertices_[vertexBufferIndex];
                    vertex.uv.y = vertices_[vertexBufferIndex + 1];
                    break;
                }
                default:
                {
                    DYNASTI_FATAL("Should never reach here!");
                    break;
                }
            }
        }
        return vertex;
    }

    
    void Mesh::SetAttributeData(const VertexAttributeType attributeType, const int vertexIndex, const Vertex &vertex)
    {
        const int vertexBufferIndex = format_.VertexBufferIndex(attributeType, vertexIndex, maxNumVertices_, format_);
    
        switch (attributeType)
        {
            case VertexAttributeType::POSITION2:
            {
                vertices_[vertexBufferIndex] = vertex.pos2.x;
                vertices_[vertexBufferIndex + 1] = vertex.pos2.y;
                break;
            }
            case VertexAttributeType::POSITION3:
            {
                vertices_[vertexBufferIndex] = vertex.pos3.x;
                vertices_[vertexBufferIndex + 1] = vertex.pos3.y;
                vertices_[vertexBufferIndex + 2] = vertex.pos3.z;
                break;
            }
            case VertexAttributeType::NORMAL3:
            {
                vertices_[vertexBufferIndex] = vertex.norm.x;
                vertices_[vertexBufferIndex + 1] = vertex.norm.y;
                vertices_[vertexBufferIndex + 2] = vertex.norm.z;
                break;
            }
            case VertexAttributeType::UV2:
            {
                vertices_[vertexBufferIndex] = vertex.uv.x;
                vertices_[vertexBufferIndex + 1] = vertex.uv.y;
                break;
            }
            default:
            {
                DYNASTI_FATAL("Should never reach here!");
                break;
            }
        }
    }
    
    
    void Mesh::Append(const Mesh &other)
    {
        DYNASTI_ASSERT(format_ == other.format_, "Cannot append meshes with different vertex formats");
        
        // Append all vertices from other mesh onto existing mesh
        const int oldNumVertices = GetNumVertices();
        for (int i = 0; i < other.GetNumVertices(); ++i)
        {
            Vertex vertex = other.GetVertex(i);
            AddVertex(vertex);
        }

        // Append all indices from the other mesh, correcting their indices on the new combined mesh
        for (int i = 0; i < other.GetNumIndices(); ++i)
        {
            const unsigned int index = other.GetIndex(i);
            AddIndex(oldNumVertices + index);
        }
    }

    
#ifdef DYNASTI_USE_TINYGLTF
    std::shared_ptr<Mesh> Mesh::ImportGltfMesh(const tinygltf::Mesh &gltfMesh, const tinygltf::Model &gltfModel,
                                               std::shared_ptr<AssetManager> assetManager)
    {
        DYNASTI_LOG("Importing mesh : " + gltfMesh.name);
        
        /*for (size_t i = 0; i < gltfModel.bufferViews.size(); ++i)
        {
            const tinygltf::BufferView &bufferView = gltfModel.bufferViews[i];
            DYNASTI_LOG_VERBOSE("BufferView target : " + std::to_string(bufferView.target));
            if (bufferView.target == 0)
            {
                DYNASTI_WARNING("BufferView target is zero");
                continue;
            }
            
            const tinygltf::Buffer &buffer = gltfModel.buffers[bufferView.buffer];
        }*/
        
        
        DYNASTI_LOG_VERBOSE("No. of primitives : " + std::to_string(gltfMesh.primitives.size()));
        if (gltfMesh.primitives.size() != 1)
        {
            DYNASTI_WARNING("glTF mesh can only be created for 1 primitive element");
            return nullptr;
        }
        
        const tinygltf::Primitive &primitive = gltfMesh.primitives[0];
        
        bool hasPositionData = false;
        bool hasNormalData = false;
        bool hasTexCoordData = false;
        for (const auto &attrib : primitive.attributes)
        {
            if (attrib.first.compare("POSITION") == 0) hasPositionData = true;
            if (attrib.first.compare("NORMAL") == 0) hasNormalData = true;
            if (attrib.first.compare("TEXCOORD_0") == 0) hasTexCoordData = true;
        }
        
        // Verify that the mesh contains the correct vertex attributes
        if (!hasPositionData || !hasNormalData || !hasTexCoordData)
        {
            DYNASTI_WARNING("glTF mesh does not contain the correct attributes");
            return nullptr;
        }
        
        const tinygltf::Accessor &indexAccessor = gltfModel.accessors[primitive.indices];
        const tinygltf::Buffer &buffer = gltfModel.buffers[0];
        const tinygltf::BufferView &bufferView = gltfModel.bufferViews[0];
        const tinygltf::BufferView &indexBufferView = gltfModel.bufferViews[indexAccessor.bufferView];
        const int numVertices = bufferView.byteLength / (3 * sizeof(float));
        const int numIndices = indexAccessor.count; //indexBufferView.byteLength / (sizeof(int));
        const VertexFormat format(VertexFormatType::BLOCKED,
                                  {VertexAttributeType::POSITION3, VertexAttributeType::NORMAL3, VertexAttributeType::UV2});
        
        /*AssetRegistry<VertexBuffer> &vertexBuffers = assetManager->GetVertexBufferRegistry();
        std::shared_ptr<VertexBuffer> vertexBuffer = VertexBuffer::VertexBufferFactory();
        
        VertexFormat format(VertexFormatType::BLOCKED,
                            {VertexAttributeType::POSITION3, VertexAttributeType::NORMAL3, VertexAttributeType::UV2});
        vertexBuffer->AddBufferDataToGpu(format, numVertices, numIndices,
                                         (void *) (buffer.data.data()),
                                         ((void *) buffer.data.data() + indexBufferView.byteOffset));*/
    
    
        AssetRegistry<Mesh> &meshes = assetManager->GetMeshRegistry();
        std::shared_ptr<Mesh> mesh = std::make_shared<Mesh>(format, numIndices, numVertices);
        meshes.Register(mesh, gltfMesh.name + "Mesh");
        
        float *data = (float *) buffer.data.data();
        for (int i = 0; i < numVertices; ++i)
        {
            int index = 8 * i;
            //std::cout << "i : " << i << "    data : ";
            //for (int k = 0; k < 8; ++k) std::cout << data[index + k] << " ";
            //std::cout << std::endl;
        }
        
        short int *indices = (short int *) (buffer.data.data() + indexBufferView.byteOffset);
        std::vector<unsigned int> bufferIndices;
        for (int i = 0; i < numIndices / 3; ++i)
        {
            int index = 3 * i;
            //std::cout << "Triangle : " << i << "    indices : " << indices[index]
            //          << " " << indices[index + 1] << " " << indices[index + 2] << std::endl;
            bufferIndices.push_back(static_cast<unsigned int>(indices[index]));
            bufferIndices.push_back(static_cast<unsigned int>(indices[index+1]));
            bufferIndices.push_back(static_cast<unsigned int>(indices[index+2]));
        }
        
        std::cout << "GetNumIndices : " << numIndices << "  " << indexAccessor.count << std::endl;
        
        std::shared_ptr<VertexBuffer> vertexBuffer = mesh->GetVertexBuffer();
        vertexBuffer->CopyDataToGpu(format, numVertices, numIndices, data, bufferIndices.data());
        
        //vertexBuffers.Register(vertexBuffer, gltfMesh.name + "VertexBuffer");
        //mesh->SetVertexBufferId(vertexBuffer->GetId());
        
        DYNASTI_LOG("No. of textures : " + std::to_string(gltfModel.textures.size()));
        
        return mesh;
    }
#endif

}
//---------------------------------------------------------------------------------------------------------------------