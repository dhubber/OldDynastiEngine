#ifndef DYNASTI_VERTEX_H
#define DYNASTI_VERTEX_H


#include <glm/glm.hpp>
#include <vector>
#include "Dynasti/Core/Debug.h"


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{
    
    /// Enumeration of all vertex attribute types.  Contains all accessor types for the glTF file format.
    enum class VertexAttributeType
    {
        POSITION2,
        POSITION3,
        NORMAL3,
        TANGENT4,
        COLOR3,
        COLOR4,
        UV2,
        UV2B,
        JOINTS4,
        WEIGHTS4
    };
    
    /// Enumeration of all vertex memory format types
    enum class VertexFormatType
    {
        INVALID_FORMAT,
        INTERLEAVED,
        BLOCKED
    };
    
    
    //=================================================================================================================
    /// \brief   Class describing a the format of a vertex buffer and the layout of attributes in memory.
    /// \author  D. A. Hubber
    /// \date    16/01/2021
    //=================================================================================================================
    class VertexFormat
    {
    public:
    
        VertexFormat() : vertexSize_(0), formatType_(VertexFormatType::INVALID_FORMAT) {};
        
        VertexFormat(VertexFormatType formatType) : vertexSize_(0), formatType_(formatType) {};
    
        VertexFormat(VertexFormatType formatType, std::initializer_list<VertexAttributeType> attributes) :
          vertexSize_(0), formatType_(formatType)
        {
            for (const VertexAttributeType attributeType : attributes)
            {
                AddNewAttribute(attributeType);
            }
        };
    
        bool operator==(const VertexFormat &other)
        {
            // Formats are automatically different if the number of vertex data elements or the memory layout is different
            if (vertexSize_ != other.vertexSize_ || formatType_ != other.formatType_)
            {
                return false;
            }
            
            // Check if each individual attribute is the same or not
            for (size_t i = 0; i < attributes_.size(); ++i)
            {
                if (attributes_[i] != other.attributes_[i])
                {
                    return false;
                }
            }
            
            // If all previous checks pass, then formats are identical
            return true;
        }
    
        /// Append the attribute list with a new attribute.  Asserts that attribute is not already included in list.
        /// \param[in] attributeType - New attribute type to be added
        void AddNewAttribute(const VertexAttributeType attributeType);
    
        /// Returns the positional id in the attribute list of the requested attribute type
        /// \param[in] attributeType - Attribute type being queried
        /// \return - Id of attribute type in attribute array if found; otherwise returns -1
        int FindAttributeIndex(const VertexAttributeType attributeType) const;
    
        /// Returns the offset (in number of floating point data elements) of the attribute at the given index
        /// \param[in] attributeIndex - Index of attribute being queried
        /// \return - Offset of the requested attribute (in no. of data elements) from the start of the attribute list
        int GetAttributeOffset(const int attributeIndex) const;
    
        /// Returns the offset (in number of floating point data elements) of the given attribute type
        /// \param[in] attributeType - Attribute type being queried
        /// \return - Offset of the requested attribute (in no. of data elements) from the start of the attribute list
        int GetAttributeOffset(const VertexAttributeType attributeType) const;
    
        /// Returns the number of data elements (i.e. floating point values) that constitute the attribute
        /// \param[in] attributeType - Attribute type being queried
        /// \return - No. of floating point data elements that make up the attribute data
        static int AttributeSize(const VertexAttributeType attributeType);
        
        static int VertexBufferIndex(const VertexFormatType formatType, const int vertexSize, const int attributeSize,
                                     const int attributeOffset, const int numVertices, const int i);
    
        static inline int VertexBufferIndex(const VertexAttributeType attributeType, const int i, const int numVertices, const VertexFormat& format)
        {
            return VertexBufferIndex(format.GetFormatType(), format.GetVertexSize(), AttributeSize(attributeType),
                                     format.GetAttributeOffset(attributeType), numVertices, i);
        }
        
        inline int NumAttributes() const { return static_cast<int>(attributes_.size()); }
    
        // Getter functions
        inline const VertexAttributeType& GetAttribute(const int i) const
        {
            DYNASTI_ASSERT(i < NumAttributes(), "Invalid attribute index : " + std::to_string(i));
            return attributes_[i];
        }
        inline VertexFormatType GetFormatType() const { return formatType_; }
        inline size_t GetVertexSize() const { return vertexSize_; }
        

    protected:
        
        size_t vertexSize_;                                 ///< No. of floating point data elements in vertex
        VertexFormatType formatType_;                       ///< Vertex memory format (e.g. blocked vs interleaved)
        std::vector<VertexAttributeType> attributes_;       ///< Order of attributes in each vertex
    
    };
    
    
    //=================================================================================================================
    /// \brief   Data structure containing all possible attributes for constructing vertices in meshes.
    /// \author  D. A. Hubber
    /// \date    15/02/2020
    //=================================================================================================================
    class Vertex
    {
    public:
    
        glm::vec2 pos2;                                    ///< 2d position
        glm::vec3 pos3;                                    ///< 3d position
        glm::vec3 norm;                                    ///< Normal vector
        glm::vec4 tangent4;                                ///< Tangent vector
        glm::vec3 color3;                                  ///< 3-channel color vector (e.g. RGB)
        glm::vec4 color4;                                  ///< 4-channel color vector (e.g. RGB with alpha)
        glm::vec2 uv;                                      ///< Texture UV coordinates
        glm::vec2 uvb;                                     ///< 2nd texture UV coordinates
        glm::vec4 joints4;                                 ///< Joints
        glm::vec4 weights4;                                ///< ..
    
        // Some constructors for common use-cases
        Vertex() = default;
        Vertex(const glm::vec2 &_pos2) : pos2(_pos2) {};
        Vertex(const glm::vec2 &_pos2, const glm::vec2 &_uv) : pos2(_pos2), uv(_uv) {};
        Vertex(const glm::vec3 &_pos3) : pos3(_pos3) {};
        Vertex(const glm::vec3 &_pos3, const glm::vec3 &_norm) : pos3(_pos3), norm(_norm) {};
        Vertex(const glm::vec3 &_pos3, const glm::vec3 &_norm, const glm::vec2 &_uv) : pos3(_pos3), norm(_norm), uv(_uv) {};
        
    };
    
}
//---------------------------------------------------------------------------------------------------------------------
#endif
