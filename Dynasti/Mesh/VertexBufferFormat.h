#ifndef DYNASTI_VERTEX_BUFFER_FORMAT_H
#define DYNASTI_VERTEX_BUFFER_FORMAT_H


#include <initializer_list>
#include <vector>
#include "glad/glad.h"
#include "Dynasti/Asset/Asset.h"
#include "Dynasti/Core/Debug.h"
#include "Dynasti/Core/Constants.h"


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{
    
    /// Enumeration of all vertex attribute types.  Contains all accessor types for the glTF file format.
    enum class VertexAttributeType
    {
        POSITION2,
        POSITION3,
        NORMAL3,
        TANGENT4,
        COLOR3,
        COLOR4,
        UV2,
        UV2B,
        JOINTS4,
        WEIGHTS4
    };
    
    
    //=================================================================================================================
    /// \brief   Class representing a single vertex attribute and its representation in a vertex buffer.
    /// \author  D. A. Hubber
    /// \date    16/01/2021
    //=================================================================================================================
    class VertexAttribute
    {
    public:
        
        VertexAttribute() : attributeType_(VertexAttributeType::NONE), offset_(0) {};
        
        VertexAttribute(const VertexAttributeType attributeType, const int offset) :
            attributeType_(attributeType), offset_(offset) {};
    
        inline int AttributeSize() const
        {
            switch(attributeType_)
            {
                case VertexAttributeType::POSITION2:
                case VertexAttributeType::UV2:
                case VertexAttributeType::UV2B:
                {
                    return 2;
                }
                case VertexAttributeType::POSITION3:
                case VertexAttributeType::COLOR3:
                case VertexAttributeType::NORMAL3:
                {
                    return 3;
                }
                case VertexAttributeType::COLOR4:
                case VertexAttributeType::TANGENT4:
                case VertexAttributeType::JOINTS4:
                case VertexAttributeType::WEIGHTS4:
                {
                    return 4;
                }
                default:
                {
                    DYNASTI_FATAL("Invalid attribute; code should never get here!");
                    return 0;
                }
            }
        }
        
        inline VertexAttributeType GetAttributeType() const { return attributeType_; }
        inline int GetOffset() const { return offset_; }
        inline void SetAttributeType(const VertexAttributeType attributeType) { attributeType_ = attributeType; }
        inline void SetOffset(const int offset) { offset_ = offset; }
        
        
    protected:
    
        VertexAttributeType attributeType_;                ///< ..
        int offset_;                                       ///< ..
        
    };
    
    
    //=================================================================================================================
    /// \brief   Class describing a the format of a vertex buffer and the layout of attributes in memory.
    /// \author  D. A. Hubber
    /// \date    16/01/2021
    //=================================================================================================================
    class VertexBufferFormat
    {
    public:
        
        VertexBufferFormat() : numElements_(0) {};
        
        inline void AddAttributes(const std::vector<VertexAttributeType>& attributeTypes)
        {
            for (const VertexAttributeType attributeType : attributeTypes)
            {
                AddAttribute(attributeType);
            }
        }
        
        inline void AddAttribute(const VertexAttributeType newAttributeType)
        {
            DYNASTI_ASSERT(FindAttributeIndex(newAttributeType) == -1, "Attribute type already added");
            VertexAttribute newAttribute(newAttributeType, numElements_);
            attributes_.push_back(newAttribute);
            numElements_ += newAttribute.AttributeSize();
        }
        
        inline int FindAttributeIndex(const VertexAttributeType attributeType)
        {
            for (int i = 0; i < attributes_.size(); ++i)
            {
                if (attributes_[i].GetAttributeType() == attributeType)
                {
                    return i;
                }
            }
            return -1;
        }
    
        inline int NumAttributes() const { return static_cast<int>(attributes_.size()); }
        
        inline const VertexAttribute& GetAttribute(const int i) const
        {
            DYNASTI_ASSERT(i < NumAttributes(), "Invalid attribute index : " + std::to_string(i));
            return attributes_[i];
        }
        inline int GetSize() const { return numElements_; }
        
        
    protected:
    
        int numElements_;                                  ///< ..
        std::vector<VertexAttribute> attributes_;          ///< ..
        
    };
    
}
//---------------------------------------------------------------------------------------------------------------------
#endif
