#include "Dynasti/Mesh/Vertex.h"


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{
    
    void VertexFormat::AddNewAttribute(const VertexAttributeType attributeType)
    {
        DYNASTI_ASSERT(FindAttributeIndex(attributeType) == -1, "Attribute type already registered");
        attributes_.push_back(attributeType);
        vertexSize_ += AttributeSize(attributeType);
    }
    
    int VertexFormat::FindAttributeIndex(const VertexAttributeType attributeType) const
    {
        for (size_t i = 0; i < attributes_.size(); ++i)
        {
            if (attributes_[i] == attributeType)
            {
                return i;
            }
        }
        return -1;
    }
    
    int VertexFormat::GetAttributeOffset(const int attributeIndex) const
    {
        int offset = 0;
        for (int i = 0; i < attributeIndex; ++i)
        {
            offset += AttributeSize(attributes_[i]);
        }
        return offset;
    }
    
    int VertexFormat::GetAttributeOffset(const VertexAttributeType attributeType) const
    {
        int offset = 0;
        for (size_t i = 0; i < attributes_.size(); ++i)
        {
            if (attributes_[i] == attributeType)
            {
                break;
            }
            offset += AttributeSize(attributes_[i]);
        }
        return offset;
    }
    
    int VertexFormat::AttributeSize(const VertexAttributeType attributeType)
    {
        switch(attributeType)
        {
            case VertexAttributeType::POSITION2:
            case VertexAttributeType::UV2:
            case VertexAttributeType::UV2B:
            {
                return 2;
            }
            case VertexAttributeType::POSITION3:
            case VertexAttributeType::COLOR3:
            case VertexAttributeType::NORMAL3:
            {
                return 3;
            }
            case VertexAttributeType::COLOR4:
            case VertexAttributeType::TANGENT4:
            case VertexAttributeType::JOINTS4:
            case VertexAttributeType::WEIGHTS4:
            {
                return 4;
            }
            default:
            {
                DYNASTI_FATAL("Invalid attribute; code should never get here!");
                return 0;
            }
        }
    }
    
    int VertexFormat::VertexBufferIndex(const VertexFormatType formatType, const int vertexSize, const int attributeSize,
                                        const int attributeOffset, const int numVertices, const int i)
    {
        switch (formatType)
        {
            case VertexFormatType::INTERLEAVED:
            {
                return vertexSize * i + attributeOffset;
            }
            case VertexFormatType::BLOCKED:
            {
                return numVertices * attributeOffset + attributeSize * i;
            }
            default:
            {
                DYNASTI_FATAL("Should never reach here!");
                return 0;
            }
        }
    }
    
}
//---------------------------------------------------------------------------------------------------------------------