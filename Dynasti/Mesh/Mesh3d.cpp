#include <iostream>
#include "Dynasti/Asset/AssetManager.h"
#include "Dynasti/Core/Constants.h"
#include "Dynasti/Core/Debug.h"
#include "Dynasti/Mesh/Mesh.h"
#include "Dynasti/Renderer/VertexBuffer.h"
#ifdef DYNASTI_USE_TINYGLTF
#include "Dynasti/Dependencies/tinygltf/tiny_gltf.h"
#endif


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{
    
    std::shared_ptr<Mesh> Mesh::Convert2dTo3d(std::shared_ptr<Mesh> mesh2d, VertexFormat format3d)
    {
        std::shared_ptr<Mesh> mesh = Mesh::MeshFactory(format3d, mesh2d->GetNumIndices(), mesh2d->GetNumVertices());
        for (int i = 0; i < mesh2d->GetNumVertices(); ++i)
        {
            Vertex vertex = mesh2d->GetVertex(i);
            vertex.pos3 = glm::vec3(vertex.pos2.x, vertex.pos2.y, 0.0f);
            mesh->AddVertex(vertex);
        }
        
        for (int i = 0; i < mesh2d->GetNumIndices(); ++i)
        {
            mesh->AddIndex(mesh2d->GetIndex(i));
        }
        
        return mesh;
    }
    

    /*ConeMesh3d::ConeMesh3d(const float radius, const float height, const bool closedBase, const int n) :
      Mesh3d(9*n, 3*n)
    {
        DYNASTI_LOG("Generating cone mesh; n : " + std::to_string(n));
        DYNASTI_ASSERT(n > 3, "Invalid no. of grid points for generating sheet mesh");
        DYNASTI_ASSERT(height > 0.0f, "Invalid cone height : " + std::to_string(height));
        const float phi = atan(radius / height);
        const float dTheta = twopi / static_cast<float>(n);

        // Set vertex positions
        for (int i = 0; i < n; ++i)
        {
            const float theta = dTheta * static_cast<float>(i);
            Vertex3d vertex;
            vertex.r = glm::vec3(radius*cos(theta), radius*sin(theta), 0.0f);
            vertex.norm = glm::vec3(cos(theta)*cos(phi), sin(theta)*cos(phi), sin(phi));
            AddVertex(vertex);
        }
        for (int i = 0; i < n; ++i)
        {
            const float theta = dTheta * static_cast<float>(i);
            Vertex3d vertex;
            vertex.r = glm::vec3(0.0f, 0.0f, height);
            vertex.norm = glm::vec3(cos(theta)*cos(phi), sin(theta)*cos(phi), sin(phi));
            AddVertex(vertex);
        }
        
        for (int i = 0; i < n; ++i)
        {
            AddIndex(i%n);
            AddIndex((i + 1)%n);
            AddIndex(i%n + n);
            AddIndex(i%n + n);
            AddIndex((i + 1)%n);
            AddIndex((i + 1)%n + n);
        }
    }*/
    
    
    std::shared_ptr<Mesh> Mesh::CuboidFactory(const float xSize, const float ySize, const float zSize, const int nx,
                                              const int ny, const int nz)
    {
        DYNASTI_LOG("Generating cuboid mesh; nx : " + std::to_string(nx) + "   ny : " + std::to_string(ny) + "   nz : " + std::to_string(nz));
        DYNASTI_ASSERT(nx > 1 && ny > 1 && nz > 1, "Invalid no. of grid points for generating cuboid mesh");
        const int numVertices = 2*nx*ny + 2*ny*nz + 2*nx*nz;
        const int numIndices = 12*((nx - 1)*(ny - 1) + (nx - 1)*(nz - 1) + (ny - 1)*(nz - 1));
        VertexFormat format(VertexFormatType::INTERLEAVED,
                            {VertexAttributeType::POSITION3, VertexAttributeType::NORMAL3, VertexAttributeType::UV2});
        std::shared_ptr<Mesh> mesh = Mesh::MeshFactory(format, numIndices, numVertices);

        // Back face (-ve z-direction)
        std::shared_ptr<Mesh> backFace2d = Mesh::SheetFactory(xSize, ySize, nx, ny);
        std::shared_ptr<Mesh> backFace = Mesh::Convert2dTo3d(backFace2d, format);
        backFace->Rotate(glm::vec3(pi, 0.0f, 0.0f));
        backFace->Translate(glm::vec3(0.0f, 0.0f, -0.5f*zSize));

        // Front face (+ve z-direction)
        std::shared_ptr<Mesh> frontFace2d = Mesh::SheetFactory(xSize, ySize, nx, ny);
        std::shared_ptr<Mesh> frontFace = Mesh::Convert2dTo3d(frontFace2d, format);
        frontFace->Translate(glm::vec3(0.0f, 0.0f, 0.5f*zSize));

        // Left face (-ve x-direction)
        std::shared_ptr<Mesh> leftFace2d = Mesh::SheetFactory(xSize, ySize, nx, ny);
        std::shared_ptr<Mesh> leftFace = Mesh::Convert2dTo3d(leftFace2d, format);
        leftFace->Rotate(glm::vec3(0.5f*pi, 0.0f, 0.0f));
        leftFace->Translate(glm::vec3(0.5f*xSize, 0.0f, 0.0f));

        // Right face (+ve x-direction)
        std::shared_ptr<Mesh> rightFace2d = Mesh::SheetFactory(xSize, ySize, nx, ny);
        std::shared_ptr<Mesh> rightFace = Mesh::Convert2dTo3d(rightFace2d, format);
        rightFace->Rotate(glm::vec3(-0.5f*pi, 0.0f, 0.0f));
        rightFace->Translate(glm::vec3(-0.5f*xSize, 0.0f, 0.0f));

        // Bottom face (-ve y-direction)
        std::shared_ptr<Mesh> bottomFace2d = Mesh::SheetFactory(xSize, ySize, nx, ny);
        std::shared_ptr<Mesh> bottomFace = Mesh::Convert2dTo3d(bottomFace2d, format);
        bottomFace->Rotate(glm::vec3(0.0f, -0.5f*pi, 0.0f));
        bottomFace->Translate(glm::vec3(0.0f, 0.5f*ySize, 0.0f));

        // Top face
        std::shared_ptr<Mesh> topFace2d = Mesh::SheetFactory(xSize, ySize, nx, ny);
        std::shared_ptr<Mesh> topFace = Mesh::Convert2dTo3d(topFace2d, format);
        topFace->Rotate(glm::vec3(0.0f, 0.5f*pi, 0.0f));
        topFace->Translate(glm::vec3(0.0f, -0.5f*ySize, 0.0f));

        // Add all 6 faces together to complete cuboid mesh
        mesh->Append(*backFace);
        mesh->Append(*frontFace);
        mesh->Append(*leftFace);
        mesh->Append(*rightFace);
        mesh->Append(*bottomFace);
        mesh->Append(*topFace);
        
        return mesh;
    }
    
    
    /*CylinderMesh3d::CylinderMesh3d(const float radius, const float height, const bool closedTop,
        const bool closedBottom, const int nTheta, const int nz) : Mesh3d(6*nTheta*nz, nTheta*nz)
    {
        DYNASTI_LOG("Generating cylinder mesh; nTheta : " + std::to_string(nTheta) + "   nz : " + std::to_string(nz));
        DYNASTI_ASSERT(nTheta > 2 && nz > 1, "Invalid no. of grid points for generating sheet mesh->");
        int numVertices = nTheta*nz;
        int numIndices  = 6*nTheta*nz;
        if (closedTop)
        {
            numVertices += (nTheta + 1);
            numIndices  += (3*nTheta);
        }
        if (closedBottom)
        {
            numVertices += (nTheta + 1);
            numIndices  += (3*nTheta);
        }

        const float dTheta = twopi / static_cast<float>(nTheta);
        const float dz = height / static_cast<float>(nz - 1);

        // Set vertex positions
        for (int j = 0; j < nz; ++j)
        {
            for (int i = 0; i < nTheta; ++i)
            {
                const float theta = dTheta * static_cast<float>(i);
                Vertex3d vertex;
                vertex.r = glm::vec3(radius*cos(theta), radius*sin(theta), dz*static_cast<float>(j) - 0.5f*height);
                vertex.norm = glm::vec3(cos(theta), sin(theta), 0.0f);
                AddVertex(vertex);
            }
        }
    
        // Set indices
        for (int j = 0; j < nz - 1; ++j)
        {
            for (int i = 0; i < nTheta; ++i)
            {
                AddIndex(i%nTheta       + nTheta*j);
                AddIndex((i + 1)%nTheta + nTheta*j);
                AddIndex(i%nTheta       + nTheta*(j + 1));
                AddIndex(i%nTheta       + nTheta*(j + 1));
                AddIndex((i + 1)%nTheta + nTheta*j);
                AddIndex((i + 1)%nTheta + nTheta*(j + 1));
            }
        }

        if (closedTop)
        {
            Mesh3d topMesh3d = EllipseMesh2d(radius, radius, twopi, nTheta);
            topMesh3d.Translate(glm::vec3(0.0f, 0.0f, 0.5f*height));
            Append(topMesh3d);
        }
        if (closedBottom)
        {
            Mesh3d bottomMesh3d = EllipseMesh2d(radius, radius, twopi, nTheta);
            bottomMesh3d.Rotate(glm::vec3(0.0f, pi, 0.0f));
            bottomMesh3d.Translate(glm::vec3(0.0f, 0.0f, -0.5f*height));
            Append(bottomMesh3d);
        }
    }
    
    
    PrismMesh3d::PrismMesh3d(const float radius, const float height, const int nTheta, const int nz) :
      Mesh3d(6*nTheta*nz, 2*nTheta*nz)
    {
        DYNASTI_LOG("Generating prism mesh; nTheta : " + std::to_string(nTheta) + "   nz : " + std::to_string(nz));
        DYNASTI_ASSERT(nTheta > 2 && nz > 1, "Invalid no. of grid points for generating sheet mesh->");
        const float dTheta = twopi / static_cast<float>(nTheta);
        const float dz = height / static_cast<float>(nz - 1);

        // Set vertex positions
        for (int i = 0; i < nTheta; ++i)
        {
            const float theta1 = dTheta * static_cast<float>(i);
            const float theta2 = dTheta * static_cast<float>(i + 1);
            const float thetaMean = 0.5f*(theta1 + theta2);
            for (int j = 0; j < nz; ++j)
            {
                Vertex3d vertex;
                vertex.r = glm::vec3(radius*cos(theta1), radius*sin(theta1), dz*static_cast<float>(j) - 0.5f*height);
                vertex.norm = glm::vec3(cos(thetaMean), sin(thetaMean), 0.0f);
                AddVertex(vertex);
            }
            for (int j = 0; j < nz; ++j)
            {
                Vertex3d vertex;
                vertex.r = glm::vec3(radius*cos(theta2), radius*sin(theta2), dz*static_cast<float>(j) - 0.5f*height);
                vertex.norm = glm::vec3(cos(thetaMean), sin(thetaMean), 0.0f);
                AddVertex(vertex);
            }
        }
    
        // Set indices
        for (int i = 0; i < nTheta; ++i)
        {
            for (int j = 0; j < nz-1; ++j)
            {
                AddIndex(nz*2*i       + j);
                AddIndex(nz*(2*i + 1) + j);
                AddIndex(nz*2*i       + (j + 1));
                AddIndex(nz*2*i       + (j + 1));
                AddIndex(nz*(2*i + 1) + j);
                AddIndex(nz*(2*i + 1) + (j + 1));
            }
        }
    }*/


    std::shared_ptr<Mesh> Mesh::SphereFactory(const float radius, const int nTheta, const int nz)
    {
        DYNASTI_LOG("Generating sphere mesh; nTheta : " + std::to_string(nTheta) + "   nz : " + std::to_string(nz));
        DYNASTI_ASSERT(nTheta > 2 && nz > 1, "Invalid no. of grid points for generating sheet mesh->");
    
        const int numVertices = nTheta*nz + 2;
        const int numIndices = 6*nTheta*nz + 6*nTheta;
        const VertexFormat format(VertexFormatType::BLOCKED,
                                  {VertexAttributeType::POSITION3, VertexAttributeType::NORMAL3}); //, VertexAttributeType::UV2});
        std::shared_ptr<Mesh> mesh = Mesh::MeshFactory(format, numIndices, numVertices);
        
        const float dTheta = twopi / static_cast<float>(nTheta);
        const float dPhi = pi / static_cast<float>(nz + 1);
    
        // Set vertex positions
        Vertex bottomVertex(glm::vec3(0.0f, 0.0f, -radius), glm::vec3(0.0f, 0.0f, -1.0f));
        mesh->AddVertex(bottomVertex);
        
        for (int j = 0; j < nz; ++j)
        {
            const float phi = dPhi * static_cast<float>(j + 1);
            for (int i = 0; i < nTheta; ++i)
            {
                const float theta = dTheta * static_cast<float>(i);
                Vertex vertex(glm::vec3(radius*sin(phi)*cos(theta), radius*sin(phi)*sin(theta), -radius*cos(phi)),
                              glm::vec3(sin(phi)*cos(theta), sin(phi)*sin(theta), -cos(phi)));
                mesh->AddVertex(vertex);
            }
        }
        
        Vertex topVertex(glm::vec3(0.0f, 0.0f, radius), glm::vec3(0.0f, 0.0f, 1.0f));
        mesh->AddVertex(topVertex);

        // Set indices
        for (int i = 0; i < nTheta; ++i)
        {
            mesh->AddIndices({0,  + (i + 1)%nTheta, 1 + i%nTheta});
        }
        for (int j = 0; j < nz-1; ++j)
        {
            for (int i = 0; i < nTheta; ++i)
            {
                mesh->AddIndices({1 + i%nTheta + nTheta*j, 1 + (i + 1)%nTheta + nTheta*j,
                                 1 + i%nTheta + nTheta*(j + 1), 1 + i%nTheta + nTheta*(j + 1),
                                 1 + (i + 1)%nTheta + nTheta*j, 1 + (i + 1)%nTheta + nTheta*(j + 1)});
            }
        }
        for (int i = 0; i < nTheta; ++i)
        {
            mesh->AddIndices({1 + nz*nTheta, 1 + i%nTheta + nTheta*(nz - 1), 1 + (i + 1)%nTheta + nTheta*(nz - 1)});
        }
        
        return mesh;
    }
    
    
    /*TorusMesh3d::TorusMesh3d(const float radius, const float thickness, const int nx, const int ny) :
      Mesh3d(6*nx*ny, nx*ny)
    {
        DYNASTI_LOG("Generating torus mesh; nx : " + std::to_string(nx) + "   ny : " + std::to_string(ny));
        DYNASTI_ASSERT(nx > 8 && ny > 6, "Invalid no. of grid points for generating torus mesh->");
        const float dTheta = twopi / static_cast<float>(nx);
        const float dPhi   = twopi / static_cast<float>(ny);
    
        // Set vertex positions
        for (int j = 0; j < ny; ++j)
        {
            const float phi = dPhi * static_cast<float>(j);
            for (int i = 0; i < nx; ++i)
            {
                const float theta = dTheta * static_cast<float>(i);
                Vertex3d vertex;
                vertex.r = glm::vec3(cos(theta)*(radius + thickness*cos(phi)),
                                     sin(theta)*(radius + thickness*cos(phi)), thickness*sin(phi));
                vertex.norm = glm::vec3(cos(theta)*cos(phi), sin(theta)*cos(phi), sin(phi));
                AddVertex(vertex);
            }
        }
        
        // Set indices
        for (int j = 0; j < ny; ++j)
        {
            for (int i = 0; i < nx; ++i)
            {
                AddIndex(i%nx       + nx*(j%ny));
                AddIndex((i + 1)%nx + nx*(j%ny));
                AddIndex(i%nx       + nx*((j + 1)%ny));
                AddIndex(i%nx       + nx*((j + 1)%ny));
                AddIndex((i + 1)%nx + nx*(j%ny));
                AddIndex((i + 1)%nx + nx*((j + 1)%ny));
            }
        }
        
    }*/

}
//---------------------------------------------------------------------------------------------------------------------
