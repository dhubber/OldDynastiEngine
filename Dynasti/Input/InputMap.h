#ifndef DYNASTI_INPUT_MAP_H
#define DYNASTI_INPUT_MAP_H


#include <map>
#include <memory>
#include <unordered_map>
#include "Dynasti/Core/Constants.h"
#include "Dynasti/Event/EventManager.h"
#include "Dynasti/Input/Key.h"


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{
    
    //=================================================================================================================
    /// \brief   Class defining a single mapping from the input to broadcasted input events.
    /// \author  D. A. Hubber
    /// \date    09/09/2020
    //=================================================================================================================
    class InputMap
    {
    public:
        
        InputMap(const std::string name, std::shared_ptr<EventManager> eventManager) :
          name_(name), eventManager_(eventManager)
        {
            DYNASTI_LOG_VERBOSE("Constructing new input mapping object : " + name);
            DYNASTI_ASSERT(eventManager_, "Invalid pointer to event manager");
        }
        
        /// Registers a new action associating a keyboard (SDL) input code with an key input event
        /// \param[in] keyActionName - Name of input action event
        /// \param[in] keyCode - KeyCode number for associated key
        /// \return - True if the input action has been successfully registered; otherwise returns false
        bool RegisterKeyActionEvent(const std::string keyActionName, const KeyCode keyCode);
        
        /// Called when a key-down event has been broadcast; updates any registered key with the correct scancode.
        /// \param[in] keyCode - KeyCode recorded by key-down event
        void OnKeyDown(const KeyCode keyCode);
    
        /// Called when a key-up event has been broadcast; updates any registered key with the correct scancode.
        /// \param[in] keyCode - KeyCode recorded by key-up event
        void OnKeyUp(const KeyCode keyCode);
        
        /// Returns a pointer to the key associated with the SDL scancode
        /// \param[in] keyCode - KeyCode of key to find
        /// \return - Pointer to key associated with scancode; if none found, then returns null
        std::shared_ptr<Key> GetKey(const KeyCode keyCode) const;
        
        /// Returns a pointer to the key associated with the input action event
        /// \param[in] keyActionName - Name of action of key to find
        /// \return - Pointer to key associated with action event; if none found, then returns null
        std::shared_ptr<Key> GetKey(const std::string keyActionName) const;
        
        /// \param[in] keyActionName - Name of key-action being polled
        /// \return - True if key associated with action is being pressed; otherwise returns false
        bool IsKeyDown(const std::string keyActionName);
        
        
    private:
    
        std::string name_;                                                 ///< Name of input mapping
        std::shared_ptr<EventManager> eventManager_;                       ///< Main event manager object
        std::unordered_map<KeyCode, std::shared_ptr<Key>> keyMap_;         ///< Map of KeyCode to Key object
        std::unordered_map<std::string, std::shared_ptr<Key>> actionMap_;  ///< Map of key event name to Key object
        
    };
    
}
//---------------------------------------------------------------------------------------------------------------------
#endif
