#ifndef DYNASTI_INPUT_MANAGER_H
#define DYNASTI_INPUT_MANAGER_H


#include <map>
#include <memory>
#include <unordered_map>
#include "Dynasti/Core/Constants.h"
#include "Dynasti/Event/EventManager.h"
#include "Dynasti/Input/InputMap.h"


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{
    
    //=================================================================================================================
    /// \brief   Main class responsible for managing all player input and broadcasting input-received events.
    /// \author  D. A. Hubber
    /// \date    08/09/2020
    //=================================================================================================================
    class InputManager
    {
    public:
        
        InputManager(std::shared_ptr<EventManager> eventManager) : eventManager_(eventManager), activeInputMap_(nullptr)
        {
            DYNASTI_LOG_VERBOSE("Constructing input manager object");
        }
        
        /// Creates a new input map object with the given name.  If first to be created, is automatically activated.
        /// \param[in] inputMapName - Human-readable name of new input map
        /// \return - Pointer to newly created input map; if name is already taken, returns null
        std::shared_ptr<InputMap> CreateNewInputMapping(const std::string inputMapName);
        
        /// Finds and returns the input map object with the given name.
        /// \param[in] inputMapName - Name of input map to search for
        /// \return - Pointer to input map object if valid; otherwise returns null
        std::shared_ptr<InputMap> GetInputMap(const std::string inputMapName);
    
        /// Activates the input map object with the given name (if it exists)
        /// \param[in] inputMapName - Name of input map to activate
        /// \return - True if input map name found and successfully activated; otherwise returns false
        bool SelectActiveMap(const std::string inputMapName);
    
        /// Processes any received key-down events.
        /// \param[in] keyCode - KeyCode of key-down event triggered by key press in window manager
        void OnKeyDown(const KeyCode keyCode);
    
        /// Processes any received key-up events.
        /// \param[in] keyCode - KeyCode of key-up event triggered by key release in window manager
        void OnKeyUp(const KeyCode keyCode);
        
        // Getter
        inline std::shared_ptr<InputMap> GetActiveInputMap() const { return activeInputMap_; }
        

    protected:
    
        std::shared_ptr<EventManager> eventManager_;       ///< Main event manager object
        std::shared_ptr<InputMap> activeInputMap_;         ///< Currently active input mapping
        std::map<std::string, std::shared_ptr<InputMap>> inputMapNames_;   ///< Maps input map names to the objects
    
    };

}
//---------------------------------------------------------------------------------------------------------------------
#endif
