#include "Dynasti/Input/InputManager.h"


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{
    
    std::shared_ptr<InputMap> InputManager::CreateNewInputMapping(const std::string inputMapName)
    {
        if (GetInputMap(inputMapName) != nullptr)
        {
            DYNASTI_WARNING("Cannot create new input mapping; name already exists : " + inputMapName);
            return nullptr;
        }
        std::shared_ptr<InputMap> newInputMap = std::make_shared<InputMap>(inputMapName, eventManager_);
        inputMapNames_[inputMapName] = newInputMap;
        if (activeInputMap_ == nullptr)
        {
            activeInputMap_ = newInputMap;
        }
        return newInputMap;
    }
    
    
    std::shared_ptr<InputMap> InputManager::GetInputMap(const std::string inputMapName)
    {
        auto it = inputMapNames_.find(inputMapName);
        if (it != std::end(inputMapNames_))
        {
            return it->second;
        }
        return nullptr;
    }
    
    
    bool InputManager::SelectActiveMap(const std::string inputMapName)
    {
        auto it = inputMapNames_.find(inputMapName);
        if (it != std::end(inputMapNames_))
        {
            activeInputMap_ = it->second;
            return true;
        }
        return false;
    }
    
    
    void InputManager::OnKeyDown(const KeyCode keyCode)
    {
        if (activeInputMap_)
        {
            activeInputMap_->OnKeyDown(keyCode);
        }
    }
    
    
    void InputManager::OnKeyUp(const KeyCode keyCode)
    {
        if (activeInputMap_)
        {
            activeInputMap_->OnKeyUp(keyCode);
        }
    }
    
}
//---------------------------------------------------------------------------------------------------------------------