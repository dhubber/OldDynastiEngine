#include "Dynasti/Input/InputMap.h"


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{
    
    bool InputMap::RegisterKeyActionEvent(const std::string keyActionName, const KeyCode keyCode)
    {
        //const std::string sdlScancodeName = SDL_GetScancodeName(sdlScancode);
        std::shared_ptr<Key> existingKey = GetKey(keyCode);
        if (existingKey)
        {
            const std::string existingEventName = existingKey->GetKeyActionName();
            if (existingEventName == keyActionName)
            {
                DYNASTI_WARNING("Event name " + keyActionName + " already registered to keyCode " + std::to_string(keyCode));
                return false;
            }
            else if (existingEventName != "")
            {
                DYNASTI_WARNING("keyCode " + std::to_string(keyCode) + " already registered to event name " + existingEventName);
                return false;
            }
            
            // If key is okay to be replaced, then delete old one here
            //keyMap_.erase(sdlScancode);
            keyMap_.erase(keyCode);
            actionMap_.erase(existingEventName);
        }
        
        std::string keyDownEventName = keyActionName + "KeyDown";
        std::string keyUpEventName = keyActionName + "KeyUp";
        EventType keyDownEventType = eventManager_->RegisterEventName(keyDownEventName);
        EventType keyUpEventType = eventManager_->RegisterEventName(keyUpEventName);
        DYNASTI_LOG("Registered key-action name " + keyActionName + " with keycode " + std::to_string(keyCode));
        
        std::shared_ptr<Key> newKey = std::make_shared<Key>(keyActionName, keyDownEventType, keyUpEventType);
        keyMap_[keyCode] = newKey;
        actionMap_[keyActionName] = newKey;
        
        return true;
    }
    
    
    void InputMap::OnKeyDown(const KeyCode keyCode)
    {
        std::shared_ptr<Key> key = GetKey(keyCode);
        if (key)
        {
            key->OnKeyDown();
            const EventType keyDownEventType = key->GetKeyDownEventType();
            if (keyDownEventType != -1)
            {
                Event keyPressEvent(keyDownEventType);
                eventManager_->BroadcastEvent(keyPressEvent);
            }
        }
    }
    

    void InputMap::OnKeyUp(const KeyCode keyCode)
    {
        std::shared_ptr<Key> key = GetKey(keyCode);
        if (key)
        {
            key->OnKeyUp();
            const EventType keyUpEventType = key->GetKeyUpEventType();
            if (keyUpEventType != -1)
            {
                Event keyPressEvent(keyUpEventType);
                eventManager_->BroadcastEvent(keyPressEvent);
            }
        }
    }
    
    
    std::shared_ptr<Key> InputMap::GetKey(const KeyCode keyCode) const
    {
        auto it = keyMap_.find(keyCode);
        if (it != std::end(keyMap_))
        {
            return it->second;
        }
        return nullptr;
    }
    
    
    std::shared_ptr<Key> InputMap::GetKey(const std::string keyActionName) const
    {
        auto it = actionMap_.find(keyActionName);
        if (it != std::end(actionMap_))
        {
            return it->second;
        }
        return nullptr;
    }
    
    
    bool InputMap::IsKeyDown(const std::string keyActionName)
    {
        std::shared_ptr<Key> key = GetKey(keyActionName);
        if (key)
        {
            return key->IsKeyDown();
        }
        return false;
    }
    
}
//---------------------------------------------------------------------------------------------------------------------