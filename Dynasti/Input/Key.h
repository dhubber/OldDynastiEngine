#ifndef DYNASTI_KEY_H
#define DYNASTI_KEY_H


#include "Dynasti/Core/Constants.h"


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{
    
    //=================================================================================================================
    /// \brief   Class representing a single key on a keyboard
    /// \author  D. A. Hubber
    /// \date    08/09/2020
    //=================================================================================================================
    class Key
    {
    public:
        
        Key(const std::string keyActionName, const EventType keyDownEventType, const EventType keyUpEventType) :
          keyActionName_(keyActionName), keyDownEventType_(keyDownEventType), keyUpEventType_(keyUpEventType),
          keyPressed_(false) {};
        
        /// Sets key-pressed state to true
        inline void OnKeyDown() { keyPressed_ = true; }
        
        /// Sets key-pressed state to false
        inline void OnKeyUp() { keyPressed_ = false; }
        
        /// \return - True if key is currently being pressed down
        inline bool IsKeyDown() const { return keyPressed_; }
        
        // Getters for private variables
        const std::string& GetKeyActionName() { return keyActionName_; }
        const EventType GetKeyDownEventType() const { return keyDownEventType_; }
        const EventType GetKeyUpEventType() const { return keyUpEventType_; }

        
    private:
        
        const std::string keyActionName_;                  ///< Input action name associated with the key
        const EventType keyDownEventType_;                 ///< Global event type to broadcast when key is pressed
        const EventType keyUpEventType_;                   ///< Global event type to broadcast when key is released
        bool keyPressed_;                                  ///< Flag if key is currently pressed or not
        
    };

}
//---------------------------------------------------------------------------------------------------------------------
#endif
