cmake_minimum_required(VERSION 3.10)
project(Dynasti)


set(CMAKE_CXX_STANDARD 14)


set(HEADERS
        Application/Application.h
        Application/Task.h
        Application/TaskManager.h
        Asset/Asset.h
        Asset/AssetManager.h
        Asset/AssetRegistry.h
        Camera/Camera.h
        Camera/CameraComponent.h
        Camera/Frustum.h
        Camera/Plane.h
        Core/Constants.h
        Core/Debug.h
        Core/FrameTimer.h
        Core/LogFile.h
        Core/Logger.h
        Core/System.h
        Core/Timer.h
        Data/ObjectPool.h
        Data/Poolable.h
        Ecs/Component.h
        Ecs/Entity.h
        Event/Event.h
        Event/EventListener.h
        Event/EventManager.h
        Input/InputManager.h
        Input/InputMap.h
        Mesh/Mesh.h
        Mesh/Vertex.h
        Mesh/VertexBufferFormat.h
        Renderer/Framebuffer.h
        Renderer/RenderableComponent.h
        Renderer/RendererSystem.h
        Renderer/Texture.h
        Renderer/VertexBuffer.h
        Renderer/Opengl/OpenglFramebuffer.h
        Renderer/Opengl/OpenglRendererSystem.h
        Renderer/Opengl/OpenglTexture.h
        Renderer/Opengl/OpenglVertexBuffer.h
        Scene/Scene.h
        Script/ScriptComponent.h
        Script/ScriptSystem.h
        Shader/GlslShader.h
        Window/WindowSystem.h
        Window/Glfw/GlfwWindow.h
        Window/Glfw/GlfwWindowSystem.h
        )


set(SOURCES
        Application/Application.cpp
        Application/TaskManager.cpp
        Asset/AssetManager.cpp
        Camera/Camera.cpp
        Camera/Frustum.cpp
        Core/Timer.cpp
        Event/EventListener.cpp
        Event/EventManager.cpp
        Input/InputManager.cpp
        Input/InputMap.cpp
        Mesh/Mesh.cpp
        Mesh/Mesh2d.cpp
        Mesh/Mesh3d.cpp
        Mesh/Vertex.cpp
        Renderer/Framebuffer.cpp
        Renderer/RenderableComponent.cpp
        Renderer/Texture.cpp
        Renderer/VertexBuffer.cpp
        Renderer/Opengl/OpenglRendererSystem.cpp
        Renderer/Opengl/OpenglTexture.cpp
        Renderer/Opengl/OpenglVertexBuffer.cpp
        Scene/Scene.cpp
        Script/ScriptSystem.cpp
        Shader/GlslShader.cpp
        Window/Glfw/GlfwWindow.cpp
        Window/Glfw/GlfwWindowSystem.cpp
        )


set(SHADER_SOURCES
        Shader/BasicColorShader.cpp
        Shader/BasicTextureShader.cpp
        Shader/WireframeShader.cpp
        )


# Check if SDL2 library exists for using SDL2 in place of GLFW
# Windows SDL2 libraries loaded as DLL in another directory; must provide path as environment variables
if (WIN32)
    set(SDL2_PATH $ENV{SDL2_PATH})
endif()
find_package(SDL2)

if (SDL2_FOUND)
    set(HEADERS ${HEADERS}
            Window/Sdl2/Sdl2Window.h
            Window/Sdl2/Sdl2WindowSystem.h
            )
    set(SOURCES ${SOURCES}
            Window/Sdl2/Sdl2Window.cpp
            Window/Sdl2/Sdl2WindowSystem.cpp
            )
endif()


add_library(Dynasti STATIC ${HEADERS} ${SOURCES} ${SHADER_SOURCES})
target_include_directories(Dynasti PUBLIC ${PROJECT_SOURCE_DIR})
#target_link_directories(Dynasti PUBLIC ${PROJECT_SOURCE_DIR}/lib)


# Include all other static libraries that don't have CMakeLists.txt files included
# GLAD : OpenGL definitions (replacement for GLEW)
# stb : Header-only image reading and writing library
# tinygltf : Header-only glTF 2.0 importer library
add_subdirectory(Dependencies)
#target_link_libraries(Dynasti tinygltf)

# GLM : GLSL-like maths library for OpenGL calculations
find_package(GLM)
if (GLM_FOUND)
    link_directories(${GLM_INCLUDE_DIR})
    include_directories(${GLM_INCLUDE_DIR})
else()
    add_subdirectory(Dependencies/glm)
    include_directories(${PROJECT_SOURCE_DIR}/Dynasti/Dependencies/glm)
endif()
if (WIN32)
    target_link_libraries(Dynasti glm)
endif()


# OpenGL libraries : Graphical API used for all rendering
if (UNIX)
    set(OpenGL_GL_PREFERENCE "GLVND")
endif()
find_package(OpenGL REQUIRED)
include_directories(${OPENGL_INCLUDE_DIRS})
target_link_libraries(Dynasti OpenGL::GL OpenGL::GLU glad ${CMAKE_DL_LIBS})


# GLFW library : Cross-platform window and OpenGL/Vulkan rendering context library
find_package(GLFW3)
if (GLFW3_FOUND)
    include_directories(${GLFW_INCLUDE_DIR})
    target_link_libraries(Dynasti glfw ${GLFW_LIBRARIES})
else()
    #set(BUILD_SHARED_LIBS ON CACHE BOOL "" FORCE)
    set(GLFW_BUILD_DOCS OFF CACHE BOOL "" FORCE)
    set(GLFW_BUILD_TESTS OFF CACHE BOOL "" FORCE)
    set(GLFW_BUILD_EXAMPLES OFF CACHE BOOL "" FORCE)
    #set(USE_MSVC_RUNTIME_LIBRARY_DLL ON CACHE BOOL "" FORCE)
    add_subdirectory(Dependencies/glfw)
    target_include_directories(Dynasti PUBLIC ${PROJECT_SOURCE_DIR}/Dependencies/glfw/include)
    if (WIN32)
        set(CMAKE_LIBRARY_PATH ${CMAKE_LIBRARY_PATH} ${GLFW_BINARY_DIR}/src)
        set(CMAKE_PREFIX_PATH ${CMAKE_PREFIX_PATH} ${GLFW_BINARY_DIR}/src)
        target_link_libraries(Dynasti glfw)
    else()
        target_link_libraries(Dynasti glfw)
    endif()
endif()


# SDL2 libraries : Cross-platform development libraries for game
if (SDL2_FOUND)
    target_link_libraries(Dynasti SDL2::Main SDL2::Core) # SDL2::Image) #SDL2::Mixer SDL2::Net SDL2::TTF)
endif()
