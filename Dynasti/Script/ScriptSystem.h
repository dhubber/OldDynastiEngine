#ifndef DYNASTI_SCRIPT_SYSTEM_H
#define DYNASTI_SCRIPT_SYSTEM_H


#include "Dynasti/Ecs/EcsSystem.h"
#include "Dynasti/Window/Window.h"


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{
    
    class AssetManager;
    class Event;
    class EventManager;
    class SceneManager;
    
    
    //=================================================================================================================
    /// \brief   System for updating all entity scripts and using
    /// \author  D. A. Hubber
    /// \date    27/10/2020
    //=================================================================================================================
    class ScriptSystem : public EcsSystem
    {
    public:
        
        ScriptSystem(std::shared_ptr<AssetManager> assetManager, std::shared_ptr<EventManager> eventManager,
                     std::shared_ptr<FrameTimer> frameTimer, std::shared_ptr<SceneManager> sceneManager) :
          EcsSystem("ScriptSystem", assetManager, eventManager, frameTimer, sceneManager) {};
        
        // Implementations of pure virtual functions
        virtual void Setup() override;
        virtual void Shutdown() override {};
        virtual void Update() override;
        
        /// Calls the update function for all script components in the given scene
        /// \param[in] scene - Pointer to scene being updated
        void UpdateScene(std::shared_ptr<Scene> scene);
        
        /// Calls OnEvent function for script components when subscribed events are broadcast by the event manager
        /// \param[in] event - Event being broadcast by the event manager
        void OnEvent(const Event event);
    
        
    protected:
        
        std::vector<ScriptComponent*> FindScriptComponents(const GlobalId entityId);
        
        //void OnCollisionEnter(const Event event);
        //void OnCollisionExit(const Event event);
        
    };
    
}
//---------------------------------------------------------------------------------------------------------------------
#endif
