#include "Dynasti/Core/FrameTimer.h"
#include "Dynasti/Scene/Scene.h"
#include "Dynasti/Scene/SceneManager.h"
#include "Dynasti/Script/ScriptComponent.h"
#include "Dynasti/Script/ScriptSystem.h"


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{
    
    void ScriptSystem::Setup()
    {
        eventListener_->SubscribeToEvent("OnCollisionEnter", std::bind(&ScriptSystem::OnEvent, this, std::placeholders::_1));
        eventListener_->SubscribeToEvent("OnCollisionExit", std::bind(&ScriptSystem::OnEvent, this, std::placeholders::_1));
        //eventListener_->SubscribeToEvent("OnCollisionEnter", std::bind(&ScriptSystem::OnCollisionEnter, this, std::placeholders::_1));
        //eventListener_->SubscribeToEvent("OnCollisionExit", std::bind(&ScriptSystem::OnCollisionExit, this, std::placeholders::_1));
    }
    

    void ScriptSystem::Update()
    {
        DYNASTI_TIMER(SCRIPT_SYSTEM);
        
        std::shared_ptr<Scene> scene = sceneManager_->GetSelectedScene();
        if (scene)
        {
            UpdateScene(scene);
        }
    }
    
    
    void ScriptSystem::UpdateScene(std::shared_ptr<Scene> scene)
    {
        if (scene == nullptr)
        {
            DYNASTI_WARNING("Scene pointer invalid");
            return;
        }
        
        //DYNASTI_LOG("Updating script components in scene " + scene->GetName());
    
        std::vector<std::shared_ptr<ObjectPoolBase>> scriptPools = scene->FindAllScriptPools();
        //DYNASTI_LOG("Number of script pools : " + std::to_string(scriptPools.size()));
        for (std::shared_ptr<ObjectPoolBase> scriptPool : scriptPools)
        {
            if (scriptPool)
            {
                //DYNASTI_LOG("Updating scripts : " + std::to_string(scriptPool->NumObjects()));
                for (int i = 0; i < scriptPool->NumObjects(); ++i)
                {
                    ScriptComponent& scriptComponent = static_cast<ScriptComponent&>((*scriptPool)[i]);
                    scriptComponent.OnUpdate(scene, frameTimer_);
                }
            }
        }
    }
    
    
    void ScriptSystem::OnEvent(const Event event)
    {
        std::shared_ptr<Scene> scene = sceneManager_->GetSelectedScene();
        if (scene)
        {
            ScriptComponent* scriptComponent = scene->FindScriptComponent(event.GetId());
            if (scriptComponent)
            {
                scriptComponent->OnEvent(event, scene);
            }
        }
    }
    
}
//---------------------------------------------------------------------------------------------------------------------