#ifndef DYNASTI_SCRIPT_COMPONENT_H
#define DYNASTI_SCRIPT_COMPONENT_H


#include <memory>
#include "Dynasti/Core/Constants.h"
#include "Dynasti/Core/Debug.h"
#include "Dynasti/Ecs/Component.h"
#include "Dynasti/Event/Event.h"


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{
    
    // Forward declarations
    class FrameTimer;
    class Scene;
    

    //=================================================================================================================
    /// \brief   Base class for all scriptable components.
    /// \author  D. A. Hubber
    /// \date    13/02/2020
    //=================================================================================================================
    class ScriptComponent : public Component
    {
    public:

        ScriptComponent(const int entityId=-1) : Component(entityId) {};
    
        /// Returns a simple human-readable name of the component type (implemented by child classes)
        /// @return - Human-readable name of the component
        virtual const std::string GetComponentName() override { return "ScriptComponent"; }
    
        /// Initialises component in the object pool when entity is spawned (implemented by child classes)
        virtual void OnCreate() override {};
        
        /// Destroys the component in the object pool when entity is destroyed (implemented by child classes)
        virtual void OnDestroy() override {};
        
        /// Function for updating the entity every frame
        virtual void OnUpdate(std::shared_ptr<Scene> scene, std::shared_ptr<FrameTimer> frameTimer) {};
    
        /// Function for responding to any event involving this entity
        /// \param[in] event - Event object containing all relevant data
        virtual void OnEvent(const Event event, std::shared_ptr<Scene> scene) {};
    
        /*/// Function for responding to any CollisionEnter event involving this entity
        /// \param[in] event - Event object containing all relevant data
        virtual void OnCollisionEnter(const Event event, std::shared_ptr<Scene> scene) {};
        
        /// Function for responding to any CollisionExit event involving this entity
        /// \param[in] event - Event object containing all relevant data
        virtual void OnCollisionExit(const Event event, std::shared_ptr<Scene> scene) {};*/
        
        
    protected:
        
        //std::shared_ptr<Scene> scene_;                       ///< Weak pointer to scene (for accessing other components)

    };

}
//---------------------------------------------------------------------------------------------------------------------
#endif
