#ifndef DYNASTI_LOGGER_H
#define DYNASTI_LOGGER_H


#include <assert.h>
#include <cstdarg>
#include <cstdlib>
#include <string>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <iostream>
#include <mutex>
#include "Dynasti/Core/Constants.h"
#include "Dynasti/Core/LogFile.h"


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{

    //=================================================================================================================
    /// \brief   Singleton class for writing messages to logs from any point in the code.
    /// \author  D. A. Hubber
    /// \date    27/10/2017
    //=================================================================================================================
    class Logger
    {
    public:

        /// Delete the copy constructor and operator to prevent additional instances from being created
        Logger(Logger const &)         = delete;
        void operator=(Logger const &) = delete;

        /// Write a message to the terminal and/or log, if either is set to at least LOG_BASIC logging mode
        /// \param[in] file - Char string of the header/source file (including the relative path) invoking the log
        /// \param[in] line - Line no. in header/source file invoking the log
        /// \param[in] message - Message to write in the log
        inline void Log(const char *file, const int line, std::string message)
        {
            if (mainLog_.IsFileOpen() == false) Open();
            if (terminalMode_ >= LogMode::LOG_BASIC) std::cout << message << std::endl;
            if (logMode_ >= LogMode::LOG_BASIC) mainLog_.WriteLog(file, line, message);
        }

        /// Write a message to the terminal and/or log when a fatal error has been encountered.
        /// \param[in] file - Char string of the header/source file (including the relative path) invoking the log
        /// \param[in] line - Line no. in header/source file invoking the log
        /// \param[in] message - Message to write in the log
        inline void LogError(const char *file, const int line, std::string message)
        {
            std::unique_lock<std::mutex> lock(loggerMutex_);
            std::string errorMessage = "Error : " + message;
            Log(file, line, errorMessage);
            Close();
        }

        /// Write a message to the terminal and/or log, if either is set to at least LOG_VERBOSE logging mode
        /// \param[in] file - Char string of the header/source file (including the relative path) invoking the log
        /// \param[in] line - Line no. in header/source file invoking the log
        /// \param[in] message - Message to write in the log
        inline void LogVerbose(const char *file, const int line, std::string message)
        {
            if (mainLog_.IsFileOpen() == false) Open();
            if (terminalMode_ >= LogMode::LOG_VERBOSE) std::cout << message << std::endl;
            if (logMode_ >= LogMode::LOG_VERBOSE) mainLog_.WriteLog(file, line, message);
        }

        /// Write a message to the terminal and/or log, if either is set to at least LOG_DEBUG logging mode
        /// \param[in] file - Char string of the header/source file (including the relative path) invoking the log
        /// \param[in] line - Line no. in header/source file invoking the log
        /// \param[in] message - Message to write in the log
        inline void LogDebug(const char *file, const int line, std::string message)
        {
            if (mainLog_.IsFileOpen() == false) Open();
            if (terminalMode_ >= LogMode::LOG_DEBUG) std::cout << message << std::endl;
            if (logMode_ >= LogMode::LOG_DEBUG) mainLog_.WriteLog(file, line, message);
        }

        /// Write a warning message to the terminal and/or log
        /// \param[in] file - Char string of the header/source file (including the relative path) invoking the log
        /// \param[in] line - Line no. in header/source file invoking the log
        /// \param[in] message - Message to write in the log
        inline void LogWarning(const char *file, const int line, std::string message)
        {
            std::string warningMessage = "Warning : " + message;
            Log(file, line, warningMessage);
        }

        /// Static function for creating the singleton instance and returning it
        /// \return - The singleton instance of the Logger class
        static inline Logger& GetInstance(const std::string logTag="")
        {
            static Logger instance(logTag);
            return instance;
        }
    
        /// \return - True if the log file has been initialised and open; otherwise false
        inline bool IsInitialised() const {return mainLog_.IsFileOpen();}

        /// Getters for private members
        inline const std::string& GetMainLogFilename() const {return mainLog_.GetFileName();}
        inline LogMode GetLogMode() const {return logMode_;}
        inline const std::string& GetLogTag() const {return logTag_;}
        inline LogMode GetTerminalMode() const {return terminalMode_;}

        /// Set the logging mode of the log files (e.g. verbose, debug)
        /// \param[in] logMode - New logging mode value
        inline void SetLogMode(const LogMode logMode) { logMode_ = logMode;}

        /// Set the log tag used to name log files
        /// \param[in] logTag - New log tag
        inline void SetLogTag(const std::string logTag) { logTag_ = logTag;}

        /// Set the logging mode of the terminal output (e.g. verbose, debug)
        /// \param[in] terminalMode -
        inline void SetTerminalMode(const LogMode terminalMode) { terminalMode_ = terminalMode;}


    private:

        std::mutex loggerMutex_;                     ///< Mutex for main logger object
        std::string logTag_;                         ///< String tag for logfiles
        LogFile mainLog_;                            ///< Main (and currently only) log object
        LogMode logMode_;                            ///< Verbosity mode for main log file
        LogMode terminalMode_;                       ///< Verbosity mode for terminal output

        /// Singleton constructor for the global logger object.  Initialises all user-changeable variables
        Logger(const std::string logTag="") : logTag_(logTag), logMode_(LogMode::LOG_VERBOSE), terminalMode_(LogMode::LOG_NULL) {};

        /// Singleton destructor which closes the main log file before destruction
        ~Logger()
        {
            Close();
        }

        /// Opens the logger and the main log file
        inline void Open()
        {
            assert(mainLog_.IsFileOpen() == false);
            if (logTag_ == "")
            {
                logTag_ = "DYNASTI";
            }
            mainLog_.Open(logTag_ + ".log");
            mainLog_.WriteMessage("Main logger singleton opened");
            //mainLog_.WriteMessage(DYNASTI_PATH_STRING);
        }

        /// Closes the logger including the main log file
        inline void Close()
        {
            if (mainLog_.IsFileOpen())
            {
                mainLog_.Close();
                logTag_ = "";
            }
        }

    };

}
//---------------------------------------------------------------------------------------------------------------------
#endif
