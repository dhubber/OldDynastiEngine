#ifndef DYNASTI_INCLUDE_OPENGL_H
#define DYNASTI_INCLUDE_OPENGL_H

#if defined(_WIN32) || defined(_WIN64)
#include <windows.h>
//#include <GL/glew.h>
//#include <GL/gl.h>
//#include <GL/glu.h>
#elif defined(__linux__) || defined(__unix__)
//#include <GL/glew.h>
//#include <GL/gl.h>
//#include <GL/glu.h>
//#include <GL/glext.h>
#elif defined(__APPLE__)
//#include <OpenGL/gl.h>
//#include <OpenGL/glu.h>
//#include <OpenGL/glext.h>
#endif
#include "glad/glad.h"

#endif
