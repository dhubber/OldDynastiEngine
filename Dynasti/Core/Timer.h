#ifndef DYNASTI_TIMER_H
#define DYNASTI_TIMER_H


#include <cstdlib>
#include <string>
#include <iostream>
#include <map>
#include <mutex>
#include <thread>
#include "Dynasti/Core/Constants.h"


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{

    /// Forward declaration of TimerBlock to resolve circular dependency
    //class TimerBlock;


    //=================================================================================================================
    /// \brief   Class representing a single timing block with functions for starting and stopping the timer clock.
    /// \author  D. A. Hubber
    /// \date    27/11/2019
    //=================================================================================================================
    class TimerBlock
    {
    public:

        /// Constructor to create timing block and register with main Timer singleton
        /// \param[in] _name - Unique human readable name of this timing block
        TimerBlock(const std::string name);

        /// Destructor of timing block which unregisters with the main Timer singleton.
        ~TimerBlock();

        /// Starts/restarts the clock to record the timed block of code (if not already running)
        void StartTimer();

        /// Stops the clock and adds the time to the total accumulated time of this block
        void StopTimer();
    
        /// \return - True if the timer block is currently running; false if not running
        inline const bool IsRunning() const { return running_; }
        
        /// Getters for private variables values
        inline const std::string GetName() const { return name_; }
        inline const MyDouble GetTotalTime() const { return totalTime_; }
        inline const std::thread::id GetThreadId() const { return threadId_; }


    private:

        const std::thread::id threadId_;             ///< System id of thread that created timing block
        const std::string name_;                      ///< Human-readable name of timing block
        bool running_;                               ///< Is timer running for this block?
        //double startTime;                            ///< Wall-clock time when block was started

        //std::chrono::duration<double> totalTime;                            ///< Accumulated running wall-clock time
        MyDouble totalTime_;                                               ///< Accumulated running wall-clock time
        std::chrono::high_resolution_clock::time_point startTime_;
        //std::chrono::high_resolution_clock::time_point totalTime;

    };



    //=================================================================================================================
    /// \brief   Singleton class for managing the internal timing routines.
    /// \author  D. A. Hubber
    /// \date    27/11/2019
    //=================================================================================================================
    class Timer
    {
    public:

        /// Delete the copy constructor and operator to prevent additional instances from being created
        Timer(Timer const &)           = delete;
        void operator= (Timer const &) = delete;

        /// Registers a new TimerBlock and records its time.  If the TimerBlock has been previously registered,
        /// the newly timed segment is added to the previously recorded time.
        /// \param[in] timerBlock - TimerBlock instance to be added to the records
        void RecordTimerBlock(const TimerBlock &timerBlock);

        /// Writes all recorded timing blocks to file with a given filename.
        /// \param[in] filename - Filename to be written to with timing statistics
        void WriteTimingStatistics(const std::string filename);

        /// Static function for creating the singleton instance and returning it
        /// \return - The singleton instance of the Timer class
        static inline Timer& GetInstance()
        {
            static Timer instance;
            return instance;
        }

        /// \return - The filename containing the outputted statistics
        inline const std::string& GetFilename() const {return filename_;}
        inline int GetNumTimerBlocks() const {return totalTimes_.size();}

        /// Sets the filename containing all outputted statistics
        /// \param[in] filename - New filename to be recorded
        inline void SetFilename(const std::string filename) {filename_ = filename;}


    private:

        double startTime_;                           ///< Wall-clock time at construction
        double totalTime_;                           ///< Total time since construction
        TimerBlock globalTimerBlock_;                ///< Timer block for measuring total running time of code
        std::mutex timerMutex_;                      ///< Mutex for main timer object
        std::string filename_;                       ///< Name of file for outputing statistics
        std::map <std::string, double> totalTimes_;  ///< Map of (total) wall clock times

        /// Singleton constructor which starts the global code timer
        Timer();

        /// Singleton destructor which stops timing and writes all timing statistics before being destroyed
        ~Timer();

    };

}
//---------------------------------------------------------------------------------------------------------------------
#endif
