#include <iomanip>
#include <iostream>
#include <fstream>
#include <string>
#include <thread>
#include "Dynasti/Core/Timer.h"


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{

    TimerBlock::TimerBlock(const std::string name) : threadId_(std::this_thread::get_id()), name_(name)
    {
        running_   = false;
        totalTime_ = MyDouble(0.0);
        StartTimer();
    }


    TimerBlock::~TimerBlock()
    {
        StopTimer();
        Timer &timer = Timer::GetInstance();
        timer.RecordTimerBlock(*this);
    }


    void TimerBlock::StartTimer()
    {
        if (!running_)
        {
            running_   = true;
            startTime_ = std::chrono::high_resolution_clock::now();
        }
    }


    void TimerBlock::StopTimer()
    {
        if (running_)
        {
            const std::chrono::high_resolution_clock::time_point endTime = std::chrono::high_resolution_clock::now();
            const MyDouble dt = std::chrono::duration_cast<std::chrono::duration<MyDouble>>(endTime - startTime_).count();
            totalTime_ += dt;
            //startTime = endTime;
            running_ = false;
        }
    }


    Timer::Timer() : globalTimerBlock_("GlobalTime")
    {
        filename_  = "dynasti.timing";
    }


    Timer::~Timer()
    {
        globalTimerBlock_.StopTimer();
        totalTime_ = globalTimerBlock_.GetTotalTime();
        WriteTimingStatistics(filename_);
    }


    void Timer::RecordTimerBlock(const TimerBlock &block)
    {
        std::unique_lock<std::mutex> lock(timerMutex_);
        const std::string key = block.GetName();
        if (totalTimes_.find(key) == totalTimes_.end())
        {
            totalTimes_[key] = 0.0;
        }
        totalTimes_[key] += block.GetTotalTime();
    }


    void Timer::WriteTimingStatistics(const std::string filename)
    {
        std::unique_lock<std::mutex> lock(timerMutex_);
        std::ofstream outfile;
        outfile.open(filename.c_str());

        std::map <std::string, double>::iterator it;
        for (it=totalTimes_.begin(); it != totalTimes_.end(); ++it)
        {
            outfile << std::fixed << std::setw(30) << it->first << "    t : "
                    << std::setw(8) << std::setprecision(5) << it->second << "     % : "
                    << std::setw(8) << std::setprecision(5) << 100.0*it->second/totalTime_ << std::endl;
        }

        outfile.close();
    }

}
//---------------------------------------------------------------------------------------------------------------------
