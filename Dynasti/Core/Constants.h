#ifndef DYNASTI_CONSTANTS_H
#define DYNASTI_CONSTANTS_H


#include <algorithm>
#include <cstdint>
#include <string>
#include <glm/glm.hpp>


#define QUOTE(name) #name
#define STR(macro) QUOTE(macro)
#define DYNASTI_PATH_STRING STR(DYNASTI_PATH)


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{
    typedef float MyFloat;
    typedef double MyDouble;
    typedef int EventType;
    typedef int GlobalId;
    typedef int KeyCode;
    
    
    static const std::string enginePath = std::string(DYNASTI_PATH_STRING);
    static const std::string shaderPath = enginePath + "/Assets/Shaders/";
    static const std::string imagePath = enginePath + "/Assets/Images/";
    static const std::string modelPath = enginePath + "/Assets/Models/";


    static const MyFloat invpi        = MyFloat(0.31830988618379);
    static const MyFloat invfourpi    = MyFloat(0.07957747154594767);
    static const MyFloat pi           = MyFloat(3.141592653589793);
    static const MyFloat twopi        = MyFloat(6.28318530717959);
    static const MyFloat big_number   = MyFloat(9.9e30);
    static const MyFloat small_number = MyFloat(1.0e-20);
    static const MyFloat twothirds    = MyFloat(2.0/3.0);
    static const MyFloat invlogetwo   = MyFloat(1.442695040);
    static const MyFloat invlog10two  = MyFloat(3.321928095);
    static const MyFloat invsqrttwo   = MyFloat(0.707106781);
    static const MyFloat sqrttwo      = MyFloat(1.414213562);


    // Some standard colors
    static const glm::vec3 red(1.0f, 0.0f, 0.0f);
    static const glm::vec3 green(0.0f, 1.0f, 0.0f);
    static const glm::vec3 blue(0.0f, 0.0f, 1.0f);
    static const glm::vec3 black(0.0f, 0.0f, 0.0f);
    static const glm::vec3 white(1.0f, 1.0f, 1.0f);
    
    
    enum class CameraMode
    {
        CAMERA_NULL,
        CAMERA_ORTHOGRAPHIC,
        CAMERA_PERSPECTIVE
    };
    
    enum class LogMode
    {
        LOG_NULL,
        LOG_BASIC,
        LOG_VERBOSE,
        LOG_DEBUG
    };
    
    enum class PlaneOrientation
    {
        PLANE_NULL,
        PLANE_COPLANAR,
        PLANE_FRONT,
        PLANE_BACK
    };
    
    enum class RenderableType
    {
        RENDERABLE_NULL,
        RENDERABLE_2D,
        RENDERABLE_3D
    };

    enum class ShaderType
    {
        SHADER_NULL,
        SHADER_WIREFRAME,
        SHADER_COLOR
    };
    
    enum class TextureType
    {
        TEXTURE_NULL,
        TEXTURE_DIFFUSE,
        TEXTURE_SPECULAR,
        TEXTURE_AMBIENT,
        TEXTURE_EMISSIVE,
        TEXTURE_HEIGHT,
        TEXTURE_NORMALS,
        TEXTURE_BASE_COLOR,
        TEXTURE_NORMAL_CAMERA,
        TEXTURE_EMISSION_COLOR,
        TEXTURE_METALNESS,
        TEXTURE_DIFFUSE_ROUGHNESS,
        TEXTURE_AMBIENT_OCCLUSION,
        TEXTURE_UNKNOWN
    };


    // Temporary place to put static inlined functions (hopefully move in the future)
    template <typename T>
    static inline T MyClamp(T value, T lowerBound, T upperBound)
    {
        return std::max(lowerBound, std::min(value, upperBound));
    }

}
//---------------------------------------------------------------------------------------------------------------------
#endif
