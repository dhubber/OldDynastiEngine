#ifndef DYNASTI_LOG_FILE_H
#define DYNASTI_LOG_FILE_H


#include <cstdarg>
#include <cstdlib>
#include <string>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <iostream>
#include <mutex>
#include "Dynasti/Core/Constants.h"


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{

    //=================================================================================================================
    /// \brief   Basic class wrapping all functions for opening. closing and writing to log-files.
    /// \author  D. A. Hubber
    /// \date    27/10/2017
    //=================================================================================================================
    class LogFile
    {
    public:

        /// Default onstructor that initialises all variables in class.
        LogFile() : fileOpen_(false), fileName_("") {};

        /// Destructor for LogFile; closes file in case it was not previously closed before destruction
        ~LogFile()
        {
            Close();
        };

        /// Opens the log file stream for writing
        /// \param[in] fileName - Name of file to be opened
        inline void Open(const std::string fileName)
        {
            assert(!fileOpen_);
            fileStream_.open(fileName.c_str());
            fileName_ = fileName;
            fileOpen_ = true;
        }

        /// Closes the file stream object and resets all variables back to their default values
        inline void Close()
        {
            if (fileOpen_)
            {
                fileStream_.close();
                fileOpen_ = false;
                fileName_ = "";
            }
        }
    
        /// Write a single line to the log stream object, including the source code file name and line number
        /// \param[in] file - Char string of the header/source file (including the relative path) invoking the log
        /// \param[in] line - Line no. in header/source file invoking the log
        /// \param[in] message - Message to write in the log
        inline void WriteMessage(const std::string message)
        {
            std::unique_lock<std::mutex> lock(logMutex_);
            assert(fileOpen_);
            fileStream_ << message << std::endl;
            lastMessage_ = message;
        }

        /// Write a single line to the log stream object, including the source code file name and line number
        /// \param[in] file - Char string of the header/source file (including the relative path) invoking the log
        /// \param[in] line - Line no. in header/source file invoking the log
        /// \param[in] message - Message to write in the log
        inline void WriteLog(const char *file, const int line, const std::string message)
        {
            std::unique_lock<std::mutex> lock(logMutex_);
            assert(fileOpen_);
            const int pathLength = enginePath.length();
            const std::string shortFileString = std::string(file).substr(pathLength + 1);
            std::stringstream locationString, lineStream, messageString;
            locationString << "[" << shortFileString << "." << line << "]  ";
            lineStream << std::setw(std::max(50, pathLength)) << std::left << locationString.str() << message;
            const std::string &lineString = lineStream.str();
            fileStream_ << lineString << std::endl;
            lastMessage_ = message;
        }

        /// Getters for returning values of private variables
        inline const std::string& GetFileName() const {return fileName_;}
        inline const std::string& GetLastMessage() const {return lastMessage_;}

        /// \return - True if LogFile stream object is already open; otherwise false
        inline const bool IsFileOpen() const {return fileOpen_;}


    private:

        bool fileOpen_;                                    ///< Has the log-file been opened for writing?
        std::mutex logMutex_;                              ///< Mutex for writing to the log
        std::string fileName_;                             ///< Human-readable filename of log-file
        std::string lastMessage_;                          ///< Final message written by log-file
        std::ofstream fileStream_;                         ///< Stream-object for writing to file

    };

}
//---------------------------------------------------------------------------------------------------------------------
#endif
