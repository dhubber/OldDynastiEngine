#ifndef DYNASTI_FRAME_TIMER_H
#define DYNASTI_FRAME_TIMER_H


#include <chrono>
#include "Dynasti/Core/Constants.h"
#include "Dynasti/Core/Debug.h"


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{
    
    //=================================================================================================================
    /// \brief   Class used to calculate the time between frames (used for system updates) and the current fps.
    /// \author  D. A. Hubber
    /// \date    18/11/2020
    //=================================================================================================================
    class FrameTimer
    {
    public:

        /// Constructor which intialises all time points
        /// \param[in] fpsUpdateInterval - Time interval between recomputing the current fps
        FrameTimer(const MyDouble fpsUpdateInterval=1.0) : numFrames_(0), numFramesLastFps_(0),
            fps_(0.0), fpsUpdateInterval_(fpsUpdateInterval)
        {
            DYNASTI_ASSERT(std::isnormal(fpsUpdateInterval_), "Invalid fps update interval : " + std::to_string(fpsUpdateInterval_));
            startTimePoint_ = std::chrono::high_resolution_clock::now();
            currentTimePoint_ = startTimePoint_;
            lastFpsTimePoint_ = startTimePoint_;
        };
        
        /// Updates all values for the current frame, including the current time, frame timestep and the fps
        void Update(const std::chrono::high_resolution_clock::time_point& newTimePoint)
        {
            ++numFrames_;
            lastTimePoint_ = currentTimePoint_;
            currentTimePoint_ = newTimePoint;
            deltaTime_ = std::chrono::duration_cast<std::chrono::duration<double>>(currentTimePoint_ - lastTimePoint_);
            totalTime_ = std::chrono::duration_cast<std::chrono::duration<double>>(currentTimePoint_ - startTimePoint_);
            
            // If needed, recompute the fps
            const double deltaTimeFps = std::chrono::duration_cast<std::chrono::duration<double>>(currentTimePoint_ - lastFpsTimePoint_).count();
            if (deltaTimeFps / fpsUpdateInterval_ >= 1.0)
            {
                fps_ = static_cast<float>(numFrames_ - numFramesLastFps_) / deltaTimeFps;
                numFramesLastFps_ = numFrames_;
                lastFpsTimePoint_ = currentTimePoint_;
                DYNASTI_LOG("fps : " + std::to_string(fps_));
            }
        }

        // Getter functions
        inline const double GetFrameInterval() const { return deltaTime_.count(); }
        inline const int GetNumFrames() const { return numFrames_; }
        inline const double GetTime() const { return totalTime_.count(); }
        inline const double GetFps() const { return fps_; }


    private:
        
        int numFrames_;                                                    ///< Total no. of frames
        int numFramesLastFps_;                                             ///< No. of frames of last fps calculation
        double fps_;                                                       ///< Frames per second
        double fpsUpdateInterval_;                                         ///< Frequency for calculating new fps
        std::chrono::duration<double> deltaTime_;                          ///< Time interval of last frame
        std::chrono::duration<double> totalTime_;                          ///< Accumulated running wall-clock time
        std::chrono::high_resolution_clock::time_point startTimePoint_;    ///< Initial time point
        std::chrono::high_resolution_clock::time_point currentTimePoint_;  ///< Current time point
        std::chrono::high_resolution_clock::time_point lastTimePoint_;     ///< Current time point
        std::chrono::high_resolution_clock::time_point lastFpsTimePoint_;  ///< Time point of last fps calculation

    };
    
}
//---------------------------------------------------------------------------------------------------------------------
#endif
