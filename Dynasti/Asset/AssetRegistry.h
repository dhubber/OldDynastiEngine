#ifndef DYNASTI_ASSET_REGISTRY_H
#define DYNASTI_ASSET_REGISTRY_H


#include <memory>
#include <string>
#include <unordered_map>
#include "Dynasti/Asset/Asset.h"
#include "Dynasti/Core/Constants.h"
#include "Dynasti/Core/Debug.h"


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{
    
    //=================================================================================================================
    /// \brief   Registry for all assets of the templated class type.
    /// \author  D. A. Hubber
    /// \date    14/08/2020
    //=================================================================================================================
    template <typename AssetType>
    class AssetRegistry
    {
    public:
    
        AssetRegistry() = default;
        virtual ~AssetRegistry() = default;
        
        ///< Registers an asset in the registry.  If the name is not unique, then an alternative name is generated.
        /// \param[in] assetName - Name of asset for easy lookup
        /// \param[in] assetPtr - Pointer to the asset object
        bool Register(std::shared_ptr<AssetType> assetPtr, const std::string assetName, const std::string assetFullPath = "")
        {
            if (!assetFullPath.empty())
            {
                if (FindImported(assetFullPath) == nullptr)
                {
                    assetPtr->SetFullPath(assetFullPath);
                    assetPathMap_[assetFullPath] = assetPtr;
                }
                else
                {
                    DYNASTI_WARNING("Asset already imported into registry : " + assetFullPath);
                    return false;
                }
            }
            
            GlobalId globalId = globalIdCounter_++;
            DYNASTI_ASSERT(Find(globalId) == nullptr, "Asset global id already registered : " + std::to_string(globalId));
            assetPtr->SetId(globalId);
            assetGlobalIdMap_[globalId] = assetPtr;
            if (Find(assetName))
            {
                const std::string alternativeName = CreateNewAssetName(assetName, globalId);
                assetPtr->SetName(alternativeName);
                assetNameMap_[alternativeName] = assetPtr;
                DYNASTI_LOG_VERBOSE("Registered alternative asset name : " + alternativeName);
            }
            else
            {
                assetPtr->SetName(assetName);
                assetNameMap_[assetName] = assetPtr;
                DYNASTI_LOG_VERBOSE("Registered asset name : " + assetName);
            }
            return true;
        }
        
        
        /// Searches for an asset using its unique global id and returns a pointer to the asset (if it exists)
        /// \param[in] globalId - Global id of asset
        /// \return - Pointer to the asset; otherwise returns nullptr if it does not exist
        std::shared_ptr<AssetType> Find(const GlobalId globalId)
        {
            auto it = assetGlobalIdMap_.find(globalId);
            if (it != assetGlobalIdMap_.end())
            {
                DYNASTI_ASSERT(it->second, "Invalid shared pointer found for asset global id : " + std::to_string(globalId));
                return it->second;
            }
            else
            {
                return nullptr;
            }
        }
        
        
        /// Searches for an asset using its unique name and returns a pointer to the asset (if it exists)
        /// \param[in] assetName - Human-readable name of the asset
        /// \return - Pointer to the asset; otherwise returns nullptr if it does not exist
        std::shared_ptr<AssetType> Find(const std::string assetName)
        {
            auto it = assetNameMap_.find(assetName);
            if (it != assetNameMap_.end())
            {
                DYNASTI_ASSERT(it->second, "Invalid shared pointer found for asset name : " + assetName);
                return it->second;
            }
            else
            {
                return nullptr;
            }
        }
        
        
        /// Searches for an imported asset using its absolute path and returns a pointer to the asset (if it exists)
        /// \param[in] assetFullPath - Full/absolute path of asset on disk
        /// \return - Pointer to the asset; otherwise returns nullptr if it does not exist
        std::shared_ptr<AssetType> FindImported(const std::string assetFullPath)
        {
            auto it = assetPathMap_.find(assetFullPath);
            if (it != assetPathMap_.end())
            {
                DYNASTI_ASSERT(it->second, "Invalid shared pointer found for imported asset at path : " + assetFullPath);
                return it->second;
            }
            else
            {
                return nullptr;
            }
        }
        
        
        /// Unregisters any asset with the given unique global id.
        /// \param[in] globalId - Global id of asset
        /// \return - 'true' if the asset existed and was unregistered; 'false' if no asset was found with that id
        bool Unregister(const GlobalId assetGlobalId)
        {
            std::shared_ptr<AssetType> assetPtr = Find(assetGlobalId);
            if (assetPtr)
            {
                const std::string assetName = assetPtr->GetName();
                DYNASTI_ASSERT(Find(assetName) == assetPtr, "Asset pointers do not match");
                assetNameMap_.erase(assetName);
                assetGlobalIdMap_.erase(assetGlobalId);
                return true;
            }
            else
            {
                return false;
            }
        }
        
        
        /// Unregisters any asset with the given unique name.
        /// \param[in] assetName - Name of the asset
        /// \return - 'true' if the asset existed and was unregistered; 'false' if no asset was found with that id
        bool Unregister(const std::string assetName)
        {
            std::shared_ptr<AssetType> assetPtr = Find(assetName);
            if (assetPtr)
            {
                const GlobalId assetGlobalId = assetPtr->GetId();
                DYNASTI_ASSERT(Find(assetGlobalId) == assetPtr, "Asset pointers do not match");
                assetNameMap_.erase(assetName);
                assetGlobalIdMap_.erase(assetGlobalId);
                return true;
            }
            else
            {
                return false;
            }
        }
        
        
        /// Unregisters all assets stored in this registry
        void UnregisterAllAssets()
        {
            assetNameMap_.clear();
            assetGlobalIdMap_.clear();
        }
        
        
        /// Returns true if an asset at the given path has already been imported; if not yet imported then false
        /// \param[in] assetFullPath - Full/absolute path to the asset file
        /// \return - True if already registered as imported; otherwise false
        bool IsImported(const std::string assetFullPath)
        {
            return (FindImported(assetFullPath) != nullptr);
        }
        
        
        /// \return - Number of registered assets
        int NumRegisteredAssets() const { return assetNameMap_.size(); }
        
        
    protected:
        
        /// Creates a new unique asset name
        /// TODO - Not guaranteed to be unique, so need to add some safety checks here
        std::string CreateNewAssetName(const std::string& origName, const int globalId)
        {
            return origName + "_" + std::to_string(globalId);
        }
        
        int globalIdCounter_ = 0;                                                    ///< Temp global id counter
        std::unordered_map<std::string,std::shared_ptr<AssetType>> assetNameMap_;    ///< Map of name to asset
        std::unordered_map<std::string,std::shared_ptr<AssetType>> assetPathMap_;    ///< Map of full path to asset
        std::unordered_map<GlobalId,std::shared_ptr<AssetType>> assetGlobalIdMap_;   ///< Map of id to asset
        //std::unordered_map<std::string,int> nameCounterMap_;                         ///< Map of names to usages
        
    };
    
}
//---------------------------------------------------------------------------------------------------------------------
#endif
