#ifndef DYNASTI_ASSET_MANAGER_H
#define DYNASTI_ASSET_MANAGER_H


#include "Dynasti/Asset/AssetRegistry.h"
#include "Dynasti/Mesh/Mesh.h"
#include "Dynasti/Scene/Scene.h"
#include "Dynasti/Shader/GlslShader.h"
#include "Dynasti/Renderer/Material.h"
#include "Dynasti/Renderer/Texture.h"
#include "Dynasti/Renderer/VertexBuffer.h"


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{

    //=================================================================================================================
    /// \brief   ...
    /// \author  D. A. Hubber
    /// \date    24/08/2020
    //=================================================================================================================
    class AssetManager
    {
    public:
        
        inline AssetRegistry<Mesh>& GetMeshRegistry() { return meshes_; }
        inline AssetRegistry<GlslShader>& GetShaderRegistry() { return shaders_; }
        inline AssetRegistry<Material>& GetMaterialRegistry() { return materials_; }
        inline AssetRegistry<Scene>& GetSceneRegistry() { return scenes_; }
        inline AssetRegistry<Texture>& GetTextureRegistry() { return textures_; }
        inline AssetRegistry<VertexBuffer>& GetVertexBufferRegistry() { return vertexBuffers_; }
    
    
    protected:
        
        AssetRegistry<Mesh> meshes_;
        AssetRegistry<GlslShader> shaders_;
        AssetRegistry<Material> materials_;
        AssetRegistry<Scene> scenes_;
        AssetRegistry<Texture> textures_;
        AssetRegistry<VertexBuffer> vertexBuffers_;
        
    };
    
}
//---------------------------------------------------------------------------------------------------------------------
#endif
