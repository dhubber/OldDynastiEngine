#ifndef DYNASTI_ASSET_H
#define DYNASTI_ASSET_H


#include "Dynasti/Core/Constants.h"


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{
    
    //=================================================================================================================
    /// \brief   Base Asset class.
    /// \author  D. A. Hubber
    /// \date    14/08/2020
    //=================================================================================================================
    class Asset
    {
    public:
    
        Asset() : imported_(false), id_(-1), fullPath_(""), name_("") {};
        
        // Getters and setters
        inline const std::string& GetFullPath() const { return fullPath_; }
        inline GlobalId GetId() const { return id_; }
        inline const std::string& GetName() const { return name_; }
        inline void SetFullPath(const std::string fullPath)
        {
            fullPath_ = fullPath;
            imported_ = true;
        }
        inline void SetId(const GlobalId id) { id_ = id; }
        inline void SetName(const std::string name) { name_ = name; }
        

    protected:
    
        bool imported_;                                    ///< True if asset imported from a file; otherwise false
        GlobalId id_;                                      ///< Unique global id
        std::string fullPath_;                             ///< Absolute path of filename (when asset is imported)
        std::string name_;                                 ///< Unique human-readable name
    
    };
    
}
//---------------------------------------------------------------------------------------------------------------------
#endif
