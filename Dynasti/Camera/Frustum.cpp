#include <glm/glm.hpp>
#include "Dynasti/Camera/Frustum.h"


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{
    
    void Frustum::SetOrthographicFrustum(const glm::vec3 &pos, const glm::vec3 &look, const glm::vec3 &up,
        const glm::vec3 &right, const float zNear, const float zFar, const float width, const float height)
    {
        const float halfHeight = 0.5f * height;            // Half-height of frustum
        const float halfWidth = 0.5f * width;              // Half-width of frustum
        rNear = pos + look * zNear;
        rFar = pos + look * zFar;
        
        // Set the positions of the corners of the far culling plane
        farPoints[0] = rFar + halfHeight * up - halfWidth * right;
        farPoints[1] = rFar - halfHeight * up - halfWidth * right;
        farPoints[2] = rFar - halfHeight * up + halfWidth * right;
        farPoints[3] = rFar + halfHeight * up + halfWidth * right;
        
        // Set the position of the corners of the near culling plane
        nearPoints[0] = rNear + halfHeight * up - halfWidth * right;
        nearPoints[1] = rNear - halfHeight * up - halfWidth * right;
        nearPoints[2] = rNear - halfHeight * up + halfWidth * right;
        nearPoints[3] = rNear + halfHeight * up + halfWidth * right;
        
        CalculatePlanes();
    }
    
    
    void Frustum::SetPerspectiveFrustum(const glm::vec3 &pos, const glm::vec3 &look, const glm::vec3 &up,
        const glm::vec3 &right, const float zNear, const float zFar, const float fovDegrees, const float aspectRatio)
    {
        const float fovRadians = glm::radians(0.5f*fovDegrees);    // (half) fov angle in radians
        const float halfHnear = tan(fovRadians) * zNear;           // Half-height of near culling plane
        const float halfWnear = halfHnear*aspectRatio;             // Half-width of near culling plane
        const float halfHfar  = tan(fovRadians) * zFar;            // Half-height of far culling plane
        const float halfWfar  = halfHfar * aspectRatio;            // Half-width of far culling plane
        rNear = pos + look * zNear;
        rFar = pos + look * zFar;
        
        // Set the positions of the corners of the far culling plane
        farPoints[0] = rFar + up*halfHfar - right*halfWfar;
        farPoints[1] = rFar - up*halfHfar - right*halfWfar;
        farPoints[2] = rFar - up*halfHfar + right*halfWfar;
        farPoints[3] = rFar + up*halfHfar + right*halfWfar;
        
        // Set the position of the corners of the near culling plane
        nearPoints[0] = rNear + up*halfHnear - right*halfWnear;
        nearPoints[1] = rNear - up*halfHnear - right*halfWnear;
        nearPoints[2] = rNear - up*halfHnear + right*halfWnear;
        nearPoints[3] = rNear + up*halfHnear + right*halfWnear;
        
        CalculatePlanes();
    }
    
    
    void Frustum::CalculatePlanes()
    {
        planes[0].CalculatePlaneFromPoints(nearPoints[3], nearPoints[0], farPoints[0]);
        planes[1].CalculatePlaneFromPoints(nearPoints[1], nearPoints[2], farPoints[2]);
        planes[2].CalculatePlaneFromPoints(nearPoints[0], nearPoints[1], farPoints[1]);
        planes[3].CalculatePlaneFromPoints(nearPoints[2], nearPoints[3], farPoints[2]);
        planes[4].CalculatePlaneFromPoints(nearPoints[0], nearPoints[3], nearPoints[2]);
        planes[5].CalculatePlaneFromPoints(farPoints[3],  farPoints[0],  farPoints[1]);
    }
    
    
    bool Frustum::ContainsPoint(const glm::vec3& r) const
    {
        for (int i = 0; i < 6; ++i)
        {
            if (planes[i].DistanceFromPlane(r) < 0.0f) return false;
        }
        return true;
    }
    
    
    bool Frustum::ContainsSphere(const glm::vec3& rCentre, const float radius) const
    {
        for (int i = 0; i < 6; ++i)
        {
            if (planes[i].DistanceFromPlane(rCentre) < -radius) return false;
        }
        return true;
    }
    
}
//---------------------------------------------------------------------------------------------------------------------

