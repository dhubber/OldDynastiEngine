#ifndef DYNASTI_FRUSTUM_H
#define DYNASTI_FRUSTUM_H


#define GLM_ENABLE_EXPERIMENTAL
#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>
#include <array>
#include <iostream>
#include "Dynasti/Camera/Plane.h"


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{
    
    //=================================================================================================================
    /// \brief   Camera view Frustum class
    /// \author  D. A. Hubber
    /// \date    23/07/2020
    //=================================================================================================================
    class Frustum
    {
    public:
        
        /// Calculates the frustum planes for an orthographic camera with the given parameters
        /// \param[in] pos - Camera position
        /// \param[in] look - Camera viewing direction
        /// \param[in] up - Camera up direction
        /// \param[in] right -  Camera right direction
        /// \param[in] zNear - Distance of near clipping plane
        /// \param[in] zFar - Distance of far clipping plane
        /// \param[in] width - Frustum width
        /// \param[in] height - Frustum height
        void SetOrthographicFrustum(const glm::vec3 &pos, const glm::vec3 &look, const glm::vec3 &up,
            const glm::vec3 &right, const float zNear, const float zFar, const float width, const float height);
    
        /// Calculates the frustum planes for a perspective camera.
        /// \param[in] pos - Camera position
        /// \param[in] look - Camera viewing direction
        /// \param[in] up - Camera up direction
        /// \param[in] right -  Camera right direction
        /// \param[in] zNear - Distance of near clipping plane
        /// \param[in] zFar - Distance of far cipping plane
        /// \param[in] fovDegrees - Field-of.view angle of the frustum in degrees
        /// \param[in] aspectRatio - Aspect ratio (i.e. ratio of height to width) of the viewing port
        void SetPerspectiveFrustum(const glm::vec3 &pos, const glm::vec3 &look, const glm::vec3 &up,
            const glm::vec3 &right, const float zNear, const float zFar, const float fovDegrees,
            const float aspectRatio);
    
        /// Calculates exactly if a given point is within the view Frustum by comparing the orientation
        /// of the point to each of the Frustum planes. Returns true if inside the Frustum; else false.
        /// \param[in] r - Position of point
        /// \return - True if point is contained within frustum volume; otherwise false
        bool ContainsPoint(const glm::vec3& r) const;
        
        /// Calculates if a given sphere is within the view Frustum by checking that the distance of the sphere
        /// centre is greater than its radius away from each of the Frustum planes.
        /// \param[in] rCentre - Position of the centre of the sphere
        /// \param[in] radius - Radius of the sphere
        /// \return - True if any part of the sphere is inside the frustum; otherwise false
        bool ContainsSphere(const glm::vec3& rCentre, float radius) const;
        
        // Getters
        const glm::vec3& GetNearPlanePos() const { return rNear; }
        const glm::vec3& GetFarPlanePos() const { return rFar; }
        
      
    private:
        
        glm::vec3 rNear;                                   ///< ..
        glm::vec3 rFar;                                    ///< ..
        std::array<glm::vec3,4> farPoints;                 ///< Points defining far clipping plane
        std::array<glm::vec3,4> nearPoints;                ///< Points defining near clipping plane
        std::array<Plane,6> planes;                        ///< Frustum planes
        
        /// Calculate the six culling planes that make up the view Frustum volume
        /// If using the RHS rule, all planes point INWARDS towards centre of Frustum volume
        void CalculatePlanes();
        
    };

}
//---------------------------------------------------------------------------------------------------------------------
#endif
