#ifndef DYNASTI_CAMERA_H
#define DYNASTI_CAMERA_H


#define GLM_ENABLE_EXPERIMENTAL
#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtx/rotate_vector.hpp>
#include "Dynasti/Camera/Plane.h"
#include "Dynasti/Camera/Frustum.h"
#include "Dynasti/Core/Constants.h"
#include "Dynasti/Core/Debug.h"


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{
    
    //=================================================================================================================
    /// \brief   Main camera class.
    /// \author  D. A. Hubber
    /// \date    18/08/2014
    //=================================================================================================================
    class Camera
    {
    public:
        
        /// Default constructor
        Camera()
        {
            DYNASTI_LOG_VERBOSE("Constructing camera object");
            pos_ = glm::vec3(0.0f, 0.0f, 0.0f);
            qRot_ = glm::quat(1.0f, 0.0f, 0.0f, 0.0f);
        }
        
        /// Resets all camera variables to their default values
        void Reset();
        
        /// Sets the camera to use an orthographic projection (e.g. for use in the editor or 2d games)
        /// \param[in] pos - Position of camera in world space
        /// \param[in] qRot - Quaternion of camera rotation in world space
        /// \param[in] width - Width of window/camera viewport in pixels
        /// \param[in] height - Height of window/camera viewport in pixels
        /// \param[in] xSize - Physical width of camera viewport
        /// \param[in] zFar - Distance of far-clipping plane from camera position
        void SetOrthographicCamera(const glm::vec3 pos, const glm::quat qRot,
            const int width, const int height, const float xSize, const float zFar);
        
        /// Sets the camera to use an orthographic projection (e.g. for use in the editor or 2d games)
        /// \param[in] pos - Position of camera in world space
        /// \param[in] qRot - Quaternion of camera rotation in world space
        /// \param[in] width - Width of camera viewport in pixels
        /// \param[in] height - Height of camera viewport in pixels
        /// \param[in] fovDegrees - Field-of-view of camera in degrees
        /// \param[in] zNear - Distance of near-clipping plane from camera position
        /// \param[in] zFar - Distance of far-clipping plane from camera position
        void SetPerspectiveCamera(const glm::vec3 pos, const glm::quat qRot, const int width, const int height,
            const float fovDegrees, const float zNear, const float zFar);
        
        /// Sets the position and rotation of the camera
        /// \param[in] pos - Position of camera in world space
        /// \param[in] qRot - Quaternion of camera rotation in world space
        void SetPositionAndRotation(const glm::vec3 pos, const glm::quat qRot);

        /// Updates the perspective and view matrices, as well as the various directional vectors (e.g. up, right).
        void UpdateMatrices();
        
        /// Updates the window size if changed during rendering (e.g. stretched by cursor)
        /// \param[in] width - New width of window in pixels
        /// \param[in] height - New height of window in pixels
        void UpdateWindowSize(const int width, const int height);
        
        // Getters
        inline CameraMode GetCameraMode() const { return cameraMode_; }
        inline int GetWidth() const { return width_; }
        inline int GetHeight() const { return height_; }
        inline float GetXSize() const { return xSize_; }
        inline float GetYSize() const { return ySize_; }
        inline float GetFovDegrees() const { return fovDegrees_; }
        inline float GetAspectRatio() const { return aspectRatio_; }
        inline float GetZNear() const { return zNear_; }
        inline float GetZFar() const { return zFar_; }
        inline const glm::mat4& GetHudProjection() const { return PHud_; }
        inline const glm::vec3& GetLook() const { return look_; }
        inline const glm::vec3& GetPosition() const { return pos_; }
        inline const glm::mat4& GetProjectionMatrix() const { return P_; }
        inline const glm::quat& GetQRotation() const { return qRot_; }
        inline const glm::vec3& GetRight() const { return right_; }
        inline const glm::vec3& GetUp() const { return up_; }
        inline const glm::mat4& GetViewMatrix() const { return V_; }
        inline const Frustum& GetFrustum() const { return frustum_; }
        
        
    private:
    
        CameraMode cameraMode_ = CameraMode::CAMERA_NULL;  ///< Current camera mode
        int width_ = -1;                                   ///< Camera viewport window width (in pixels)
        int height_ = -1;                                  ///< Camera viewport window height (in pixels)
        float xSize_ = 0.0f;                               ///< Window width at near clipping plane
        float ySize_ = 0.0f;                               ///< Window height at near clipping plane
        float fovDegrees_ = 0.0f;                          ///< Field of view (in degrees)
        float aspectRatio_ = 0.0f;                         ///< Aspect ratio
        float zNear_ = 0.0f;                               ///< Near clipping plane distance
        float zFar_ = 0.0f;                                ///< Far clipping plane distance
        glm::vec3 look_;                                   ///< Look direction
        glm::vec3 up_;                                     ///< 'Up' direction vector
        glm::vec3 right_;                                  ///< 'Right' direction vector
        glm::vec3 pos_;                                    ///< Position vector
        glm::vec3 target_;                                 ///< Camera 'target' position
        glm::quat qRot_;                                   ///< Quaternion representation of rotation
        glm::mat4 V_;                                      ///< View matrix
        glm::mat4 P_;                                      ///< Projection matrix
        glm::mat4 PHud_;                                   ///< HUD projection matrix
        glm::mat4 R_;                                      ///< Camera Euler angle rotation matrix
        Frustum frustum_;                                  ///< Main camera view Frustum
        
    };
    
}
//---------------------------------------------------------------------------------------------------------------------
#endif
