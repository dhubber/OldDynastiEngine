#include <iostream>
#include "Dynasti/Camera/Camera.h"


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{
    
    void Camera::Reset()
    {
        cameraMode_ = CameraMode::CAMERA_NULL;
        width_ = -1;
        height_ = -1;
        xSize_ = 0.0f;
        ySize_ = 0.0f;
        fovDegrees_ = 0.0f;
        aspectRatio_ = 0.0f;
        zNear_ = 0.0f;
        zFar_ = 0.0f;
    }
    
    
    void Camera::SetOrthographicCamera(const glm::vec3 pos, const glm::quat qRot,
        const int width, const int height, const float xSize, const float zFar)
    {
        DYNASTI_LOG_VERBOSE_IF(cameraMode_ != CameraMode::CAMERA_ORTHOGRAPHIC, "Setting camera to orthogonal mode");
        cameraMode_  = CameraMode::CAMERA_ORTHOGRAPHIC;
        pos_         = pos;
        qRot_        = qRot;
        width_       = width;
        height_      = height;
        aspectRatio_ = float(width) / float(height);
        xSize_       = xSize;
        ySize_       = xSize / aspectRatio_;
        zNear_       = 0.0f;
        zFar_        = zFar;
        UpdateMatrices();
    }
    
    
    void Camera::SetPerspectiveCamera(const glm::vec3 pos, const glm::quat qRot, const int width, const int height,
        const float fovDegrees, const float zNear, const float zFar)
    {
        DYNASTI_LOG_VERBOSE_IF(cameraMode_ != CameraMode::CAMERA_PERSPECTIVE, "Setting camera to perspective mode");
        cameraMode_  = CameraMode::CAMERA_PERSPECTIVE;
        pos_         = pos;
        qRot_        = qRot;
        width_       = width;
        height_      = height;
        aspectRatio_ = float(width) / float(height);
        fovDegrees_  = fovDegrees;
        zNear_       = zNear;
        zFar_        = zFar;
        UpdateMatrices();
    }
    
    
    void Camera::SetPositionAndRotation(const glm::vec3 pos, const glm::quat qRot)
    {
        pos_  = pos;
        qRot_ = qRot;
        UpdateMatrices();
    }
    
    
    void Camera::UpdateMatrices()
    {
        // Update all other matrices and vectors
        R_      = glm::mat4_cast(qRot_);
        look_   = glm::vec3(R_*glm::vec4(0.0f, 0.0f, -1.0f, 0.0f));
        up_     = glm::vec3(R_*glm::vec4(0.0f, 1.0f, 0.0f, 0.0f));
        right_  = glm::cross(look_, up_);
        target_ = pos_ + look_;
        V_      = glm::lookAt(pos_, target_, up_);
        PHud_   = glm::ortho(0.0f, float(width_), 0.0f, float(height_));
    
        // Update camera mode depending on whether we are using 2d orthogonal, or 3d perspective mode
        if (cameraMode_ == CameraMode::CAMERA_ORTHOGRAPHIC)
        {
            frustum_.SetOrthographicFrustum(pos_, look_, up_, right_, zNear_, zFar_, xSize_, ySize_);
            P_ = glm::ortho(-0.5f * xSize_, 0.5f * xSize_, -0.5f * ySize_, 0.5f * ySize_, zNear_, zFar_);
        }
        else if (cameraMode_ == CameraMode::CAMERA_PERSPECTIVE)
        {
            frustum_.SetPerspectiveFrustum(pos_, look_, up_, right_, zNear_, zFar_, fovDegrees_, aspectRatio_);
            P_ = glm::perspective(fovDegrees_, aspectRatio_, zNear_, zFar_);
        }
    }
    
    
    void Camera::UpdateWindowSize(const int width, const int height)
    {
        width_       = width;
        height_      = height;
        aspectRatio_ = float(width) / float(height);
        ySize_       = xSize_ / aspectRatio_;
        UpdateMatrices();
    }
    
}
//---------------------------------------------------------------------------------------------------------------------
