#ifndef DYNASTI_PLANE_H
#define DYNASTI_PLANE_H


#include <glm/glm.hpp>
#include "Dynasti/Core/Constants.h"


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{
    
    //=================================================================================================================
    /// \brief   Inlined class representing a plane.  Used to construct the view frustrum.
    /// \author  D. A. Hubber
    /// \date    15/11/2014
    //=================================================================================================================
    class Plane
    {
    public:
        
        /// Default constructor; initialises all variables
        Plane()
        {
            normal_ = glm::vec3(0.0f, 1.0f, 0.0f);
            planeDist_ = 0.0f;
        }
    
        /// Constructs plane from the normal vector and a single position on the plane
        /// \param[in] normal - Normal vector from plane surface
        /// \param[in] r - Any given point on the plane
        Plane(const glm::vec3 &r, const glm::vec3 &normal)
        {
            normal_ = normal;
            planeDist_ = glm::dot(normal, r);
        }
    
        /// Constructs plane from 3 distinct positions on the plane
        /// \param[in] r1 - 1st point on plane
        /// \param[in] r2 - 2nd point on plane
        /// \param[in] r3 - 3rd point on plane
        Plane(const glm::vec3 &r1, const glm::vec3 &r2, const glm::vec3 &r3)
        {
            CalculatePlaneFromPoints(r1, r2, r3);
        }
        
        /// \param[in] r - Test point position
        /// \return - Distance of point from the plane.  Sign depends on the orientation from the plane
        inline float DistanceFromPlane(const glm::vec3 &r) const
        {
            return glm::dot(normal_, r) - planeDist_;
        }
        
        /// \param[in] r - Test point position
        /// \return - Orientation of point relative to the plane
        inline PlaneOrientation ComputeOrientation(const glm::vec3 &r) const
        {
            const float dist = DistanceFromPlane(r);
            if (dist > tolerance_) return PlaneOrientation::PLANE_FRONT;
            else if (dist < -tolerance_) return PlaneOrientation::PLANE_BACK;
            else return PlaneOrientation::PLANE_COPLANAR;
        }
        
        /// Calculates all plane properties from three unique and non-colinear points, r1, r2 and r3
        /// \param[in] r1 - 1st point on plane
        /// \param[in] r2 - 2nd point on plane
        /// \param[in] r3 - 3rd point on plane
        inline void CalculatePlaneFromPoints(const glm::vec3 &r1, const glm::vec3 &r2, const glm::vec3 &r3)
        {
            const glm::vec3 dr1 = r2 - r1;
            const glm::vec3 dr2 = r3 - r1;
            normal_ = glm::normalize(glm::cross(dr1, dr2));
            planeDist_ = glm::dot(normal_, r1);
        }
        
        const glm::vec3& GetNormal() const { return normal_; }


    private:
    
        static constexpr float tolerance_ = 0.0001f;       ///< Tolerance factor [ToDo : Think of better solution]
        glm::vec3 normal_;                                 ///< Plane normal
        float planeDist_;                                  ///< Distance of plane from origin
        
    };
    
}
//---------------------------------------------------------------------------------------------------------------------
#endif
