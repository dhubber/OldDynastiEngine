#ifndef DYNASTI_CAMERA_COMPONENT_H
#define DYNASTI_CAMERA_COMPONENT_H


#define GLM_FORCE_RADIANS
#include <assert.h>
#include <map>
#include <string>
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include "glad/glad.h"
#include "Dynasti/Camera/Camera.h"
#include "Dynasti/Core/Constants.h"
#include "Dynasti/Ecs/Component.h"


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{

    //=================================================================================================================
    /// \brief   Component class containing all data for rendering a mesh to the viewport.
    /// \author  D. A. Hubber
    /// \date    13/02/2020
    //=================================================================================================================
    class CameraComponent : public Component
    {
    public:

        CameraComponent(const int entityId=-1) : Component(entityId) {};
        virtual ~CameraComponent() {};
    
        /// Returns a simple human-readable name of the component type (implemented by child classes)
        /// @return - Human-readable name of the component
        virtual const std::string GetComponentName() override { return "CameraComponent"; }
    
        /// Initialises component in the object pool when entity is spawned (implemented by child classes)
        virtual void OnCreate() override { camera_.Reset(); };
        
        /// Destroys the component in the object pool when entity is destroyed (implemented by child classes)
        virtual void OnDestroy() override {};
        
        /// \return - Reference to the camera object
        Camera& GetCamera() { return camera_; }
        

    protected:

        Camera camera_;                                    ///< Camera object

    };

}
//---------------------------------------------------------------------------------------------------------------------
#endif
