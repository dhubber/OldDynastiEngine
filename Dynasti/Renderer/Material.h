#ifndef DYNASTI_MATERIAL_H
#define DYNASTI_MATERIAL_H


#include <glm/glm.hpp>
#include <unordered_map>
#include <string>
#include <vector>
#include "glad/glad.h"
#include "Dynasti/Asset/Asset.h"
#include "Dynasti/Core/Constants.h"
#include "Dynasti/Core/Debug.h"


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{
    
    //=================================================================================================================
    /// \brief   Basic class for storing material data.
    /// \author  D. A. Hubber
    /// \date    05/08/2017
    //=================================================================================================================
    class Material : public Asset
    {
    public:

        static std::shared_ptr<Material> MaterialFactory()
        {
            return std::make_shared<Material>();
        }
        
        Material() : Asset() {};

        inline glm::vec3 GetColor3() const { return glm::vec3(baseColor.r, baseColor.g, baseColor.b); }
        inline glm::vec4 GetBaseColor() const { return baseColor; }
        
        inline GlobalId GetTextureId(const TextureType textureType) const
        {
            auto it = textureIdMap_.find(textureType);
            if (it != textureIdMap_.end())
            {
                return it->second;
            }
            else
            {
                return -1;
            }
        }

        inline void SetBaseColor(const glm::vec3& color) { baseColor = glm::vec4(color, 1.0f); }
        inline void SetBaseColor(const glm::vec4& color) { baseColor = color; }
        
        inline void SetTextureId(const TextureType textureType, const GlobalId textureId)
        {
            textureIdMap_[textureType] = textureId;
        }
        
        
    protected:

        glm::vec4 baseColor = glm::vec4(white, 1.0f);            ///< ..
        std::unordered_map<TextureType, GlobalId> textureIdMap_;     ///< ..
        
    };
    
}
//---------------------------------------------------------------------------------------------------------------------
#endif
