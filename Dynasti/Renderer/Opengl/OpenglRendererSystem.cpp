#include "Dynasti/Asset/AssetManager.h"
#include "Dynasti/Camera/Camera.h"
#include "Dynasti/Core/FrameTimer.h"
#include "Dynasti/Event/EventManager.h"
#include "Dynasti/Input/InputManager.h"
#include "Dynasti/Mesh/Mesh.h"
//#include "Dynasti/Mesh/Mesh3d.h"
#include "Dynasti/Renderer/Material.h"
#include "Dynasti/Renderer/RenderableComponent.h"
#include "Dynasti/Renderer/VertexBuffer.h"
#include "OpenglFramebuffer.h"
#include "OpenglRendererSystem.h"
#include "OpenglTexture.h"
#include "OpenglVertexBuffer.h"
#include "Dynasti/Scene/Scene.h"
#include "Dynasti/Scene/SceneManager.h"
#include "Dynasti/Shader/GlslShader.h"
#include "Dynasti/Window/Window.h"


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{
    
    void OpenglRendererSystem::Setup()
    {
        if (activeWindow_)
        {
            screenFramebuffer_ = std::make_unique<OpenglFramebuffer>();
            screenFramebuffer_->Create(activeWindow_->GetWidth(), activeWindow_->GetHeight());
            
            postProcessShader_ = std::make_shared<GlslShader>();
            postProcessShader_->LoadFromFile(GL_VERTEX_SHADER, shaderPath + "PostProcess.vert");
            postProcessShader_->LoadFromFile(GL_FRAGMENT_SHADER, shaderPath + "PostProcess.frag");
            postProcessShader_->CreateAndLinkProgram();
            postProcessShader_->Use();
            postProcessShader_->AddUniform("screenTexture");
            postProcessShader_->UnUse();

            AssetRegistry<GlslShader>& shaders = assetManager_->GetShaderRegistry();
            shaders.Register(postProcessShader_, "pp");
        }
    
        std::shared_ptr<InputMap> inputMap = inputManager_->GetActiveInputMap();
        eventListener_->SubscribeToEvent("TogglePPKeyDown", [&](const Event event){postProcessActive_ = !postProcessActive_;});
    }
    
    
    void OpenglRendererSystem::Shutdown()
    {
        AssetRegistry<GlslShader> &shaders = assetManager_->GetShaderRegistry();
        shaders.UnregisterAllAssets();
    }
    
    
    void OpenglRendererSystem::Update()
    {
        DYNASTI_TIMER(RENDERER_SYSTEM);
        if (postProcessActive_ && screenFramebuffer_)
        {
            
            
            
            screenFramebuffer_->Bind();
        }
        glClearColor(0.3f, 0.3f, 0.3f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        glEnable(GL_DEPTH_TEST);
    
        std::shared_ptr<Scene> scene = sceneManager_->GetSelectedScene();
    
        if (scene)
        {
            std::shared_ptr<Camera> camera = scene->GetActiveCamera();
            if (camera)
            {
                camera->SetPerspectiveCamera(glm::vec3(0.0f, 0.0f, 5.0f), glm::quat(0.0f, 0.0f, 0.0f, 0.0f), activeWindow_->GetWidth(),
                                              activeWindow_->GetHeight(), 20.0f, 0.01f, 1000.0f);
                RenderScene(scene, camera);
            }
        } else
        {
            DYNASTI_WARNING("Invalid scene or active window pointers in Renderer");
        }
    
        screenFramebuffer_->Unbind();
    
        if (postProcessActive_ && postProcessShader_ && screenFramebuffer_)
        {
            screenFramebuffer_->Render(postProcessShader_,
                                       glm::vec2(activeWindow_->GetWidth(), activeWindow_->GetHeight()));
            //screenFramebuffer_->Unbind();
        }
    }
    
    
    void OpenglRendererSystem::RenderScene(std::shared_ptr<Scene> scene, std::shared_ptr<Camera> camera)
    {
        AssetRegistry<Material> &materials = assetManager_->GetMaterialRegistry();
        AssetRegistry<Mesh> &meshes = assetManager_->GetMeshRegistry();
        AssetRegistry<GlslShader> &shaders = assetManager_->GetShaderRegistry();
        AssetRegistry<Texture> &textures = assetManager_->GetTextureRegistry();
        //AssetRegistry<VertexBuffer> &vertexBuffers = assetManager_->GetVertexBufferRegistry();
        std::shared_ptr<ObjectPool<RenderableComponent>> renderablePool = scene->FindComponentPool<RenderableComponent>();

        for (int i = 0; i < renderablePool->NumObjects(); ++i)
        {
            const RenderableComponent& rComp = (*renderablePool)[i];
            
            // Skip to next renderable if not visible
            if (!rComp.IsVisible())
            {
                continue;
            }

            const GlobalId shaderId = rComp.GetShaderId();
            const GlobalId meshId = rComp.GetMeshId();
            std::shared_ptr<GlslShader> activeShader = shaders.Find(shaderId);
            std::shared_ptr<Mesh> activeMesh = meshes.Find(meshId);
            
            if (activeShader && activeMesh)
            {
                activeShader->Use();
                activeShader->SetGlFlags();
                activeShader->SetGlobalUniforms(*camera, *scene);

                const GlobalId materialId = rComp.GetMaterialId() == -1 ? activeMesh->GetMaterialId() : rComp.GetMaterialId();

                std::shared_ptr<OpenglVertexBuffer> activeVertexBuffer =
                    std::dynamic_pointer_cast<OpenglVertexBuffer>(activeMesh->GetVertexBuffer());
                    //std::dynamic_pointer_cast<OpenglVertexBuffer>(vertexBuffers.Find(vertexBufferId));
                std::shared_ptr<Material> activeMaterial = materials.Find(materialId);
    
                if (activeVertexBuffer && activeMaterial)
                {
                    activeShader->SetLocalUniforms(rComp, *activeMaterial);
        
                    // Use the default mesh texture, unless an alternative is selected by the renderable component
                    const GlobalId textureId = activeMaterial->GetTextureId(TextureType::TEXTURE_DIFFUSE);
                    std::shared_ptr<OpenglTexture> activeTexture = std::dynamic_pointer_cast<OpenglTexture>(textures.Find(textureId));
                    if (activeTexture)
                    {
                        activeTexture->BindTextureToGpu(GL_TEXTURE0);
                    }
                    
                    // Render mesh
                    glBindVertexArray(activeVertexBuffer->GetVertexArrayObject());
                    glDrawElements(activeVertexBuffer->GetPrimitiveType(), activeVertexBuffer->GetNumIndices(),
                                   activeVertexBuffer->GetIndexType(), (GLvoid *) 0);
                    glBindVertexArray(0);
        
                    activeShader->UnUse();
                }
                DYNASTI_LOG_IF(activeShader == nullptr, "Did not find active shader : " + std::to_string(shaderId))
            }
        }
        
    }
    
}
//---------------------------------------------------------------------------------------------------------------------
