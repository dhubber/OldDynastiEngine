#include "Dynasti/Renderer/Opengl/OpenglVertexBuffer.h"


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{
    
    bool OpenglVertexBuffer::CopyDataToGpu(const VertexFormat& format, int numVertices, int numIndices,
                                           float* vertices, unsigned int *indices)
    {
        DYNASTI_LOG("Adding buffer data to GPU : " + std::to_string(numVertices) + "  " + std::to_string(numIndices));
        //DYNASTI_ASSERT(format.GetSize() > 0, "Cannot add zero-size buffer to GPU");
        DYNASTI_ASSERT(numVertices > 0, "Cannot add zero-vertex buffer to GPU");
        DYNASTI_ASSERT(numIndices > 0, "Cannot add zero-index buffer to GPU");
    
        // Generate vertex-array object
        if (vao_ == 0) glGenVertexArrays(1, &vao_);
        glBindVertexArray(vao_);
    
        const VertexFormatType formatType = format.GetFormatType();
        const int vertexSize = format.GetVertexSize();
        const int vertexSizeBytes = vertexSize * sizeof(float);
    
        // Create VBO for main vertex data
        if (vboVertices_ == 0) glGenBuffers(1, &vboVertices_);
        glBindBuffer(GL_ARRAY_BUFFER, vboVertices_);
        glBufferData(GL_ARRAY_BUFFER, vertexSizeBytes*numVertices, vertices, GL_STATIC_DRAW);
    
        //DYNASTI_LOG("Copying interleaved vertex data to GPU : " + std::to_string(vertexStride));
        for (int i = 0; i < format.NumAttributes(); ++i)
        {
            glEnableVertexAttribArray(i);
            const VertexAttributeType attributeType = format.GetAttribute(i);
            const int attributeSize = VertexFormat::AttributeSize(attributeType);
            const int attributeOffset = format.GetAttributeOffset(i);
        
            switch(formatType)
            {
                case VertexFormatType::INTERLEAVED:
                {
                    glVertexAttribPointer(i, attributeSize, GL_FLOAT, GL_FALSE, vertexSizeBytes,
                                          (const GLvoid *) (attributeOffset * sizeof(float)));
                    break;
                }
                case VertexFormatType::BLOCKED:
                {
                    glVertexAttribPointer(i, attributeSize, GL_FLOAT, GL_FALSE, attributeSize * sizeof(float),
                                          (const GLvoid *) (numVertices * attributeOffset * sizeof(float)));
                    break;
                }
            }
        }

        // Create buffer for indices
        if (vboIndices_ == 0) glGenBuffers(1, &vboIndices_);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vboIndices_);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, numIndices*sizeof(GLuint), (GLuint*) indices, GL_STATIC_DRAW);

        //glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
        //glBindBuffer(GL_ARRAY_BUFFER, 0);
        glBindVertexArray(0);
        allocatedOnGpu_ = true;
        numIndices_ = numIndices;
        return allocatedOnGpu_;
    }
    
    
    void OpenglVertexBuffer::DeleteDataFromGpu()
    {
        if (allocatedOnGpu_)
        {
            glDeleteBuffers(1, &vboIndices_);
            glDeleteVertexArrays(1, &vao_);
            glDeleteBuffers(1, &vboVertices_);
            allocatedOnGpu_ = false;
        }
    }

}
//---------------------------------------------------------------------------------------------------------------------