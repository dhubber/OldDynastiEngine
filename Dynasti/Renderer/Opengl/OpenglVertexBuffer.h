#ifndef DYNASTI_OPENGL_VERTEX_BUFFER_H
#define DYNASTI_OPENGL_VERTEX_BUFFER_H


#include "Dynasti/Core/Debug.h"
#include "Dynasti/Mesh/Mesh.h"
#include "Dynasti/Renderer/VertexBuffer.h"


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{
    
    //=================================================================================================================
    /// \brief   OpenGL vertex Buffer class
    /// \author  D. A. Hubber
    /// \date    16/01/2021
    //=================================================================================================================
    class OpenglVertexBuffer : public VertexBuffer
    {
    public:
        
        OpenglVertexBuffer() : VertexBuffer(), indexType_(GL_UNSIGNED_INT), primType_(GL_TRIANGLES), vao_(0), vboIndices_(0), vboVertices_(0) {};
    
        // Implementations of pure virtual functions
        virtual bool CopyDataToGpu(const VertexFormat& format, int numVertices, int numIndices,
                                   float* vertices, unsigned int *indices) override;
        virtual void DeleteDataFromGpu() override;
    
        // Getters
        inline GLenum GetIndexType() const { return indexType_; }
        inline GLenum GetPrimitiveType() const { return primType_; }
        inline GLuint GetVertexArrayObject() const { return vao_; }
    
    
    protected:
        
        GLenum indexType_;                                 ///< OpenGL index type (e.g. GL_SHORT, GL_UNSIGNED_INT)
        GLenum primType_;                                  ///< OpenGL primitive type for rendering
        GLuint vao_;                                       ///< GPU vertex attribute object pointer
        GLuint vboVertices_;                               ///< GPU vertex buffer object pointer
        GLuint vboIndices_;                                ///< GPU index buffer object pointer
    
    };
    
}
//---------------------------------------------------------------------------------------------------------------------
#endif
