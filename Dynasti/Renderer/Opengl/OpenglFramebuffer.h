#ifndef DYNASTI_OPENGL_FRAME_BUFFER_H
#define DYNASTI_OPENGL_FRAME_BUFFER_H


//#define GL_GLEXT_PROTOTYPES 1
#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>
#include "glad/glad.h"
#include "Dynasti/Core/Debug.h"
#include "Dynasti/Renderer/Framebuffer.h"
#include "Dynasti/Shader/GlslShader.h"


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{
    
    //=================================================================================================================
    /// \brief   Class for setting up and storing a frameebuffer object in OpenGL for the renderer.
    /// \author  D. A. Hubber
    /// \date    02/11/2017
    //=================================================================================================================
    class OpenglFramebuffer : public Framebuffer
    {
    public:
        
        OpenglFramebuffer() : fbo_(0), colorbuffer_(0), quadVao_(0), quadVbo_(0), rbo_(0)
        {
            DYNASTI_LOG_VERBOSE("Constructing Framebuffer object");
        }
        
        
        virtual ~OpenglFramebuffer()
        {
            DYNASTI_LOG_VERBOSE("Destroying Framebuffer object");
        };
        
        
        virtual void Create(const int width, const int height) override
        {
            width_ = width;
            height_ = height;
            DYNASTI_LOG_VERBOSE("Creating framebuffer with size : " + std::to_string(width) + "  " + std::to_string(height));
            DYNASTI_ASSERT(width > 0, "Framebuffer width not a positive integer : " + std::to_string(width));
            DYNASTI_ASSERT(height > 0, "Framebuffer height not a positive integer : " + std::to_string(height));

            glGenFramebuffers(1, &fbo_);
            glBindFramebuffer(GL_FRAMEBUFFER, fbo_);
            
            glGenTextures(1, &colorbuffer_);
            glActiveTexture(GL_TEXTURE0);
            glBindTexture(GL_TEXTURE_2D, colorbuffer_);
            //glTexStorage2D(GL_TEXTURE_2D, 1, GL_RGBA8, width, height);
            glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
            glBindTexture(GL_TEXTURE_2D, 0);
            glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, colorbuffer_, 0);
            
            glGenRenderbuffers(1, &rbo_);
            glBindRenderbuffer(GL_RENDERBUFFER, rbo_);
            glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, width, height);
            //glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8, width, height);
            glBindRenderbuffer(GL_RENDERBUFFER, 0);
            glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, rbo_);
            //glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_RENDERBUFFER, rbo_);
            
            if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
            {
                DYNASTI_FATAL("Error!  Incomplete framebuffer");
            }
            glBindFramebuffer(GL_FRAMEBUFFER, 0);
            
            float quadVertices[] = {
                // positions   // texCoords
                -1.0f,  1.0f, 0.0f, 1.0f,
                -1.0f, -1.0f, 0.0f, 0.0f,
                1.0f, -1.0f,  1.0f, 0.0f,
                
                -1.0f,  1.0f, 0.0f, 1.0f,
                1.0f, -1.0f,  1.0f, 0.0f,
                1.0f,  1.0f,  1.0f, 1.0f
            };
            
            // Generate vertex-array object
            glGenVertexArrays(1, &quadVao_);
            glBindVertexArray(quadVao_);
            glGenBuffers(1, &quadVbo_);
            glBindBuffer(GL_ARRAY_BUFFER, quadVbo_);
            glBufferData(GL_ARRAY_BUFFER, 4*6*sizeof(GLfloat), quadVertices, GL_STATIC_DRAW);
            glEnableVertexAttribArray(0);
            glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 4*sizeof(GLfloat), (const GLvoid*) 0);
            glEnableVertexAttribArray(1);
            glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 4*sizeof(GLfloat), (const GLvoid*) (2*sizeof(GLfloat)));
            glBindVertexArray(0);
        }
    
    
        virtual void Destroy() override
        {
            glDeleteBuffers(1, &quadVbo_);
            glDeleteVertexArrays(1, &quadVao_);
            glDeleteRenderbuffers(1, &rbo_);
            glDeleteTextures(1, &colorbuffer_);
            glDeleteFramebuffers(1, &fbo_);
        }
        
        
        /// Render the framebuffer to the screen using the selected post-processing shader
        /// \param frameBufferSize - Size of framebuffer rendering window
        /// \param shader - GLSL shader used to render the image (e.g. for post-processing)
        /// \param clearColor - Background/clear color used by OpenG
        virtual void Render(std::shared_ptr<GlslShader> shader, const glm::vec2 frameBufferSize,
                            const glm::vec4 clearColor = glm::vec4(1.0f, 1.0f, 1.0f, 1.0f)) override
        {
            DYNASTI_ASSERT(shader, "Invalid shader pointer passed for creating framebuffer");
            glDisable(GL_BLEND);
            glBindFramebuffer(GL_FRAMEBUFFER, 0);
            glClearColor(clearColor.r, clearColor.g, clearColor.b, clearColor.a);
            glClear(GL_COLOR_BUFFER_BIT);
            glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
            shader->Use();
            //shader->SetUniform("frameBufferSize", frameBufferSize);
            shader->SetUniform("screenTexture", 0);
            glBindVertexArray(quadVao_);
            glDisable(GL_DEPTH_TEST);
            //glActiveTexture(GL_TEXTURE0);
            glBindTexture(GL_TEXTURE_2D, colorbuffer_);
            glDrawArrays(GL_TRIANGLES, 0, 6);
            shader->UnUse();
        }
    
        virtual void Bind() override { glBindFramebuffer(GL_FRAMEBUFFER, fbo_); }
    
        virtual void Unbind() override { glBindFramebuffer(GL_FRAMEBUFFER, 0); }
        
        inline unsigned int GetFbo() { return fbo_; }
     
        
    private:
        
        unsigned int fbo_;                                 ///< Framebuffer object handle (internal to OpenGL)
        unsigned int colorbuffer_;                         ///< ..
        unsigned int rbo_;                                 ///< Render buffer object handle (internal to OpenGL)
        unsigned int quadVao_;                             ///< VAO for rectangle
        unsigned int quadVbo_;                             ///< VBO for rectangle
    
    };

}
//---------------------------------------------------------------------------------------------------------------------
#endif
