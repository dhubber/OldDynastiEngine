#ifndef DYNASTI_OPENGL_TEXTURE_H
#define DYNASTI_OPENGL_TEXTURE_H


#include <string>
#include <vector>
#include "glad/glad.h"
#include "Dynasti/Renderer/Texture.h"


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{
    
    //=================================================================================================================
    /// \brief   Basic class for storing texture data and allocating to GPU via OpenGL commands.
    /// \author  D. A. Hubber
    /// \date    05/08/2017
    //=================================================================================================================
    class OpenglTexture : public Texture
    {
    public:
        
        OpenglTexture();
        
        virtual ~OpenglTexture();
        
        // OpenGL implementations of pure virtual functions for Texture class
        bool LoadImageFromFile(const std::string filename) override;
        bool LoadImageToGpu() override;
        bool UnloadImageFromGpu() override;
        
        /// Bind the texture on the GPU to the given OpenGL texture unit
        /// \param[in] textureUnit - OpenGL texture unit to bind the texture to
        void BindTextureToGpu(GLenum textureUnit);
        
        // Getter function
        inline GLenum GetFormat() const {return format_;}
    
    
    protected:
        
        GLenum format_;                                    ///< Format of the pixel data
        GLuint gpuId_;                                     ///< OpenGL id/handle of texture
        GLenum target_;                                    ///< OpenGL texture type
        GLint internalFormat_;                             ///< No. of color components in texture image
        GLint filter_;                                     ///< Texture sampling filter
        GLint wrap_;                                       ///< Wrap mode for out-of-range texture coordinates
        GLubyte *imageData_;                               ///< Pointer to RGBA image data array
        
    };
    
}
//---------------------------------------------------------------------------------------------------------------------
#endif
