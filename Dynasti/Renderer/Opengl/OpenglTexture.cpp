#include <memory>
#include "Dynasti/Renderer/Opengl/OpenglTexture.h"

//#define STB_IMAGE_IMPLEMENTATION
//#include "Dependencies/stb/stb_image.h"
#include "Dynasti/Dependencies/tinygltf/stb_image.h"


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{
    
    OpenglTexture::OpenglTexture(): Texture(), format_(GL_RGBA), gpuId_(0), target_(GL_TEXTURE_2D),
        internalFormat_(GL_RGBA), filter_(GL_LINEAR), wrap_(GL_CLAMP_TO_EDGE), imageData_(nullptr)
    {
        DYNASTI_LOG_VERBOSE("Constructing Texture object");
    }
    
    
    OpenglTexture::~OpenglTexture()
    {
        if (imageData_)
        {
            UnloadImageFromGpu();
            stbi_image_free(imageData_);
            imageData_ = nullptr;
        }
    }
    
    
    bool OpenglTexture::LoadImageFromFile(const std::string filename)
    {
        // If an image has previously been loaded, then free memory before loading new image
        if (imageData_)
        {
            UnloadImageFromGpu();
            stbi_image_free(imageData_);
            imageData_ = nullptr;
        }
        
        stbi_set_flip_vertically_on_load(true);
        if (imageData_ = stbi_load(filename.c_str(), &width_, &height_, &bytesPerPixel_, STBI_rgb_alpha))
        {
            internalFormat_ = GL_RGBA;
            format_ = GL_RGBA;
            return true;
        }
        else
        {
            DYNASTI_WARNING("Error!  Could not load image file : " + filename);
            return false;
        }
    }
    
    
    bool OpenglTexture::LoadImageToGpu() //(GLenum gpuLocation)
    {
        glGenTextures(1, &gpuId_);
        if (gpuId_ == 0)
        {
            DYNASTI_WARNING("Could not load image to GPU.  Invalid OpenGL texture id");
            return false;
        }
        
        glBindTexture(target_, gpuId_);
        //glPixelStorei(GL_UNPACK_ALIGNMENT, packBits);
        glTexParameteri(target_, GL_TEXTURE_WRAP_S, wrap_);
        glTexParameteri(target_, GL_TEXTURE_WRAP_T, wrap_);
        glTexParameteri(target_, GL_TEXTURE_MIN_FILTER, filter_);
        glTexParameteri(target_, GL_TEXTURE_MAG_FILTER, filter_);
        glTexImage2D(target_, 0, internalFormat_, width_, height_, 0, format_, GL_UNSIGNED_BYTE, imageData_);
        glGenerateMipmap(target_);
        glBindTexture(target_, 0);
        return true;
    }
    

    bool OpenglTexture::UnloadImageFromGpu()
    {
        if (gpuId_ != 0)
        {
            glDeleteTextures(1, &gpuId_);
            gpuId_ = 0;
            return true;
        }
        return false;
    }
    
    
    void OpenglTexture::BindTextureToGpu(GLenum textureUnit)
    {
        glActiveTexture(textureUnit);
        glBindTexture(target_, gpuId_);
    }
    
}
//---------------------------------------------------------------------------------------------------------------------
