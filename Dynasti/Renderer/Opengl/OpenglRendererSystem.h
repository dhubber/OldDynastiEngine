#ifndef DYNASTI_OPENGL_RENDERER_SYSTEM_H
#define DYNASTI_OPENGL_RENDERER_SYSTEM_H


#include <memory>
#include "Dynasti/Core/System.h"
#include "Dynasti/Renderer/RendererSystem.h"
#include "Dynasti/Transform/Transform3d.h"


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{
    
    class AssetManager;
    class EventManager;
    class GlslShader;
    class InputManager;
    class SceneManager;
    class OpenglTexture;
    class Window;
    
    
    //=================================================================================================================
    /// \brief   OpenGL renderer system class.
    /// \author  D. A. Hubber
    /// \date    23/02/2020
    //=================================================================================================================
    class OpenglRendererSystem : public RendererSystem
    {
    public:
    
        OpenglRendererSystem(std::shared_ptr<AssetManager> assetManager, std::shared_ptr<EventManager> eventManager,
                             std::shared_ptr<FrameTimer> frameTimer, std::shared_ptr<SceneManager> sceneManager,
                             std::shared_ptr<InputManager> inputManager) :
            RendererSystem("OpenglRendererSystem", assetManager, eventManager, frameTimer, sceneManager, inputManager) {};
    
        // Implementations of pure virtual functions
        virtual void Setup() override;
        virtual void Shutdown() override;
        virtual void Update() override;

        
    protected:
        
        bool postProcessActive_ = true;                    ///< Flag if using post-process shader
        std::shared_ptr<GlslShader> postProcessShader_;    ///< Pointer to post-process shader
        
        virtual void RenderScene(std::shared_ptr<Scene> scene, std::shared_ptr<Camera> camera) override;
        
    };

}
//---------------------------------------------------------------------------------------------------------------------
#endif
