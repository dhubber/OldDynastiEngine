#include <memory>
#include "Dynasti/Dependencies/stb/stb_image.h"
#include "Dynasti/Core/Debug.h"
#include "Dynasti/Renderer/Texture.h"
#ifdef DYNASTI_USE_OPENGL
#include "Dynasti/Renderer/Opengl/OpenglTexture.h"
#endif

//#define STB_IMAGE_IMPLEMENTATION
//#include "Dependencies/stb/stb_image.h"

//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{
    
    std::shared_ptr<Texture> Texture::TextureFactory()
    {
#ifdef DYNASTI_USE_OPENGL
        return std::make_shared<OpenglTexture>();
#else
        DYNASTI_WARNING("Cannot create a texture object.  No valid option chosen");
        return nullptr;
#endif
    }
    
}
//---------------------------------------------------------------------------------------------------------------------