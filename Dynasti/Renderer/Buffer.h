#ifndef DYNASTI_BUFFER_H
#define DYNASTI_BUFFER_H


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{
    
    //=================================================================================================================
    /// \brief   Virtual base class for representing GPU buffers.
    /// \author  D. A. Hubber
    /// \date    16/01/2021
    //=================================================================================================================
    class Buffer
    {
    public:

        Buffer() = default;
        virtual ~Buffer() = default;
    
        /// Delete data/deallocate memory from GPU
        virtual void DeleteDataFromGpu() = 0;
 
        
    protected:
    
        bool allocatedOnGpu_ = false;                      ///< Flag if buffer is currently allocated on the GPU
        
    };
    
}
//---------------------------------------------------------------------------------------------------------------------
#endif
