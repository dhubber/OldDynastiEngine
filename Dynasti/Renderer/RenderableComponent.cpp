#include "Dynasti/Core/Debug.h"
#include "Dynasti/Renderer/RenderableComponent.h"


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{
    
    RenderableComponent::RenderableComponent(const int entityId) : Component(entityId)
    {
        OnCreate();
    }
    
    
    RenderableComponent::~RenderableComponent()
    {
        OnDestroy();
    }

    
    void RenderableComponent::OnCreate()
    {
        visible_     = true;
        meshId_      = -1;
        shaderId_    = -1;
        textureId_   = -1;
    }
    
    
    void RenderableComponent::OnDestroy()
    {
    }
    
}
//---------------------------------------------------------------------------------------------------------------------