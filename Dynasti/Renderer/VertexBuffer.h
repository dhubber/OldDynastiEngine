#ifndef DYNASTI_VERTEX_BUFFER_H
#define DYNASTI_VERTEX_BUFFER_H


#include <initializer_list>
#include <vector>
#include "glad/glad.h"
#include "Dynasti/Asset/Asset.h"
#include "Dynasti/Core/Debug.h"
#include "Dynasti/Core/Constants.h"
#include "Dynasti/Mesh/Vertex.h"
#include "Dynasti/Renderer/Buffer.h"


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{
 
    class Mesh;
    
    
    //=================================================================================================================
    /// \brief   Virtual base class for all GPU vertex buffer implementations.
    /// \author  D. A. Hubber
    /// \date    16/01/2021
    //=================================================================================================================
    class VertexBuffer : public Buffer
    {
    public:
    
        /// Factory function for creating new vertex buffer objects
        static std::shared_ptr<VertexBuffer> VertexBufferFactory();
        
        /// Default constructor for VertexBuffer
        VertexBuffer() : Buffer(), numIndices_(0)
        {
            DYNASTI_LOG_VERBOSE("Constructed Vertex buffer object");
        };
        
        /// Allocates the required memory on the GPU and copies all index and vertex data to the GPU
        /// \param[in] format - Format of data in vertex buffer
        /// \param[in] numVertices - Number of vertices to be copied to GPU
        /// \param[in] numIndices - Number of indices to be copied to GPU
        /// \param[in] vertices - Pointer to vertex array on CPU
        /// \param[in] indices - Pointer to index array on CPU
        virtual bool CopyDataToGpu(const VertexFormat& format, int numVertices, int numIndices,
                                   float* vertices, unsigned int *indices) = 0;
    
        /// Allocates the required memory on the GPU and copies all mesh data to the GPU
        /// \param[in] mesh - Pointer to mesh object to be copied to GPU
        bool CopyDataToGpu(std::shared_ptr<Mesh> mesh);
        
        // Getter function
        inline int GetNumIndices() const { return numIndices_; }
        
        
    protected:
        
        int numIndices_;                                   ///< No. of indices in index buffer
    
    };
    
}
//---------------------------------------------------------------------------------------------------------------------
#endif
