#ifndef DYNASTI_RENDERABLE_COMPONENT_H
#define DYNASTI_RENDERABLE_COMPONENT_H


#define GLM_FORCE_RADIANS
#include <assert.h>
#include <map>
#include <string>
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include "glad/glad.h"
#include "Dynasti/Core/Constants.h"
#include "Dynasti/Transform//Transform3d.h"
#include "Dynasti/Mesh/Vertex.h"
#include "Dynasti/Ecs/Component.h"


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{

    //=================================================================================================================
    /// \brief   Component class containing all data for rendering a mesh to the viewport.
    /// \author  D. A. Hubber
    /// \date    13/02/2020
    //=================================================================================================================
    class RenderableComponent : public Component
    {
    public:

        RenderableComponent(const int entityId=-1);
        virtual ~RenderableComponent();
    
        /// Returns a simple human-readable name of the component type (implemented by child classes)
        /// @return - Human-readable name of the component
        virtual const std::string GetComponentName() override { return "RenderableComponent"; }
    
        /// Initialises component in the object pool when entity is spawned (implemented by child classes)
        virtual void OnCreate() override;
        
        /// Destroys the component in the object pool when entity is destroyed (implemented by child classes)
        virtual void OnDestroy() override;
        
        const Transform3d& GetTransform() const { return transform; }
        void SetPosition(const glm::vec3 newPosition)
        {
            transform.SetPosition(newPosition);
            transform.UpdateModelMatrix();
        }
    
        void SetRotation(const glm::quat newQuat)
        {
            transform.SetQRotation(newQuat);
            transform.UpdateModelMatrix();
        }
        
        // Getters and setters for protected variables
        GlobalId GetMaterialId() const { return materialId_; }
        GlobalId GetMeshId() const { return meshId_; }
        GlobalId GetShaderId() const { return shaderId_; }
        GlobalId GetTextureId() const { return textureId_; }
        bool IsVisible() const { return visible_; }
        void SetMaterialId(const GlobalId materialId) { materialId_ = materialId; }
        void SetMeshId(const GlobalId meshId) { meshId_ = meshId; }
        void SetShaderId(const GlobalId shaderId) { shaderId_ = shaderId; }
        void SetTextureId(const GlobalId textureId) { textureId_ = textureId; }
        void SetVisible(const bool visible) { visible_ = visible; }


    protected:
        
        bool visible_;                                     ///< Flag if renderable is visible for rendering
        GlobalId materialId_ = -1;                         ///< ..
        GlobalId meshId_ = -1;                             ///< Id of mesh to render
        GlobalId shaderId_ = -1;                           ///< Id of shader used when rendering
        GlobalId textureId_ = -1;                          ///< Id of texture used when rendering
        Transform3d transform;                             ///< World transform of the renderable component
        
    };

}
//---------------------------------------------------------------------------------------------------------------------
#endif
