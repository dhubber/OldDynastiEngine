#include "Dynasti/Core/Debug.h"
#include "Dynasti/Mesh/Mesh.h"
#include "Dynasti/Renderer/VertexBuffer.h"
#ifdef DYNASTI_USE_OPENGL
#include "Dynasti/Renderer/Opengl/OpenglVertexBuffer.h"
#endif


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{
    
    std::shared_ptr<VertexBuffer> VertexBuffer::VertexBufferFactory()
    {
#ifdef DYNASTI_USE_OPENGL
        return std::make_shared<OpenglVertexBuffer>();
#else
        DYNASTI_WARNING("Cannot create a vertex buffer object.  No valid option chosen");
        return nullptr;
#endif
    }
    
    bool VertexBuffer::CopyDataToGpu(std::shared_ptr<Mesh> mesh)
    {
        return CopyDataToGpu(mesh->GetFormat(), mesh->GetNumVertices(), mesh->GetNumIndices(),
                             mesh->VertexArrayPointer(), mesh->IndexArrayPointer());
    }
    
}
//---------------------------------------------------------------------------------------------------------------------