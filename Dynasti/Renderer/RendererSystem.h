#ifndef DYNASTI_RENDERER_SYSTEM_H
#define DYNASTI_RENDERER_SYSTEM_H


#include "Dynasti/Ecs/EcsSystem.h"
#include "Dynasti/Window/Window.h"


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{
    
    // Forward declarations
    class AssetManager;
    class Camera;
    class EventManager;
    class Framebuffer;
    class InputManager;
    class Scene;
    class SceneManager;

    
    //=================================================================================================================
    /// \brief   Base class of all renderer systems.
    /// \author  D. A. Hubber
    /// \date    23/02/2020
    //=================================================================================================================
    class RendererSystem : public EcsSystem
    {
    public:
        
        RendererSystem(const std::string name, std::shared_ptr<AssetManager> assetManager,
                       std::shared_ptr<EventManager> eventManager, std::shared_ptr<FrameTimer> frameTimer,
                       std::shared_ptr<SceneManager> sceneManager, std::shared_ptr<InputManager> inputManager) :
          EcsSystem(name, assetManager, eventManager, frameTimer, sceneManager),
          inputManager_(inputManager)
        {
        };
        
        void SetActiveWindow(std::shared_ptr<Window> window)
        {
            DYNASTI_ASSERT(window, "Invalid window pointer");
            activeWindow_ = window;
            DYNASTI_LOG_VERBOSE("Registered window with renderer: " + activeWindow_->GetTitle());
        }
    
        
    protected:
        
        std::shared_ptr<Window> activeWindow_;             ///< ..
        std::shared_ptr<InputManager> inputManager_;       ///< Main player input manager object
        std::shared_ptr<Framebuffer> screenFramebuffer_;   ///< ..

        // TODO : Register default shaders in the RendererSystem class
        //void RegisterDefaultShaders();
    
        /// Renders an individual scene using the given camera object
        /// \param[in] scene - Pointer to scene being rendered
        /// \param[in] camera - Pointer to camera used when rendering scene
        virtual void RenderScene(std::shared_ptr<Scene> scene, std::shared_ptr<Camera> camera) = 0;
        
    };
    
}
//---------------------------------------------------------------------------------------------------------------------
#endif
