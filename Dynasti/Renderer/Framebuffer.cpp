#include "Dynasti/Core/Debug.h"
#include "Dynasti/Renderer/Framebuffer.h"
#ifdef DYNASTI_USE_OPENGL
#include "Dynasti/Renderer/Opengl/OpenglFramebuffer.h"
#endif


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{
    
    std::shared_ptr<Framebuffer> Framebuffer::FramebufferFactory()
    {
#ifdef DYNASTI_USE_OPENGL
        return std::make_shared<OpenglFramebuffer>();
#else
        DYNASTI_WARNING("Cannot create a framebuffer object.  No valid option chosen");
        return nullptr;
#endif
    }
    
}
//---------------------------------------------------------------------------------------------------------------------