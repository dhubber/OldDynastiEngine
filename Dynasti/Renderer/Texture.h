#ifndef DYNASTI_TEXTURE_H
#define DYNASTI_TEXTURE_H


#include <string>
#include <vector>
#include "glad/glad.h"
#include "Dynasti/Asset/Asset.h"
#include "Dynasti/Core/Constants.h"
#include "Dynasti/Core/Debug.h"


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{
    
    //=================================================================================================================
    /// \brief   Virtual base class for storing texture data on the GPU.
    /// \author  D. A. Hubber
    /// \date    05/08/2017
    //=================================================================================================================
    class Texture : public Asset
    {
    public:
    
        /// Factory function for creating new texture assets
        /// \return - Shared pointer to new texture object
        static std::shared_ptr<Texture> TextureFactory();
        
        Texture() : Asset(), bytesPerPixel_(4), width_(0), height_(0)
        {
            DYNASTI_LOG_VERBOSE("Constructing texture object");
        };
        
        virtual ~Texture()
        {
            DYNASTI_LOG_VERBOSE("Destroying texture object");
        };
        
        /// Loads an image stored in the given filename
        /// \param filename - Name of file containing image to be loaded
        /// \return - True if image loaded successfully; otherwise false
        virtual bool LoadImageFromFile(const std::string filename) = 0;
    
        /// Loads image data stored in CPU memory to the GPU
        /// \return - True if data successfully loaded to the GPU; otherwise false
        virtual bool LoadImageToGpu() = 0;
        
        /// Unloads the image from the GPU
        /// \return - True if the image was successfully unloaded; otherwise false
        virtual bool UnloadImageFromGpu() = 0;
    
        /// \return - Total number of bytes contained within image pixmap data
        inline int NumBytesImageData() {return width_ * height_ * bytesPerPixel_;}
    
        // Getters of private image class variables
        inline int GetBytesPerPixel() const { return bytesPerPixel_; }
        inline int GetHeight() const { return height_; }
        inline int GetWidth() const { return width_; }
        

    protected:
        
        int bytesPerPixel_;                                ///< No. of bytes per pixel
        int width_;                                        ///< Image width in pixels
        int height_;                                       ///< Image height in pixels

    };

}
//---------------------------------------------------------------------------------------------------------------------
#endif
