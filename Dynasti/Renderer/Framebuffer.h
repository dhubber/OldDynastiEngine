#ifndef DYNASTI_FRAME_BUFFER_H
#define DYNASTI_FRAME_BUFFER_H


//#define GL_GLEXT_PROTOTYPES 1
#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>
#include "glad/glad.h"
#include "Dynasti/Core/Debug.h"
#include "Dynasti/Shader/GlslShader.h"


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{
    
    //=================================================================================================================
    /// \brief   Class for setting up and storing a general framebuffer object.
    /// \author  D. A. Hubber
    /// \date    02/11/2017
    //=================================================================================================================
    class Framebuffer
    {
    public:
        
        Framebuffer() : width_(0), height_(0)
        {
            DYNASTI_LOG_VERBOSE("Constructing Framebuffer object");
        }
        
        virtual ~Framebuffer()
        {
            DYNASTI_LOG_VERBOSE("Destroying Framebuffer object");
        };
    
        /// Factory function for creating new texture assets
        /// \return - Shared pointer to new texture object
        static std::shared_ptr<Framebuffer> FramebufferFactory();
        
        /// Creates a framebuffer on the GPU with the given dimensions.
        virtual void Create(const int width, const int height) = 0;
        
        virtual void Destroy() = 0;
        
        /// Render the framebuffer to the screen using the selected post-processing shader
        /// \param frameBufferSize - Size of framebuffer rendering window
        /// \param shader - GLSL shader used to render the image (e.g. for post-processing)
        /// \param clearColor - Background/clear color used by OpenG
        virtual void Render(std::shared_ptr<GlslShader> shader, const glm::vec2 frameBufferSize,
                            const glm::vec4 clearColor = glm::vec4(1.0f, 1.0f, 1.0f, 1.0f)) = 0;
        
        /// Binds the framebuffer on the GPU
        virtual void Bind() = 0;
        
        virtual void Unbind() = 0;
        
        inline unsigned int GetWidth() const { return width_; }
        inline unsigned int GetHeight() const { return height_; }

        
    protected:
    
        unsigned int width_;
        unsigned int height_;

    };

}
//---------------------------------------------------------------------------------------------------------------------
#endif
