#define STB_IMAGE_IMPLEMENTATION
#define STB_IMAGE_WRITE_IMPLEMENTATION
#ifdef DYNASTI_USE_TINYGLTF
#define TINYGLTF_IMPLEMENTATION
#include "Dynasti/Dependencies/tinygltf/tiny_gltf.h"
#endif
#include "Dynasti/Asset/AssetManager.h"
#include "Dynasti/Camera/Camera.h"
#include "Dynasti/Renderer/RenderableComponent.h"
#include "Dynasti/Renderer/Texture.h"
#include "Dynasti/Scene/Scene.h"


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{
    
    Scene::Scene(std::shared_ptr<AssetManager> assetManager) : Asset(), assetManager_(assetManager), entityIdCounter(0)
    {
        DYNASTI_LOG("Constructing Scene object");
        editorCamera_ = std::make_shared<Camera>();
    }
    

    Entity& Scene::CreateNewEntity(const std::string name)
    {
        Entity& newEntity = entityPool_.NewObject(entityIdCounter++);
        newEntity.name = name;
        return newEntity;
    }
    
    
    bool Scene::Attach(Entity &entity, Entity &parentEntity)
    {
        // Some basic sanity-checking
        DYNASTI_ASSERT(entity.parentId == -1, "Entity already attached to another entity");
    
        // Record the old child id (if any were present)
        const GlobalId oldChildId = parentEntity.childId;
        parentEntity.childId = entity.id;
        //parent->SetFlag(EntityFlags::ENTITY_DIRTY_CHILDREN_FLAG);
    
        // Set reciprocal id of parent to child, and update linked list of children (if more than one)
        entity.parentId = parentEntity.id;
        //child->SetFlag(EntityFlags::ENTITY_DIRTY_FLAG);
        if (oldChildId != -1) entity.nextId = oldChildId;
        //if (oldChildId != -1) child->SetNextId(oldChildId);
        
        return false;
    }
    
    
    bool Scene::Detach(Entity &entity)
    {
        // If entity has no parent, then simply return immediately
        if (entity.parentId == -1) return false;
        
        const GlobalId nextId = entity.nextId;
        const GlobalId parentId = entity.parentId;
        entity.nextId = -1;
        entity.parentId = -1;
        
        // If the pointer to the parent is null for some reason, then also return immediately
        Entity* parentEntity = entityPool_.FindObjectWithGlobalId(parentId);
        if (parentEntity == nullptr) return false;
        
        // Check if child to be detached is the first in the linked list of siblings.
        // If so, then make parent point directly to the second child
        GlobalId id = parentEntity->childId;
        if (id == entity.id)
        {
            parentEntity->childId = nextId;
            return true;
        }
    
        // Otherwise iterate through all children to find the required entity in the linked list
        while (id != -1)
        {
            Entity* nextEntity = entityPool_.FindObjectWithGlobalId(id);
            id = nextEntity->nextId;
            if (id == entity.id)
            {
                nextEntity->nextId = nextId;
                return true;
            }
        }
        
        // Should not get here really so print an error that something is wrong
        DYNASTI_FATAL("Could not find entity in linked list of parent#s children : " + std::to_string(entity.id));
        return false;
    }
    
    
    std::shared_ptr<ObjectPoolBase> Scene::FindComponentPool(const std::type_index typeIndex)
    {
        auto it = componentPoolMap_.find(typeIndex);
        if (it != componentPoolMap_.end())
        {
            return it->second;
        }
        return nullptr;
    }
    
    
    std::vector<std::shared_ptr<ObjectPoolBase>> Scene::FindAllScriptPools()
    {
        std::vector<std::shared_ptr<ObjectPoolBase>> scriptPools;
        for (auto scriptTypeIndex : scriptTypeIndexSet_)
        {
            std::shared_ptr<ObjectPoolBase> scriptPool = FindComponentPool(scriptTypeIndex);
            if (scriptPool)
            {
                scriptPools.push_back(scriptPool);
            }
        }
        return scriptPools;
    }
    
    
    ScriptComponent* Scene::FindScriptComponent(const GlobalId entityId)
    {
        auto it = scriptCompTypeMap_.find(entityId);
        if (it != scriptCompTypeMap_.end())
        {
            const std::type_index typeIndex = it->second;
            std::shared_ptr<ObjectPoolBase> scriptComponentPool = FindComponentPool(typeIndex);
            if (scriptComponentPool)
            {
                return dynamic_cast<ScriptComponent*>(scriptComponentPool->FindObjectWithLocalId(entityId));
            }
        }
        return nullptr;
    }
    
    
    std::vector<Component*> Scene::FindAllEntityComponents(const GlobalId entityId)
    {
        std::vector<Component*> components;
        //for (auto const& [typeId, componentPool] : componentPoolMap_)
        for (const auto& it : componentPoolMap_)
        {
            Component* component = dynamic_cast<Component*>(it.second->FindObjectWithGlobalId(entityId));
            if (component)
            {
                components.push_back(component);
            }
        }
        return components;
    }


#ifdef DYNASTI_USE_TINYGLTF
    bool Scene::ImportGltfModel(const std::string filename)
    {
        DYNASTI_LOG_VERBOSE("Attempting to load gltf2.0 file : " + filename);
    
        tinygltf::TinyGLTF gltfLoader;
        tinygltf::Model gltfModel;
        std::string error;
        std::string warning;
        bool result = gltfLoader.LoadASCIIFromFile(&gltfModel, &error, &warning, filename);
        
        if (result)
        {
            DYNASTI_WARNING_IF(!warning.empty(), warning);
            DYNASTI_LOG("Importing gltf2.0 model; filename : " + filename);
            
            // List all scenes
            DYNASTI_LOG_VERBOSE("No. of scenes       : " + std::to_string(gltfModel.scenes.size()));
            for (const auto &scene : gltfModel.scenes)
            {
                DYNASTI_LOG("Found scene : " + scene.name + "   No. of nodes : " + std::to_string(scene.nodes.size()));
            }
            
            // List all nodes
            DYNASTI_LOG_VERBOSE("No. of nodes        : " + std::to_string(gltfModel.nodes.size()));
            for (const auto &node : gltfModel.nodes)
            {
                DYNASTI_LOG("Found node : " + node.name + "   No. of children : " + std::to_string(node.children.size()))
            }
            
            DYNASTI_LOG_VERBOSE("No. of buffers      : " + std::to_string(gltfModel.buffers.size()));
            if (gltfModel.buffers.size() != 1)
            {
                DYNASTI_WARNING("Can only load a glTF mesh with a single buffer");
                return false;
            }
    
            DYNASTI_LOG_VERBOSE("No. of buffer views : " + std::to_string(gltfModel.bufferViews.size()));
            if (gltfModel.bufferViews.size() != 4)
            {
                DYNASTI_WARNING("Can only load a glTF mesh with 4 accessors");
                return false;
            }
            
            DYNASTI_LOG_VERBOSE("No. of accessors    : " + std::to_string(gltfModel.accessors.size()));

            // List all meshes
            DYNASTI_LOG_VERBOSE("No. of meshes       : " + std::to_string(gltfModel.meshes.size()));
            for (const auto &gltfMesh : gltfModel.meshes)
            {
                AssetRegistry<Mesh> &meshes = assetManager_->GetMeshRegistry();
                if (!meshes.IsImported(filename))
                {
                    std::shared_ptr<Mesh> newMesh = Mesh::ImportGltfMesh(gltfMesh, gltfModel, assetManager_);
                    if (newMesh)
                    {
                        meshes.Register(newMesh, newMesh->GetName(), filename);
                    }
                }
            }
            
            return true;
        }
        else
        {
            DYNASTI_FATAL(error);
            return false;
        }
    }
    
    
    void Scene::ImportGltfMesh(const tinygltf::Model& model, const std::string& modelDirectory, const GlobalId parentId)
    {
        for (const tinygltf::BufferView& bufferView : model.bufferViews)
        {
            if (bufferView.target == 0)
            {
                DYNASTI_WARNING("bufferView target is zero");
                continue;
            }
            
            const tinygltf::Buffer &buffer = model.buffers[bufferView.buffer];
            DYNASTI_LOG("bufferView; numBytes : " + std::to_string(bufferView.byteLength) + "   stride : " + std::to_string(bufferView.byteStride));
        }
    }
#endif

    
    /*GlobalId Scene::InstantiateSceneAsEntity(Scene *scene, GlobalId parentEntityId)
    {
        // If scene pointer is invalid, then return immediately with error code
        if (scene == nullptr) return -1;
        
        if (scene->)
    }*/
    
}
//---------------------------------------------------------------------------------------------------------------------