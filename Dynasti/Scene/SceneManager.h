#ifndef DYNASTI_SCENE_MANAGER_H
#define DYNASTI_SCENE_MANAGER_H


#include <list>
#include <memory>
#include "Dynasti/Asset/AssetManager.h"
#include "Dynasti/Core/Constants.h"
#include "Dynasti/Core/Debug.h"
#include "Dynasti/Scene/Scene.h"


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{
    
    //=================================================================================================================
    /// \brief   Basic class defining a ...
    /// \author  D. A. Hubber
    /// \date    08/08/2020
    //=================================================================================================================
    class SceneManager
    {
    public:
        
        SceneManager(std::shared_ptr<AssetManager> assetManager) : assetManager_(assetManager)
        {
            mainScene_ = std::make_shared<Scene>(assetManager);
            selectedScene_ = mainScene_;
        };
        
        
        void SelectScene(std::shared_ptr<Scene> scene)
        {
            if (scene)
            {
                selectedScene_ = scene;
            }
        }
    
        // Getters
        std::shared_ptr<Scene> GetMainScene() { return mainScene_; }
        std::shared_ptr<Scene> GetSelectedScene() { return selectedScene_; }
        

    protected:
    
        std::shared_ptr<AssetManager> assetManager_;       ///< Main asset manager object
        std::shared_ptr<Scene> mainScene_;                 ///< Main scene
        std::shared_ptr<Scene> selectedScene_;             ///< Currently selected scene (mainly for editor)

    };
    
}
//---------------------------------------------------------------------------------------------------------------------
#endif
