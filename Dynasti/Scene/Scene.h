#ifndef DYNASTI_SCENE_H
#define DYNASTI_SCENE_H


#include <vector>
#include <typeindex>
#include <typeinfo>
#include <type_traits>
#include <unordered_map>
#include <unordered_set>
#include "Dynasti/Asset/Asset.h"
#include "Dynasti/Core/Debug.h"
#include "Dynasti/Data/ObjectPool.h"
#include "Dynasti/Ecs/Entity.h"
#include "Dynasti/Script/ScriptComponent.h"
#ifdef DYNASTI_USE_ASSIMP
#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>
#endif
//#ifdef DYNASTI_USE_TINYGLTF
//#include "Dynasti/Dependencies/tinygltf/tiny_gltf.h"
//#endif

#ifdef DYNASTI_USE_TINYGLTF
// Forward declaration from tinygltf library
namespace tinygltf
{
    class Model;
    struct Mesh;
}
#endif


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{
    
    class AssetManager;
    class Camera;
    class Material;
    class Mesh3d;
    
    
    //=================================================================================================================
    /// \brief   Class describing a single scene with functions to create entities and components
    /// \author  D. A. Hubber
    /// \date    23/10/2020
    //=================================================================================================================
    class Scene : public Asset
    {
    public:
        
        Scene(std::shared_ptr<AssetManager> assetManager);
        
        /// Creates a new entity in the scene with an optional name
        Entity& CreateNewEntity(const std::string name="");

        /// Attach an entity to a given parent entity
        /// \param[inout] entity - Reference to entity that will be attached as a child
        /// \param[inout] parentEntity - Reference to entity that will parent the other entity
        /// \return - True if entity successfully atttached; otherwise false
        bool Attach(Entity& entity, Entity& parentEntity);
        
        /// Detach an entity from any parent it may be attached to
        /// \param[inout] entity - Reference to entity being detached
        bool Detach(Entity& entity);
        
#ifdef DYNASTI_USE_TINYGLTF
        bool ImportGltfModel(const std::string filename);
#endif
    
        /// Returns a pointer to the base object pool containing components of the class with the given type index.
        /// \return - Pointer to the component object pool; if not yet created, then returns nullptr.
        std::shared_ptr<ObjectPoolBase> FindComponentPool(const std::type_index typeIndex);
    
        /// Returns a pointer to the object pool containing all components of the templated type (T).
        /// If a pool of that component type does not yet exist, then it is created, registered and returned.
        /// \return - Pointer to the component object pool
        std::vector<std::shared_ptr<ObjectPoolBase>> FindAllScriptPools();
        
        /// Returns a pointer to any script component registered for the given entity, or nullptr if unregistered
        /// \param[in] entityId - Global id of entity to find script component for
        /// \return - Pointer to registered script component (if any registered); otherwise nullptr
        ScriptComponent* FindScriptComponent(const GlobalId entityId);
        
        /// Returns an array contain pointers to all components registered to the given entity
        /// \param[in] entityId - Global id of entity to find all components for
        /// \return - Array of pointers of all components registered to the given entity
        std::vector<Component*> FindAllEntityComponents(const GlobalId entityId);
    
        /// \return - Pointer to the currently active camera in the scene (for rendering)
        std::shared_ptr<Camera> GetActiveCamera() {return editorCamera_;}  // { return activeCamera_; }
        
        inline Entity* FindEntity(const GlobalId entityId) { return entityPool_.FindObjectWithGlobalId(entityId); }
        
        /// \return - Reference to the entity pool
        const ObjectPool<Entity>& GetEntityPool() const { return entityPool_; }
        
        /// \return - Array containing ids of all entities with dirty transforms that must be updated
        const std::vector<GlobalId>& GetDirtyEntities() const { return dirtyEntites_; }
    
        /// \return - Number of entites in the scene
        inline int NumEntites() const { return entityPool_.NumObjects(); }
    
        /// \return - Number of component pools registered in this scene
        inline int NumComponentPools() const { return componentPoolMap_.size(); }
    
        /// Creates a component of the type T to the entity with the given global id.  If the component itself has not
        /// yet been registered, then an object pool of that type is created before creating a component of that type.
        /// Also ensures that only a single script component type for any given entity can be created.
        /// \param entityId - Global id of the entity for which the component is being created
        /// \return - Pointer to newly formed component, or nullptr if no component was created
        template <typename T>
        T* CreateComponent(const GlobalId entityId)
        {
            std::shared_ptr<ObjectPool<T>> componentPool = FindOrCreateComponentPool<T>();
            DYNASTI_ASSERT(componentPool, "Pointer to component pool invalid");
            
            if (componentPool->FindObjectWithGlobalId(entityId) == nullptr)
            {
                // If component is inherited from ScriptComponent, then only add a new script component if there is
                // no other script component created for this entity
                if (std::is_base_of<ScriptComponent, T>::value)
                {
                    if (FindScriptComponent(entityId))
                    {
                        DYNASTI_WARNING("Script component already exists for entity " + std::to_string(entityId));
                        return nullptr;
                    }
                    else
                    {
                        std::type_index typeIndex = std::type_index(typeid(T));
                        scriptTypeIndexSet_.insert(typeIndex);
                    }
                }
                
                return &(componentPool->NewObject(entityId));
            }
            else
            {
                DYNASTI_WARNING("Component of type T already exists for entity " + std::to_string(entityId));
                return nullptr;
            }
        }
        
        /// Finds and returns a pointer to the component of type T for an entity if one exists, or nullptr.
        /// \param entityId - Global id of entity to find component for
        /// \return - Pointer to the component type for the entity; if none exists, then returns nullptr
        template <typename T>
        inline T* FindComponent(const GlobalId entityId)
        {
            std::shared_ptr<ObjectPool<T>> componentPool = FindComponentPool<T>();
            if (componentPool)
            {
                return componentPool->FindObjectWithGlobalId(entityId);
            }
            DYNASTI_WARNING("Could not find component for entity " + std::to_string(entityId));
            return nullptr;
        }
    
        /// Returns a pointer to the object pool containing all components of the templated type (T).
        /// \return - Pointer to the component object pool; if not yet created, then returns nullptr.
        template <typename T>
        inline std::shared_ptr<ObjectPool<T>> FindComponentPool()
        {
            std::type_index typeIndex = std::type_index(typeid(T));
            return std::dynamic_pointer_cast<ObjectPool<T>>(FindComponentPool(typeIndex));
        }
        
        /// Returns a pointer to the object pool containing all components of the templated type (T).
        /// If a pool of that component type does not yet exist, then it is created, registered and returned.
        /// \return - Pointer to the component object pool
        template <typename T>
        inline std::shared_ptr<ObjectPool<T>> FindOrCreateComponentPool()
        {
            std::shared_ptr<ObjectPool<T>> existingPool = FindComponentPool<T>();
            if (existingPool)
            {
                return existingPool;
            }
            return CreateComponentPool<T>();
        }
    
        /// Creates and returns a new object pool containing components of the templated type (T).
        /// If T is derived from ScriptComponent, then this new pool is also registered for the script system.
        /// \param[in] maxNumComponents - Number of components to allocate in the pool (optional)
        /// \return - Point to the new component object pool
        template <typename T>
        std::shared_ptr<ObjectPool<T>> CreateComponentPool(const int maxNumComponents=128)
        {
            DYNASTI_ASSERT(FindComponentPool<T>() == nullptr, "Ccomponent pool already exists");
            static std::type_index typeIndex = std::type_index(typeid(T));
    
            // If component type is a scriptable component, then add type_index to script set
            if (std::is_base_of<ScriptComponent, T>::value)
            {
                scriptTypeIndexSet_.insert(typeIndex);
            }

            DYNASTI_LOG("Creating object pool for component type : ")
            std::shared_ptr<ObjectPool<T>> newPool = std::make_shared<ObjectPool<T>>();
            componentPoolMap_[typeIndex] = newPool;
            return newPool;
        }
        
        
    protected:
    
        GlobalId entityIdCounter;                          ///< Counter for assigning new entity ids
        std::shared_ptr<AssetManager> assetManager_;       ///< Main asset manager object
        std::shared_ptr<Camera> activeCamera_;             ///< Currently active camera
        std::shared_ptr<Camera> editorCamera_;             ///< Camera used inside the editor
        std::vector<GlobalId> dirtyEntites_;               ///< Array containing ids of all dirty entities
        ObjectPool<Entity> entityPool_;              ///< Object pool for creating new entites
        std::unordered_set<std::type_index> scriptTypeIndexSet_;     ///< Set containing class types of all script components
        std::unordered_map<std::type_index, std::shared_ptr<ObjectPoolBase>> componentPoolMap_;  ///< Map of all component pools
        std::unordered_map<GlobalId, std::type_index> scriptCompTypeMap_;   ///< Map of entity ids to script compoennt types
    
    
        void ImportGltfMesh(const tinygltf::Model& model, const std::string& modelDirectory, const GlobalId parentId);
    
        void ImportGltfMaterial(const tinygltf::Model& model, const std::string& modelDirectory, const GlobalId parentId) {};
        
    /*
        /// Recursive function to process a single node in the scene tree as imported using ASSIMP
        /// \param assimpNode - Pointer to node inside the loaded ASSIMP model
        /// \param assimpScene - Pointer to main scene when loading ASSIMP model
        /// \param modelDirectory - Absolute path containing directory of the model file
        /// \param parentId - Global id of main model entity
        void ProcessAssimpNode(aiNode* assimpNode, const aiScene* assimpScene, const std::string& modelDirectory, const GlobalId parentId);
    
        /// Imports a mesh asset, including any textures, that is included inside an ASSIMP model
        /// \param assimpMesh - Pointer to mesh being loaded from ASSIMP model
        /// \param assimpScene - Pointer to main scene when loading ASSIMP model
        /// \param modelDirectory - Absolute path containing directory of the model file
        /// \param parentId - Global id of main model entity
        void ProcessAssimpMesh(aiMesh* assimpMesh, const aiScene* assimpScene, const std::string& modelDirectory, const GlobalId parentId);
    
        /// Imports a texture asset included in the ASSIMP model
        /// \param mesh - Pointer to mesh containing the texture
        /// \param assimpMaterial - Pointer to ASSIMP material which is using the texture
        /// \param assimpTextureType - Type of texture being loaded (internal to ASSIMP)
        /// \param textureType - Type of texture being loaded (Dynasti enum)
        /// \param textureTypeName - Human readable name of texture type (used for logging and debug purposes)
        /// \param modelDirectory - Absolute path containing directory of model file
        void ProcessMaterialTexture(std::shared_ptr<Material> material, aiMaterial* assimpMaterial, const aiTextureType assimpTextureType,
                                    const TextureType textureType, const std::string textureTypeName, const std::string& modelDirectory);
        */
        /// \return - Unique id for a newly created entity
        inline GlobalId GetNewEntityId() { return entityIdCounter++; }
        
        /// \return - Type index for the provided template class
        template <typename T>
        inline std::type_index GetTypeIndex() const
        {
            static std::type_index typeIndex = std::type_index(typeid(T));
            return typeIndex;
        }
        
    };
    
}
//---------------------------------------------------------------------------------------------------------------------
#endif
