#ifndef DYNASTI_GLSL_SHADER_H
#define DYNASTI_GLSL_SHADER_H


#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <assert.h>
#include <map>
#include <string>
#include "glad/glad.h"
#include "Dynasti/Asset/Asset.h"
#include "Dynasti/Core/Debug.h"


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{
    
    class Camera;
    class Material;
    class RenderableComponent;
    class Scene;
    

    //=================================================================================================================
    /// \brief   GLSL shader class for OpenGL and Vulkan rendering
    /// \author  D. A. Hubber
    /// \date    11/09/2014
    //=================================================================================================================
    class GlslShader : public Asset
    {
    public:
        
        GlslShader();
        virtual ~GlslShader();
        
        /// Sets all OpenGL flags and states required for using this shader
        virtual void SetGlFlags() {};
    
        /// Sets all global uniforms for the shader, such as the camera matrices and light variables
        /// \param[in] camera - Current camera being used to render scene
        /// \param[in] scene - Scene being rendered
        virtual void SetGlobalUniforms(const Camera& camera, const Scene& scene) {};
        
        /// Sets local uniform values for specific renderable currently being drawn
        virtual void SetLocalUniforms(const RenderableComponent &renderable, const Material& material) {};

        /// Loads a shader program from a char string containing GLSL code
        /// \param shaderType - Enumerated type of shader
        /// \param shaderCode - Pointer to string containing shader code
        /// \return - True if shader code loaded and compiled successfully; otherwise returns false
        bool LoadFromString(const GLenum shaderType, const std::string& shaderCodeString);
    
        /// Loads a shader program from a file containing GLSL code
        /// \param shaderType - Enumerated type of shader
        /// \param fileName - Name of file containing GLSL code
        /// \return - True if shader code read from file and compiled successfully; otherwise returns false
        bool LoadFromFile(const GLenum shaderType, const std::string fileName);

        /// Create the main GLSL shader program and link the different shader programmes
        bool CreateAndLinkProgram();
    
        /// Activate the main shader program for rendering on the GPU
        inline void Use()
        {
            glUseProgram(program_);
        }
    
        /// Deactivate the shader program currently bound to the GPU
        inline void UnUse()
        {
            glUseProgram(0);
        }

        /// Add a named attribute to the GLSL shader program
        /// \param[in] attribute - Name of new attribute
        inline void AddAttribute(const std::string& attribute)
        {
            glBindAttribLocation(program_, numAttributes_++, attribute.c_str());
        }
    
        inline void AddUniform(const std::string& uniform)
        {
            GLuint location = glGetUniformLocation(program_, uniform.c_str());
            uniformLocationList_[uniform] = location;
            DYNASTI_ASSERT(location != GL_INVALID_VALUE, "Invalid uniform location");
            DYNASTI_ASSERT(location != GL_INVALID_OPERATION, "Invalid operation");
        }
        
        /// \return - Human-readable name of GLSL shader
        inline const std::string& GetName() const {return name_;}
    
        inline GLuint GetUniform(const std::string& uniform)
        {
            return uniformLocationList_[uniform];
        }
    
        inline void SetUniform(const std::string &uniform, const GLfloat x, const GLfloat y, const GLfloat z)
        {
            GLint location = GetUniform(uniform);
            glUniform3f(location, x, y, z);
        }
        
        inline void SetUniform(const std::string &uniform, const glm::vec3 &v)
        {
            this->SetUniform(uniform, v.x, v.y, v.z);
        }
        
        inline void SetUniform(const std::string &uniform, const glm::vec4 &v)
        {
            GLint location = GetUniform(uniform);
            glUniform4f(location, v.x, v.y, v.z, v.w);
        }
        
        inline void SetUniform(const std::string &uniform, const glm::vec2 &v)
        {
            GLint location = GetUniform(uniform);
            glUniform2f(location, v.x, v.y);
        }
        
        inline void SetUniform(const std::string &uniform, const glm::mat4 &m)
        {
            GLint location = GetUniform(uniform);
            glUniformMatrix4fv(location, 1, GL_FALSE, glm::value_ptr(m));
        }
        
        inline void SetUniform(const std::string &uniform, const glm::mat3 &m)
        {
            GLint location = GetUniform(uniform);
            glUniformMatrix3fv(location, 1, GL_FALSE, glm::value_ptr(m));
        }
        
        inline void SetUniform(const std::string &uniform, const GLfloat val)
        {
            GLint location = GetUniform(uniform);
            glUniform1f(location, val);
        }

        inline void SetUniform(const std::string &uniform, const int val)
        {
            GLint location = GetUniform(uniform);
            glUniform1i(location, val);
        }
        
        inline void SetUniform(const std::string &uniform, const bool val)
        {
            GLint location = GetUniform(uniform);
            glUniform1i(location, val);
        }
        
        inline void SetUniform(const std::string &uniform, const GLuint val)
        {
            GLint location = GetUniform(uniform);
            glUniform1ui(location, val);
        }
        

    protected:
        
        int shaderId_;                                     ///< Internal (to OpenGL) shader id number
        int numAttributes_;                                ///< No. of attributes
        bool active_;                                      ///< ..
        GLuint program_;                                   ///< Linked shader program id
        GLuint vertexShader_;                              ///< Vertex shader program id
        GLuint fragmentShader_;                            ///< Fragment shader program id
        GLuint geometryShader_;                            ///< Geometry shader program id
        GLuint tessControlShader_;                         ///< Tessellation control shader pr
        GLuint tessEvalShader_;                            ///< Tessellation evaluation shader
        GLuint computeShader_;                             ///< Computer shader program id
        std::map<std::string,GLuint> attributeList_;       ///< List of all attribute string ids
        std::map<std::string,GLuint> uniformLocationList_; ///< List of uniform locations
    
        /// Attach all compiled shader stages to the main shader program
        void AttachShaders();
    
        /// Delete all compiled shader stages
        void DeleteShaders();
    
        /// Deletes the main shader program from the GPU
        void DeleteShaderProgram();
    };
    
    
    //=================================================================================================================
    /// \brief   Basic shader for rendering a mesh with a uniform color in the fragment shader.
    /// \author  D. A. Hubber
    /// \date    11/09/2014
    //=================================================================================================================
    class BasicColorShader : public GlslShader
    {
    public:
    
        BasicColorShader();
        
        virtual void SetGlFlags() override;
        virtual void SetGlobalUniforms(const Camera &camera, const Scene &scene) override;
        virtual void SetLocalUniforms(const RenderableComponent &renderable, const Material& material) override;
    
    };
    
    
    //=================================================================================================================
    /// \brief   Basic shader for rendering a mesh with a uniform color in the fragment shader.
    /// \author  D. A. Hubber
    /// \date    11/09/2014
    //=================================================================================================================
    class BasicTextureShader : public GlslShader
    {
    public:
        
        BasicTextureShader();
        
        virtual void SetGlFlags() override;
        virtual void SetGlobalUniforms(const Camera &camera, const Scene &scene) override;
        virtual void SetLocalUniforms(const RenderableComponent &renderable, const Material& material) override;
        
    };
    
    
    //=================================================================================================================
    /// \brief   Basic shader for rendering the mesh in wireframe mode.
    /// \author  D. A. Hubber
    /// \date    11/09/2014
    //=================================================================================================================
    class WireframeShader : public GlslShader
    {
    public:
        
        WireframeShader();

        virtual void SetGlFlags() override;
        virtual void SetGlobalUniforms(const Camera &camera, const Scene &scene) override;
        virtual void SetLocalUniforms(const RenderableComponent &renderable, const Material& material) override;
        
    };

}
//---------------------------------------------------------------------------------------------------------------------
#endif
