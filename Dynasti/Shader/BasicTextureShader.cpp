#include "Dynasti/Camera/Camera.h"
#include "Dynasti/Core/Debug.h"
#include "Dynasti/Transform/Transform3d.h"
#include "Dynasti/Renderer/RenderableComponent.h"
#include "Dynasti/Scene/Scene.h"
#include "Dynasti/Shader/GlslShader.h"


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{
    BasicTextureShader::BasicTextureShader() : GlslShader()
    {
        DYNASTI_LOG("Constructing BasicTextureShader object");
        LoadFromFile(GL_VERTEX_SHADER,  shaderPath + "PassThrough3d.vert");
        LoadFromFile(GL_FRAGMENT_SHADER, shaderPath + "SimpleTexture.frag");
        CreateAndLinkProgram();
        Use();
        AddUniform("view");
        AddUniform("projection");
        AddUniform("model");
        UnUse();
    }
    

    void BasicTextureShader::SetGlFlags()
    {
        glEnable(GL_DEPTH_TEST);
        glDisable(GL_BLEND);
        //glCullFace(GL_FRONT);
        //glCullFace(GL_BACK);
        glPolygonMode( GL_FRONT_AND_BACK, GL_FILL );
    }
    

    void BasicTextureShader::SetGlobalUniforms(const Camera &camera, const Scene &scene)
    {
        SetUniform("view", camera.GetViewMatrix());
        SetUniform("projection", camera.GetProjectionMatrix());
    }
    
    
    void BasicTextureShader::SetLocalUniforms(const RenderableComponent &renderable, const Material& material)
    {
        const Transform3d &transform = renderable.GetTransform();
        const glm::mat4 &M = transform.GetModelMatrix();

        glm::vec3 color = glm::vec3(1.0f,0.0f, 0.0f);
    
        SetUniform("model", M);
        //SetUniform("meshTexture", meshTexture);
    }
    
}
//---------------------------------------------------------------------------------------------------------------------
