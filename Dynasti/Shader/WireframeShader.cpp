#include "Dynasti/Camera/Camera.h"
#include "Dynasti/Core/Debug.h"
#include "Dynasti/Transform/Transform3d.h"
#include "Dynasti/Renderer/Material.h"
#include "Dynasti/Renderer/RenderableComponent.h"
#include "Dynasti/Scene/Scene.h"
#include "Dynasti/Shader/GlslShader.h"


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{

    WireframeShader::WireframeShader() : GlslShader()
    {
        DYNASTI_LOG("Constructing WireframeShader object");
        LoadFromFile(GL_VERTEX_SHADER, shaderPath +"PassThrough3d.vert");
        LoadFromFile(GL_FRAGMENT_SHADER, shaderPath + "UniformColor.frag");
        CreateAndLinkProgram();
        Use();
        AddUniform("view");
        AddUniform("projection");
        AddUniform("model");
        AddUniform("color");
        UnUse();
    }
    

    void WireframeShader::SetGlFlags()
    {
        glEnable(GL_DEPTH_TEST);
        glDisable(GL_BLEND);
        //glCullFace(GL_FRONT);
        //glCullFace(GL_BACK);
        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    }
    

    void WireframeShader::SetGlobalUniforms(const Camera &camera, const Scene &scene)
    {
        SetUniform("view", camera.GetViewMatrix());
        SetUniform("projection", camera.GetProjectionMatrix());
    }
    
    
    void WireframeShader::SetLocalUniforms(const RenderableComponent &renderable, const Material& material)
    {
        const Transform3d &transform = renderable.GetTransform();
        const glm::mat4 &M = transform.GetModelMatrix();
    
        SetUniform("model", M);
        SetUniform("color", material.GetColor3());
    }
    
}
//---------------------------------------------------------------------------------------------------------------------
