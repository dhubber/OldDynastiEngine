#include <istream>
#include <sstream>
#include "Dynasti/Core/Debug.h"
#include "Dynasti/Shader/GlslShader.h"


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{

    GlslShader::GlslShader() : Asset(), shaderId_(-1), numAttributes_(0),
        active_(false), program_(0), vertexShader_(0), fragmentShader_(0), geometryShader_(0),
        tessControlShader_(0), tessEvalShader_(0), computeShader_(0)
    {
        DYNASTI_LOG("Constructing GlslShader object");
    }
    
    
    GlslShader::~GlslShader()
    {
        /*if (program_ != 0)
        {
            DeleteShaderProgram();
            program_ = 0;
        }*/
    }
    

    bool GlslShader::LoadFromString(const GLenum shaderType, const std::string& shaderCodeString)
    {
        DYNASTI_LOG_VERBOSE("Loading GLSL shader from string");

        // Check whether the shader loads and compiles fine
        GLuint shader = glCreateShader(shaderType);
        DYNASTI_ASSERT(shader != 0, "GlslShader id given invalid value by OpenGL");
        const char* shaderCodePtr = shaderCodeString.c_str();
        glShaderSource(shader, 1, &shaderCodePtr, NULL);
        glCompileShader(shader);

        // Write error message if there is a problem with shader compilation
        GLint status;
        glGetShaderiv(shader, GL_COMPILE_STATUS, &status);
        if (status == GL_FALSE)
        {
            GLint infoLogLength;
            glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &infoLogLength);
            GLchar *infoLog = new GLchar[infoLogLength];
            glGetShaderInfoLog(shader, infoLogLength, NULL, infoLog);
            std::string infoString = infoLog;
            DYNASTI_LOG("Problem compiling shader stage; Compile log: " + infoString);
            delete[] infoLog;
            return false;
        }

        // Set shader identifier depending on shader type
        if (shaderType == GL_VERTEX_SHADER)
        {
            DYNASTI_LOG_VERBOSE("Successfully created vertex shader from string");
            vertexShader_ = shader;
        }
        else if (shaderType == GL_FRAGMENT_SHADER)
        {
            DYNASTI_LOG_VERBOSE("Successfully created fragment shader from string");
            fragmentShader_ = shader;
        }
        else if (shaderType == GL_GEOMETRY_SHADER)
        {
            DYNASTI_LOG_VERBOSE("Successfully created geometry shader from string");
            geometryShader_ = shader;
        }
        else if (shaderType == GL_TESS_CONTROL_SHADER)
        {
            DYNASTI_LOG_VERBOSE("Successfully created tessellation control shader from string");
            tessControlShader_ = shader;
        }
        else if (shaderType == GL_TESS_EVALUATION_SHADER)
        {
            DYNASTI_LOG_VERBOSE("Successfully created tessellation evaluation shader from string");
            tessEvalShader_ = shader;
        }
        else if (shaderType == GL_COMPUTE_SHADER)
        {
            DYNASTI_LOG_VERBOSE("Successfully created compute shader from string");
            computeShader_ = shader;
        }
        else
        {
            DYNASTI_LOG("Invalid shader stage type selected");
            return false;
        }

        return true;
    }
    
    
    bool GlslShader::LoadFromFile(const GLenum shaderType, const std::string fileName)
    {
        std::string line;
        std::stringstream inStringStream;
        std::ifstream inFile(fileName);
        if (inFile)
        {
            while (getline(inFile, line))
            {
                inStringStream << line << std::endl;
            };
            inFile.close();
            return LoadFromString(shaderType, inStringStream.str());
        }
        DYNASTI_WARNING("Could not open shader source file : " + fileName);
        return false;
    }


    bool GlslShader::CreateAndLinkProgram()
    {
        GLint status;

        program_ = glCreateProgram();
        DYNASTI_ASSERT(program_ != 0, "Invalid GlslShader program id returned by OpenGL");

        // Attach all shader stages, link and check whether the program links fine
        AttachShaders();
        glLinkProgram(program_);
        glGetProgramiv(program_, GL_LINK_STATUS, &status);
        if (status == GL_FALSE)
        {
            GLint infoLogLength;
            glGetProgramiv(program_, GL_INFO_LOG_LENGTH, &infoLogLength);
            GLchar *infoLog = new GLchar[infoLogLength];
            glGetProgramInfoLog(program_, infoLogLength, NULL, infoLog);
            std::string infoString = infoLog;
            DYNASTI_LOG("Problem linking GlslShader; Link log: " + infoString);
            delete[] infoLog;
            DeleteShaders();
            return false;
        }
        else
        {
            DeleteShaders();
            DYNASTI_LOG("Successfully linked GlslShader");
            return true;
        }
    }


    void GlslShader::AttachShaders()
    {
        if (vertexShader_ != 0)
        {
            glAttachShader(program_, vertexShader_);
        }
        if (fragmentShader_ != 0)
        {
            glAttachShader(program_, fragmentShader_);
        }
        if (geometryShader_ != 0)
        {
            glAttachShader(program_, geometryShader_);
        }
        if (tessControlShader_ != 0)
        {
            glAttachShader(program_, tessControlShader_);
        }
        if (tessEvalShader_ != 0)
        {
            glAttachShader(program_, tessEvalShader_);
        }
        if (computeShader_ != 0)
        {
            glAttachShader(program_, computeShader_);
        }
    }

    
    void GlslShader::DeleteShaders()
    {
        if (computeShader_ != 0)
        {
            glDeleteShader(computeShader_);
        }
        if (tessEvalShader_ != 0)
        {
            glDeleteShader(tessEvalShader_);
        }
        if (tessControlShader_ != 0)
        {
            glDeleteShader(tessControlShader_);
        }
        if (geometryShader_ != 0)
        {
            glDeleteShader(geometryShader_);
        }
        if (fragmentShader_ != 0)
        {
            glDeleteShader(fragmentShader_);
        }
        if (vertexShader_ != 0)
        {
            glDeleteShader(vertexShader_);
        }
    }


    void GlslShader::DeleteShaderProgram()
    {
        glDeleteProgram(program_);
    }
    
}
//---------------------------------------------------------------------------------------------------------------------
