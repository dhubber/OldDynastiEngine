#ifndef DYNASTI_OBJECT_POOL_H
#define DYNASTI_OBJECT_POOL_H


#include <unordered_map>
#include <vector>
#include "Dynasti/Core/Constants.h"
#include "Dynasti/Core/Debug.h"
#include "Dynasti/Data/Poolable.h"


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{
    
    //=================================================================================================================
    /// \brief   Base class for object pools.   Poolable objects must provide 'OnCreate' and 'OnDestroy' functions.
    /// \author  D. A. Hubber
    /// \date    07/08/2020
    //=================================================================================================================
    class ObjectPoolBase
    {
    public:
        
        ObjectPoolBase() : numObjects_(0) {};
    
        /// Assignment operator (for read-only look-up)
        Poolable& operator[] (const int localId)
        {
            DYNASTI_WARNING_IF(!IsLocalIdAssigned(localId), "Object with id " + std::to_string(localId) + " not assigned in object pool ");
            DYNASTI_ASSERT(IsLocalIdValid(localId), "Invalid localId for ObjectPool : " + std::to_string(localId) + "  " + std::to_string(numObjects_));
            return (*FindObjectWithLocalId(localId));
        }
    
        /// Assignment operator (for read-only look-up)
        const Poolable& operator[] (const int localId) const
        {
            DYNASTI_WARNING_IF(!IsLocalIdAssigned(localId), "Object with id " + std::to_string(localId) + " not assigned in object pool ");
            DYNASTI_ASSERT(IsLocalIdValid(localId), "Invalid localId for ObjectPool : " + std::to_string(localId) + "  " + std::to_string(numObjects_));
            return (*FindObjectWithLocalId(localId));
        }
    
        /// Returns the reference to a newly created poolable object
        /// \param[in] globalId - Global id of the newly created object
        /// \return - Reference to the newly created object
        virtual Poolable& NewObject(const GlobalId globalId) = 0;
    
        /// Gets a pointer to the object with the given local id
        /// \param[in] localId - Local id of the requested object
        /// \return - Pointer to the requested object
        virtual Poolable* FindObjectWithLocalId(const int localId) = 0;
    
        /// Gets a pointer to the object with the given local id
        /// \param[in] localId - Local id of the requested object
        /// \return - Pointer to the requested object
        virtual const Poolable* const FindObjectWithLocalId(const int localId) const = 0;
    
        /// Gets a pointer to the object with the given global id
        /// \param[in] globalId - Global id of the requested object
        /// \return - Pointer to the requested object
        virtual Poolable* FindObjectWithGlobalId(const GlobalId globalId) = 0;
    
        /// Frees object in the pool.  Moved to end of pool to maintain contiguous array of used objects.
        /// \param[in] localId - Local id of the object in the pool to be freed.
        virtual bool FreeObject(const int localId) = 0;
    
        /// Free object with the given associated global id
        /// \param[in] globalId - Global id associated with the object to be freed
        virtual bool FreeObjectWithGlobalId(const GlobalId globalId) = 0;
    
        /// Free/destroy all objects in pool, but keep all allocated memory
        virtual void Clear() = 0;
        
        virtual std::shared_ptr<ObjectPoolBase> Clone() = 0;
        
        /// \return - Max. number of objects in the pool
        virtual int MaxNumObjects() const = 0;
    
        /// \return - True if the given local id contains a valid object in the pool (assigned or not); otherwise false
        inline bool IsLocalIdValid(const int localId) const { return (localId >= 0 && localId < MaxNumObjects()); }
        
        /// \return - True if the given local id contains a valid object in the pool (assigned or not); otherwise false
        inline bool IsLocalIdAssigned(const int localId) const { return (localId >= 0 && localId < numObjects_); }

        /// \return - Current number of objects in the pool
        inline int NumObjects() const {return numObjects_;}


    protected:
    
        int numObjects_;                                   ///< No. of elements in pool
        std::unordered_map<GlobalId,int> idHash_;          ///< Hash table for mapping global to local ids
        
    };
    
    
    //=================================================================================================================
    /// \brief   Object pool for objects that also have a unique global integer id.
    /// \author  D. A. Hubber
    /// \date    07/08/2020
    //=================================================================================================================
    template <typename ObjectType>
    class ObjectPool: public ObjectPoolBase
    {
    public:
        
        using ObjectPoolBase::IsLocalIdAssigned;

        ObjectPool(const int maxNumObjects=128) : ObjectPoolBase()
        {
            DYNASTI_LOG("Constructing object pool with capacity : " + std::to_string(maxNumObjects));
            objects_.resize(maxNumObjects, ObjectType());
            idHash_.reserve(maxNumObjects);
        }
    
        ~ObjectPool()
        {
            DYNASTI_LOG("Destroying object pool");
            Clear();
        }
    
        /// Assignment operator
        ObjectType& operator[] (const int localId)
        {
            DYNASTI_WARNING_IF(!IsLocalIdAssigned(localId), "Object with id " + std::to_string(localId) + " not assigned in object pool ");
            DYNASTI_ASSERT(IsLocalIdValid(localId), "Invalid localId for ObjectPool : " + std::to_string(localId) + "  " + std::to_string(numObjects_));
            return objects_[localId];
        }
    
        /// Assignment operator (for read-only look-up)
        const ObjectType& operator[] (const int localId) const
        {
            DYNASTI_WARNING_IF(!IsLocalIdAssigned(localId), "Object with id " + std::to_string(localId) + " not assigned in object pool ");
            DYNASTI_ASSERT(IsLocalIdValid(localId), "Invalid localId for ObjectPool : " + std::to_string(localId) + "  " + std::to_string(numObjects_));
            return objects_[localId];
        }
        
        /// Creates a new object with a given associated global id, and returns a reference to that new object
        /// \param[in] globalId - Unique global id associated with the newly created object
        /// \return - Reference to the new object
        ObjectType& NewObject(const GlobalId globalId)
        {
            DYNASTI_ASSERT(!IsFull(), "Reached capacity of DynamicObjectPool : " + std::to_string(objects_.capacity()));
            DYNASTI_ASSERT(globalId >= 0, "Invalid global id provided for new object in pool : " + std::to_string(globalId));
            DYNASTI_ASSERT(FindLocalId(globalId) == -1, "Global id already registered : " + std::to_string(globalId));
            ObjectType &obj = objects_[numObjects_++];
            obj.id = globalId;
            obj.OnCreate();
            idHash_[globalId] = numObjects_ - 1;
            return obj;
        }
    
        /// If the pool contains an object with a given associated global id, then returns its local id
        /// \param[in] globalId - Global id associated with the object
        /// \return - Local id of the object with the given global id; -1 if global id is not found.
        inline int FindLocalId(const GlobalId globalId) const
        {
            auto it = idHash_.find(globalId);
            if (it != idHash_.end())
            {
                DYNASTI_ASSERT(it->second >= 0 && it->second < numObjects_, "Invalid local id found for globalId : " + std::to_string(globalId));
                return it->second;
            }
            else
            {
                return -1;
            }
        }
    
        /// Gets a pointer to the object with the given local id
        /// \param[in] localId - Local id of the requested object
        /// \return - Pointer to the requested object
        virtual ObjectType* FindObjectWithLocalId(const int localId) override
        {
            if (IsLocalIdAssigned(localId))
            {
                return &(objects_[localId]);
            }
            else
            {
                return nullptr;
            }
        }
    
        /// Gets a pointer to the object with the given local id
        /// \param[in] localId - Local id of the requested object
        /// \return - Pointer to the requested object
        virtual const ObjectType* const FindObjectWithLocalId(const int localId) const override
        {
            return FindObjectWithLocalId(localId);
        }
        
        /// Gets a pointer to the object with the given global id
        /// \param[in] globalId - Global id of the requested object
        /// \return - Pointer to the requested object
        virtual ObjectType* FindObjectWithGlobalId(const GlobalId globalId) override
        {
            const int localId = FindLocalId(globalId);
            if (localId != -1)
            {
                return &(objects_[localId]);
            }
            else
            {
                return nullptr;
            }
        }
        
        /// Frees object in the pool.  Moved to end of pool to maintain contiguous array of used objects.
        /// \param[in] localId - Local id of the object in the pool to be freed.
        virtual bool FreeObject(const int localId) override
        {
            // Return immediately with failure if local id is not valid for this pool
            if (!IsLocalIdAssigned(localId)) return false;
            
            //DYNASTI_ASSERT(localId >= 0 && localId < numObjects_, "Invalid localId for DynamicObjectPool : " + std::to_string(localId) + "  " + std::to_string(numObjects_));
            ObjectType& object = objects_[localId];
            const GlobalId globalId = objects_[localId].id;
            idHash_.erase(globalId);
            object.OnDestroy();
            object.id = -2;
            
            // Swap empty object slot with final allocated object to maintain contiguous array of objects in pool
            if (localId < numObjects_ - 1)
            {
                const GlobalId newGlobalId = objects_[numObjects_ - 1].id;
                objects_[localId] = objects_[numObjects_ - 1];
                idHash_[newGlobalId] = localId;
            }
            --numObjects_;
            return true;
        }
    
        /// Free object with the given associated global id
        /// \param[in] globalId - Global id associated with the object to be freed
        virtual bool FreeObjectWithGlobalId(const GlobalId globalId) override
        {
            const int localId = FindLocalId(globalId);
            if (localId == -1) return false;
            return FreeObject(localId);
        }
        
        /// Free/destroy all objects in pool, but keep all allocated memory
        virtual void Clear() override
        {
            for (int i = 0; i < numObjects_; ++i)
            {
                ObjectType& object = objects_[i];
                object.OnDestroy();
                object.id = -2;
            }
            numObjects_ = 0;
            idHash_.clear();
        }
        
        virtual std::shared_ptr<ObjectPoolBase> Clone() override
        {
            return std::make_shared<ObjectPool<ObjectType>>(*this);
        }
    
        /// \return - True if object pool has reached maximum capacity; otherwise false
        inline bool IsFull() const { return (numObjects_ == static_cast<int>(objects_.capacity())); }
    
        /// \return - Max. number of objects in the pool
        inline int MaxNumObjects() const {return objects_.size();}
        

    protected:

        using ObjectPoolBase::numObjects_;
        using ObjectPoolBase::idHash_;
        std::vector<ObjectType> objects_;                  ///< Vector containing all objects in pool
        
    };
    
}
//---------------------------------------------------------------------------------------------------------------------
#endif
