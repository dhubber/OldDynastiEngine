#ifndef DYNASTI_POOLABLE_H
#define DYNASTI_POOLABLE_H


#include "Dynasti/Core/Constants.h"


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{
    
    //=================================================================================================================
    /// \brief   Class defining interface for poolable objects; must provide 'OnCreate' and 'OnDestroy' functions.
    /// \author  D. A. Hubber
    /// \date    07/08/2020
    //=================================================================================================================
    class Poolable
    {
    public:
    
        explicit Poolable(const GlobalId newId=-2) : id(newId) {};

        /// Required functon interface for creating new objects in the pool
        virtual void OnCreate() = 0;
        
        /// Required function interface for destroying objects in the pool
        virtual void OnDestroy() = 0;

        GlobalId id;                                       ///< Global id of object stored in pool
        
    };
    
}
//---------------------------------------------------------------------------------------------------------------------
#endif
