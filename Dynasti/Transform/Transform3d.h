#ifndef DYNASTI_TRANSFORM_3D_H
#define DYNASTI_TRANSFORM_3D_H


#define GLM_ENABLE_EXPERIMENTAL
#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/matrix_decompose.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/quaternion.hpp>
#include <iostream>


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{

    //=================================================================================================================
    /// \brief   Transform class which contains world position, rotation and scaling data for 3d.
    /// \author  D. A. Hubber
    /// \date    19/12/2014
    //=================================================================================================================
    class Transform3d
    {
    public:

        /// Default constructor; initialises all variables to default values
        Transform3d() : dirty_(false), pos_(glm::vec3(0.0f, 0.0f, 0.0f)),
          scale_(glm::vec3(1.0f, 1.0f, 1.0f)), qrot_(glm::quat(1.0f, 0.0f, 0.0f, 0.0f)),
          modelMatrix_(glm::mat4(1.0f))
        {
        };
    
        /// Default constructor; initialises all variables to default values
        Transform3d(const glm::vec3& pos, const glm::vec3& scale, const glm::quat& qrot) : dirty_(false),
          pos_(pos), scale_(scale), qrot_(qrot)
        {
            UpdateModelMatrix();
        };

        /// Initialise all transform data values
        void Initialise()
        {
            dirty_       = false;
            pos_         = glm::vec3(0.0f, 0.0f, 0.0f);
            scale_       = glm::vec3(1.0f, 1.0f, 1.0f);
            qrot_        = glm::quat(1.0f, 0.0f, 0.0f, 0.0f);
            modelMatrix_ = glm::mat4(1.0f);
        }

        /// Decomposes the model matrix into the individual position, scale and quaternion components
        inline void DecomposeModelMatrix()
        {
            glm::vec3 skew;
            glm::vec4 perspective;
            glm::quat qrotConj;
            glm::decompose(modelMatrix_, scale_, qrotConj, pos_, skew, perspective);
            qrot_ = glm::conjugate(qrotConj);
        }

        /// Updates the values in the model matrix based on new values of the position, scale and quaternions.
        inline void UpdateModelMatrix()
        {
            modelMatrix_ = glm::mat4(1.0);
            glm::mat4 scaleMatrix = glm::scale(glm::mat4(1.0), scale_);
            glm::mat4 rotationMatrix = glm::mat4_cast(qrot_);
            glm::mat4 translationMatrix = glm::translate(glm::mat4(1.0), pos_);
            modelMatrix_ = translationMatrix*rotationMatrix*scaleMatrix;
        }
    
        /// \return - True if the transform is dirty and needs to be updated; false if up-to-date
        inline bool IsDirty() const {return dirty_;}

        // Basic getters for important Transform3d quantities
        inline const glm::mat4 & GetModelMatrix() const {return modelMatrix_;}
        inline const glm::vec3 & GetPosition() const {return pos_;}
        inline const glm::quat & GetQRotation() const {return qrot_;}
        inline const glm::vec3 & GetScale() const {return scale_;}

        /// \param[in] modelMatrix - New value of the model matrix
        inline void SetModelMatrix(const glm::mat4 modelMatrix)
        {
            modelMatrix_ = modelMatrix;
            dirty_ = true;
        }

        /// \param[in] pos - New position
        inline void SetPosition(const glm::vec3 pos)
        {
            pos_ = pos;
            dirty_ = true;
        }

        /// \param[in] qrot - New quaternion
        inline void SetQRotation(const glm::quat qrot)
        {
            qrot_ = qrot;
            dirty_ = true;
        }

        /// \param[in] scale - New 3d scale
        inline void SetScale(const glm::vec3 scale)
        {
            scale_ = scale;
            dirty_ = true;
        }

        /// \return - Rotation matrix as constructed from the transform's quaternion
        inline const glm::mat4 RotationMatrix() const { return glm::mat4_cast(qrot_); }

        /// \return - Scaling 4x4 matrix constructed from the 3d scale vector
        inline glm::mat4 ScaleMatrix() const { return glm::scale(glm::mat4(1.0), scale_); }

        /// \return - Translation 4x4 matrix constructed from the position vector
        inline glm::mat4 TranslationMatrix() const { return glm::translate(glm::mat4(1.0), pos_); }

        /// \return - Forward direction vector
        inline glm::vec3 ForwardDirection() const
        {
            return glm::vec3(RotationMatrix() * glm::vec4(0.0f, 0.0f, -1.0f, 0.0f));
        }

        /// \return - Right direction vectpr
        inline glm::vec3 RightDirection() const
        {
            return glm::vec3(RotationMatrix() * glm::vec4(1.0f, 0.0f, 0.0f, 0.0f));
        }

        /// \return - Up direction vector
        inline glm::vec3 UpDirection() const
        {
            return glm::vec3(RotationMatrix() * glm::vec4(0.0f, 1.0f, 0.0f, 0.0f));
        }

        /// Converts an axis-angle representation to a quaternion, which is then returned
        /// \param[in] ex - x value of direction vector
        /// \param[in] ey - y value of direction vector
        /// \param[in] ez - z value of direction vector
        /// \param[in] angle -
        static inline glm::quat AxisAngleToQuaternion(const float ex, const float ey, const float ez, const float angle)
        {
            const float cosHalfAngle = cos(0.5f * angle);
            const float sinHalfAngle = sin(0.5f * angle);
            glm::quat q;
            q.x = sinHalfAngle * ex;
            q.y = sinHalfAngle * ey;
            q.z = sinHalfAngle * ez;
            q.w = cosHalfAngle;
            return q;
        }

        /// Converts an axis-angle representation to a quaternion, which is then returned
        /// \param[in] aa - Axis-angle 4-vector
        static inline glm::quat AxisAngleToQuaternion(const glm::vec4 aa)
        {
            return AxisAngleToQuaternion(aa.x, aa.y, aa.z, aa.w);
        }
    
        /// Converts an axis-angle representation to a quaternion, which is then returned
        /// \param[in] aa - Axis-angle 4-vector
        static inline glm::quat AxisAngleToQuaternion(const glm::vec3 axis, const float angle)
        {
            return AxisAngleToQuaternion(axis.x, axis.y, axis.z, angle);
        }
        

    private:
        
        bool dirty_;                                       ///< Flag if the transform needs to be fully updated
        glm::vec3 pos_;                                    ///< Position
        glm::vec3 scale_;                                  ///< 3d scale factors
        glm::quat qrot_;                                   ///< Rotation quaternion
        glm::mat4 modelMatrix_;                            ///< 4x4 model matrix (for rendering transforms)

    };

}
//---------------------------------------------------------------------------------------------------------------------
#endif
