#ifndef DYNASTI_ECS_SYSTEM_H
#define DYNASTI_ECS_SYSTEM_H


#include <string>
#include "Dynasti/Core/Debug.h"
#include "Dynasti/Core/System.h"


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{
    
    // Forward declarations
    class AssetManager;
    class EventListener;
    class EventManager;
    class Scene;
    class SceneManager;
    
    
    //=================================================================================================================
    /// \brief   Virtual base class for all engine systems that need to communicate via the event system.
    /// \author  D. A. Hubber
    /// \date    12/06/2020
    //=================================================================================================================
    class EcsSystem : public System
    {
    public:
        
        EcsSystem(const std::string name, std::shared_ptr<AssetManager> assetManager,
                  std::shared_ptr<EventManager> eventManager, std::shared_ptr<FrameTimer> frameTimer,
                  std::shared_ptr<SceneManager> sceneManager) :
            System(name, assetManager, eventManager, frameTimer), sceneManager_(sceneManager)
        {
            DYNASTI_LOG_VERBOSE("Constructing ECS system object : " + std::string(name_));
        };
        
        virtual ~EcsSystem()
        {
            DYNASTI_LOG_VERBOSE("Destroying ECS system object : " + std::string(name_));
        }
        
        
    protected:
        
        std::shared_ptr<SceneManager> sceneManager_;         ///< Pointer to engine scene manager

    };
    
}
//---------------------------------------------------------------------------------------------------------------------
#endif
