#ifndef DYNASTI_ENTITY_H
#define DYNASTI_ENTITY_H


#include "Dynasti/Core/Constants.h"
#include "Dynasti/Core/Debug.h"
#include "Dynasti/Data/Poolable.h"


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{
    
    //=================================================================================================================
    /// \brief   Basic class defining a single entity and its relation to other entities in the scene graph.
    /// \author  D. A. Hubber
    /// \date    08/08/2020
    //=================================================================================================================
    class Entity : public Poolable
    {
    public:
        
        GlobalId childId;                                  ///< (1st) child entity id
        GlobalId nextId;                                   ///< id of next sibling (i.e. owned by same parent)
        GlobalId parentId;                                 ///< Parent entity id
        std::string name;                                  ///< Human-readable name of entity
        
        Entity() : Poolable(-1), childId(-1), nextId(-1), parentId(-1) {};
    
        /// Resets all variables to default values when created in ObjectPool
        virtual void OnCreate() override
        {
            childId = -1;
            nextId = -1;
            parentId = -1;
            name = "";
        };
        
        virtual void OnDestroy() override {};
    
    };
    
}
//---------------------------------------------------------------------------------------------------------------------
#endif
