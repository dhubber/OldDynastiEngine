#ifndef DYNASTI_COMPONENT_H
#define DYNASTI_COMPONENT_H


#include <cstring>
#include "Dynasti/Core/Constants.h"
#include "Dynasti/Data/Poolable.h"


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{
    
    //=================================================================================================================
    /// \brief   Virtual base class for defining all components in entities.
    /// \author  D. A. Hubber
    /// \date    07/08/2020
    //=================================================================================================================
    class Component : public Poolable
    {
    public:
        
        /// Default constructor
        Component(const GlobalId entityId=-1) : Poolable(entityId) {};
        
        /// Returns a simple human-readable name of the component type (implemented by child classes)
        /// @return - Human-readable name of the component
        virtual const std::string GetComponentName() = 0;
        
    };
    
}
//---------------------------------------------------------------------------------------------------------------------
#endif
