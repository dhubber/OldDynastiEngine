#ifndef DYNASTI_EVENT_H
#define DYNASTI_EVENT_H


#include "Dynasti/Core/Constants.h"


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{
    
    //=================================================================================================================
    /// \brief   Class representing a simple event data structure.
    /// \author  D. A. Hubber
    /// \date    29/06/2020
    //=================================================================================================================
    class Event
    {
    public:
        
        Event(const EventType type=-1, const int id=-1) : type_(type), id_(id) {};

        // Getters for event data members
        inline EventType GetType() const {return type_;}
        inline int GetId() const {return id_;}
        

    private:

        const EventType type_;                             ///< Event type id (as registered in the EventManager)
        const int id_;                                     ///< id associated with event (e.g. entity or scene id)

    };

}
//---------------------------------------------------------------------------------------------------------------------
#endif
