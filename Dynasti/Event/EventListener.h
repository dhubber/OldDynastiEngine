#ifndef DYNASTI_EVENT_LISTENER_H
#define DYNASTI_EVENT_LISTENER_H


#include <functional>
#include <map>
#include <memory>
#include "Dynasti/Event/Event.h"


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{
    
    class EventManager;
    
    
    //=================================================================================================================
    /// \brief   ...
    /// \author  D. A. Hubber
    /// \date    29/06/2020
    //=================================================================================================================
    class EventListener : public std::enable_shared_from_this<EventListener>
    {
    public:
        
        EventListener(std::shared_ptr<EventManager> eventManager, const std::string name="EventListener");
        
        /// Subscribes to a given event type with a callback function to be called whenever that event is broadcast
        /// \param[in] eventName - Human-readable name of event type registered with EventManager
        /// \param[in] callbackFunction - Callback function to be executed when event is broadcast
        EventType SubscribeToEvent(const std::string eventName, std::function<void(const Event)> callbackFunction);
        
        /// Called when a subscribed event is broadcast, which in turn calls all registered callback functions for the given event
        /// \param[in] event - Event object being broadcast by the EventManager to all subscribed listeners
        void OnEvent(const Event event);
        
        // Getter
        const std::string& GetName() { return name_; }
        
    protected:

        const std::string name_;                           ///< Name of event listener object
        std::shared_ptr<EventManager> eventManager_;       ///< Pointer to main event manager object
        std::map<EventType, std::function<void(const Event)>> eventCallbackMap;   ///< Map linking event types to their registered callbacks

    };

}
//---------------------------------------------------------------------------------------------------------------------
#endif
