#include "Dynasti/Core/Debug.h"
#include "Dynasti/Event/EventManager.h"


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{
    
    EventType EventManager::RegisterEventName(const std::string eventName)
    {
        EventType eventType = GetEventType(eventName);
        if (eventType != -1)
        {
            DYNASTI_WARNING("Event name already registered : " + eventName);
            return eventType;
        }

        eventType = eventTypes_.size();
        eventTypes_[eventName] = eventType;
        eventNames_[eventType] = eventName;
        DYNASTI_LOG_VERBOSE("Registering new event type : " + eventName);
        return eventType;
    }


    EventType EventManager::SubscribeToEvent(const std::string eventName, std::shared_ptr<EventListener> eventListener)
    {
        if (eventListener)
        {
            EventType eventType = GetEventType(eventName);
            if (eventType == -1)
            {
                eventType = RegisterEventName(eventName);
            }
            subscribers_.insert(std::make_pair(eventType, eventListener));
            return eventType;
        }
        return -1;
    }


    bool EventManager::UnsubscribeToEvent(const std::string eventName, std::shared_ptr<EventListener> eventListener)
    {
        EventType eventType = GetEventType(eventName);
        if (eventListener && eventType != -1)
        {
            for (auto iter = subscribers_.lower_bound(eventType); iter != subscribers_.upper_bound(eventType); ++iter)
            {
                if (iter->second == eventListener)
                {
                    subscribers_.erase(iter);
                    return true;
                }
            }
        }
        return false;
    }


    void EventManager::BroadcastEvent(const Event event)
    {
        const EventType eventType = event.GetType();
        DYNASTI_LOG_VERBOSE("EventManager broadcasting event : " + GetEventName(eventType));
        for (auto iter = subscribers_.lower_bound(eventType); iter != subscribers_.upper_bound(eventType); ++iter)
        {
            std::shared_ptr<EventListener> eventListener = iter->second;
            if (eventListener)
            {
                eventListener->OnEvent(event);
            }
        }
    }


    EventType EventManager::GetEventType(const std::string eventName) const
    {
        auto it = eventTypes_.find(eventName);
        if (it != std::end(eventTypes_))
        {
            return it->second;
        }
        return -1;
    }
    
    
    const std::string EventManager::GetEventName(const EventType eventType) const
    {
        auto it = eventNames_.find(eventType);
        if (it != std::end(eventNames_))
        {
            return it->second;
        }
        return {};
    }
    
    
    int EventManager::GetNumEventSubscribers(EventType eventType) const
    {
        int numSubscribers = 0;
        for (auto iter = subscribers_.lower_bound(eventType); iter != subscribers_.upper_bound(eventType); ++iter)
        {
            ++numSubscribers;
        }
        return numSubscribers;
    }
    
    
    void EventManager::LogAllRegisteredEvents()
    {
        DYNASTI_LOG("Total number of registered event types : " + std::to_string(eventTypes_.size()));
        for (auto iter = std::cbegin(eventTypes_); iter != std::cend(eventTypes_); ++iter)
        {
            int numSubscribers = 0;
            const EventType eventType = iter->second;
            DYNASTI_LOG("Event name : " + iter->first + "    id : " + std::to_string(eventType));
            for (auto iter = subscribers_.lower_bound(eventType); iter != subscribers_.upper_bound(eventType); ++iter)
            {
                DYNASTI_LOG_VERBOSE_IF(iter->second, "   Subscriber #" + std::to_string(++numSubscribers) + " : " + iter->second->GetName());
            }
        }
    }
    
}
//---------------------------------------------------------------------------------------------------------------------