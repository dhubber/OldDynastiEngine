#include <functional>
#include <memory>
#include "Dynasti/Core/Debug.h"
#include "Dynasti/Event/EventListener.h"
#include "Dynasti/Event/EventManager.h"


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{
    
    EventListener::EventListener(std::shared_ptr<EventManager> eventManager, const std::string name) :
      eventManager_(eventManager), name_(name)
    {
        DYNASTI_LOG_VERBOSE("Constructing EventListener object : " + name_);
    }
    
    
    EventType EventListener::SubscribeToEvent(const std::string eventName, std::function<void(const Event)> callbackFunction)
    {
        if (eventManager_)
        {
            EventType eventType = eventManager_->SubscribeToEvent(eventName, shared_from_this());
            eventCallbackMap.insert(std::make_pair(eventType, callbackFunction));
            return eventType;
        }
        DYNASTI_FATAL("Invalid pointer to EventManager while subscribing to event");
        return -1;
    }
    
    
    void EventListener::OnEvent(const Event event)
    {
        const EventType eventType = event.GetType();
        DYNASTI_LOG_VERBOSE("EventListener : " + GetName() + " receiving event type " + std::to_string(eventType));
        auto it = eventCallbackMap.find(eventType);
        if (it != std::end(eventCallbackMap))
        {
            std::function<void(const Event)> callbackFunction = it->second;
            if (callbackFunction)
            {
                callbackFunction(event);
            }
            else
            {
                DYNASTI_WARNING("Event callback function not valid");
            }
        }
        else
        {
            DYNASTI_WARNING("Subscribed event not found in local callback map");
        }
    }
    
}
//---------------------------------------------------------------------------------------------------------------------