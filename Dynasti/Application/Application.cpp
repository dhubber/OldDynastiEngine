#include <chrono>
#include <functional>
#include "Dynasti/Application/Application.h"
#include "Dynasti/Core/FrameTimer.h"
#include "Dynasti/Input/InputManager.h"
#include "Dynasti/Scene/SceneManager.h"


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{
    
    Application::Application(const std::string name) : name_(name)
    {
        quit = false;
        
        DYNASTI_LOG("Constructing Application object : " + name);
        frameTimer_ = std::make_shared<FrameTimer>();
        
        assetManager_ = std::make_shared<AssetManager>();
        eventManager_ = std::make_shared<EventManager>();
        inputManager_ = std::make_shared<InputManager>(eventManager_);
        sceneManager_ = std::make_shared<SceneManager>(assetManager_);
        taskManager_ = std::make_unique<TaskManager>();
        
        eventListener_ = std::make_shared<EventListener>(eventManager_);
        eventListener_->SubscribeToEvent("QuitApp", std::bind(&Application::OnQuitApp, this, std::placeholders::_1));
    }
    
    
    Application::~Application()
    {
        DYNASTI_LOG("Destroying Application object : " + name_);
    }
    
    
    void Application::Run()
    {
        while (!quit)
        {
            frameTimer_->Update(std::chrono::high_resolution_clock::now());
            AddTasksToQueue();
            taskManager_->ExecuteAllTasks();
        };
    }
    
}
//---------------------------------------------------------------------------------------------------------------------
