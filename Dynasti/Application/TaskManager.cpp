#include "Dynasti/Application/TaskManager.h"


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{
    
    void TaskManager::AddTaskToQueue(std::shared_ptr<Task> task)
    {
        if (task)
        {
            taskQueue_.push(task);
        }
    }
    
    
    void TaskManager::ExecuteAllTasks()
    {
        while (taskQueue_.size() > 0)
        {
            std::shared_ptr<Task> task = taskQueue_.front();
            taskQueue_.pop();
            task->Execute();
        };
    }
    
}
//---------------------------------------------------------------------------------------------------------------------
