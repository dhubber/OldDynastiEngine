#ifndef DYNASTI_TASK_MANAGER_H
#define DYNASTI_TASK_MANAGER_H


#include <memory>
#include <queue>
#include "Dynasti/Application/Task.h"
#include "Dynasti/Core/Debug.h"


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{
    
    //=================================================================================================================
    /// \brief   Virtual base class for defining all game engine tasks.
    /// \author  D. A. Hubber
    /// \date    13/06/2020
    //=================================================================================================================
    class TaskManager
    {
    public:
        
        TaskManager()
        {
            DYNASTI_LOG("Constructing task manager");
        }
        
        virtual ~TaskManager()
        {
            DYNASTI_LOG("Destroying task manager");
        }

        /// Adds a single task to the tail of the queue
        /// \param[in] task - Pointer to new task being added to the queue
        void AddTaskToQueue(std::shared_ptr<Task> task);
        
        /// Executes all tasks in the task queue in FIFO order
        void ExecuteAllTasks();
        
        /// \return - No. of tasks currently in the queue
        inline int GetNumTasks() const {return taskQueue_.size();}
        
        
    protected:
        
        std::queue<std::shared_ptr<Task>> taskQueue_;     /// Queue of tasks to be performed

    };
    
}
//---------------------------------------------------------------------------------------------------------------------
#endif
