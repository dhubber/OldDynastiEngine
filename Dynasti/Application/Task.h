#ifndef DYNASTI_TASK_H
#define DYNASTI_TASK_H


#include <functional>
#include <string>
//#include <list>


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{
    
    //=================================================================================================================
    /// \brief   Virtual base class for defining all game engine tasks.
    /// \author  D. A. Hubber
    /// \date    13/06/2020
    //=================================================================================================================
    class Task
    {
    public:
        
        Task(std::function<void()> func, const std::string name="task", int priority=-1) :
            taskFunction_(func), name_(name), priority_(priority) {};
        
        /// Executes/runs the function associated with the task
        inline void Execute() { taskFunction_(); }
        
        /// \return - True if task has a valid function to execute; otherwise returns false
        inline bool IsValidTask() const { return (taskFunction_ != nullptr); }
        
        /// Getters for private/protected task properties
        inline const std::string& GetName() const { return name_; }
        inline int GetPriority() const { return priority_; }
    
    
    protected:
    
        std::function<void()> taskFunction_;               ///< Pointer to function to be executed by task
        const std::string name_;                           ///< Human-readable name of task
        const int priority_;                               ///< Priority for task amongst entire task list
        /*int threadId_;                                     ///< Must run on given thread id (unless -1)
        std::list<Task*> dependencies_;                    ///< Pointers to task external
        std::list<Task*> dependantTasks_;                  ///< Array of dependent tasks*/
        
    };
    
}
//---------------------------------------------------------------------------------------------------------------------
#endif
