#ifndef DYNASTI_APPLICATION_H
#define DYNASTI_APPLICATION_H


#include <memory>
#include <string>
#include "Dynasti/Asset/AssetManager.h"
#include "Dynasti/Application/TaskManager.h"
#include "Dynasti/Core/Debug.h"
#include "Dynasti/Event/EventListener.h"
#include "Dynasti/Event/EventManager.h"


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{
    
    // Forward declarations
    class FrameTimer;
    class InputManager;
    class SceneManager;
    
    
    //=================================================================================================================
    /// \brief   Virtual parent base class for defining main application object.
    /// \author  D. A. Hubber
    /// \date    13/06/2020
    //=================================================================================================================
    class Application
    {
    public:
        
        Application(const std::string name="DynastiApp");
        virtual ~Application();
        
        /// Main function to run application (e.g. executing tasks in the TaskManager as part of the main loop)
        virtual void Run();
        
        /// Submits all tasks to be run in this frame to the task manager queue before executing in the main loop
        virtual void AddTasksToQueue() = 0;
        
        /// Set-up the application, allocate memory, create objects, etc..
        virtual void Setup() = 0;
        
        /// Shuts down all application systems, deallocate memory, destroys objects, etc..
        virtual void Shutdown() = 0;
        
        // TODO : Replace this over-simplistic function with something more flexible
        /// Callback function for when the OnQuit event is sent to the applications
        void OnQuitApp(const Event event) { quit = true; }
        
        // Inline functions
        //inline const AppState GetAppState() const {return appState;}
        inline const std::string GetName() const { return name_; }

        
    protected:
    
        const std::string name_;                                 ///< Human-readable application name
        bool quit;                                               ///< Flag if application should quit/shutdown
    
        std::shared_ptr<EventListener> eventListener_;           ///< Event listener for application object
        std::shared_ptr<FrameTimer> frameTimer_;                 ///< Main frame timer object
        
        std::shared_ptr<AssetManager> assetManager_;             ///< Asset manager object
        std::shared_ptr<EventManager> eventManager_;             ///< Event manager for all communication
        std::shared_ptr<InputManager> inputManager_;             ///< Input manager object
        std::shared_ptr<SceneManager> sceneManager_;             ///< Scene manager object
        std::unique_ptr<TaskManager> taskManager_;               ///< Application task manager
    
    };
    
}
//---------------------------------------------------------------------------------------------------------------------
#endif
