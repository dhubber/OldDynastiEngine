#version 460 core

in vec2 textureCoord;
out vec4 outColor;
uniform vec3 color;

void main()
{
  outColor = vec4(color, 1.0f);
}
