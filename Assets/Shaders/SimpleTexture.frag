#version 460 core

in vec2 textureCoord;
out vec4 outColor;
uniform sampler2D meshTexture;

void main()
{
  outColor = texture(meshTexture, textureCoord);
  //outColor = vec4(0.5, 0.0, 0.0, 1.0);
}
