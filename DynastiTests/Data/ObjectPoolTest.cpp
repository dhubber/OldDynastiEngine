#include "gtest/gtest.h"
#include "Dynasti/Data/ObjectPool.h"
#include "Dynasti/Data/Poolable.h"


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{
    
    //=================================================================================================================
    /// \brief   Unit tests of the object pool data structure
    /// \author  D. A. Hubber
    /// \date    22/06/2020
    //=================================================================================================================
    class ObjectPoolTest : public testing::Test
    {
    public:
        
        enum class PoolableState
        {
            NullState,
            Constructed,
            AllocatedInPool,
            Deallocated
        };
    
        template<typename BaseClass>
        class TestPoolableBase : public BaseClass
        {
        public:
            PoolableState state;
            TestPoolableBase() : state(PoolableState::Constructed) {};
            virtual void OnCreate() override {state = PoolableState::AllocatedInPool;}
            virtual void OnDestroy() override {state = PoolableState::Deallocated;}
        };
        
        using TestPoolable = TestPoolableBase<Poolable>;
        //using TestPoolable = TestPoolableBase<Poolable>;
        
        void SetUp() {};
        void TearDown() {};

    };
    
    
    // Verify object pool memory allocation using the constructor
    TEST_F(ObjectPoolTest, Constructor)
    {
        // Verify that an empty pool indeed has no objects and potentially no memory allocated
        ObjectPool<TestPoolable> emptyPool;
        ASSERT_GE(emptyPool.MaxNumObjects(), 0);
        ASSERT_EQ(emptyPool.NumObjects(), 0);
    
        // Verify that an allocated pool has enough memory allocated but no objects assigned
        ObjectPool<TestPoolable> dummyPool(32);
        ASSERT_GE(dummyPool.MaxNumObjects(), 32);
        ASSERT_EQ(dummyPool.NumObjects(), 0);
        for (int i = 0; i < dummyPool.MaxNumObjects(); ++i)
        {
            const TestPoolable& dummyObject = dummyPool[i];
            ASSERT_EQ(dummyObject.state, PoolableState::Constructed);
        }
    }
    
    
    // Verify the functionality of the pool when creating objects that have global ids
    TEST_F(ObjectPoolTest, Poolable)
    {
        ObjectPool<TestPoolable> pool(32);
        
        // Verify creating some objects with an assigned global id
        TestPoolable& obj1 = pool.NewObject(256);
        ASSERT_EQ(obj1.id, 256);
        ASSERT_EQ(obj1.state, PoolableState::AllocatedInPool);
        ASSERT_EQ(pool.NumObjects(), 1);
    
        TestPoolable& obj2 = pool.NewObject(99);
        ASSERT_EQ(obj2.id, 99);
        ASSERT_EQ(obj2.state, PoolableState::AllocatedInPool);
        ASSERT_EQ(pool.NumObjects(), 2);
    
        TestPoolable& obj3 = pool.NewObject(8642);
        ASSERT_EQ(obj3.id, 8642);
        ASSERT_EQ(obj3.state, PoolableState::AllocatedInPool);
        ASSERT_EQ(pool.NumObjects(), 3);
        
        // Verify that we find the correct local ids for the given global ids or -1 for an invalid global id
        ASSERT_EQ(pool.FindLocalId(256), 0);
        ASSERT_EQ(pool.FindLocalId(99), 1);
        ASSERT_EQ(pool.FindLocalId(8642), 2);
        ASSERT_EQ(pool.FindLocalId(0), -1);
        ASSERT_EQ(pool.FindLocalId(12345), -1);
        
        // Verify that we can return the correct pointer by local id
        ASSERT_EQ(pool.FindObjectWithLocalId(0), &obj1);
        ASSERT_EQ(pool.FindObjectWithLocalId(1), &obj2);
        ASSERT_EQ(pool.FindObjectWithLocalId(2), &obj3);
        ASSERT_EQ(pool.FindObjectWithLocalId(3), nullptr);
        ASSERT_EQ(pool.FindObjectWithLocalId(-1), nullptr);
        
        // Verify that we return the correct pointer by global id
        ASSERT_EQ(pool.FindObjectWithGlobalId(256), &obj1);
        ASSERT_EQ(pool.FindObjectWithGlobalId(99), &obj2);
        ASSERT_EQ(pool.FindObjectWithGlobalId(8642), &obj3);
        ASSERT_EQ(pool.FindObjectWithGlobalId(400), nullptr);
        ASSERT_EQ(pool.FindObjectWithGlobalId(-1), nullptr);
        
        // Verify that freeing/destroying an object is correct and maintains a contiguous array
        ASSERT_EQ(pool.FreeObjectWithGlobalId(256), true);
        ASSERT_EQ(pool.NumObjects(), 2);
        ASSERT_EQ(pool.FindLocalId(8642), 0);
        ASSERT_EQ(pool.FindLocalId(99), 1);
        
        // Verify that attempting to free unused objects results in a failure (including previously existing objects)
        ASSERT_EQ(pool.FreeObjectWithGlobalId(256), false);
        ASSERT_EQ(pool.FreeObjectWithGlobalId(0), false);
        
        // Verify clear function, for freeing/destroying all currently allocated objects
        pool.Clear();
        ASSERT_GE(pool.MaxNumObjects(), 32);
        ASSERT_EQ(pool.NumObjects(), 0);
        
    }
    
    
    // Verify the functionality of the pool when creating objects that have global ids
    TEST_F(ObjectPoolTest, ObjectPoolBase)
    {
        ObjectPool<TestPoolable> pool(32);
        ObjectPoolBase &poolBase = pool;
        
        ASSERT_EQ(poolBase.NumObjects(), 0);
        ASSERT_EQ(poolBase.IsLocalIdAssigned(0), false);
    
        // Verify creating an object via ObjectPoolBase interface
        TestPoolable &anonymousObject = pool.NewObject(1);
        ASSERT_EQ(poolBase.NumObjects(), 1);
        ASSERT_EQ(poolBase.IsLocalIdAssigned(0), true);
    
        // Verify that attempting to free an unused object fails
        ASSERT_EQ(poolBase.FreeObject(16), false);
        ASSERT_EQ(poolBase.FreeObject(0), true);
        ASSERT_EQ(poolBase.NumObjects(), 0);
    }

}
//---------------------------------------------------------------------------------------------------------------------
