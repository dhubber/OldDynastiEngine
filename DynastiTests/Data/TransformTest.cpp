#include "gtest/gtest.h"
#include "Dynasti/Transform//Transform3d.h"


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{
    
    //=================================================================================================================
    /// \brief   Unit tests of all entity and entity system functions.
    /// \author  D. A. Hubber
    /// \date    04/04/2018
    //=================================================================================================================
    class TransformTest : public testing::Test
    {
    public:
        
        void SetUp() {};
        void TearDown() {};
        
    };
    
    
    TEST_F(TransformTest, Transform3dConstructor)
    {
        Transform3d transform3d;
        ASSERT_FLOAT_EQ(transform3d.GetPosition().x, 0.0f);
        ASSERT_FLOAT_EQ(transform3d.GetPosition().y, 0.0f);
        ASSERT_FLOAT_EQ(transform3d.GetPosition().z, 0.0f);
        ASSERT_FLOAT_EQ(transform3d.GetScale().x, 1.0f);
        ASSERT_FLOAT_EQ(transform3d.GetScale().y, 1.0f);
        ASSERT_FLOAT_EQ(transform3d.GetScale().z, 1.0f);
    }

}
//---------------------------------------------------------------------------------------------------------------------
