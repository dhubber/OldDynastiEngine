#ifndef DYNASTI_APPLICATION_MOCK_H
#define DYNASTI_APPLICATION_MOCK_H


#include "gmock/gmock.h"
#include "Dynasti/Application/Application.h"


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{
    
    class ApplicationMock : public Application
    {
    public:
        
        ApplicationMock(const std::string _name="ApplicationMock") : Application(_name) {};
        
        MOCK_METHOD(void, OnEvent, (const Event event));
        MOCK_METHOD(void, Setup,());
        MOCK_METHOD(void, AddTasksToQueue, ());
        MOCK_METHOD(void, Shutdown, ());
        
    };
    
}
//---------------------------------------------------------------------------------------------------------------------
#endif
