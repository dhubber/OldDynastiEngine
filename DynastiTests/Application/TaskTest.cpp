#include "gtest/gtest.h"
#include "Dynasti/Application/Task.h"
#include "Dynasti/Application/TaskManager.h"


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{
    
    //=================================================================================================================
    /// \brief   Unit tests of all task classes, functions and uses.
    /// \author  D. A. Hubber
    /// \date    14/06/2020
    //=================================================================================================================
    class TaskTest : public testing::Test
    {
    public:
        
        void SetUp() {};
        void TearDown() {};
        
        void DummyTask() { counter++; }
        
        
    protected:
        
        int counter = 0;

    };
    
    
    /// Verify default values and setting initial values of a task via the constructor
    TEST_F(TaskTest, Constructor)
    {
        Task task1(nullptr);
        ASSERT_EQ(task1.IsValidTask(), false);
        ASSERT_EQ(task1.GetName(), "task");
        ASSERT_EQ(task1.GetPriority(), -1);
        
        Task task2(nullptr, "other_task", 99);
        ASSERT_EQ(task2.IsValidTask(), false);
        ASSERT_EQ(task2.GetName(), "other_task");
        ASSERT_EQ(task2.GetPriority(), 99);
    }
    
    
    TEST_F(TaskTest, Execution)
    {
        Task task(std::bind(&TaskTest::DummyTask, this), "execution_task");
        ASSERT_EQ(task.IsValidTask(), true);
        ASSERT_EQ(counter, 0);
        task.Execute();
        ASSERT_EQ(counter, 1);
        task.Execute();
        ASSERT_EQ(counter, 2);
    }
    
    
    TEST_F(TaskTest, TaskManager)
    {
        std::shared_ptr<Task> task = std::make_shared<Task>(std::bind(&TaskTest::DummyTask, this), "execution_task");
        TaskManager taskManager;
        ASSERT_EQ(taskManager.GetNumTasks(), 0);
        
        taskManager.AddTaskToQueue(task);
        ASSERT_EQ(taskManager.GetNumTasks(), 1);
    
        taskManager.AddTaskToQueue(task);
        ASSERT_EQ(taskManager.GetNumTasks(), 2);
        
        taskManager.ExecuteAllTasks();
        ASSERT_EQ(taskManager.GetNumTasks(), 0);
    }

}
//---------------------------------------------------------------------------------------------------------------------
