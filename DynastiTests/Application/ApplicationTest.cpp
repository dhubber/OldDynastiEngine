#include "gtest/gtest.h"
#include "Dynasti/Application/Application.h"
#include "DynastiTests/Application/ApplicationMock.h"


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{
    
    //=================================================================================================================
    /// \brief   Unit tests of all task classes, functions and uses.
    /// \author  D. A. Hubber
    /// \date    14/06/2020
    //=================================================================================================================
    class ApplicationTest : public testing::Test
    {
    public:
        
        void SetUp() {};
        void TearDown() {};

    };
    
    
    /// Verify the default values for the various transform vectors.
    TEST_F(ApplicationTest, App)
    {
        ApplicationMock app;
        ASSERT_EQ(app.GetName(), "ApplicationMock");
    }

}
//---------------------------------------------------------------------------------------------------------------------
