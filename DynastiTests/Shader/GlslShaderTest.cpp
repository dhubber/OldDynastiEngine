#include "gtest/gtest.h"
#include "Dynasti/Shader/GlslShader.h"


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{
    
    //=================================================================================================================
    /// \brief   Unit tests of all entity and entity system functions.
    /// \author  D. A. Hubber
    /// \date    04/04/2018
    //=================================================================================================================
    class GlslShaderTest : public testing::Test
    {
    public:
        
        void SetUp() {};
        void TearDown() {};

    };
    
    
    /// Verify the default values for the various transform vectors.
    TEST_F(GlslShaderTest, Constructor)
    {
        GlslShader glslShader;
        ASSERT_EQ(glslShader.GetName(), "GlslShader");
    }

}
//---------------------------------------------------------------------------------------------------------------------
