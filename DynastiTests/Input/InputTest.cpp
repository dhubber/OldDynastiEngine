#include "gtest/gtest.h"
#include "GLFW/glfw3.h"
#include <iostream>
#include <memory>
#include "Dynasti/Event/EventManager.h"
#include "Dynasti/Input/InputManager.h"


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{

    //=================================================================================================================
    /// \brief   Basic test of the all event classes
    /// \author  D. A. Hubber
    /// \date    29/06/2020
    //=================================================================================================================
    class InputTest : public testing::Test
    {
    public:

        void SetUp() {};
        void TearDown() {};

    };
    
    
    /// Test all functionality of a sin
    TEST_F(InputTest, Key)
    {
        // Verify member values as set via the constructor
        Key testKey("TestKeyAction", 2, 4);
        ASSERT_EQ(testKey.GetKeyActionName(), "TestKeyAction");
        ASSERT_EQ(testKey.GetKeyDownEventType(), 2);
        ASSERT_EQ(testKey.GetKeyUpEventType(), 4);
        ASSERT_EQ(testKey.IsKeyDown(), false);
        
        // Verify the key-up and key-down functions
        testKey.OnKeyDown();
        ASSERT_EQ(testKey.IsKeyDown(), true);
        testKey.OnKeyUp();
        ASSERT_EQ(testKey.IsKeyDown(), false);
    }


    /// Test all functionality of a sin
    TEST_F(InputTest, InputMap)
    {
        std::shared_ptr<EventManager> eventManager = std::make_shared<EventManager>();
        InputMap inputMap("TestInputMap", eventManager);
        
        // Verify various scan codes are not registered
        ASSERT_EQ(eventManager->GetNumEventTypes(), 0);
        ASSERT_EQ(inputMap.GetKey(static_cast<KeyCode>(GLFW_KEY_A)), nullptr);
        ASSERT_EQ(inputMap.GetKey(static_cast<KeyCode>(GLFW_KEY_SPACE)), nullptr);
        ASSERT_EQ(inputMap.GetKey("Left"), nullptr);
        ASSERT_EQ(inputMap.GetKey("Fire"), nullptr);
        
        // Register some key actions and verify the keys are correctly created
        ASSERT_EQ(inputMap.RegisterKeyActionEvent("Left", static_cast<KeyCode>(GLFW_KEY_A)), true);
        ASSERT_EQ(inputMap.RegisterKeyActionEvent("Fire", static_cast<KeyCode>(GLFW_KEY_SPACE)), true);
        
        std::shared_ptr<Key> leftKey = inputMap.GetKey("Left");
        std::shared_ptr<Key> fireKey = inputMap.GetKey("Fire");
        ASSERT_NE(leftKey, nullptr);
        ASSERT_NE(fireKey, nullptr);
        ASSERT_EQ(inputMap.GetKey(static_cast<KeyCode>(GLFW_KEY_A)), leftKey);
        ASSERT_EQ(inputMap.GetKey(static_cast<KeyCode>(GLFW_KEY_SPACE)), fireKey);
        
        // Verify the key-up and key-down events are correctly registered with the event manager
        ASSERT_EQ(eventManager->GetNumEventTypes(), 4);
        ASSERT_EQ(eventManager->GetEventType("LeftKeyDown"), leftKey->GetKeyDownEventType());
        ASSERT_EQ(eventManager->GetEventType("LeftKeyUp"), leftKey->GetKeyUpEventType());
        ASSERT_EQ(eventManager->GetEventType("FireKeyDown"), fireKey->GetKeyDownEventType());
        ASSERT_EQ(eventManager->GetEventType("FireKeyUp"), fireKey->GetKeyUpEventType());
    
        // Verify key-up and key-down events are correctly broadcast
        int keyDownCount = 0;
        int keyUpCount = 0;
        std::shared_ptr<EventListener> eventListener_ = std::make_shared<EventListener>(eventManager);
        eventListener_->SubscribeToEvent("LeftKeyDown", std::bind([&keyDownCount]{ ++keyDownCount; }));
        eventListener_->SubscribeToEvent("LeftKeyUp", std::bind([&keyUpCount]{ ++keyUpCount; }));
        ASSERT_EQ(leftKey->IsKeyDown(), false);
        inputMap.OnKeyDown(static_cast<KeyCode>(GLFW_KEY_A));
        ASSERT_EQ(leftKey->IsKeyDown(), true);
        ASSERT_EQ(keyDownCount, 1);
        ASSERT_EQ(keyUpCount, 0);
        inputMap.OnKeyUp(static_cast<KeyCode>(GLFW_KEY_A));
        ASSERT_EQ(leftKey->IsKeyDown(), false);
        ASSERT_EQ(keyDownCount, 1);
        ASSERT_EQ(keyUpCount, 1);
    }
    
    
    /// Test all functionality of the Input Manager
    TEST_F(InputTest, InputManager)
    {
        // Verify there are no existing input maps when creating the input manager
        std::shared_ptr<EventManager> eventManager = std::make_shared<EventManager>();
        InputManager inputManager(eventManager);
        ASSERT_EQ(inputManager.GetActiveInputMap(), nullptr);

        // Verify that the first created input map is active by default
        std::shared_ptr<InputMap> inputMap1 = inputManager.CreateNewInputMapping("InputMap1");
        ASSERT_EQ(inputManager.GetActiveInputMap(), inputMap1);
        ASSERT_EQ(inputManager.GetInputMap("InputMap1"), inputMap1);
        ASSERT_EQ(inputManager.GetInputMap("AnotherInputMap"), nullptr);
    
        // Verify that creating further maps does not change the active map
        std::shared_ptr<InputMap> inputMap2 = inputManager.CreateNewInputMapping("AnotherInputMap");
        ASSERT_EQ(inputManager.GetActiveInputMap(), inputMap1);
        ASSERT_EQ(inputManager.GetInputMap("InputMap1"), inputMap1);
        ASSERT_EQ(inputManager.GetInputMap("AnotherInputMap"), inputMap2);
        
        // Verify functions for changing the active map
        ASSERT_EQ(inputManager.SelectActiveMap("NonExistingInputMap"), false);
        ASSERT_EQ(inputManager.GetActiveInputMap(), inputMap1);
        ASSERT_EQ(inputManager.SelectActiveMap("AnotherInputMap"), true);
        ASSERT_EQ(inputManager.GetActiveInputMap(), inputMap2);
        
        // Create key for a map and verify correct state when using inputManager functions
        inputMap2->RegisterKeyActionEvent("Fire", static_cast<KeyCode>(GLFW_KEY_SPACE));
        std::shared_ptr<Key> fireKey = inputMap2->GetKey("Fire");
        ASSERT_EQ(fireKey->IsKeyDown(), false);
        inputManager.OnKeyDown(static_cast<KeyCode>(GLFW_KEY_SPACE));
        ASSERT_EQ(fireKey->IsKeyDown(), true);
        inputManager.OnKeyUp(static_cast<KeyCode>(GLFW_KEY_SPACE));
        ASSERT_EQ(fireKey->IsKeyDown(), false);
    }
    
}
//---------------------------------------------------------------------------------------------------------------------
