#include "gtest/gtest.h"
#include "Dynasti/Asset/AssetRegistry.h"


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{
    
    //=================================================================================================================
    /// \brief   Unit tests of all Asset classes.
    /// \author  D. A. Hubber
    /// \date    24/08/2020
    //=================================================================================================================
    class AssetTest : public testing::Test
    {
    public:
        
        void SetUp() {};
        void TearDown() {};

    };
    
    
    /// Verify default values for default constructor with new Camera object
    TEST_F(AssetTest, Asset)
    {
        // Verify default Asset constructor
        Asset dummyAsset;
        ASSERT_EQ(dummyAsset.GetFullPath(), "");
        ASSERT_EQ(dummyAsset.GetId(), -1);
        ASSERT_EQ(dummyAsset.GetName(), "");
        
        // Verify basic Asset class getter and setter functions
        dummyAsset.SetFullPath("dummy/full/path");
        dummyAsset.SetId(54321);
        dummyAsset.SetName("someRandomName");
        ASSERT_EQ(dummyAsset.GetFullPath(), "dummy/full/path");
        ASSERT_EQ(dummyAsset.GetId(), 54321);
        ASSERT_EQ(dummyAsset.GetName(), "someRandomName");
    }
    
    
    /// Verify default values for default constructor with new Camera object
    TEST_F(AssetTest, AssetRegistry)
    {
        // Verify behaviour for empty registry
        AssetRegistry<Asset> dummyAssetRegistry;
        ASSERT_EQ(dummyAssetRegistry.NumRegisteredAssets(), 0);
        ASSERT_EQ(dummyAssetRegistry.Find(0), nullptr);
        ASSERT_EQ(dummyAssetRegistry.Find("DummyAsset"), nullptr);
        ASSERT_EQ(dummyAssetRegistry.FindImported("some/random/path"), nullptr);
        
        // Verify adding a single asset correctly affects both asset and registry
        std::shared_ptr<Asset> dummyAsset = std::make_shared<Asset>();
        dummyAssetRegistry.Register(dummyAsset, "DummyAsset", "some/random/path");
        ASSERT_EQ(dummyAsset->GetId(), 0);
        ASSERT_EQ(dummyAsset->GetName(), "DummyAsset");
        ASSERT_EQ(dummyAsset->GetFullPath(), "some/random/path");
        ASSERT_EQ(dummyAssetRegistry.NumRegisteredAssets(), 1);
        ASSERT_EQ(dummyAssetRegistry.Find(0), dummyAsset);
        ASSERT_EQ(dummyAssetRegistry.Find("DummyAsset"), dummyAsset);
        ASSERT_EQ(dummyAssetRegistry.FindImported("some/random/path"), dummyAsset);
    
        std::shared_ptr<Asset> dummyAsset2 = std::make_shared<Asset>();
        dummyAssetRegistry.Register(dummyAsset2, "DummyAsset2", "some/random/path/asset2");
        ASSERT_EQ(dummyAsset2->GetId(), 1);
        ASSERT_EQ(dummyAsset2->GetName(), "DummyAsset2");
        ASSERT_EQ(dummyAsset2->GetFullPath(), "some/random/path/asset2");
        ASSERT_EQ(dummyAssetRegistry.NumRegisteredAssets(), 2);
        ASSERT_EQ(dummyAssetRegistry.Find(1), dummyAsset2);
        ASSERT_EQ(dummyAssetRegistry.Find("DummyAsset2"), dummyAsset2);
        ASSERT_EQ(dummyAssetRegistry.FindImported("some/random/path/asset2"), dummyAsset2);
        
        // Verify that removing a non existing asset fails
        ASSERT_EQ(dummyAssetRegistry.Unregister("FakeAsset"), false);
        ASSERT_EQ(dummyAssetRegistry.Unregister(234), false);
        ASSERT_EQ(dummyAssetRegistry.NumRegisteredAssets(), 2);
        
        // Verify that removing a registered asset works the first time but fails subsequently
        ASSERT_EQ(dummyAssetRegistry.Unregister("DummyAsset"), true);
        ASSERT_EQ(dummyAssetRegistry.NumRegisteredAssets(), 1);
        ASSERT_EQ(dummyAssetRegistry.Unregister("DummyAsset"), false);
        ASSERT_EQ(dummyAssetRegistry.NumRegisteredAssets(), 1);
        
        // Verify unregister all function
        dummyAssetRegistry.UnregisterAllAssets();
        ASSERT_EQ(dummyAssetRegistry.NumRegisteredAssets(), 0);
    }
    
}
//---------------------------------------------------------------------------------------------------------------------
