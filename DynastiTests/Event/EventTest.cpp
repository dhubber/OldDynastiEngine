#include "gtest/gtest.h"
#include <iostream>
#include <memory>
#include "Dynasti/Event/Event.h"
#include "Dynasti/Event/EventListener.h"
#include "Dynasti/Event/EventManager.h"


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{

    //=================================================================================================================
    /// \brief   Basic test of the all event classes
    /// \author  D. A. Hubber
    /// \date    29/06/2020
    //=================================================================================================================
    class EventTest : public testing::Test
    {
    public:

        void SetUp() {};
        void TearDown() {};

    };


    /// Test an individual log file object in isolation.
    TEST_F(EventTest, Event)
    {
        Event event1;
        ASSERT_EQ(event1.GetType(), -1);
        ASSERT_EQ(event1.GetId(), -1);

        Event event2(16);
        ASSERT_EQ(event2.GetType(), 16);
        ASSERT_EQ(event2.GetId(), -1);

        Event event3(24, 1234);
        ASSERT_EQ(event3.GetType(), 24);
        ASSERT_EQ(event3.GetId(), 1234);
    }


    /// Verify basic usage of the event manager including registering new event types
    TEST_F(EventTest, EventManager)
    {
        // Verify default state of event manager
        EventManager eventManager;
        ASSERT_EQ(eventManager.GetNumEventTypes(), 0);
        
        // Register some events
        EventType dummyEventType = eventManager.RegisterEventName("DummyEvent");
        ASSERT_EQ(eventManager.GetNumEventTypes(), 1);
        ASSERT_EQ(eventManager.GetNumEventSubscribers(dummyEventType), 0);
    
        EventType dummyEvent2Type = eventManager.RegisterEventName("DummyEvent2");
        ASSERT_EQ(eventManager.GetNumEventTypes(), 2);
        ASSERT_EQ(eventManager.GetNumEventSubscribers(dummyEvent2Type), 0);
        
        // Verify we cannot register the same event more than once
        EventType duplicateEventType = eventManager.RegisterEventName("DummyEvent");
        ASSERT_EQ(eventManager.GetNumEventTypes(), 2);
        ASSERT_EQ(duplicateEventType, dummyEventType);
    }
    
    
    /// Verify the event manager singleton behaves correctly.
    TEST_F(EventTest, EventListener)
    {
        int x = 1;
        int y = 1;
        
        std::shared_ptr<EventManager> eventManager = std::make_shared<EventManager>();
        EventType dummyEventType = eventManager->RegisterEventName("Dummy");
        ASSERT_EQ(eventManager->GetNumEventTypes(), 1);
        ASSERT_EQ(eventManager->GetNumEventSubscribers(dummyEventType), 0);
        
        // Register an event listener with an existing event type
        std::shared_ptr<EventListener> eventListener = std::make_shared<EventListener>(eventManager, "EM1");
        eventListener->SubscribeToEvent("Dummy", [](const Event event) {});
        ASSERT_EQ(eventManager->GetNumEventTypes(), 1);
        ASSERT_EQ(eventManager->GetNumEventSubscribers(dummyEventType), 1);
        
        // Register a new event type via the event listener
        EventType multiplyEventType = eventListener->SubscribeToEvent("Multiply",
            [&x](const Event event) { x *= event.GetId(); });
        ASSERT_EQ(eventManager->GetNumEventTypes(), 2);
        ASSERT_EQ(eventManager->GetNumEventSubscribers(dummyEventType), 1);
        ASSERT_EQ(eventManager->GetNumEventSubscribers(multiplyEventType), 1);
        
        // Verify event broadcast to a single listener
        Event multiplyEvent(multiplyEventType, 64);
        eventManager->BroadcastEvent(multiplyEvent);
        ASSERT_EQ(x, 64);
        ASSERT_EQ(y, 1);
    
        // Register a 2nd event event listener with an existing event type
        std::shared_ptr<EventListener> eventListener2 = std::make_shared<EventListener>(eventManager, "EM2");
        eventListener2->SubscribeToEvent("Multiply", [&y](const Event event) { y *= event.GetId(); });
        ASSERT_EQ(eventManager->GetNumEventTypes(), 2);
        ASSERT_EQ(eventManager->GetNumEventSubscribers(dummyEventType), 1);
        ASSERT_EQ(eventManager->GetNumEventSubscribers(multiplyEventType), 2);
    
        // Verify event broadcast to multiple listeners
        Event multiply2Event(multiplyEventType, 4);
        eventManager->BroadcastEvent(multiply2Event);
        ASSERT_EQ(x, 256);
        ASSERT_EQ(y, 4);
        
        // Unsubscribe from an event
        eventManager->UnsubscribeToEvent("Multiply", eventListener);
        ASSERT_EQ(eventManager->GetNumEventTypes(), 2);
        ASSERT_EQ(eventManager->GetNumEventSubscribers(dummyEventType), 1);
        ASSERT_EQ(eventManager->GetNumEventSubscribers(multiplyEventType), 1);
    }

}
//---------------------------------------------------------------------------------------------------------------------
