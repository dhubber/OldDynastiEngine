#ifndef DYNASTI_APPLICATION_MOCK_H
#define DYNASTI_APPLICATION_MOCK_H


#include "gmock/gmock.h"
#include "Dynasti/Event/EventListener.h"


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{
    
    class EventListenerMock : public EventListener
    {
    public:
        
        EventListenerMock(std::shared_ptr<EventManager> eventManager) : EventListener() {};
        
        MOCK_METHOD(void, OnEvent, (const Event event), (override));
    };
    
}
//---------------------------------------------------------------------------------------------------------------------
#endif
