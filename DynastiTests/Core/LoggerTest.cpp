#include "gtest/gtest.h"
#include <iostream>
#include "Dynasti/Core/Logger.h"


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{

    //=================================================================================================================
    /// \brief   Basic test of the log-file class and the main singleton log-class.
    /// \author  D. A. Hubber
    /// \date    26/01/2018
    //=================================================================================================================
    class LoggerTest : public testing::Test
    {
    public:

        void SetUp() {};
        void TearDown() {};

    };


    /// Test an individual log file object in isolation.
    TEST_F(LoggerTest, LogFile)
    {
        LogFile logFile;
        ASSERT_EQ(logFile.IsFileOpen(), false);
        ASSERT_EQ(logFile.GetFileName(), "");
        ASSERT_EQ(logFile.GetLastMessage(), "");

        logFile.Open("test.log");
        ASSERT_EQ(logFile.IsFileOpen(), true);
        ASSERT_EQ(logFile.GetFileName(), "test.log");
        
        logFile.WriteMessage("A simple test message");
        ASSERT_EQ(logFile.GetLastMessage(), "A simple test message");

        logFile.Close();
        ASSERT_EQ(logFile.IsFileOpen(), false);
        ASSERT_EQ(logFile.GetFileName(), "");
    }


    /// Verify the logger singleton behaves correctly.
    TEST_F(LoggerTest, Logger)
    {
        Logger &logger = Logger::GetInstance("DynastiUnitTests");
        ASSERT_EQ(logger.IsInitialised(), false);
        ASSERT_EQ(logger.GetLogMode(), LogMode::LOG_VERBOSE);
        ASSERT_EQ(logger.GetLogTag(), "DynastiUnitTests");
        ASSERT_EQ(logger.GetMainLogFilename(), "");
        ASSERT_EQ(logger.GetTerminalMode(), LogMode::LOG_NULL);

        // Verify logger is a singleton by trying (and failing) to create a second separate instance
        Logger &logger2 = Logger::GetInstance();
        ASSERT_EQ(&logger, &logger2);
        
        // Verify changing logging settings work correctly
        logger.SetLogMode(LogMode::LOG_BASIC);
        logger.SetTerminalMode(LogMode::LOG_BASIC);
        ASSERT_EQ(logger.GetLogMode(), LogMode::LOG_BASIC);
        ASSERT_EQ(logger.GetTerminalMode(), LogMode::LOG_BASIC);
    
        // Switching off terminal log output to stop spamming console during unit tests
        logger.SetLogMode(LogMode::LOG_VERBOSE);
        logger.SetTerminalMode(LogMode::LOG_NULL);
    }

}
//---------------------------------------------------------------------------------------------------------------------
