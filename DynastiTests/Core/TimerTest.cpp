#include "gtest/gtest.h"
#include <chrono>
#include <iostream>
#include <thread>
#include "Dynasti/Core/Timer.h"


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{

    //=================================================================================================================
    /// \brief   Basic unit tests of all internal code timing classes.
    /// \author  D. A. Hubber
    /// \date    26/01/2018
    //=================================================================================================================
    class TimerTest : public testing::Test
    {
    public:

        void SetUp() {};
        void TearDown() {};

    };
    
    
    /// Verify the Timer singleton behaves correctly.
    TEST_F(TimerTest, TimerSingleton)
    {
        // Verify default values when creating singleton
        Timer &timer = Timer::GetInstance();
        ASSERT_EQ(timer.GetFilename(), "dynasti.timing");
        ASSERT_EQ(timer.GetNumTimerBlocks(), 0);
        
        // Verify that the singleton is indeed a singleton
        Timer &timer2 = Timer::GetInstance();
        ASSERT_EQ(&timer, &timer2);
        
        // Verify that setters work correctly
        timer.SetFilename("testfile.timing");
        ASSERT_EQ(timer.GetFilename(), "testfile.timing");
    }


    /// Test the basic functionality of an individual TimerBlock object
    TEST_F(TimerTest, TimerBlock)
    {
        Timer &timer = Timer::GetInstance();
        {
            TimerBlock timerBlock("testblock");
            ASSERT_EQ(timerBlock.GetName(), "testblock");
            ASSERT_EQ(timerBlock.GetThreadId(), std::this_thread::get_id());
            ASSERT_EQ(timerBlock.IsRunning(), true);
            ASSERT_FLOAT_EQ(timerBlock.GetTotalTime(), 0.0);
        
            std::this_thread::sleep_for(std::chrono::nanoseconds(10));
            timerBlock.StopTimer();
            ASSERT_GT(timerBlock.GetTotalTime(), 0.0);
            
            // Verify that timer block is not yet registered by singleton
            ASSERT_EQ(timer.GetNumTimerBlocks(), 0);
        }
    
        // Verify that timer block is registered with timer singleton correctly after destruction
        ASSERT_EQ(timer.GetNumTimerBlocks(), 1);
    }

}
//---------------------------------------------------------------------------------------------------------------------
