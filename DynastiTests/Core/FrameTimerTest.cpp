#include "gtest/gtest.h"
#include <chrono>
#include <iostream>
#include <thread>
#include "Dynasti/Core/FrameTimer.h"


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{

    //=================================================================================================================
    /// \brief   Basic unit tests of the main frame timer class.
    /// \author  D. A. Hubber
    /// \date    20/11/2020
    //=================================================================================================================
    class FrameTimerTest : public testing::Test
    {
    public:

        void SetUp() {};
        void TearDown() {};

    };
    
    
    /// Verify the Timer singleton behaves correctly.
    TEST_F(FrameTimerTest, Constructor)
    {
        FrameTimer frameTimer;
        ASSERT_FLOAT_EQ(frameTimer.GetTime(), 0.0);
        ASSERT_FLOAT_EQ(frameTimer.GetFrameInterval(), 0.0);
        ASSERT_FLOAT_EQ(frameTimer.GetFps(), 0.0);
        ASSERT_EQ(frameTimer.GetNumFrames(), 0);
    }
    
    
    /// Verify the Timer singleton behaves correctly.
    TEST_F(FrameTimerTest, Functions)
    {
        FrameTimer frameTimer;
        ASSERT_FLOAT_EQ(frameTimer.GetTime(), 0.0);
        ASSERT_FLOAT_EQ(frameTimer.GetFps(), 0.0);
        ASSERT_EQ(frameTimer.GetNumFrames(), 0);

        frameTimer.Update(std::chrono::high_resolution_clock::now());
        ASSERT_GT(frameTimer.GetTime(), 0.0);
        ASSERT_GT(frameTimer.GetFrameInterval(), 0.0);
        ASSERT_EQ(frameTimer.GetNumFrames(), 1);
    
        frameTimer.Update(std::chrono::high_resolution_clock::now());
        ASSERT_GT(frameTimer.GetTime(), 0.0);
        ASSERT_GT(frameTimer.GetFrameInterval(), 0.0);
        ASSERT_EQ(frameTimer.GetNumFrames(), 2);
    }
    
}
//---------------------------------------------------------------------------------------------------------------------
