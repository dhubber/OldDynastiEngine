#include "gtest/gtest.h"
#include "Dynasti/Mesh/Vertex.h"


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{
    
    //=================================================================================================================
    /// \brief   Unit tests of all mesh classes and functions.
    /// \author  D. A. Hubber
    /// \date    03/08/2020
    //=================================================================================================================
    class VertexTest : public testing::Test
    {
    public:
        
        void SetUp() {};
        void TearDown() {};

    };
    
    
    /// Verify the basic constructors for the VertexFormat class
    TEST_F(VertexTest, VertexConstructors)
    {
        Vertex vertex1(glm::vec2(1.0f, 2.0f));
        ASSERT_FLOAT_EQ(vertex1.pos2.x, 1.0f);
        ASSERT_FLOAT_EQ(vertex1.pos2.y, 2.0f);
    
        Vertex vertex2(glm::vec2(3.0f, 4.0f), glm::vec2(5.0f, 6.0f));
        ASSERT_FLOAT_EQ(vertex2.pos2.x, 3.0f);
        ASSERT_FLOAT_EQ(vertex2.pos2.y, 4.0f);
        ASSERT_FLOAT_EQ(vertex2.uv.x, 5.0f);
        ASSERT_FLOAT_EQ(vertex2.uv.y, 6.0f);
    
        Vertex vertex3(glm::vec3(7.0f, 8.0f, 9.0f));
        ASSERT_FLOAT_EQ(vertex3.pos3.x, 7.0f);
        ASSERT_FLOAT_EQ(vertex3.pos3.y, 8.0f);
        ASSERT_FLOAT_EQ(vertex3.pos3.z, 9.0f);
    
        Vertex vertex4(glm::vec3(10.0f, 11.0f, 12.0f), glm::vec3(13.0f, 14.0f, 15.0f));
        ASSERT_FLOAT_EQ(vertex4.pos3.x, 10.0f);
        ASSERT_FLOAT_EQ(vertex4.pos3.y, 11.0f);
        ASSERT_FLOAT_EQ(vertex4.pos3.z, 12.0f);
        ASSERT_FLOAT_EQ(vertex4.norm.x, 13.0f);
        ASSERT_FLOAT_EQ(vertex4.norm.y, 14.0f);
        ASSERT_FLOAT_EQ(vertex4.norm.z, 15.0f);
    
        Vertex vertex5(glm::vec3(16.0f, 17.0f, 18.0f), glm::vec3(19.0f, 20.0f, 21.0f), glm::vec2(22.0f, 23.0f));
        ASSERT_FLOAT_EQ(vertex5.pos3.x, 16.0f);
        ASSERT_FLOAT_EQ(vertex5.pos3.y, 17.0f);
        ASSERT_FLOAT_EQ(vertex5.pos3.z, 18.0f);
        ASSERT_FLOAT_EQ(vertex5.norm.x, 19.0f);
        ASSERT_FLOAT_EQ(vertex5.norm.y, 20.0f);
        ASSERT_FLOAT_EQ(vertex5.norm.z, 21.0f);
        ASSERT_FLOAT_EQ(vertex5.uv.x, 22.0f);
        ASSERT_FLOAT_EQ(vertex5.uv.y, 23.0f);
    }
    
    
    /// Verify the basic constructors for the VertexFormat class
    TEST_F(VertexTest, VertexFormatConstructor)
    {
        VertexFormat format1;
        ASSERT_EQ(format1.GetFormatType(), VertexFormatType::INVALID_FORMAT);
        ASSERT_EQ(format1.GetVertexSize(), 0);
        ASSERT_EQ(format1.NumAttributes(), 0);
        
        VertexFormat format2(VertexFormatType::INTERLEAVED);
        ASSERT_EQ(format2.GetFormatType(), VertexFormatType::INTERLEAVED);
        ASSERT_EQ(format2.GetVertexSize(), 0);
        ASSERT_EQ(format2.NumAttributes(), 0);
    
        VertexFormat format3(VertexFormatType::BLOCKED);
        ASSERT_EQ(format3.GetFormatType(), VertexFormatType::BLOCKED);
        ASSERT_EQ(format3.GetVertexSize(), 0);
        ASSERT_EQ(format3.NumAttributes(), 0);
    }
    
    
    /// Verify the sizes for all possible enumerated attribute types
    TEST_F(VertexTest, AttributeSize)
    {
        ASSERT_EQ(VertexFormat::AttributeSize(VertexAttributeType::POSITION2), 2);
        ASSERT_EQ(VertexFormat::AttributeSize(VertexAttributeType::UV2), 2);
        ASSERT_EQ(VertexFormat::AttributeSize(VertexAttributeType::UV2B), 2);
    
        ASSERT_EQ(VertexFormat::AttributeSize(VertexAttributeType::POSITION3), 3);
        ASSERT_EQ(VertexFormat::AttributeSize(VertexAttributeType::NORMAL3), 3);
        ASSERT_EQ(VertexFormat::AttributeSize(VertexAttributeType::COLOR3), 3);
    
        ASSERT_EQ(VertexFormat::AttributeSize(VertexAttributeType::COLOR4), 4);
        ASSERT_EQ(VertexFormat::AttributeSize(VertexAttributeType::TANGENT4), 4);
        ASSERT_EQ(VertexFormat::AttributeSize(VertexAttributeType::JOINTS4), 4);
        ASSERT_EQ(VertexFormat::AttributeSize(VertexAttributeType::WEIGHTS4), 4);
    }
    
    
    /// Verify function for adding attributes one at a time to VertexFormat object
    TEST_F(VertexTest, AddAttribute)
    {
        VertexFormat format;
        ASSERT_EQ(format.NumAttributes(), 0);
        ASSERT_EQ(format.GetVertexSize(), 0);
        ASSERT_EQ(format.FindAttributeIndex(VertexAttributeType::POSITION2), -1);
        ASSERT_EQ(format.FindAttributeIndex(VertexAttributeType::COLOR3), -1);
        ASSERT_EQ(format.FindAttributeIndex(VertexAttributeType::UV2), -1);
        
        format.AddNewAttribute(VertexAttributeType::POSITION2);
        ASSERT_EQ(format.NumAttributes(), 1);
        ASSERT_EQ(format.GetVertexSize(), 2);
        ASSERT_EQ(format.FindAttributeIndex(VertexAttributeType::POSITION2), 0);
        ASSERT_EQ(format.FindAttributeIndex(VertexAttributeType::COLOR3), -1);
        ASSERT_EQ(format.FindAttributeIndex(VertexAttributeType::UV2), -1);
    
        format.AddNewAttribute(VertexAttributeType::COLOR3);
        ASSERT_EQ(format.NumAttributes(), 2);
        ASSERT_EQ(format.GetVertexSize(), 5);
        ASSERT_EQ(format.FindAttributeIndex(VertexAttributeType::POSITION2), 0);
        ASSERT_EQ(format.FindAttributeIndex(VertexAttributeType::COLOR3), 1);
        ASSERT_EQ(format.FindAttributeIndex(VertexAttributeType::UV2), -1);

        format.AddNewAttribute(VertexAttributeType::UV2);
        ASSERT_EQ(format.NumAttributes(), 3);
        ASSERT_EQ(format.GetVertexSize(), 7);
        ASSERT_EQ(format.FindAttributeIndex(VertexAttributeType::POSITION2), 0);
        ASSERT_EQ(format.FindAttributeIndex(VertexAttributeType::COLOR3), 1);
        ASSERT_EQ(format.FindAttributeIndex(VertexAttributeType::UV2), 2);
    }
    
    
    /// Verify creating an interleaved vertex format
    TEST_F(VertexTest, InterleavedFormat)
    {
        VertexFormat interleavedFormat(VertexFormatType::INTERLEAVED,
                                       {VertexAttributeType::POSITION3, VertexAttributeType::NORMAL3, VertexAttributeType::UV2});
        ASSERT_EQ(interleavedFormat.NumAttributes(), 3);
        ASSERT_EQ(interleavedFormat.GetVertexSize(), 8);
        ASSERT_EQ(interleavedFormat.FindAttributeIndex(VertexAttributeType::POSITION3), 0);
        ASSERT_EQ(interleavedFormat.FindAttributeIndex(VertexAttributeType::NORMAL3), 1);
        ASSERT_EQ(interleavedFormat.FindAttributeIndex(VertexAttributeType::UV2), 2);
    
        ASSERT_EQ(interleavedFormat.GetAttributeOffset(VertexAttributeType::POSITION3), 0);
        ASSERT_EQ(interleavedFormat.GetAttributeOffset(VertexAttributeType::NORMAL3), 3);
        ASSERT_EQ(interleavedFormat.GetAttributeOffset(VertexAttributeType::UV2), 6);
    
        ASSERT_EQ(interleavedFormat.GetAttributeOffset(0), 0);
        ASSERT_EQ(interleavedFormat.GetAttributeOffset(1), 3);
        ASSERT_EQ(interleavedFormat.GetAttributeOffset(2), 6);
        
        ASSERT_EQ(VertexFormat::VertexBufferIndex(VertexAttributeType::POSITION3, 0, 64, interleavedFormat), 0);
        ASSERT_EQ(VertexFormat::VertexBufferIndex(VertexAttributeType::POSITION3, 1, 64, interleavedFormat), 8*1);
        ASSERT_EQ(VertexFormat::VertexBufferIndex(VertexAttributeType::POSITION3, 8, 64, interleavedFormat), 8*8);
        ASSERT_EQ(VertexFormat::VertexBufferIndex(VertexAttributeType::POSITION3, 63, 64, interleavedFormat), 8*63);
    
        ASSERT_EQ(VertexFormat::VertexBufferIndex(VertexAttributeType::NORMAL3, 0, 64, interleavedFormat), 3);
        ASSERT_EQ(VertexFormat::VertexBufferIndex(VertexAttributeType::NORMAL3, 1, 64, interleavedFormat), 8*1 + 3);
        ASSERT_EQ(VertexFormat::VertexBufferIndex(VertexAttributeType::NORMAL3, 8, 64, interleavedFormat), 8*8 + 3);
        ASSERT_EQ(VertexFormat::VertexBufferIndex(VertexAttributeType::NORMAL3, 63, 64, interleavedFormat), 8*63 + 3);
    
        ASSERT_EQ(VertexFormat::VertexBufferIndex(VertexAttributeType::UV2, 0, 64, interleavedFormat), 6);
        ASSERT_EQ(VertexFormat::VertexBufferIndex(VertexAttributeType::UV2, 1, 64, interleavedFormat), 8*1 + 6);
        ASSERT_EQ(VertexFormat::VertexBufferIndex(VertexAttributeType::UV2, 8, 64, interleavedFormat), 8*8 + 6);
        ASSERT_EQ(VertexFormat::VertexBufferIndex(VertexAttributeType::UV2, 63, 64, interleavedFormat), 8*63 + 6);
    }


    /// Verify creating a blocked vertex format
    TEST_F(VertexTest, BlockedFormat)
    {
        VertexFormat blockedFormat(VertexFormatType::BLOCKED,
                                   {VertexAttributeType::POSITION3, VertexAttributeType::NORMAL3, VertexAttributeType::UV2});
        ASSERT_EQ(blockedFormat.NumAttributes(), 3);
        ASSERT_EQ(blockedFormat.GetVertexSize(), 8);
        ASSERT_EQ(blockedFormat.FindAttributeIndex(VertexAttributeType::POSITION3), 0);
        ASSERT_EQ(blockedFormat.FindAttributeIndex(VertexAttributeType::NORMAL3), 1);
        ASSERT_EQ(blockedFormat.FindAttributeIndex(VertexAttributeType::UV2), 2);
        
        ASSERT_EQ(blockedFormat.GetAttributeOffset(VertexAttributeType::POSITION3), 0);
        ASSERT_EQ(blockedFormat.GetAttributeOffset(VertexAttributeType::NORMAL3), 3);
        ASSERT_EQ(blockedFormat.GetAttributeOffset(VertexAttributeType::UV2), 6);
        
        ASSERT_EQ(blockedFormat.GetAttributeOffset(0), 0);
        ASSERT_EQ(blockedFormat.GetAttributeOffset(1), 3);
        ASSERT_EQ(blockedFormat.GetAttributeOffset(2), 6);
        
        ASSERT_EQ(VertexFormat::VertexBufferIndex(VertexAttributeType::POSITION3, 0, 64, blockedFormat), 0);
        ASSERT_EQ(VertexFormat::VertexBufferIndex(VertexAttributeType::POSITION3, 1, 64, blockedFormat), 3*1);
        ASSERT_EQ(VertexFormat::VertexBufferIndex(VertexAttributeType::POSITION3, 8, 64, blockedFormat), 3*8);
        ASSERT_EQ(VertexFormat::VertexBufferIndex(VertexAttributeType::POSITION3, 63, 64, blockedFormat), 3*63);
        
        ASSERT_EQ(VertexFormat::VertexBufferIndex(VertexAttributeType::NORMAL3, 0, 64, blockedFormat), 3*64);
        ASSERT_EQ(VertexFormat::VertexBufferIndex(VertexAttributeType::NORMAL3, 1, 64, blockedFormat), 3*64 + 3*1);
        ASSERT_EQ(VertexFormat::VertexBufferIndex(VertexAttributeType::NORMAL3, 8, 64, blockedFormat), 3*64 + 3*8);
        ASSERT_EQ(VertexFormat::VertexBufferIndex(VertexAttributeType::NORMAL3, 63, 64, blockedFormat), 3*64 + 3*63);
        
        ASSERT_EQ(VertexFormat::VertexBufferIndex(VertexAttributeType::UV2, 0, 64, blockedFormat), 6*64);
        ASSERT_EQ(VertexFormat::VertexBufferIndex(VertexAttributeType::UV2, 1, 64, blockedFormat), 6*64 + 2*1);
        ASSERT_EQ(VertexFormat::VertexBufferIndex(VertexAttributeType::UV2, 8, 64, blockedFormat), 6*64 + 2*8);
        ASSERT_EQ(VertexFormat::VertexBufferIndex(VertexAttributeType::UV2, 63, 64, blockedFormat), 6*64 + 2*63);
    }

}
//---------------------------------------------------------------------------------------------------------------------
