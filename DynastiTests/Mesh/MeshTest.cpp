#include "gtest/gtest.h"
#include "Dynasti/Mesh/Mesh.h"
//#include "Dynasti/Mesh/Mesh3d.h"


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{
    
    //=================================================================================================================
    /// \brief   Unit tests of all mesh classes and functions.
    /// \author  D. A. Hubber
    /// \date    03/08/2020
    //=================================================================================================================
    class MeshTest : public testing::Test
    {
    public:
        
        void SetUp() {};
        void TearDown() {};

    };
    
    TEST_F(MeshTest, MeshFunctions)
    {
        const int maxNumIndices = 6;
        const int maxNumVertices = 4;
        VertexFormat format(VertexFormatType::INTERLEAVED,
                            {VertexAttributeType::POSITION3, VertexAttributeType::NORMAL3, VertexAttributeType::UV2});
        std::shared_ptr<Mesh> mesh = Mesh::MeshFactory(format, maxNumIndices, maxNumVertices);
        ASSERT_NE(mesh, nullptr);
        ASSERT_EQ(mesh->GetMaxNumIndices(), maxNumIndices);
        ASSERT_EQ(mesh->GetMaxNumVertices(), maxNumVertices);
        ASSERT_EQ(mesh->GetNumIndices(), 0);
        ASSERT_EQ(mesh->GetNumVertices(), 0);
        ASSERT_EQ(mesh->GetFormat().GetFormatType(), VertexFormatType::INTERLEAVED);
        ASSERT_NE(mesh->GetVertexBuffer(), nullptr);
        
        // Verify functions for adding new vertices
        Vertex vertex0{glm::vec3(1.0f, 2.0f, 3.0f)};
        Vertex vertex1{glm::vec3(4.0f, 5.0f, 6.0f)};
        Vertex vertex2{glm::vec3(7.0f, 8.0f, 9.0f)};
        Vertex vertex3{glm::vec3(10.0f, 11.0f, 12.0f)};
        mesh->AddVertex(vertex0);
        ASSERT_EQ(mesh->GetNumVertices(), 1);
        mesh->AddVertex(vertex1);
        ASSERT_EQ(mesh->GetNumVertices(), 2);
        mesh->AddVertex(vertex2);
        ASSERT_EQ(mesh->GetNumVertices(), 3);
        mesh->AddVertex(vertex3);
        ASSERT_EQ(mesh->GetNumVertices(), 4);
        
        // Verify functions for adding new indices
        mesh->AddIndex(0);
        ASSERT_EQ(mesh->GetNumIndices(), 1);
        mesh->AddIndex(1);
        ASSERT_EQ(mesh->GetNumIndices(), 2);
        mesh->AddIndex(2);
        ASSERT_EQ(mesh->GetNumIndices(), 3);
        mesh->AddIndices({1, 3, 2});
        ASSERT_EQ(mesh->GetNumIndices(), 6);
    }
    
    
    /// Check that the default Mesh2d factories are giving the expected meshes
    TEST_F(MeshTest, Mesh2dFactories)
    {
        /*std::shared_ptr<Mesh> ellipseMesh = Mesh::EllipseFactory(2.0f, 1.0f, twopi, 8);
        ASSERT_EQ(ellipseMesh.NumIndices(), 24);
        ASSERT_EQ(ellipseMesh.NumVertices(), 9);*/
    
        std::shared_ptr<Mesh> sheetMesh = Mesh::SheetFactory(1.0f, 1.0f, 4, 4);
        ASSERT_NE(sheetMesh, nullptr);
        ASSERT_EQ(sheetMesh->GetNumIndices(), 54);
        ASSERT_EQ(sheetMesh->GetNumVertices(), 16);
    }
    
    
    /// Check that the default Mesh2d factories are giving the expected meshes
    TEST_F(MeshTest, Mesh3dFactories)
    {
        std::shared_ptr<Mesh> cuboidMesh = Mesh::CuboidFactory(2.0f, 2.0f, 2.0f, 2, 2, 2);
        ASSERT_NE(cuboidMesh, nullptr);
        ASSERT_EQ(cuboidMesh->GetNumIndices(), 36);
        ASSERT_EQ(cuboidMesh->GetNumVertices(), 24);
    }
        
        /// Verify the default values for the various transform vectors.
    /*TEST_F(MeshTest, Mesh2dFunctions)
    {
        // Verify the constructor allocates the correct memory
        Mesh2d mesh2d(0, 1);
        ASSERT_GE(mesh2d.MaxNumIndices(), 0);
        ASSERT_GE(mesh2d.MaxNumVertices(), 1);
        ASSERT_EQ(mesh2d.NumIndices(), 0);
        ASSERT_EQ(mesh2d.NumVertices(), 0);
        
        // Verify adding a single vertex directly
        mesh2d.AddVertex(Vertex2d(glm::vec2(-1.0f, -1.0f)));
        ASSERT_EQ(mesh2d.NumVertices(), 1);
        
        // Verify adding a single vertex only via the position
        mesh2d.AddVertex(glm::vec2(1.0f, -1.0f));
        ASSERT_GE(mesh2d.MaxNumVertices(), 2);
        ASSERT_EQ(mesh2d.NumVertices(), 2);
        
        // Verify adding vertices via an initializer list
        mesh2d.AddVertices({glm::vec2(-1.0f, 1.0f), glm::vec2(1.0f, 1.0f)});
        ASSERT_GE(mesh2d.MaxNumVertices(), 4);
        ASSERT_EQ(mesh2d.NumVertices(), 4);
        
        // Verify the copy constructor works correctly
        Mesh2d copiedMesh2d = mesh2d;
        ASSERT_EQ(copiedMesh2d.NumIndices(), 0);
        ASSERT_EQ(copiedMesh2d.NumVertices(), 4);
        
        // Verify the translation function
        glm::vec2 dr(2.0f, 2.0f);
        copiedMesh2d.Translate(dr);
        for (int i = 0; i < mesh2d.NumVertices(); ++i)
        {
            const Vertex2d& origVertex2d = mesh2d.GetVertex(i);
            const Vertex2d& translatedVertex2d = copiedMesh2d.GetVertex(i);
            ASSERT_FLOAT_EQ(translatedVertex2d.r[0], origVertex2d.r[0] + dr[0]);
            ASSERT_FLOAT_EQ(translatedVertex2d.r[1], origVertex2d.r[1] + dr[1]);
        }
    }
    
    
    /// Verify the default values for the various transform vectors.
    TEST_F(MeshTest, Mesh2dOperators)
    {
        Mesh2d mesh2d(3, 3);
        mesh2d.AddVertices({glm::vec2(-1.0f, -1.0f), glm::vec2(1.0f, -1.0f), glm::vec2(0.0f, 1.0f)});
        mesh2d.AddIndices({0, 1, 2});
        
        // Verify the copy/assignment operator works correctly
        Mesh2d assignedMesh2d;
        ASSERT_EQ(assignedMesh2d.NumIndices(), 0);
        ASSERT_EQ(assignedMesh2d.NumVertices(), 0);
        assignedMesh2d = mesh2d;
        ASSERT_EQ(assignedMesh2d.NumIndices(), 3);
        ASSERT_EQ(assignedMesh2d.NumVertices(), 3);
    
        // Verify the append operator
        mesh2d += assignedMesh2d;
        ASSERT_EQ(mesh2d.NumIndices(), 6);
        ASSERT_EQ(mesh2d.NumVertices(), 6);
    }
    
    
    /// Verify the default values for the various transform vectors.
    TEST_F(MeshTest, Mesh3dFunctions)
    {
        Mesh3d mesh3d(2, 2);
        ASSERT_GE(mesh3d.MaxNumIndices(), 2);
        ASSERT_GE(mesh3d.MaxNumVertices(), 2);
        ASSERT_EQ(mesh3d.NumIndices(), 0);
        ASSERT_EQ(mesh3d.NumVertices(), 0);
        
        // Verify adding a single vertex directly
        mesh3d.AddVertex(Vertex3d(glm::vec3(-1.0f, -1.0f, -1.0f)));
        ASSERT_EQ(mesh3d.NumVertices(), 1);
    
        // Verify adding a vertex via a simple position vector
        mesh3d.AddVertex(glm::vec3(1.0f, -1.0f, -1.0f));
        ASSERT_EQ(mesh3d.NumVertices(), 2);
        
        // Verify adding several vertices and indices via initializer lists
        mesh3d.AddVertices({glm::vec3(0.0f, 1.0f, -1.0f), glm::vec3(0.0f, 0.0f, 1.0f)});
        ASSERT_GE(mesh3d.MaxNumVertices(), 4);
        ASSERT_EQ(mesh3d.NumVertices(), 4);
    
        // Verify the copy constructor works correctly
        Mesh3d copiedMesh3d = mesh3d;
        ASSERT_EQ(copiedMesh3d.NumIndices(), 0);
        ASSERT_EQ(copiedMesh3d.NumVertices(), 4);
    
        // Verify the translation function
        glm::vec3 dr(2.0f, 2.0f, 2.0f);
        copiedMesh3d.Translate(dr);
        for (int i = 0; i < mesh3d.NumVertices(); ++i)
        {
            const Vertex3d& origVertex3d = mesh3d.GetVertex(i);
            const Vertex3d& translatedVertex2d = copiedMesh3d.GetVertex(i);
            ASSERT_FLOAT_EQ(translatedVertex2d.r[0], origVertex3d.r[0] + dr[0]);
            ASSERT_FLOAT_EQ(translatedVertex2d.r[1], origVertex3d.r[1] + dr[1]);
            ASSERT_FLOAT_EQ(translatedVertex2d.r[2], origVertex3d.r[2] + dr[2]);
        }
    }
    
    
    /// Verify the default values for the various transform vectors.
    TEST_F(MeshTest, Mesh3dOperators)
    {
        Mesh3d mesh3d(3, 3);
        mesh3d.AddVertices({glm::vec3(-1.0f, -1.0f, 0.0f), glm::vec3(1.0f, -1.0f, 0.0f), glm::vec3(0.0f, 1.0f, 0.0f)});
        mesh3d.AddIndices({0, 1, 2});
        
        // Verify the copy/assignment operator works correctly
        Mesh3d assignedMesh3d;
        ASSERT_EQ(assignedMesh3d.NumIndices(), 0);
        ASSERT_EQ(assignedMesh3d.NumVertices(), 0);
        assignedMesh3d = mesh3d;
        ASSERT_EQ(assignedMesh3d.NumIndices(), 3);
        ASSERT_EQ(assignedMesh3d.NumVertices(), 3);
        
        // Verify the append operator
        mesh3d += assignedMesh3d;
        ASSERT_EQ(mesh3d.NumIndices(), 6);
        ASSERT_EQ(mesh3d.NumVertices(), 6);
    }
    
    
    
    /// Check that the default Mesh2d factories are giving the expected meshes
    TEST_F(MeshTest, Mesh2dToMesh3d)
    {
        SheetMesh2d sheetMesh2d(1.0f, 1.0f, 3, 3);
        ASSERT_EQ(sheetMesh2d.NumIndices(), 24);
        ASSERT_EQ(sheetMesh2d.NumVertices(), 9);
        
        // Verify constructor
        Mesh3d sheetMesh3d(sheetMesh2d);
        ASSERT_EQ(sheetMesh3d.NumIndices(), 24);
        ASSERT_EQ(sheetMesh3d.NumVertices(), 9);
        for (int i = 0; i < sheetMesh3d.NumVertices(); ++i)
        {
            Vertex2d vertex2d = sheetMesh2d.GetVertex(i);
            Vertex3d vertex3d = sheetMesh3d.GetVertex(i);
            ASSERT_FLOAT_EQ(vertex3d.r[0], vertex2d.r[0]);
            ASSERT_FLOAT_EQ(vertex3d.r[1], vertex2d.r[1]);
            ASSERT_FLOAT_EQ(vertex3d.r[2], 0.0f);
        }
        
        // Verify assignment operator
        Mesh3d otherMesh3d;
        ASSERT_EQ(otherMesh3d.NumIndices(), 0);
        ASSERT_EQ(otherMesh3d.NumVertices(), 0);
        otherMesh3d = sheetMesh2d;
        ASSERT_EQ(otherMesh3d.NumIndices(), 24);
        ASSERT_EQ(otherMesh3d.NumVertices(), 9);
    }*/
    
}
//---------------------------------------------------------------------------------------------------------------------
