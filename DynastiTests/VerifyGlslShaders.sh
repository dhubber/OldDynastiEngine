FILES=./Assets/Shaders/*
for f in $FILES
do
  echo "Verifying $f file with glslangValidator"
  glslangValidator $f
done
