#include "gtest/gtest.h"
#include "Dynasti/Asset/AssetManager.h"
#include "Dynasti/Camera/CameraComponent.h"
#include "Dynasti/Renderer/RenderableComponent.h"
#include "Dynasti/Scene/Scene.h"
#include "Dynasti/Scene/SceneManager.h"


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{
    
    //=================================================================================================================
    /// \brief   Unit tests of Scene class.
    /// \author  D. A. Hubber
    /// \date    27/08/2020
    //=================================================================================================================
    class SceneTest : public testing::Test
    {
    public:
        
        void SetUp()
        {
            assetManager = std::make_shared<AssetManager>();
            scene = std::make_shared<Scene>(assetManager);
        };
        void TearDown() {};

        std::shared_ptr<AssetManager> assetManager;
        std::shared_ptr<Scene> scene;
    };
    
    
    /// Verify the default values for the default constructor
    TEST_F(SceneTest, Constructor)
    {
        ASSERT_EQ(scene->GetName(), "");
        ASSERT_EQ(scene->GetId(), -1);
        ASSERT_EQ(scene->NumEntites(), 0);
    }
    
    
    /// Verify the default values for the default constructor
    TEST_F(SceneTest, SceneManager)
    {
        std::shared_ptr<Scene> scene = std::make_shared<Scene>(assetManager);
        
    }
    
    
    /// Verify the default values for the default constructor
    TEST_F(SceneTest, Entities)
    {
        // Verify creating a single entity
        Entity& rootEntity = scene->CreateNewEntity("RootEntity");
        ASSERT_EQ(scene->NumEntites(), 1);
    
        // Verify creating a second entity and that it is not associated with the first
        Entity& childEntity1 = scene->CreateNewEntity("ChildEntity1");
        ASSERT_EQ(scene->NumEntites(), 2);
        ASSERT_EQ(rootEntity.childId, -1);
        ASSERT_EQ(childEntity1.parentId, -1);
        ASSERT_EQ(childEntity1.nextId, -1);
        
        // Verify attaching an entity to another entity
        scene->Attach(childEntity1, rootEntity);
        ASSERT_EQ(scene->NumEntites(), 2);
        ASSERT_EQ(rootEntity.childId, childEntity1.id);
        ASSERT_EQ(childEntity1.parentId, rootEntity.id);
        ASSERT_EQ(childEntity1.nextId, -1);
    
        // Verify that attaching a 2nd entity makes that entity a sibling of the 1st child
        Entity& childEntity2 = scene->CreateNewEntity("ChildEntity2");
        scene->Attach(childEntity2, rootEntity);
        ASSERT_EQ(scene->NumEntites(), 3);
        ASSERT_EQ(rootEntity.childId, childEntity2.id);
        ASSERT_EQ(childEntity2.parentId, rootEntity.id);
        ASSERT_EQ(childEntity1.nextId, -1);
        ASSERT_EQ(childEntity2.nextId, childEntity1.id);
        
        // Verify detaching 1st child from from parent leaves 2nd child as only child
        scene->Detach(childEntity1);
        ASSERT_EQ(scene->NumEntites(), 3);
        ASSERT_EQ(rootEntity.childId, childEntity2.id);
        ASSERT_EQ(childEntity1.parentId, -1);
        ASSERT_EQ(childEntity1.nextId, -1);
        ASSERT_EQ(childEntity2.parentId, rootEntity.id);
        ASSERT_EQ(childEntity2.nextId, -1);
    }
    
    
    /// Verify the default values for the default constructor
    TEST_F(SceneTest, Components)
    {
        Entity& rootEntity = scene->CreateNewEntity("RootEntity");
        ASSERT_EQ(scene->NumEntites(), 1);
        ASSERT_EQ(scene->NumComponentPools(), 0);
        ASSERT_EQ(scene->FindComponentPool<RenderableComponent>(), nullptr);
        ASSERT_EQ(scene->FindComponentPool<CameraComponent>(), nullptr);
        
        // Verify creating a single component for a given entity
        RenderableComponent* rComp = scene->CreateComponent<RenderableComponent>(rootEntity.id);
        ASSERT_EQ(scene->NumComponentPools(), 1);
        ASSERT_NE(scene->FindComponentPool<RenderableComponent>(), nullptr);
        ASSERT_NE(rComp, nullptr);
        ASSERT_EQ(scene->FindComponent<RenderableComponent>(rootEntity.id), rComp);
        if (rComp)
        {
            ASSERT_EQ(rComp->GetComponentName(), "RenderableComponent");
            ASSERT_EQ(rComp->id, rootEntity.id);
        }
        
        // Verify that trying to add a second component of the same type fails and returns nullptr
        RenderableComponent* rComp2 = scene->CreateComponent<RenderableComponent>(rootEntity.id);
        ASSERT_EQ(scene->NumComponentPools(), 1);
        ASSERT_EQ(rComp2, nullptr);
        ASSERT_EQ(scene->FindComponent<RenderableComponent>(rootEntity.id), rComp);
        
        // Verify that adding a second component type succeeds and returns a valid pointer
        CameraComponent* cameraComp = scene->CreateComponent<CameraComponent>(rootEntity.id);
        ASSERT_EQ(scene->NumComponentPools(), 2);
        ASSERT_NE(scene->FindComponentPool<CameraComponent>(), nullptr);
        ASSERT_NE(cameraComp, nullptr);
        ASSERT_EQ(scene->FindComponent<CameraComponent>(rootEntity.id), cameraComp);
        if (cameraComp)
        {
            ASSERT_EQ(cameraComp->GetComponentName(), "CameraComponent");
            ASSERT_EQ(cameraComp->id, rootEntity.id);
        }
        
        // Verify that we find all the registered components of the given entity
        std::vector<Component*> components = scene->FindAllEntityComponents(rootEntity.id);
        ASSERT_EQ(components.size(), 2);
    }
    
    
    /// Verify the default values for the default constructor
    TEST_F(SceneTest, ComponentPools)
    {
        std::shared_ptr<ObjectPool<RenderableComponent>> renderablePool = scene->FindComponentPool<RenderableComponent>();
        ASSERT_EQ(scene->NumComponentPools(), 0);
        ASSERT_EQ(renderablePool, nullptr);
        
        renderablePool = scene->CreateComponentPool<RenderableComponent>();
        ASSERT_EQ(scene->NumComponentPools(), 1);
        ASSERT_NE(renderablePool, nullptr);
        ASSERT_EQ(renderablePool, scene->FindComponentPool<RenderableComponent>());
    }
    
}
//---------------------------------------------------------------------------------------------------------------------
