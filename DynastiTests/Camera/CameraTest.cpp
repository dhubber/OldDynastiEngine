#include "gtest/gtest.h"
#include "Dynasti/Camera/Camera.h"


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{
    
    //=================================================================================================================
    /// \brief   Unit tests of all task and task manager classes.
    /// \author  D. A. Hubber
    /// \date    22/06/2020
    //=================================================================================================================
    class CameraTest : public testing::Test
    {
    public:
        
        void SetUp() {};
        void TearDown() {};

    };
    
    
    /// Verify default values for default constructor with new Camera object
    TEST_F(CameraTest, Constructor)
    {
        Camera camera;
        ASSERT_EQ(camera.GetCameraMode(), CameraMode::CAMERA_NULL);
        ASSERT_EQ(camera.GetWidth(), -1);
        ASSERT_EQ(camera.GetHeight(), -1);
        ASSERT_FLOAT_EQ(camera.GetXSize(), 0.0f);
        ASSERT_FLOAT_EQ(camera.GetYSize(), 0.0f);
        ASSERT_FLOAT_EQ(camera.GetFovDegrees(), 0.0f);
        ASSERT_FLOAT_EQ(camera.GetAspectRatio(), 0.0f);
        ASSERT_FLOAT_EQ(camera.GetZNear(), 0.0f);
        ASSERT_FLOAT_EQ(camera.GetZFar(), 0.0f);
    }
    
    
    /// Verify correct values when setting up an orthographic camera
    TEST_F(CameraTest, Orthographic)
    {
        glm::vec3 pos;
        glm::quat quat;
        Camera camera;
        camera.SetOrthographicCamera(pos, quat, 800, 400, 2.0f, 100.0f);
        const Frustum& frustum = camera.GetFrustum();
        
        ASSERT_EQ(camera.GetCameraMode(), CameraMode::CAMERA_ORTHOGRAPHIC);
        ASSERT_EQ(camera.GetWidth(), 800);
        ASSERT_EQ(camera.GetHeight(), 400);
        ASSERT_FLOAT_EQ(camera.GetXSize(), 2.0f);
        ASSERT_FLOAT_EQ(camera.GetYSize(), 1.0f);
        ASSERT_FLOAT_EQ(camera.GetAspectRatio(), 2.0f);
        ASSERT_FLOAT_EQ(camera.GetZNear(), 0.0f);
        ASSERT_FLOAT_EQ(camera.GetZFar(), 100.0f);
    }
    
    
    /// Verify correct values when setting up a perspective camera
    TEST_F(CameraTest, Perspective)
    {
        glm::vec3 pos;
        glm::quat quat;
        Camera camera;
        camera.SetPerspectiveCamera(pos, quat, 800, 400, 20.0f, 0.01f, 100.0f);
        const Frustum& frustum = camera.GetFrustum();
        
        ASSERT_EQ(camera.GetCameraMode(), CameraMode::CAMERA_PERSPECTIVE);
        ASSERT_EQ(camera.GetWidth(), 800);
        ASSERT_EQ(camera.GetHeight(), 400);
        ASSERT_FLOAT_EQ(camera.GetFovDegrees(), 20.0f);
        ASSERT_FLOAT_EQ(camera.GetAspectRatio(), 2.0f);
        ASSERT_FLOAT_EQ(camera.GetZNear(), 0.01f);
        ASSERT_FLOAT_EQ(camera.GetZFar(), 100.0f);
    }

}
//---------------------------------------------------------------------------------------------------------------------
