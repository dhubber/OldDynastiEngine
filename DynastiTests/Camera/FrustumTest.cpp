#include "gtest/gtest.h"
#include "Dynasti/Camera/Frustum.h"


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{
    
    //=================================================================================================================
    /// \brief   Unit tests of the different modes for the view frustum
    /// \author  D. A. Hubber
    /// \date    04/04/2018
    //=================================================================================================================
    class FrustumTest : public testing::Test
    {
    public:
        
        void SetUp() {};
        void TearDown() {};

    };
    
    
    /// Verify the orthographic view frustum works correctly
    TEST_F(FrustumTest, OrthographicView)
    {
        Frustum frustum;
        frustum.SetOrthographicFrustum(glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, 0.0f, 1.0f),
                                       glm::vec3(0.0f, 1.0f, 0.0f), glm::vec3(1.0f, 0.0f, 0.0f),
                                       0.0f, 100.0f, 800, 400);
        const glm::vec3& rNear1 = frustum.GetNearPlanePos();
        const glm::vec3& rFar1 = frustum.GetFarPlanePos();
        ASSERT_FLOAT_EQ(rNear1.z, 0.0f);
        ASSERT_FLOAT_EQ(rFar1.z, 100.0f);
        ASSERT_EQ(frustum.ContainsPoint(glm::vec3(0.0f, 0.0f, -10.0f)), false);
        //ASSERT_EQ(frustum.ContainsPoint(glm::vec3(0.0f, 0.0f, 0.0f)), true);
        //ASSERT_EQ(frustum.ContainsPoint(glm::vec3(0.0f, 0.0f, 50.0f)), true);
        //ASSERT_EQ(frustum.ContainsPoint(glm::vec3(0.0f, 0.0f, 100.0f)), true);
        ASSERT_EQ(frustum.ContainsPoint(glm::vec3(0.0f, 0.0f, 110.0f)), false);
    }
    
    
    /// Verify the perspective view frustum works correctly
    TEST_F(FrustumTest, PerspectiveView)
    {
        Frustum frustum;
        frustum.SetPerspectiveFrustum(glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, 0.0f, 1.0f),
                                      glm::vec3(0.0f, 1.0f, 0.0f), glm::vec3(1.0f, 0.0f, 0.0f),
                                      0.01f, 100.0f, 10.0f, 1.0f);
        const glm::vec3& rNear1 = frustum.GetNearPlanePos();
        const glm::vec3& rFar1 = frustum.GetFarPlanePos();
        ASSERT_FLOAT_EQ(rNear1.z, 0.01f);
        ASSERT_FLOAT_EQ(rFar1.z, 100.0f);
        ASSERT_EQ(frustum.ContainsPoint(glm::vec3(0.0f, 0.0f, -10.0f)), false);
        ASSERT_EQ(frustum.ContainsPoint(glm::vec3(0.0f, 0.0f, 0.0f)), false);
        //ASSERT_EQ(frustum.ContainsPoint(glm::vec3(0.0f, 0.0f, 0.01f)), true);
        //ASSERT_EQ(frustum.ContainsPoint(glm::vec3(0.0f, 0.0f, 50.0f)), true);
        //ASSERT_EQ(frustum.ContainsPoint(glm::vec3(0.0f, 0.0f, 100.0f)), true);
        ASSERT_EQ(frustum.ContainsPoint(glm::vec3(0.0f, 0.0f, 110.0f)), false);
    }

}
//---------------------------------------------------------------------------------------------------------------------
