#include "gtest/gtest.h"
#include <glm/glm.hpp>
#include "Dynasti/Camera/Plane.h"


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{
    
    //=================================================================================================================
    /// \brief   Unit tests of all entity and entity system functions.
    /// \author  D. A. Hubber
    /// \date    04/04/2018
    //=================================================================================================================
    class PlaneTest : public testing::Test
    {
    public:
        
        void SetUp() {};
        void TearDown() {};
        
        // Different planes to test the orientation tests on
        Plane xPlane{glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(1.0f, 0.0f, 0.0f)};
        Plane yPlane{glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, 1.0f, 0.0f)};
        Plane zPlane{glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, 0.0f, 1.0f)};
        
    };
    
    
    /// Verify the default values for the various transform vectors.
    TEST_F(PlaneTest, DistanceFromPlane)
    {
        ASSERT_EQ(xPlane.ComputeOrientation(glm::vec3(-1.0f, 0.0f, 0.0f)), PlaneOrientation::PLANE_BACK);
        ASSERT_EQ(xPlane.ComputeOrientation(glm::vec3(0.0f, 0.0f, 0.0f)), PlaneOrientation::PLANE_COPLANAR);
        ASSERT_EQ(xPlane.ComputeOrientation(glm::vec3(1.0f, 0.0f, 0.0f)), PlaneOrientation::PLANE_FRONT);
    
        ASSERT_EQ(yPlane.ComputeOrientation(glm::vec3(0.0f, -1.0f, 0.0f)), PlaneOrientation::PLANE_BACK);
        ASSERT_EQ(yPlane.ComputeOrientation(glm::vec3(0.0f, 0.0f, 0.0f)), PlaneOrientation::PLANE_COPLANAR);
        ASSERT_EQ(yPlane.ComputeOrientation(glm::vec3(0.0f, 1.0f, 0.0f)), PlaneOrientation::PLANE_FRONT);
    
        ASSERT_EQ(zPlane.ComputeOrientation(glm::vec3(0.0f, 0.0f, -1.0f)), PlaneOrientation::PLANE_BACK);
        ASSERT_EQ(zPlane.ComputeOrientation(glm::vec3(0.0f, 0.0f, 0.0f)), PlaneOrientation::PLANE_COPLANAR);
        ASSERT_EQ(zPlane.ComputeOrientation(glm::vec3(0.0f, 0.0f, 1.0f)), PlaneOrientation::PLANE_FRONT);
    }
    
    
    /// Verify the default values for the various transform vectors.
    TEST_F(PlaneTest, PlaneOrientation)
    {
        ASSERT_EQ(xPlane.DistanceFromPlane(glm::vec3(-1.0f, 0.0f, 0.0f)), -1.0f);
        ASSERT_EQ(xPlane.DistanceFromPlane(glm::vec3(0.0f, 0.0f, 0.0f)), 0.0f);
        ASSERT_EQ(xPlane.DistanceFromPlane(glm::vec3(1.0f, 0.0f, 0.0f)), 1.0f);
    
        ASSERT_EQ(yPlane.DistanceFromPlane(glm::vec3(0.0f, -1.0f, 0.0f)), -1.0f);
        ASSERT_EQ(yPlane.DistanceFromPlane(glm::vec3(0.0f, 0.0f, 0.0f)), 0.0f);
        ASSERT_EQ(yPlane.DistanceFromPlane(glm::vec3(0.0f, 1.0f, 0.0f)), 1.0f);
    
        ASSERT_EQ(zPlane.DistanceFromPlane(glm::vec3(0.0f, 0.0f, -1.0f)), -1.0f);
        ASSERT_EQ(zPlane.DistanceFromPlane(glm::vec3(0.0f, 0.0f, 0.0f)), 0.0f);
        ASSERT_EQ(zPlane.DistanceFromPlane(glm::vec3(0.0f, 0.0f, 1.0f)), 1.0f);
    }

}
//---------------------------------------------------------------------------------------------------------------------
