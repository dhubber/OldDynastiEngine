#include "gtest/gtest.h"
#include "Dynasti/Renderer/Opengl/OpenglTexture.h"


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{
    
    //=================================================================================================================
    /// \brief   Unit tests of Texture class.
    /// \author  D. A. Hubber
    /// \date    27/08/2020
    //=================================================================================================================
    class TextureTest : public testing::Test
    {
    public:
        
        void SetUp() {};
        void TearDown() {};

    };
    
    
    /// Verify the default values for the default constructor
    TEST_F(TextureTest, Constructor)
    {
        OpenglTexture texture;
        ASSERT_EQ(texture.GetId(), -1);
        ASSERT_EQ(texture.GetBytesPerPixel(), 4);
    }
    
    
    /// Verify the default values for the default constructor
    TEST_F(TextureTest, LoadFromJpegFile)
    {
       OpenglTexture jpegTexture;
       bool successFlag = jpegTexture.LoadImageFromFile(imagePath + "welsh_flag.jpg");
       ASSERT_EQ(successFlag, true);
       ASSERT_GE(jpegTexture.GetWidth(), 0);
       ASSERT_GE(jpegTexture.GetHeight(), 0);
       ASSERT_EQ(jpegTexture.GetFormat(), GL_RGBA);
    }
    
    
    /// Verify the default values for the default constructor
    TEST_F(TextureTest, LoadFromPngFile)
    {
        OpenglTexture pngTexture;
        bool successFlag = pngTexture.LoadImageFromFile(imagePath + "germany_flag.png");
        ASSERT_EQ(successFlag, true);
        ASSERT_GE(pngTexture.GetWidth(), 0);
        ASSERT_GE(pngTexture.GetHeight(), 0);
        ASSERT_EQ(pngTexture.GetFormat(), GL_RGBA);
    }
    
}
//---------------------------------------------------------------------------------------------------------------------
