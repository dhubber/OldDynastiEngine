#include <memory>
#include "gtest/gtest.h"
#include "Dynasti/Window/Glfw/GlfwWindowSystem.h"


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{
    
    //=================================================================================================================
    /// \brief   Unit tests of the GLFW window system class.
    /// \author  D. A. Hubber
    /// \date    05/07/2020
    //=================================================================================================================
    class GlfwTest : public testing::Test
    {
    public:
        
        void SetUp() {};
        void TearDown() {};

    };
    
    
    TEST_F(GlfwTest, Glfw)
    {
        ASSERT_EQ(glfwInit(), GLFW_TRUE);
        glfwTerminate();
    }

}
//---------------------------------------------------------------------------------------------------------------------
