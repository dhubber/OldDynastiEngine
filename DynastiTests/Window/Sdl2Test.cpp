#include <memory>
#include "gtest/gtest.h"
#include "Dynasti/Window/Sdl2/Sdl2WindowSystem.h"


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{
    
    //=================================================================================================================
    /// \brief   Unit tests of all Window classes and functions.
    /// \author  D. A. Hubber
    /// \date    05/07/2020
    //=================================================================================================================
    class Sdl2Test : public testing::Test
    {
    public:
        
        void SetUp() {};
        void TearDown() {};

    };
    
    
    TEST_F(Sdl2Test, Sdl)
    {
        int sdlStatus;
        sdlStatus = SDL_VERSION_ATLEAST(2, 0, 8);
        ASSERT_EQ(sdlStatus, true);
    
        sdlStatus = SDL_InitSubSystem(SDL_INIT_EVENTS);
        ASSERT_EQ(sdlStatus, 0);
    
        sdlStatus = SDL_InitSubSystem(SDL_INIT_AUDIO);
        ASSERT_EQ(sdlStatus, 0);
    
        sdlStatus = SDL_InitSubSystem(SDL_INIT_GAMECONTROLLER);
        ASSERT_EQ(sdlStatus, 0);
        
        sdlStatus = SDL_InitSubSystem(SDL_INIT_JOYSTICK);
        ASSERT_EQ(sdlStatus, 0);
    
        sdlStatus = SDL_InitSubSystem(SDL_INIT_HAPTIC);
        ASSERT_EQ(sdlStatus, 0);
    
        sdlStatus = SDL_InitSubSystem(SDL_INIT_TIMER);
        ASSERT_EQ(sdlStatus, 0);
        
        SDL_Quit();
    }


    /*TEST_F(Sdl2Test, Sdl2WindowSystem)
    {
        std::shared_ptr<EventManager> eventManager = std::make_shared<EventManager>();
        std::unique_ptr<Sdl2WindowSystem> windowSystem = std::make_unique<Sdl2WindowSystem>(eventManager);
        ASSERT_NE(windowSystem, nullptr);
        windowSystem->Setup();
    
        std::shared_ptr<Sdl2Window> primaryWindow = windowSystem->GetPrimaryWindow();
        ASSERT_NE(primaryWindow, nullptr);
        
        windowSystem->Shutdown();
    }*/

}
//---------------------------------------------------------------------------------------------------------------------
