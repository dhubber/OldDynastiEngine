# DynastiEngine

Dynasti is a simple cross-platform game engine written as an educational project but also with the goal of eventually creating simple games.  Dynasti 
- Is written in modern C++14 with extensive use of STL
- Uses Cmake for relatively simple cross-platform compilation
- Is designed using an Entity-Component-System architecture to leverage better data-driven performance
- Uses events to communicate between sub-systems therefore minimising coupling

Implemented
- GLFW3 window with OpenGL 4.6 context created
- Perspective (3d) or orthographic (2d) camera modes
- ECS implementation allowing easy creation of new component types via templates
- Basic shaders for drawing meshes with solid colors, wireframes or textures
- Basic image and 3d model loading
- Simple event system, where events are processed immediately (e.g. Observer pattern)
- Simple keyboard input mapping system which trigger key up/down events, or can be polled
- Simple loggers and time profilers

Currently under development
- Ability to attach scripts to entities (e.g. via a script component)
- Camera components for entities
- Framebuffers for implementing post-process effects (e.g. anti-aliasing)
- Phong material and lighting model
- Basic Editor GUI using ImGui

To-do/Wish-list (in no particular order)
- Events that include some data using variants
- Event queues with system mailboxes (e.g. the Actor model)
- Thread pool for running parallel/concurrent engine tasks
- Mouse and Gamepad input
- 2d collision with box2d
- 3d collision with Bullet
- Scene serialisation, to save scenes to disk
- PBR material and lighting model
- Batched rendering
- Multiple rendering viewports for split-screen gameplay
- Audio assets and components
- Font assets and text rendering
- Frustum culling
- OpenGL ES 2.0 or 3.0
- Android compilation using the NDK
- Touch screen input
- macOS compilation
- Vulkan rendering


## Dependencies
Dynasti requires the following dependencies, which may need to be installed beforehand or can be installed during the build process depending on your chosen platform
- A C++ compiler (e.g. g++ for Linux, Visual Studio C++ for Windows)
- OpenGL 4.6
- [git](https://git-scm.com/)
- [cmake](https://cmake.org/)
- [GLFW](https://github.com/glfw/glfw)
- [ASSIMP](https://www.assimp.org/)
- [GLM](https://github.com/g-truc/glm)
- [stb](https://github.com/nothings/stb)
- [googletest/googlemock](https://github.com/google/googletest)


## Installation

### Linux (Command-line)
- Install all dependencies via the command-line or a package manager, depending on your distro.  For Debian/Ubuntu-users : 
```bash
sudo apt-get install cmake g++ mesa-common-dev libglu1-mesa-dev freeglut3-dev libxrandr-dev libxinerama-dev libxcursor-dev libxi-dev libassimp-dev libglm-dev libglfw3 libglfw3-dev
```
- Create the directory that you wish to download the code to and change into it
```bash
mkdir /path/to/code
cd /path/to/code
```
- Download the code from gitlab using git (including simulataneously downloading all submodules)
```bash
git clone --recurse-submodules https://gitlab.com/dhubber/DynastiEngine.git
```
- Create a build directory for cmake and change into it
```bash
mkdir DynastiEngine/build
cd DynastiEngine/build
```
- Run cmake to construct the Makefiles and other important files for the build
```bash
cmake -DCMAKE_BUILD_TYPE=Debug ../.
```
- Run make to compile the code
```bash
make -j
```
- To run any executables, e.g. the Editor, change into that directory and type the executable command
```bash
cd Editor
./Editor
```

## Licence
This engine is released under the terms of the MIT licence (See LICENCE.txt for the full description of the licence).


## Acknowledgements
The development of this engine has been made much easier thanks to the incredible contributions of the many open-source libraries developed, either by individuals, larger online communities or commercial entities.
For testing and in some demos, public-domain or open-source assets such as images and 3d models were included in the engine code.  All such assets are listed and acknowledged here : 

- Filename : Assets/Images/germany_flag.png
  - Title : Flag of Germany
  - Author : NA
  - Source : https://en.wikipedia.org/wiki/File:Flag_of_Germany.svg
  - Licence : NA (Public Domain)


- Filename : Assets/Images/tirol_flag.png
  - Title : Flag of Tirol (state)
  - Author : NA
  - Source : https://commons.wikimedia.org/wiki/File:Flag_of_Tirol_(state).svg 
  - Licence : NA (Public Domain)


- Filename : Assets/Images/welsh_flag.png
  - Title : Flag of Wales (1959 - present)
  - Author : United Kingdom Government (Public Domain)
  - Source : https://commons.wikimedia.org/wiki/File:Flag_of_Wales_(1959%E2%80%93present).svg
  - Licence : Creative Commons CC0 1.0 Universal Public Domain Dedication
  