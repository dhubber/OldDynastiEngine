#ifndef DYNASTI_IMGUI_WINDOW_SYSTEM_H
#define DYNASTI_IMGUI_WINDOW_SYSTEM_H


#include <memory>
#include "Dynasti/Core/Constants.h"
#include "Dynasti/Window/WindowSystem.h"
#include "Dynasti/Window/Glfw/GlfwWindow.h"
#include "Dynasti/Window/Glfw/GlfwWindowSystem.h"


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{
    
    //=================================================================================================================
    /// \brief   GLFW implementation of WindowSystem class.
    /// \author  D. A. Hubber
    /// \date    12/06/2020
    //=================================================================================================================
    class EditorWindowSystem : public GlfwWindowSystem
    {
    public:
        
        EditorWindowSystem(std::shared_ptr<AssetManager> assetManager, std::shared_ptr<EventManager> eventManager,
                         std::shared_ptr<FrameTimer> frameTimer, std::shared_ptr<InputManager> inputManager) :
            GlfwWindowSystem(assetManager, eventManager, frameTimer, inputManager)
        {
        };
    
        virtual void Setup() override;
        virtual void Shutdown() override;
        virtual void Update() override;
        
    
    };
    
}
//---------------------------------------------------------------------------------------------------------------------
#endif
