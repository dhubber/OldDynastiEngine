#define GLFW_INCLUDE_NONE
#include "glad/glad.h"
#include "GLFW/glfw3.h"
#include "Dynasti/Core/Debug.h"
#include "Dynasti/Core/FrameTimer.h"
#include "Dynasti/Event/EventListener.h"
#include "Dynasti/Event/EventManager.h"
#include "Dynasti/Input/InputManager.h"
#include "Editor/Dependencies/imgui/imgui.h"
#include "EditorWindowSystem.h"

#include "Editor/Dependencies/imgui/backends/imgui_impl_opengl3.h"
#include "Editor/Dependencies/imgui/backends/imgui_impl_glfw.h"


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{
    
    void EditorWindowSystem::Setup()
    {
        GlfwWindowSystem::Setup();
        
        const char* glsl_version = "#version 460";
    
        // Setup Dear ImGui context
        IMGUI_CHECKVERSION();
        ImGui::CreateContext();
        ImGuiIO& io = ImGui::GetIO(); (void)io;
        io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;       // Enable Keyboard Controls
        //io.ConfigFlags |= ImGuiConfigFlags_NavEnableGamepad;      // Enable Gamepad Controls
        //io.ConfigFlags |= ImGuiConfigFlags_DockingEnable;           // Enable Docking
        //io.ConfigFlags |= ImGuiConfigFlags_ViewportsEnable;         // Enable Multi-Viewport / Platform Windows
        //io.ConfigViewportsNoAutoMerge = true;
        //io.ConfigViewportsNoTaskBarIcon = true;
    
        // Setup Dear ImGui style
        ImGui::StyleColorsDark();
        //ImGui::StyleColorsClassic();
    
        // When viewports are enabled we tweak WindowRounding/WindowBg so platform windows can look identical to regular ones.
        ImGuiStyle& style = ImGui::GetStyle();
        if (io.ConfigFlags) // & ImGuiConfigFlags_ViewportsEnable)
        {
            style.WindowRounding = 0.0f;
            style.Colors[ImGuiCol_WindowBg].w = 1.0f;
        }
    
        // Setup Platform/Renderer backends
        ImGui_ImplGlfw_InitForOpenGL(glfwWindow_->GetGlfwInternalWindow(), true);
        ImGui_ImplOpenGL3_Init(glsl_version);
        
    }
    
    
    void EditorWindowSystem::Shutdown()
    {
        // Cleanup
        ImGui_ImplOpenGL3_Shutdown();
        ImGui_ImplGlfw_Shutdown();
        ImGui::DestroyContext();
        
        glfwTerminate();
    }
    
    
    void EditorWindowSystem::Update()
    {
        DYNASTI_TIMER(WINDOW_SYSTEM);
        glfwPollEvents();
        glfwWindow_->SwapBuffers();
    }
    
}
//---------------------------------------------------------------------------------------------------------------------
