#include <iostream>
#include <memory>
//#include <QApplication>
//#include "DynastiEditor/EditorWindow.h"
#include "Dynasti/Core/Debug.h"
#include "Editor/Application/EditorApplication.h"

int main(int argc, char **argv)
{
    std::unique_ptr<Dynasti::EditorApplication> editorApp = std::make_unique<Dynasti::EditorApplication>();
    
    editorApp->Setup();
    editorApp->Run();
    editorApp->Shutdown();
    
    return 0;
}
