#ifndef DYNASTI_PANEL_H
#define DYNASTI_PANEL_H


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{
    
    class FrameTimer;
    class Scene;
    
    
    //=================================================================================================================
    /// \brief   Base class for separate panels/sub-windows in the Editor using the 'Dear ImGui' framework.
    /// \author  D. A. Hubber
    /// \date    12/05/2021
    //=================================================================================================================
    class Panel
    {
    public:
        
        Panel() : width_(1), height_(1)
        {
            DYNASTI_LOG_VERBOSE("Constructed Panel object");
        }
        
        /// Update function (called every frame) to refresh the panel data for rendering to screen with ImGui
        /// \param[in] scene - Currently active/selected scene
        /// \param[in] frameTimer - System frame timer
        virtual void Update(std::shared_ptr<Scene> scene, std::shared_ptr<FrameTimer> frameTimer) = 0;
        
        // Getters
        inline unsigned int GetWidth() const { return width_; }
        inline unsigned int GetHeight() const { return height_; }
        
    
    protected:
    
        unsigned int width_;                               ///< Panel width (in pixels)
        unsigned int height_;                              ///< Panel height (in pixels)
        
    };
    
}
//---------------------------------------------------------------------------------------------------------------------
#endif
