#ifndef DYNASTI_SCENE_VIEW_PANEL_H
#define DYNASTI_SCENE_VIEW_PANEL_H


#include <memory>
#include "Dynasti/Renderer/Framebuffer.h"
#include "Editor/Panel/Panel.h"
#include "Editor/Dependencies/imgui/imgui.h"
#include "Dynasti/Renderer/Opengl/OpenglFramebuffer.h"


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{
    
    //=================================================================================================================
    /// \brief   Base class for separate panels in Editor
    /// \author  D. A. Hubber
    /// \date    12/05/2021
    //=================================================================================================================
    class SceneViewPanel : public Panel
    {
    public:
    
        SceneViewPanel(std::shared_ptr<Framebuffer> screenBuffer) : Panel(), screenBuffer_(screenBuffer)
        {
        }
        
        virtual void Update(std::shared_ptr<Scene> scene, std::shared_ptr<FrameTimer> frameTimer) override
        {
            ImGui::Begin("Scene View");
            {
                ImGui::BeginChild("Scene");

                if (EditorData::cameraMode == CameraMode::CAMERA_PERSPECTIVE)
                {
                    if (ImGui::Button("Perspective"))
                    {
                        EditorData::cameraMode = CameraMode::CAMERA_ORTHOGRAPHIC;
                    }
                }
                else if (EditorData::cameraMode == CameraMode::CAMERA_ORTHOGRAPHIC)
                {
                    if (ImGui::Button("Orthographic"))
                    {
                        EditorData::cameraMode = CameraMode::CAMERA_PERSPECTIVE;
                    }
                }

                static ImGuiTabBarFlags tab_bar_flags = ImGuiTabBarFlags_Reorderable;
                if (ImGui::BeginTabBar("MyTabBar", tab_bar_flags)) {
                    if (ImGui::BeginTabItem(scene->GetName().c_str())) {
                        ImGui::EndTabItem();
                    }
                    ImGui::EndTabBar();
                }

                ImVec2 wsize = ImGui::GetContentRegionAvail();

                if (width_ != (unsigned int) wsize.x || height_ != (unsigned int) wsize.y) {
                    width_ = (unsigned int) wsize.x;
                    height_ = (unsigned int) wsize.y;
                    DYNASTI_LOG("Updated SceneViewPanel; width : " + std::to_string(width_) + "  height : " +
                                std::to_string(height_));
                }

                unsigned int fbo = static_cast<OpenglFramebuffer *>(screenBuffer_.get())->GetFbo();
                ImGui::Image((ImTextureID) fbo, wsize, ImVec2(0, 1), ImVec2(1, 0));

                ImGui::EndChild();
            }
            ImGui::End();
        }
        
    
    protected:
        
        std::shared_ptr<Framebuffer> screenBuffer_;
        
    };
    
}
//---------------------------------------------------------------------------------------------------------------------
#endif
