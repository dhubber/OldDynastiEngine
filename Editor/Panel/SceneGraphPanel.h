#ifndef DYNASTI_SCENE_GRAPH_PANEL_H
#define DYNASTI_SCENE_GRAPH_PANEL_H


#include <memory>
#include "Dynasti/Renderer/Framebuffer.h"
#include "Dynasti/Scene/Scene.h"

#include "Editor/Application/EditorData.h"
#include "Editor/Panel/Panel.h"
#include "Editor/Dependencies/imgui/imgui.h"
#include "Dynasti/Renderer/Opengl/OpenglFramebuffer.h"


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{
    
    //=================================================================================================================
    /// \brief   Base class for separate panels in Editor
    /// \author  D. A. Hubber
    /// \date    12/05/2021
    //=================================================================================================================
    class SceneGraphPanel : public Panel
    {
    public:
    
        SceneGraphPanel() : Panel()
        {
        }
        
        virtual void Update(std::shared_ptr<Scene> scene, std::shared_ptr<FrameTimer> frameTimer) override
        {
            static ImGuiTreeNodeFlags base_flags = ImGuiTreeNodeFlags_OpenOnArrow | ImGuiTreeNodeFlags_OpenOnDoubleClick | ImGuiTreeNodeFlags_SpanAvailWidth;
            
            ImGui::Begin("Scene Graph View");
            {
                ImGui::BeginChild("SceneGraph");
                ImGui::TextColored(ImVec4(1, 1, 0, 1), "Scene : %s", scene->GetName().c_str());
                ImGui::Separator();
                
                const ObjectPool<Entity> &entityPool = scene->GetEntityPool();
                for (int i = 0; i < entityPool.NumObjects(); ++i)
                {
                    ImGuiTreeNodeFlags node_flags = base_flags;
                    const Entity &entity = entityPool[i];
                    if (entity.id == EditorData::selectedEntityId)
                    {
                        node_flags |= ImGuiTreeNodeFlags_Selected;
                    }
                    node_flags |= ImGuiTreeNodeFlags_Leaf | ImGuiTreeNodeFlags_NoTreePushOnOpen;
                    ImGui::TreeNodeEx((void*)(intptr_t)i, node_flags, "%s", entity.name.c_str());
                    if (ImGui::IsItemClicked())
                    {
                        EditorData::selectedEntityId = entity.id;
                    }
                }
                ImGui::EndChild();
            }
            ImGui::End();
        }
        
    };
    
}
//---------------------------------------------------------------------------------------------------------------------
#endif
