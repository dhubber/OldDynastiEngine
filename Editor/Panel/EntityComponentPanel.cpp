#include "Editor/Panel/EntityComponentPanel.h"
#include "Editor/Dependencies/imgui/imgui.h"


/*#include <memory>
#include "Dynasti/Renderer/Framebuffer.h"
#include "Dynasti/Scene/Scene.h"

#include "Editor/Application/EditorData.h"
#include "Editor/Panel/Panel.h"
#include "Editor/Dependencies/imgui/imgui.h"
#include "Dynasti/Renderer/Opengl/OpenglFramebuffer.h"*/


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{
    void EntityComponentPanel::Update(std::shared_ptr<Scene> scene, std::shared_ptr<FrameTimer> frameTimer)
    {
        ImGui::Begin("Entity View");
        {
            ImGui::BeginChild("Entity");
            if (EditorData::selectedEntityId != -1 && scene)
            {
                Entity *entity = scene->FindEntity(EditorData::selectedEntityId);
                if (entity)
                {
                    ImGui::TextColored(ImVec4(1, 1, 0, 1),"Name      : %s", entity->name.c_str());
                    ImGui::TextColored(ImVec4(1, 1, 0, 1),"Id        : %d", entity->id);
                    ImGui::TextColored(ImVec4(1, 1, 0, 1),"Parent id : %d", entity->parentId);
                    ImGui::Separator();
                    
                    std::vector<Component*> components = scene->FindAllEntityComponents(EditorData::selectedEntityId);
                    for (Component* component : components)
                    {
                        if (ImGui::CollapsingHeader(component->GetComponentName().c_str(), ImGuiTreeNodeFlags_None))
                        {
                            ImGui::Text("Global id : %d", component->id);
                        }
                    }
                }
                else
                {
                    ImGui::TextColored(ImVec4(1, 1, 0, 1),"No entity selected");
                }
            }
            else
            {
                ImGui::TextColored(ImVec4(1, 1, 0, 1),"No entity selected");
            }
            
            ImGui::EndChild();
        }
        
        ImGui::End();
    }
    
}
//---------------------------------------------------------------------------------------------------------------------

