#ifndef DYNASTI_STATS_PANEL_H
#define DYNASTI_STATS_PANEL_H


#include <memory>
#include "Dynasti/Core/FrameTimer.h"
#include "Dynasti/Renderer/Framebuffer.h"
#include "Dynasti/Scene/Scene.h"

#include "Editor/Application/EditorData.h"
#include "Editor/Panel/Panel.h"
#include "Editor/Dependencies/imgui/imgui.h"
#include "Dynasti/Renderer/Opengl/OpenglFramebuffer.h"


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{
    
    //=================================================================================================================
    /// \brief   Base class for separate panels in Editor
    /// \author  D. A. Hubber
    /// \date    12/05/2021
    //=================================================================================================================
    class StatsPanel : public Panel
    {
    public:
    
        StatsPanel() : Panel()
        {
        }
        
        virtual void Update(std::shared_ptr<Scene> scene, std::shared_ptr<FrameTimer> frameTimer) override
        {
            static ImGuiTreeNodeFlags base_flags = ImGuiTreeNodeFlags_OpenOnArrow | ImGuiTreeNodeFlags_OpenOnDoubleClick | ImGuiTreeNodeFlags_SpanAvailWidth;
            
            ImGui::Begin("Stats View");
            {
                ImGui::BeginChild("Statistics");
                ImGui::TextColored(ImVec4(1, 1, 0, 1), "FPS : %f", frameTimer->GetFps());
                ImGui::TextColored(ImVec4(1, 1, 0, 1), "No. frames : %d", frameTimer->GetNumFrames());
                ImGui::TextColored(ImVec4(1, 1, 0, 1), "Time       : %f", frameTimer->GetTime());
                ImGui::EndChild();
            }
            ImGui::End();
        }
        
    };
    
}
//---------------------------------------------------------------------------------------------------------------------
#endif
