#ifndef DYNASTI_ENTITY_COMPONENT_PANEL_H
#define DYNASTI_ENTITY_COMPONENT_PANEL_H


#include <memory>
#include "Dynasti/Renderer/Framebuffer.h"
#include "Dynasti/Scene/Scene.h"

#include "Editor/Application/EditorData.h"
#include "Editor/Panel/Panel.h"
#include "Editor/Dependencies/imgui/imgui.h"
#include "Dynasti/Renderer/Opengl/OpenglFramebuffer.h"


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{
    
    //=================================================================================================================
    /// \brief   Base class for separate panels in Editor
    /// \author  D. A. Hubber
    /// \date    12/05/2021
    //=================================================================================================================
    class EntityComponentPanel : public Panel
    {
    public:
    
        EntityComponentPanel() : Panel()
        {
        }
        
        virtual void Update(std::shared_ptr<Scene> scene, std::shared_ptr<FrameTimer> frameTimer) override;
        
    };
    
}
//---------------------------------------------------------------------------------------------------------------------
#endif
