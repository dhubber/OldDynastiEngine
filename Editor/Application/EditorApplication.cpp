#include <functional>
#include "Dynasti/Application/Task.h"
#include "Dynasti/Input/InputManager.h"
#include "Dynasti/Mesh/Mesh.h"
#include "Dynasti/Renderer/Texture.h"
#include "Dynasti/Renderer/VertexBuffer.h"
#include "Dynasti/Renderer/RenderableComponent.h"
#include "Dynasti/Renderer/Opengl/OpenglRendererSystem.h"
#include "Dynasti/Scene/SceneManager.h"
#include "Dynasti/Script/ScriptComponent.h"
#include "Dynasti/Script/ScriptSystem.h"
#include "Editor/Application/EditorApplication.h"
#include "Editor/Application/RotatingScript.h"
#ifdef DYNASTI_USE_GLFW
#include "Dynasti/Window/Glfw/GlfwWindowSystem.h"
#include "Editor/Renderer/EditorRendererSystem.h"
#include "Editor/Window/EditorWindowSystem.h"
#elif DYNASTI_USE_SDL2
#include "Dynasti/Window/Sdl2/Sdl2WindowSystem.h"
#endif


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{
    
    EditorApplication::EditorApplication() : Application("EditorApp")
    {
        DYNASTI_LOG("Constructing EditorApplication object");
#ifdef DYNASTI_USE_GLFW
        windowSystem_ = std::make_shared<EditorWindowSystem>(assetManager_, eventManager_, frameTimer_, inputManager_);
        std::shared_ptr<InputMap> inputMap = inputManager_->CreateNewInputMapping("EditorInputMap");
        inputMap->RegisterKeyActionEvent("Quit", GLFW_KEY_ESCAPE);
        inputMap->RegisterKeyActionEvent("Deselect", GLFW_KEY_U);
        inputMap->RegisterKeyActionEvent("Left", GLFW_KEY_A);
        inputMap->RegisterKeyActionEvent("Right", GLFW_KEY_D);
        inputMap->RegisterKeyActionEvent("Up", GLFW_KEY_E);
        inputMap->RegisterKeyActionEvent("Down", GLFW_KEY_Q);
        inputMap->RegisterKeyActionEvent("Forward", GLFW_KEY_W);
        inputMap->RegisterKeyActionEvent("Backward", GLFW_KEY_S);
        inputMap->RegisterKeyActionEvent("TurnLeft", GLFW_KEY_LEFT);
        inputMap->RegisterKeyActionEvent("TurnRight", GLFW_KEY_RIGHT);
        inputMap->RegisterKeyActionEvent("TurnUp", GLFW_KEY_DOWN);
        inputMap->RegisterKeyActionEvent("TurnDown", GLFW_KEY_UP);
        inputMap->RegisterKeyActionEvent("TogglePP", GLFW_KEY_P);
        inputMap->RegisterKeyActionEvent("ZoomIn", GLFW_KEY_H);
        inputMap->RegisterKeyActionEvent("ZoomOut", GLFW_KEY_B);
#elif DYNASTI_USE_SDL2
        windowSystem_ = std::make_shared<Sdl2WindowSystem>(assetManager_, eventManager_, inputManager_);
        std::shared_ptr<InputMap> inputMap = inputManager_->CreateNewInputMapping("EditorInputMap");
        inputMap->RegisterKeyActionEvent("Quit", SDL_SCANCODE_ESCAPE);
        inputMap->RegisterKeyActionEvent("Left", SDL_SCANCODE_A);
        inputMap->RegisterKeyActionEvent("Right", SDL_SCANCODE_D);
        inputMap->RegisterKeyActionEvent("Up", SDL_SCANCODE_E);
        inputMap->RegisterKeyActionEvent("Down", SDL_SCANCODE_Q);
        inputMap->RegisterKeyActionEvent("Forward", SDL_SCANCODE_W);
        inputMap->RegisterKeyActionEvent("Backward", SDL_SCANCODE_S);
        inputMap->RegisterKeyActionEvent("TurnLeft", SDL_SCANCODE_LEFT);
        inputMap->RegisterKeyActionEvent("TurnRight", SDL_SCANCODE_RIGHT);
        inputMap->RegisterKeyActionEvent("TurnUp", SDL_SCANCODE_DOWN);
        inputMap->RegisterKeyActionEvent("TurnDown", SDL_SCANCODE_UP);
#endif
        scriptSystem_ = std::make_shared<ScriptSystem>(assetManager_, eventManager_, frameTimer_, sceneManager_);
        rendererSystem_ = std::make_shared<EditorRendererSystem>(assetManager_, eventManager_, frameTimer_, sceneManager_, inputManager_);
        
        eventListener_->SubscribeToEvent("QuitKeyDown", std::bind(&Application::OnQuitApp, this, std::placeholders::_1));
        eventListener_->SubscribeToEvent("DeselectKeyDown", [](const Event event){ EditorData::selectedEntityId = -1; }); // std::bind(&EditorApplication::OnDeselect, this, std::placeholders::_1));
    }
    
    
    void EditorApplication::Setup()
    {
        windowSystem_->Setup();
        scriptSystem_->Setup();
        rendererSystem_->SetActiveWindow(windowSystem_->GetPrimaryWindow());
        rendererSystem_->Setup();
    
        std::shared_ptr<Scene> flagScene = CreateExampleScene("FlagScene");
        const std::string filename = modelPath + "Suzanne/Suzanne.gltf";  //"Suzanne/Suzanne.obj";
        //std::shared_ptr<Scene> suzanneScene = LoadExampleScene("SuzanneScene", filename);
        sceneManager_->SelectScene(flagScene);
        //sceneManager_->SelectScene(suzanneScene);
        //sceneManager_->ActivateScene("FlagScene");
    }
    
    
    void EditorApplication::AddTasksToQueue()
    {
        std::function<void()> f = std::bind(&WindowSystem::Update, windowSystem_);
        std::shared_ptr<Task> windowSystemUpdate = std::make_shared<Task>(f);
        taskManager_->AddTaskToQueue(windowSystemUpdate);
        
        std::function<void()> f2 = std::bind(&ScriptSystem::Update, scriptSystem_);
        std::shared_ptr<Task> scriptSystemUpdate = std::make_shared<Task>(f2);
        taskManager_->AddTaskToQueue(scriptSystemUpdate);
        
        std::function<void()> f3 = std::bind(&RendererSystem::Update, rendererSystem_);
        std::shared_ptr<Task> rendererSystemUpdate = std::make_shared<Task>(f3);
        taskManager_->AddTaskToQueue(rendererSystemUpdate);
    }
    
    
    void EditorApplication::Shutdown()
    {
        rendererSystem_->Shutdown();
        scriptSystem_->Shutdown();
        windowSystem_->Shutdown();
    }
    
    
    std::shared_ptr<Scene> EditorApplication::LoadExampleScene(const std::string sceneName, const std::string filename)
    {
        AssetRegistry<Mesh> &meshes = assetManager_->GetMeshRegistry();
        AssetRegistry<Scene> &scenes = assetManager_->GetSceneRegistry();
        AssetRegistry<GlslShader> &shaders = assetManager_->GetShaderRegistry();
        AssetRegistry<Texture> &textures = assetManager_->GetTextureRegistry();
    
        std::shared_ptr<GlslShader> wireframeShader = std::make_shared<WireframeShader>();
        std::shared_ptr<GlslShader> basicColorShader = std::make_shared<BasicColorShader>();
        std::shared_ptr<GlslShader> basicTextureShader = std::make_shared<BasicTextureShader>();
        shaders.Register(wireframeShader, "WireframeShader");
        shaders.Register(basicColorShader, "BasicColorShader");
        shaders.Register(basicTextureShader, "BasicTextureShader");
        
        exampleScene_ = std::make_shared<Scene>(assetManager_);
#ifdef DYNASTI_USE_TINYGLTF
        exampleScene_->ImportGltfModel(filename);
#endif
        scenes.Register(exampleScene_, sceneName);
        
        return exampleScene_;
    }
    
    
    std::shared_ptr<Scene> EditorApplication::CreateExampleScene(const std::string sceneName)
    {
        exampleScene_ = std::make_shared<Scene>(assetManager_);
        //exampleScene_->RegisterComponentTypeWithBase<RotatingScript, ScriptComponent>(8);
    
        AssetRegistry<Material> &materials = assetManager_->GetMaterialRegistry();
        AssetRegistry<Mesh> &meshes = assetManager_->GetMeshRegistry();
        AssetRegistry<Scene> &scenes = assetManager_->GetSceneRegistry();
        AssetRegistry<GlslShader> &shaders = assetManager_->GetShaderRegistry();
        AssetRegistry<Texture> &textures = assetManager_->GetTextureRegistry();
        AssetRegistry<VertexBuffer> &vertexBuffers = assetManager_->GetVertexBufferRegistry();
        
        std::shared_ptr<GlslShader> wireframeShader = std::make_shared<WireframeShader>();
        std::shared_ptr<GlslShader> basicColorShader = std::make_shared<BasicColorShader>();
        std::shared_ptr<GlslShader> basicTextureShader = std::make_shared<BasicTextureShader>();
        shaders.Register(wireframeShader, "WireframeShader");
        shaders.Register(basicColorShader, "BasicColorShader");
        shaders.Register(basicTextureShader, "BasicTextureShader");
        
        //std::shared_ptr<OpenglTexture> welshTexture = std::make_shared<OpenglTexture>();
        std::shared_ptr<Texture> welshTexture = Texture::TextureFactory();
        std::shared_ptr<Material> welshMaterial = Material::MaterialFactory();
        if (welshTexture->LoadImageFromFile(imagePath + "welsh_flag.jpg"))   //(imagePath + "square.jpg"))
        {
            welshTexture->LoadImageToGpu();
            textures.Register(welshTexture, "welshTexture");
            welshMaterial->SetTextureId(TextureType::TEXTURE_DIFFUSE, welshTexture->GetId());
            materials.Register(welshMaterial, "welshMaterial");
        }
        else
        {
            DYNASTI_WARNING("Could not load image file");
        }
        
        std::shared_ptr<Texture> germanTexture = Texture::TextureFactory();
        std::shared_ptr<Material> germanMaterial = Material::MaterialFactory();
        if (germanTexture->LoadImageFromFile(imagePath + "germany_flag.png"))
        {
            germanTexture->LoadImageToGpu();
            textures.Register(germanTexture, "germanTexture");
            germanMaterial->SetTextureId(TextureType::TEXTURE_DIFFUSE, germanTexture->GetId());
            materials.Register(germanMaterial, "germanMaterial");
        }
        else
        {
            DYNASTI_WARNING("Could not load image file");
        }
    
        std::shared_ptr<Texture> tirolTexture = Texture::TextureFactory();
        std::shared_ptr<Material> tirolMaterial = Material::MaterialFactory();
        if (tirolTexture->LoadImageFromFile(imagePath + "tirol_flag.png"))
        {
            tirolTexture->LoadImageToGpu();
            textures.Register(tirolTexture, "tirolTexture");
            tirolMaterial->SetTextureId(TextureType::TEXTURE_DIFFUSE, tirolTexture->GetId());
            materials.Register(tirolMaterial, "tirolMaterial");
        }
        else
        {
            DYNASTI_WARNING("Could not load image file");
        }

        std::shared_ptr<Material> wireframeMaterial = Material::MaterialFactory();
        materials.Register(wireframeMaterial, "wireframeMaterial");

        std::shared_ptr<Material> redMaterial = Material::MaterialFactory();
        redMaterial->SetBaseColor(red);
        materials.Register(redMaterial, "redMaterial");
    
        std::shared_ptr<Mesh> cuboidMesh = Mesh::CuboidFactory(1.5f, 1.5f, 1.5f, 4, 4, 4);
        cuboidMesh->SetMaterialId(welshMaterial->GetId());
        cuboidMesh->CopyDataToGpu();
        meshes.Register(cuboidMesh, "cuboidMesh");
        
        std::shared_ptr<Mesh> sphereMesh = Mesh::SphereFactory(2.0f, 12, 12);
        sphereMesh->SetMaterialId(wireframeMaterial->GetId());
        sphereMesh->CopyDataToGpu();
        meshes.Register(sphereMesh, "sphereMesh");
    
        Entity& cubeEntity = exampleScene_->CreateNewEntity("cube");
        RenderableComponent* renderable = exampleScene_->CreateComponent<RenderableComponent>(cubeEntity.id);
        renderable->SetMeshId(cuboidMesh->GetId());
        renderable->SetShaderId(basicTextureShader->GetId());
        renderable->SetMaterialId(welshMaterial->GetId());
        renderable->SetPosition(glm::vec3(-2.0f, -1.5f, 0.0f));
        RotatingScript* rotScript = exampleScene_->CreateComponent<RotatingScript>(cubeEntity.id);
        rotScript->rotationSpeed = 0.5f;
    
        Entity& cube2Entity = exampleScene_->CreateNewEntity("cube2");
        RenderableComponent* renderable2 = exampleScene_->CreateComponent<RenderableComponent>(cube2Entity.id);
        renderable2->SetMeshId(cuboidMesh->GetId());
        renderable2->SetShaderId(basicColorShader->GetId());
        renderable2->SetMaterialId(redMaterial->GetId());
        renderable2->SetPosition(glm::vec3(2.0f, -1.5f, 0.0f));
    
        Entity& cube3Entity = exampleScene_->CreateNewEntity("cube3");
        RenderableComponent* renderable3 = exampleScene_->CreateComponent<RenderableComponent>(cube3Entity.id);
        renderable3->SetMeshId(cuboidMesh->GetId());
        renderable3->SetShaderId(wireframeShader->GetId());
        renderable3->SetMaterialId(wireframeMaterial->GetId());
        renderable3->SetPosition(glm::vec3(-2.0f, 1.5f, 0.0f));
        RotatingScript* rotScript3 = exampleScene_->CreateComponent<RotatingScript>(cube3Entity.id);
        rotScript3->rotationSpeed = 0.35f;
        rotScript3->rotationAxis = glm::vec3(0.0f, 0.0f, 1.0f);
    
        Entity& cube4Entity = exampleScene_->CreateNewEntity("cube4");
        RenderableComponent* renderable4 = exampleScene_->CreateComponent<RenderableComponent>(cube4Entity.id);
        renderable4->SetMeshId(cuboidMesh->GetId());
        renderable4->SetShaderId(basicTextureShader->GetId());
        renderable4->SetMaterialId(tirolMaterial->GetId());
        //renderable4->SetTextureId(tirolTexture->GetId());
        renderable4->SetPosition(glm::vec3(2.0f, 1.5f, 0.0f));
    
        Entity& sphereEntity = exampleScene_->CreateNewEntity("sphere");
        RenderableComponent* renderable5 = exampleScene_->CreateComponent<RenderableComponent>(sphereEntity.id);
        renderable5->SetMeshId(sphereMesh->GetId());
        renderable5->SetShaderId(wireframeShader->GetId());
        renderable5->SetPosition(glm::vec3(0.0f, -4.0f, 0.0f));

#ifdef DYNASTI_USE_TINYGLTF
        const std::string filename = modelPath + "Suzanne/Suzanne.gltf";
        exampleScene_->ImportGltfModel(filename);
        std::shared_ptr<Mesh> suzanneMesh = meshes.Find("SuzanneMesh");
        
        if (suzanneMesh)
        {
            DYNASTI_LOG_VERBOSE("Found mesh : " + suzanneMesh->GetName());
            Entity &suzanneEntity = exampleScene_->CreateNewEntity("suzanne");
            RenderableComponent *renderable6 = exampleScene_->CreateComponent<RenderableComponent>(suzanneEntity.id);
            renderable6->SetMeshId(suzanneMesh->GetId());
            renderable6->SetShaderId(wireframeShader->GetId());
            renderable6->SetMaterialId(wireframeMaterial->GetId());
            renderable6->SetPosition(glm::vec3(0.0f, 0.0f, 0.0f));
        }
        else
        {
            DYNASTI_WARNING("Did not find mesh");
        }
#endif
        
        scenes.Register(exampleScene_, sceneName);
        return exampleScene_;
    }
    
}
//---------------------------------------------------------------------------------------------------------------------