#ifndef DYNASTI_ROTATING_SCRIPT_H
#define DYNASTI_ROTATING_SCRIPT_H


#include "glm/glm.hpp"
#include "Dynasti/Core/FrameTimer.h"
#include "Dynasti/Renderer/RenderableComponent.h"
#include "Dynasti/Scene/Scene.h"
#include "Dynasti/Script/ScriptComponent.h"
#include "Dynasti/Transform/Transform3d.h"


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{
    
    //=================================================================================================================
    /// \brief   Base class for all scriptable components.
    /// \author  D. A. Hubber
    /// \date    13/02/2020
    //=================================================================================================================
    class RotatingScript : public ScriptComponent
    {
    public:
        
        float rotationSpeed;                               ///< ...
        glm::vec3 rotationAxis;                            ///< ..
        

        RotatingScript(const int entityId=-1) : ScriptComponent(entityId) {};
    
        /// Initialises component in the object pool when entity is spawned (implemented by child classes)
        virtual void OnCreate() override
        {
            rotationSpeed = 0.0f;
            rotationAxis = glm::vec3(0.0f, 1.0f, 0.0f);
        };
    
        /// Returns a simple human-readable name of the component type (implemented by child classes)
        /// @return - Human-readable name of the component
        virtual const std::string GetComponentName() override { return "RotatingScript"; }
        
        /// Function for updating the entity every frame
        virtual void OnUpdate(std::shared_ptr<Scene> scene, std::shared_ptr<FrameTimer> frameTimer) override
        {
            RenderableComponent* rComp = scene->FindComponent<RenderableComponent>(id);
            if (rComp)
            {
                const Transform3d& transform3d = rComp->GetTransform();
                glm::quat quat = transform3d.GetQRotation();
                glm::quat rotQuat = Transform3d::AxisAngleToQuaternion(rotationAxis, rotationSpeed*frameTimer->GetFrameInterval());
                quat *= rotQuat;
                rComp->SetRotation(quat);
            }
        };
        
    };

}
//---------------------------------------------------------------------------------------------------------------------
#endif
