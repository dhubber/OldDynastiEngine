#ifndef DYNASTI_EDITOR_DATA_H
#define DYNASTI_EDITOR_DATA_H

#include "Dynasti/Core/Constants.h"

//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{
    //=================================================================================================================
    /// \brief   Class representing a simple event data structure.
    /// \author  D. A. Hubber
    /// \date    29/06/2020
    //=================================================================================================================
    class EditorData
    {
    public:
        static GlobalId selectedEntityId;
        static float fov;
        static float orthoSize;
        static CameraMode cameraMode;
    };
}
#endif
