#include "Editor/Application/EditorData.h"


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{
    GlobalId EditorData::selectedEntityId = -1;
    float EditorData::fov = 20.0f;
    float EditorData::orthoSize = 10.0f;
    CameraMode EditorData::cameraMode = CameraMode::CAMERA_PERSPECTIVE;
}
//---------------------------------------------------------------------------------------------------------------------