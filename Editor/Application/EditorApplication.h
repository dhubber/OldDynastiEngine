#ifndef DYNASTI_EDITOR_APPLICATION_H
#define DYNASTI_EDITOR_APPLICATION_H


#include "Dynasti/Application/Application.h"
#include "Editor/Application/EditorData.h"


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{
    
    // Forward declarations
    class RendererSystem;
    class Scene;
    class ScriptSystem;
    class WindowSystem;
    
    
    //=================================================================================================================
    /// \brief   Virtual parent base class for defining main application object.
    /// \author  D. A. Hubber
    /// \date    13/06/2020
    //=================================================================================================================
    class EditorApplication : public Application
    {
    public:
        
        EditorApplication();
    
        virtual void AddTasksToQueue() override;
        virtual void Setup() override;
        virtual void Shutdown() override;
    
        std::shared_ptr<Scene> CreateExampleScene(const std::string sceneName);
        std::shared_ptr<Scene> LoadExampleScene(const std::string sceneName, const std::string filename);

        
    protected:
    
        std::shared_ptr<RendererSystem> rendererSystem_;     ///< ..
        std::shared_ptr<WindowSystem> windowSystem_;         ///< ...
        std::shared_ptr<ScriptSystem> scriptSystem_;         ///< ..
        std::shared_ptr<Scene> exampleScene_;                ///< ..
    
    };
    
}
//---------------------------------------------------------------------------------------------------------------------
#endif
