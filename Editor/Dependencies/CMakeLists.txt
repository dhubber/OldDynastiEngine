cmake_minimum_required(VERSION 3.10)
project(Dynasti)

add_library(imgui STATIC imgui/imgui.h imgui/imgui_internal.h imgui/imgui.cpp imgui/imgui_demo.cpp imgui/imgui_draw.cpp
        imgui/imgui_tables.cpp imgui/imgui_widgets.cpp imgui/backends/imgui_impl_glfw.h imgui/backends/imgui_impl_glfw.cpp
        imgui/backends/imgui_impl_opengl3.h imgui/backends/imgui_impl_opengl3.cpp)
target_include_directories(imgui PUBLIC imgui)
target_include_directories(imgui PUBLIC ../../Dynasti/Dependencies/glad/include)
target_include_directories(imgui PUBLIC ../../Dynasti/Dependencies/glfw/include)
target_compile_definitions(imgui PRIVATE -DIMGUI_IMPL_OPENGL_LOADER_GLAD)
set_target_properties(imgui PROPERTIES LINKED_LANGUAGE CXX)
