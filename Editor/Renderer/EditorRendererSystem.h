#ifndef DYNASTI_EDITOR_RENDERER_SYSTEM_H
#define DYNASTI_EDITOR_RENDERER_SYSTEM_H


#include <memory>
#include "Dynasti/Core/System.h"
#include "Dynasti/Renderer/Opengl/OpenglRendererSystem.h"
#include "Dynasti/Transform/Transform3d.h"


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{
    
    class AssetManager;
    class EntityComponentPanel;
    class EventManager;
    class GlslShader;
    class InputManager;
    class SceneManager;
    class SceneGraphPanel;
    class SceneViewPanel;
    class StatsPanel;
    class OpenglTexture;
    class Window;
    
    
    //=================================================================================================================
    /// \brief   OpenGL renderer system class.
    /// \author  D. A. Hubber
    /// \date    23/02/2020
    //=================================================================================================================
    class EditorRendererSystem : public OpenglRendererSystem
    {
    public:
    
        EditorRendererSystem(std::shared_ptr<AssetManager> assetManager, std::shared_ptr<EventManager> eventManager,
                             std::shared_ptr<FrameTimer> frameTimer, std::shared_ptr<SceneManager> sceneManager,
                             std::shared_ptr<InputManager> inputManager) :
            OpenglRendererSystem(assetManager, eventManager, frameTimer, sceneManager, inputManager) {};
    
        // Implementations of pure virtual functions
        virtual void Setup() override;
        virtual void Shutdown() override;
        virtual void Update() override;

        
    protected:
        
        std::shared_ptr<EntityComponentPanel> entityComponentPanel_;
        std::shared_ptr<SceneGraphPanel> sceneGraphPanel_;
        std::shared_ptr<SceneViewPanel> sceneViewPanel_;
        std::shared_ptr<StatsPanel> statsPanel_;

        Transform3d cameraTransform;                       ///< Transform of main camera
        std::shared_ptr<Camera> editorCamera_;             ///< Camera used inside the editor
        
    };

}
//---------------------------------------------------------------------------------------------------------------------
#endif
