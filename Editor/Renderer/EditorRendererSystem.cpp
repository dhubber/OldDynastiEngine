#include "Dynasti/Asset/AssetManager.h"
#include "Dynasti/Camera/Camera.h"
#include "Dynasti/Core/FrameTimer.h"
#include "Dynasti/Event/EventManager.h"
#include "Dynasti/Input/InputManager.h"
#include "Dynasti/Mesh/Mesh.h"
//#include "Dynasti/Mesh/Mesh3d.h"
#include "Dynasti/Renderer/Framebuffer.h"
#include "Dynasti/Renderer/Material.h"
#include "Dynasti/Renderer/RenderableComponent.h"
#include "Dynasti/Renderer/VertexBuffer.h"
#include "Dynasti/Scene/Scene.h"
#include "Dynasti/Scene/SceneManager.h"
#include "Dynasti/Shader/GlslShader.h"
#include "Dynasti/Window/Window.h"
#include "Editor/Renderer/EditorRendererSystem.h"

#include "Editor/Panel/EntityComponentPanel.h"
#include "Editor/Panel/SceneGraphPanel.h"
#include "Editor/Panel/SceneViewPanel.h"
#include "Editor/Panel/StatsPanel.h"

#include "Editor/Dependencies/imgui/imgui.h"
#include "Editor/Dependencies/imgui/backends/imgui_impl_opengl3.h"
#include "Editor/Dependencies/imgui/backends/imgui_impl_glfw.h"

#include "Dynasti/Renderer/Opengl/OpenglFramebuffer.h"
#include "Dynasti/Window/Glfw/GlfwWindow.h"


//---------------------------------------------------------------------------------------------------------------------
namespace Dynasti
{
    
    void EditorRendererSystem::Setup()
    {
        OpenglRendererSystem::Setup();
    
        entityComponentPanel_ = std::make_shared<EntityComponentPanel>();
        sceneGraphPanel_ = std::make_shared<SceneGraphPanel>();
        sceneViewPanel_ = std::make_shared<SceneViewPanel>(screenFramebuffer_);
        statsPanel_ = std::make_shared<StatsPanel>();
        
        editorCamera_ = std::make_shared<Camera>();
    }
    
    
    void EditorRendererSystem::Shutdown()
    {
        AssetRegistry<GlslShader> &shaders = assetManager_->GetShaderRegistry();
        shaders.UnregisterAllAssets();
    }
    
    
    void EditorRendererSystem::Update()
    {
        DYNASTI_TIMER(RENDERER_SYSTEM);
        std::shared_ptr<Scene> scene = sceneManager_->GetSelectedScene();
        
        if (postProcessActive_ && screenFramebuffer_)
        {
            if (sceneViewPanel_->GetWidth() != screenFramebuffer_->GetWidth() || sceneViewPanel_->GetHeight() != screenFramebuffer_->GetHeight())
            {
                screenFramebuffer_->Destroy();
                screenFramebuffer_->Create(sceneViewPanel_->GetWidth(), sceneViewPanel_->GetHeight());
            }
            screenFramebuffer_->Bind();
        }
        glClearColor(0.3f, 0.3f, 0.3f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        glEnable(GL_DEPTH_TEST);
        glViewport(0, 0, screenFramebuffer_->GetWidth(), screenFramebuffer_->GetHeight());
    
        if (editorCamera_)
        {
            glm::vec3 pos = editorCamera_->GetPosition();
            glm::quat quat = editorCamera_->GetQRotation();
            Transform3d cameraTransform(pos, glm::vec3(1.0f), quat);
    
            std::shared_ptr<InputMap> inputMap = inputManager_->GetActiveInputMap();
            if (inputMap->IsKeyDown("Left")) pos -= 0.1f * cameraTransform.RightDirection();
            if (inputMap->IsKeyDown("Right")) pos += 0.1f * cameraTransform.RightDirection();
            if (inputMap->IsKeyDown("Up")) pos += 0.1f * cameraTransform.UpDirection();
            if (inputMap->IsKeyDown("Down")) pos -= 0.1f * cameraTransform.UpDirection();
            if (inputMap->IsKeyDown("Forward")) pos += 0.1f * cameraTransform.ForwardDirection();
            if (inputMap->IsKeyDown("Backward")) pos -= 0.1f * cameraTransform.ForwardDirection();
            if (inputMap->IsKeyDown("TurnLeft"))
                quat *= Transform3d::AxisAngleToQuaternion(
                    glm::vec4(0.0f, 1.0f, 0.0f, 0.025f));   //(glm::vec4(cameraTransform.UpDirection(), 0.025f));
            if (inputMap->IsKeyDown("TurnRight"))
                quat *= Transform3d::AxisAngleToQuaternion(
                    glm::vec4(0.0f, 1.0f, 0.0f, -0.025f));   //(glm::vec4(cameraTransform.UpDirection(), -0.025f));
            if (inputMap->IsKeyDown("TurnUp"))
                quat *= Transform3d::AxisAngleToQuaternion(glm::vec4(cameraTransform.RightDirection(), 0.025f));
            if (inputMap->IsKeyDown("TurnDown"))
                quat *= Transform3d::AxisAngleToQuaternion(glm::vec4(cameraTransform.RightDirection(), -0.025f));
            if (inputMap->IsKeyDown("ZoomIn"))
            {
                EditorData::fov /= 1.001f;
                EditorData::orthoSize /= 1.001f;
            }
            if (inputMap->IsKeyDown("ZoomOut"))
            {
                EditorData::fov *= 1.001f;
                EditorData::orthoSize *= 1.001f;
            }

            if (EditorData::cameraMode == CameraMode::CAMERA_ORTHOGRAPHIC) {
                editorCamera_->SetOrthographicCamera(pos, quat, sceneViewPanel_->GetWidth(),
                                                     sceneViewPanel_->GetHeight(), EditorData::orthoSize, 1000.0f);
            }
            else {
                editorCamera_->SetPerspectiveCamera(pos, quat, sceneViewPanel_->GetWidth(),
                                                    sceneViewPanel_->GetHeight(), EditorData::fov, 0.01f, 1000.0f);
            }
        }
        
        if (scene)
        {
            RenderScene(scene, editorCamera_);
        }
        else
        {
            DYNASTI_WARNING("Invalid scene or active window pointers in Renderer");
        }
        
        if (postProcessActive_ && postProcessShader_ && screenFramebuffer_)
        {
            screenFramebuffer_->Render(postProcessShader_, glm::vec2(sceneViewPanel_->GetWidth(), sceneViewPanel_->GetHeight()));
            screenFramebuffer_->Unbind();
        }

        // Start the Dear ImGui frame
        ImGui_ImplOpenGL3_NewFrame();
        ImGui_ImplGlfw_NewFrame();
        ImGui::NewFrame();
    
        ImGuiIO& io = ImGui::GetIO();
        static bool show_demo_window = true;
        static bool show_another_window = false;
        ImVec4 clear_color = ImVec4(0.45f, 0.55f, 0.60f, 1.00f);
        
        entityComponentPanel_->Update(scene, frameTimer_);
        sceneGraphPanel_->Update(scene, frameTimer_);
        sceneViewPanel_->Update(scene, frameTimer_);
        statsPanel_->Update(scene, frameTimer_);
    
        // 1. Show the big demo window (Most of the sample code is in ImGui::ShowDemoWindow()! You can browse its code to learn more about Dear ImGui!).
        if (show_demo_window)
            ImGui::ShowDemoWindow(&show_demo_window);
        
        // Rendering
        ImGui::Render();
        int display_w, display_h;
        glfwGetFramebufferSize(static_cast<GlfwWindow*>(activeWindow_.get())->GetGlfwInternalWindow(), &display_w, &display_h);
        glViewport(0, 0, display_w, display_h);
        glClearColor(clear_color.x, clear_color.y, clear_color.z, clear_color.w);
        glClear(GL_COLOR_BUFFER_BIT);
        ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
    
        // Update and Render additional Platform Windows
        // (Platform functions may change the current OpenGL context, so we save/restore it to make it easier to paste this code elsewhere.
        //  For this specific demo app we could also call glfwMakeContextCurrent(window) directly)
        /*if (io.ConfigFlags & ImGuiConfigFlags_ViewportsEnable)
        {
            GLFWwindow* backup_current_context = glfwGetCurrentContext();
            ImGui::UpdatePlatformWindows();
            ImGui::RenderPlatformWindowsDefault();
            glfwMakeContextCurrent(backup_current_context);
        }*/
    }
    
}
//---------------------------------------------------------------------------------------------------------------------
